/*
 * Pathnames for files used by xtend
 *
 * $FreeBSD: src/libexec/xtend/paths.h,v 1.3.2.1 1999/09/05 11:20:27 peter Exp $
 */

#define X10DIR		"/var/spool/xten"
#define X10LOGNAME	"Log"
#define X10STATNAME	"Status"
#define X10DUMPNAME	"status.out"
#define TWPATH		"/dev/tw0"
#define SOCKPATH	"/var/run/tw523"
#define PIDPATH		"/var/run/xtend.pid"
