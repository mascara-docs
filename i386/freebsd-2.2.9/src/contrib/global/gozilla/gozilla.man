.\"
.\" Copyright (c) 1997 Shigio Yamaguchi. All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by Shigio Yamaguchi.
.\" 4. Neither the name of the author nor the names of any co-contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.Dd Sep 17, 1997
.Dt GOZILLA 1
.Os BSD 4
.Sh NAME
.Nm gozilla
.Nd force mozilla to display specified source file
.Sh SYNOPSIS
\fBgozilla\fP [\fB+\fP\fIno\fP]
.Op Fl b Ar browser
.Op Fl p
.Ar file
.Nm gozilla
.Fl C Ar command
.Sh DESCRIPTION
First form:
.br
.Nm Gozilla
force mozilla (Netscape Navigator) to display specified source file
as a hypertext.
If mozilla has not loaded then
.Nm
loads it.
.Pp
In advance of using this command, you must execute
.Xr gtags 1
and
.Xr htags 1
at the root directory of the source tree to make tag files.
Then you can execute
.Nm
at anywhere in the source tree.
.br
You can specify source file and the line number optionally.
.Pp
Second form:
.br
.Nm Gozilla
send
.Ar command
to mozilla directly.
.Ar Command
is undocumented. But the hint is in the resource file of mozilla.
.Pp
The following options are available:
.Bl -tag -width Ds
.It Ar \fB+\fP\fIno\fP
line number. It must be a line on which function definition or function
reference is exist.
If you execute
.Xr htags 1
with -l option, you can specify any line.
.It Fl b Ar browser
browser to use. By default, assumes mozilla.
If you specify another browser,
.Nm
waits for exiting of the browser.
.It Fl p
just print generated target URL.
.It Ar file
path of source file.
.It Fl C Ar command
send
.Ar command
to mozilla directly.
.El
.Sh FILES
.Bl -tag -width tags -compact
.It Pa HTML/
hypertext of source tree.
.It Pa GTAGS
tags file for function definitions.
.El
.Sh ENVIRONMENT
The following environment variables affect the execution of gozilla.
.Pp
.Bl -tag -width indent
.It Ev GTAGSROOT
The directory which is the root of source tree.
.It Ev GTAGSDBPATH
The directory on which HTML directory exist. This value is ignored
when GTAGSROOT is not defined.
.It Ev BROWSER
browser to use. By default, assumes mozilla.
.El
.Sh EXAMPLES

  % global -x main
  main              82 ctags.c          main(argc, argv)
  % gozilla +82 ctags.c
  % gozilla -C pageDown
  % gozilla -C back

.Sh DIAGNOSTICS
.Nm Gozilla
exits with a non 0 value if an error occurred, 0 otherwise.
.Sh SEE ALSO
.Xr global 1 ,
.Xr gtags 1 ,
.Xr htags 1 .
.Sh NOTES
Netscape Navigator is a registered trademark of Netscape Communications Corporation
in the United States and other countries.
.Pp
.Nm Gozilla
means 'Global for mozilla'.
.Sh BUGS
.Nm Gozilla
can treat not only source file but also normal file, directory, HTML file
and even URL, because it is omnivorous.
.Sh AUTHORS
Shigio Yamaguchi (shigio@wafu.netgate.net)
