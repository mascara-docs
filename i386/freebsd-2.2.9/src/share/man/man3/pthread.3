.\" Copyright (c) 1996 John Birrell <jb@cimlogic.com.au>.
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by John Birrell.
.\" 4. Neither the name of the author nor the names of any co-contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY JOHN BIRRELL AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man3/pthread.3,v 1.1.2.4 1999/09/05 11:26:55 peter Exp $
.\"
.Dd April 4, 1996
.Dt PTHREAD 3
.Os BSD 4
.Sh NAME
.Nm pthread
.Nd POSIX thread functions
.Sh DESCRIPTION
POSIX threads are a set of functions that support applications with
requirements for multiple flows of control, called
.Fa threads ,
within a process. Multithreading is used to improve the performance of a
program.
.Pp
The POSIX thread functions are summarized in this section in the following
groups:
.Bl -bullet -offset indent
.It
Thread routines
.It
Attribute Object Routines
.It
Mutex Routines
.It
Condition Variable Routines
.It
Per-Thread Context Routines
.It
Cleanup Routines
.El
.Sh THREAD ROUTINES
.Bl -tag -width Er
.It int Fn pthread_create "pthread_t *thread" "const pthread_attr_t *attr" "void *(*start_routine)(void *)" "void *arg"
Creates a new thread of execution.
.It int Fn pthread_detach "pthread_t thread"
Marks a thread for deletion.
.It int Fn pthread_equal "pthread_t t1" "pthread_t t2"
Compares two thread IDs.
.It void Fn pthread_exit "void *value_ptr"
Terminates the calling thread.
.It int Fn pthread_join "pthread_t thread" "void **value_ptr"
Causes the calling thread to wait for the termination of the specified thread.
.It int Fn pthread_once "pthread_once_t *once_control" "void (*init_routine)(void)"
Calls an initialization routine once.
.It pthread_t Fn pthread_self void
Returns the thread ID of the calling thread.
.El
.Sh ATTRIBUTE OBJECT ROUTINES
.Bl -tag -width Er
.It int Fn pthread_attr_destroy "pthread_attr_t *attr"
Destroy a thread attributes object.
.It int Fn pthread_attr_getinheritsched "pthread_attr_t *attr" "int *inheritsched"
Get the inherit scheduling attribute from a thread attributes object.
.It int Fn pthread_attr_getschedparam "pthread_attr_t *attr" "struct sched_param *param"
Get the scheduling parameter attribute from a thread attributes object.
.It int Fn pthread_attr_getschedpolicy "pthread_attr_t *attr" "int *policy"
Get the scheduling policy attribute from a thread attributes object.
.It int Fn pthread_attr_getscope "pthread_attr_t *attr" "int *contentionscope"
Get the contention scope attribute from a thread attributes object.
.It int Fn pthread_attr_getstacksize "pthread_attr_t *attr" "size_t *stacksize"
Get the stack size attribute from a thread attributes object.
.It int Fn pthread_attr_getstackaddr "pthread_attr_t *attr" "void **stackaddr"
Get the stack address attribute from a thread attributes object.
.It int Fn pthread_attr_getdetachstate "pthread_attr_t *attr" "int *detachstate"
Get the detach state attribute from a thread attributes object.
.It int Fn pthread_attr_init "pthread_attr_t *attr"
Initialize a thread attributes object with default values.
.It int Fn pthread_attr_setinheritsched "pthread_attr_t *attr" "int inheritsched"
Set the inherit scheduling attribute in a thread attributes object.
.It int Fn pthread_attr_setschedparam "pthread_attr_t *attr" "struct sched_param *param"
Set the scheduling parameter attribute in a thread attributes object.
.It int Fn pthread_attr_setschedpolicy "pthread_attr_t *attr" "int policy"
Set the scheduling policy attribute in a thread attributes object.
.It int Fn pthread_attr_setscope "pthread_attr_t *attr" "int contentionscope"
Set the contention scope attribute in a thread attributes object.
.It int Fn pthread_attr_setstacksize "pthread_attr_t *attr" "size_t stacksize"
Set the stack size attribute in a thread attributes object.
.It int Fn pthread_attr_setstackaddr "pthread_attr_t *attr" "void *stackaddr"
Set the stack address attribute in a thread attributes object.
.It int Fn pthread_attr_setdetachstate "pthread_attr_t *attr" "int detachstate"
Set the detach state in a thread attributes object.
.El
.Sh MUTEX ROUTINES
.Bl -tag -width Er
.It int Fn pthread_mutexattr_destroy "pthread_mutexattr_t *attr"
Destroy a mutex attributes object.
.It int Fn pthread_mutexattr_init "pthread_mutexattr_t *attr"
Initialize a mutex attributes object with default values.
.It int Fn pthread_mutex_destroy "pthread_mutex_t *mutex"
Destroy a mutex.
.It int Fn pthread_mutex_init "pthread_mutex_t *mutex" "const pthread_mutexattr_t *attr"
Initialize a mutex with specified attributes.
.It int Fn pthread_mutex_lock "pthread_mutex_t *mutex"
Lock a mutex and block until it becomes available.
.It int Fn pthread_mutex_trylock "pthread_mutex_t *mutex"
Try to lock a mutex, but don't block if the mutex is locked by another thread,
including the current thread.
.It int Fn pthread_mutex_unlock "pthread_mutex_t *mutex"
Unlock a mutex.
.El
.Sh CONDITION VARIABLE ROUTINES
.Bl -tag -width Er
.It int Fn pthread_condattr_init "pthread_condattr_t *attr"
Initialize a condition variable attributes object with default values.
.It int Fn pthread_condattr_destroy "pthread_condattr_t *attr"
Destroy a condition variable attributes object.
.It int Fn pthread_cond_broadcast "pthread_cond_t *cond"
Unblock all threads currently blocked on the specified condition variable.
.It int Fn pthread_cond_destroy "pthread_cond_t *cond"
Destroy a condition variable.
.It int Fn pthread_cond_init "pthread_cond_t *cond" "const pthread_condattr_t *attr"
Initialize a condition variable with specified attributes.
.It int Fn pthread_cond_signal "pthread_cond_t *cond"
Unblock at least one of the threads blocked on the specified condition variable.
.It int Fn pthread_cond_timedwait "pthread_cond_t *cond" "pthread_mutex_t *mutex" "const struct timespec *abstime"
Wait no longer than the specified time for a condition and lock the specified mutex.
.It int Fn pthread_cond_wait "pthread_cond_t *" "pthread_mutex_t *mutex"
Wait for a condition and lock the specified mutex.
.El
.Sh PER-THREAD CONTEXT ROUTINES
.Bl -tag -width Er
.It int Fn pthread_key_create "pthread_key_t *key" "void (*routine)(void *)"
Create a thread-specific data key.
.It int Fn pthread_key_delete "pthread_key_t key"
Delete a thread-specific data key.
.It void * Fn pthread_getspecific "pthread_key_t key" "void **value_ptr"
Get the thread-specific value for the specified key.
.It int Fn pthread_setspecific "pthread_key_t key" "const void *value_ptr"
Set the thread-specific value for the specified key.
.El
.Sh CLEANUP ROUTINES
.Bl -tag -width Er
.It void Fn pthread_cleanup_pop "int execute"
Remove the routine at the top of the calling thread's cancellation cleanup
stack and optionally invoke it.
.It void Fn pthread_cleanup_push "void (*routine)(void *)" "void *routine_arg"
Push the specified cancellation cleanup handler onto the calling thread's 
cancellation stack.
.El
.Sh INSTALLATION
The current FreeBSD POSIX thread implementation is built in the library
.Fa libc_r
which contains both thread-safe libc functions and the thread functions.
This library replaces
.Fa libc
for threaded applications.
.Pp
By default,
.Fa libc_r
is built as part of a 'make world'.  To disable the build of
.Fa libc_r
you must supply the '-DNOLIBC_R' option to
.Xr make 1 .
.Pp
A FreeBSD specific option has been added to gcc to make linking
threaded processes simple.
.Fa gcc -pthread
links a threaded process against
.Fa libc_r
instead of
.Fa libc.
.Sh STANDARDS
The functions in
.Fa libc_r
with the
.Fa pthread_
prefix and not
.Fa _np
suffix conform to IEEE
.Pq Dq Tn POSIX
Std 1003.1 Second Edition 1996-07-12
.Pp
The functions in libc_r with the
.Fa pthread_
prefix and
.Fa _np
suffix are non-portable extensions to POSIX threads.
