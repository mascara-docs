.\" Copyright (c) 1996 David Nugent <davidn@blaze.net.au>
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, is permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice immediately at the beginning of the file, without modification,
.\"    this list of conditions, and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. This work was done expressly for inclusion into FreeBSD.  Other use
.\"    is permitted provided this notation is included.
.\" 4. Absolutely no warranty of function or purpose is made by the author
.\"    David Nugent.
.\" 5. Modifications may be freely made to this file providing the above
.\"    conditions are met.
.\"
.\" $FreeBSD: src/usr.bin/limits/limits.1,v 1.2.2.7 1999/09/05 11:32:35 peter Exp $
.\"
.Dd January 15, 1996
.Dt LIMITS 1
.Os FreeBSD
.Sh NAME
.Nm limits
.Nd set or display process resource limits
.Sh SYNOPSIS
.Nm limits
.Op Fl C Ar class
.Op Fl SHB
.Op Fl ea
.Op Fl cdflmnstu Op val
.Nm limits
.Op Fl C Ar class
.Op Fl SHB
.Op Fl cdflmnstu Op val
.Op Fl E
.Op Ar name=value ...
.Op Ar command
.Nm limits
.Op Fl U Ar user
.Op Fl SHB
.Op Fl ea
.Op Fl cdflmnstu Op val
.Nm limits
.Op Fl U Ar user
.Op Fl SHB
.Op Fl cdflmnstu Op val
.Op Fl E
.Op Ar name=value ...
.Op Ar command
.Sh DESCRIPTION
.Nm Limits
ether prints or sets kernel resource limits, and may optionally set
environment variables like
.Xr env 1
and run a program with the selected resources.
Three uses of the
.Nm limits
command are possible:
.Pp
.Bl -hang -width indent
.It Nm limits Op Ar limitflags
.Op Ar name=value
.Ar command
.Pp
This usage sets limits according to
.Ar limitflags ,
optionally sets environment variables given as
.Ar name=value
pairs, and then runs the specified command.
.It Nm limits Op Ar limitflags
.Pp
This usage determines values of resource settings according to
.Ar limitflags ,
does not attempt to set them and outputs these values to
standard output.
By default, this will output the current kernel resource settings
active for the calling process.
Using the
.Fl C Ar class
or
.Fl U Ar user
flags, you may also display the current resource settings modified
by the the appropriate login class resource limit entries from
the
.Xr login.conf 5
login capabilities database.
.It Nm limits Fl e Op Ar limitflags
.Pp
This usage determines values of resource settings according to
.Ar limitflags ,
but does not set them itself.
Like the previous usage it outputs these values to standard
output, except that it will emit them in
.Em eval
format, suitable for the calling shell.
The calling shell is determined by examining the entries in the
.Pa /proc
filesystem for the parent process.
If the shell is known (ie. it is one of sh, csh, bash, tcsh, ksh,
pdksh or rc),
.Nm limits
emits 'limit' or 'ulimit' commands in the format understood by
that shell.
If the name of the shell cannot be determined, then the 'ulimit'
format used by
.Pa /bin/sh
is used.
.Pp
This is very useful for setting limits used by scripts, or prior
launching of daemons and other background tasks with specific
resource limit settings, and provides the benefit of allowing
global configuration of maximum resource usage by maintaining a
central database of settings in the login class database.
.Pp
Within a shell script,
.Nm limits
will normally be used with eval within backticks as follows:
.Pp
.Dl eval `limits -e -C daemon`
.Pp
which causes the output of
.Nm limits
to be evaluated and set by the current shell.
.El
.Pp
The value of limitflags specified in the above contains one or more of the
following options:
.Pp
.Bl -tag -width "-d [limit]"
.It Fl C Ar class
Use current resource values, modified by the resource entries applicable
for the login class "class".
.It Fl U Ar user
Use current resource values, modified by the resource entries applicable
to the login class which "user" belongs to.
If the user does not belong to a class, then the resource capabilities
for the "default" class are used, if it exists, or the "root" class if
the user is a superuser account.
.It Fl S
Selects display or setting of "soft" (or current) resource limits.
If specific limits settings follow this switch, only soft limits are
affected unless overridden later with either the
.Fl H
or
.Fl B
flags.
.It Fl H
Selects display or setting of "hard" (or maximum) resource limits.
If specific limits settings follow this switch, only hard limits are
affected until overridden later with either the
.Fl S
or
.Fl B
flags.
.It Fl B
Selects display or setting of both "soft" (current) or "hard" (maximum)
resource limits.
If specific limits settings follow this switch, both soft and hard
limits are affected until overridden later with either the
.Fl S
or 
.Fl H
flags.
.Fl e
Selects "eval mode" formatting for output.
This is valid only on display mode and cannot be used when running a
command.
The exact syntax used for output depends upon the type of shell from
which
.Nm limits
is invoked.
.It Fl c Op Ar limit
Selects or sets (if 'limit' is specified) the
.Em coredumpsize
resource limit.
A value of 0 disables core dumps.
.It Fl d Op Ar limit
Selects or sets (if 'limit' is specified) the
.Em datasize
resource limit.
.It Fl f Op Ar limit
Selects or sets the
.Em filesize
resource limit.
.It Fl l Op Ar limit
Selects or sets the
.Em memorylocked
resource limit.
.It Fl m Op Ar limit
Selects or sets the
.Em memoryuse
size limit.
.It Fl n Op Ar limit
Selects or sets the
.Em openfiles
resource limit.
.It Fl s Op Ar limit
Selects or sets the
.Em stacksize
resource limit.
.It Fl t Op Ar limit
Selects or sets the
.Em cputime
resource limit.
.It Fl u Op Ar limit
Selects or sets the
.Em maxproc
resource limit.
.Pp
Valid values for 'limit' in the above set of flags consist of either the
string 'infinity' or 'inf' for an infinite (or kernel-defined maximum)
limit, or a numeric value maybe followed by a suffix.
Values which relate to size default to a value in bytes, or one of the
following suffixes may be used as a multiplier:
.Pp
.Bl -tag -offset indent -width "xxxx" -compact
.It b
512 byte blocks.
.It k
kilobytes (1024 bytes).
.It m
megabytes (1024*1024 bytes).
.It g
gigabytes.
.It t
terabytes.
.El
.Pp
The
.Em cputime
resource defaults to a number of seconds, but a multiplier may be
used, and as with size values, multiple values separated by a valid
suffix are added together:
.Bl -tag -offset indent -width "xxxx" -compact
.It s
seconds.
.It m
minutes.
.It h
hours.
.It d
days.
.It w
weeks.
.It y
365 day years.
.El
.Pp
.It Fl E
The option
.Sq Fl E
causes
.Nm limits
to completely ignore the environment it inherits.
.It Fl a
This option forces all resource settings to be displayed even if
other specific resource settings have been specified.
For example, if you wish to disable core dumps when starting up
the usenet news system, but wish to set all other resource settings
as well that apply to the 'news' account, you might use:
.Pp
.Dl eval `limits -U news -aBec 0`
.Pp
As with the 
.Xr setrlimit 3
call, only the superuser may raise process "hard" resource limits.
Non-root users may, however, lower them or change "soft" resource limits
within to any value below the hard limit.
When invoked to execute a program, the failure of
.Nm limits
to raise a hard limit is considered a fatal error.
.El
.Sh DIAGNOSTICS
.Nm Limits
exits with EXIT_FAILURE if usage is incorrect in any way; ie. an invalid
option, or set/display options are selected in the same invocation,
.Fl e
is used when running a program, etc.
When run in display or eval mode,
.Nm limits
exits with with a status of EXIT_SUCCESS.
When run in command mode and execution of the command succeeds, the exit status
will be whatever the executed program returns.
.Sh SEE ALSO
.Xr csh 1 ,
.Xr env 1 ,
.Xr limit 1 ,
.Xr sh 1 ,
.Xr getrlimit 2 ,
.Xr setrlimit 2 ,
.Xr login_cap 3 ,
.Xr login.conf 5
.Sh BUGS
.Nm Limits
does not handle commands with equal (``='') signs in their
names, for obvious reasons.
.Pp
When eval output is selected, the /proc filesystem must be installed
and mounted for the shell to be correctly determined, and therefore
output syntax correct for the running shell.
The default output is valid for /bin/sh, so this means that any
usage of
.Nm limits
in eval mode prior mounting /proc may only occur in standard bourne
shell scripts.
.Pp
.Nm Limits
makes no effort to ensure that resource settings emitted or displayed
are valid and settable by the current user.
Only a superuser account may raise hard limits, and when doing so
the FreeBSD kernel will silently lower limits to values less than
specified if the values given are too high.
