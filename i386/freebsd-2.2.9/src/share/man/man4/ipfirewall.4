.\"
.\" $FreeBSD: src/share/man/man4/ipfirewall.4,v 1.6.2.6 1999/09/05 11:26:56 peter Exp $
.\"
.Dd June 22, 1997
.Dt IPFIREWALL 4
.Os
.Sh NAME
.Nm ipfirewall
.Nd IP packet filter and traffic accounting
.Sh SYNOPSIS
.Fd #include <sys/types.h>
.Fd #include <sys/queue.h>
.Fd #include <netinet/in.h>
.Fd #include <netinet/ip_fw.h>
.Ft int
.Fn setsockopt raw_socket IPPROTO_IP "ipfw option" "struct ipfw" size
.Sh DESCRIPTION
Ipfirewall (alias ipfw) is a system facility which allows filtering,
redirecting, and other operations on IP packets travelling through
system interfaces. Packets are matched by applying an ordered list
of pattern rules against each packet until a match is found, at
which point the corresponding action is taken. Rules are numbered
from 1 to 65534; multiple rules may share the same number.
.Pp
There is one rule that always exists, rule number 65535. This rule
normally causes all packets to be dropped. Hence, any packet which does not
match a lower numbered rule will be dropped.  However, a kernel compile
time option
.Dq IPFIREWALL_DEFAULT_TO_ACCEPT
allows the administrator to change this fixed rule to permit everything.
.Pp
The value passed to 
.Fn setsockopt
is a struct ip_fw describing the rule (see below). In some cases
(such as IP_FW_DEL), only the rule number is significant.
.Sh COMMANDS
The following socket options are used to manage the rule list:
.Pp
IP_FW_ADD inserts the rule into the rule list.
.Pp
IP_FW_DEL deletes all rules having the matching rule number.
.Pp
IP_FW_GET returns the (first) rule having the matching rule number.
.Pp
IP_FW_ZERO zeros the statistics associated with all rules having the
matching rule number. If the rule number is zero, all rules are zeroed.
.Pp
IP_FW_FLUSH removes all rules (except 65535).
.Pp
When the kernel security level is greater than 2, only IP_FW_GET
is allowed.
.Sh RULE STRUCTURE
Rules are described by the following structure:
.Bd -literal
/* Specify an interface */
union ip_fw_if {
    struct in_addr fu_via_ip;   /* Specified by IP address */
    struct {                    /* Specified by interface name */
#define FW_IFNLEN       6       /* To keep structure on 2^x boundary */
            char  name[FW_IFNLEN];
            short unit;         /* -1 means match any unit */
    } fu_via_if;
};

/* One ipfw rule */
struct ip_fw {
    u_long fw_pcnt,fw_bcnt;         /* Packet and byte counters */
    struct in_addr fw_src, fw_dst;  /* Source and destination IP addr */
    struct in_addr fw_smsk, fw_dmsk;/* Mask for src and dest IP addr */
    u_short fw_number;              /* Rule number */
    u_short fw_flg;                 /* Flags word */
#define IP_FW_MAX_PORTS 10          /* A reasonable maximum */
    u_short fw_pts[IP_FW_MAX_PORTS];/* Array of port numbers to match */
    u_char fw_ipopt,fw_ipnopt;      /* IP options set/unset */
    u_char fw_tcpf,fw_tcpnf;        /* TCP flags set/unset */
#define IP_FW_ICMPTYPES_DIM (256 / (sizeof(unsigned) * 8))
    unsigned fw_icmptypes[IP_FW_ICMPTYPES_DIM]; /* ICMP types bitmap */
    long timestamp;                 /* timestamp (tv_sec) of last match */
    union ip_fw_if fw_in_if, fw_out_if;/* Incoming / outgoing interfaces */
    union {
        u_short fu_divert_port;     /* Divert/tee port */
        u_short fu_skipto_rule;     /* SKIPTO command rule number */
        u_short fu_reject_code;     /* REJECT response code */
    } fw_un;
    u_char fw_prot;                 /* IP protocol */
    u_char fw_nports;               /* N'of src ports and # of dst ports */
                                    /* in ports array (dst ports follow */
                                    /* src ports; max of 10 ports in all */
                                    /* count of 0 means match all ports) */
};

/* Encoding of number of source/dest ports from "fw_nports" */

#define IP_FW_GETNSRCP(rule)            ((rule)->fw_nports & 0x0f)
#define IP_FW_SETNSRCP(rule, n)         do {                            \\
                                          (rule)->fw_nports &= ~0x0f;   \\
                                          (rule)->fw_nports |= (n);     \\
                                        } while (0)
#define IP_FW_GETNDSTP(rule)            ((rule)->fw_nports >> 4)
#define IP_FW_SETNDSTP(rule, n)         do {                            \\
                                          (rule)->fw_nports &= ~0xf0;   \\
                                          (rule)->fw_nports |= (n) << 4;\\
                                        } while (0)

/* Flags values for "flags" field */

#define IP_FW_F_IN      0x0001  /* Check inbound packets                */
#define IP_FW_F_OUT     0x0002  /* Check outbound packets               */
#define IP_FW_F_IIFACE  0x0004  /* Apply inbound interface test         */
#define IP_FW_F_OIFACE  0x0008  /* Apply outbound interface test        */

#define IP_FW_F_COMMAND 0x0070  /* Mask for type of chain entry:        */
#define IP_FW_F_DENY    0x0000  /* This is a deny rule                  */
#define IP_FW_F_REJECT  0x0010  /* Deny and send a response packet      */
#define IP_FW_F_ACCEPT  0x0020  /* This is an accept rule               */
#define IP_FW_F_COUNT   0x0030  /* This is a count rule                 */
#define IP_FW_F_DIVERT  0x0040  /* This is a divert rule                */
#define IP_FW_F_TEE     0x0050  /* This is a tee rule                   */
#define IP_FW_F_SKIPTO  0x0060  /* This is a skipto rule                */

#define IP_FW_F_PRN     0x0080  /* Print if this rule matches           */

#define IP_FW_F_SRNG    0x0100  /* The first two src ports are a min    *
                                 * and max range (stored in host byte   *
                                 * order).                              */

#define IP_FW_F_DRNG    0x0200  /* The first two dst ports are a min    *
                                 * and max range (stored in host byte   *
                                 * order).                              */

#define IP_FW_F_IIFNAME 0x0400  /* In interface by name/unit (not IP)   */
#define IP_FW_F_OIFNAME 0x0800  /* Out interface by name/unit (not IP)  */

#define IP_FW_F_INVSRC  0x1000  /* Invert sense of src check            */
#define IP_FW_F_INVDST  0x2000  /* Invert sense of dst check            */

#define IP_FW_F_FRAG    0x4000  /* Fragment                             */

#define IP_FW_F_ICMPBIT 0x8000  /* ICMP type bitmap is valid            */

#define IP_FW_F_MASK    0xFFFF  /* All possible flag bits mask          */
.Ed

.Sh RULE ACTIONS
Each rule has an action described by the IP_FW_F_COMMAND bits in the
flags word:

  IP_FW_F_DENY          - drop packet
  IP_FW_F_REJECT        - drop packet; send rejection via ICMP or TCP
  IP_FW_F_ACCEPT        - accept packet
  IP_FW_F_COUNT         - increment counters; continue matching
  IP_FW_F_DIVERT        - divert packet to a divert(4) socket
  IP_FW_F_TEE           - copy packet to a divert(4) socket; continue
  IP_FW_F_SKIPTO        - skip to rule number fu_skipto_rule
.Pp
In the case of IP_FW_F_REJECT, if the fu_reject_code is a number
from 0 to 255, then an ICMP unreachable packet is sent back to the
original packet's source IP address, with the corresponding code.
Otherwise, the value must be 256 and the protocol IPPROTO_TCP,
in which case a TCP reset packet is sent instead.
.Pp
With IP_FW_F_SKIPTO, all succeeding rules having rule number less
than fu_skipto_rule are skipped.
.Sh KERNEL OPTIONS
Options in the kernel configuration file:
  IPFIREWALL               - enable ipfirewall.
  IPFIREWALL_VERBOSE       - enable firewall output
  IPFIREWALL_VERBOSE_LIMIT - limit firewall output
  DIVERT                   - enable divert(4) sockets.
.Pp
When packets match a rule with the IP_FW_F_PRN bit set, a message
is logged to the console if IPFIREWALL_VERBOSE has been enabled;
IPFIREWALL_VERBOSE_LIMIT limits the maximum number of times each
rule can cause cause a log message. These variables are also
available via the
.Xr sysctl 3
interface.
.Sh DIAGNOSTICS

[EINVAL]  The IP option field was improperly formed; an option
          field was shorter than the minimum value or longer than
          the option buffer provided. A structural error in 
          ip_fw structure occurred (n_src_p+n_dst_p too big,
          ports set for ALL/ICMP protocols etc.). An invalid
          rule number was used.
.Sh SEE ALSO
.Xr setsockopt 2 ,
.Xr divert 4 ,
.Xr ip 4 ,
.Xr ipfw 8 ,
.Xr sysctl 8 .
.Sh BUGS
The ``tee'' rule is not yet implemented (currently it has no effect).
.Pp
This man page still needs work.
.Sh HISTORY
The ipfw facility was initially written as package to BSDI
by Daniel Boulet <danny@BouletFermat.ab.ca>.
It has been heavily modified and ported to FreeBSD
by Ugen J.S.Antsilevich <ugen@NetVision.net.il>.
.Pp
Several enhancements added by Archie Cobbs <archie@whistle.com>.
