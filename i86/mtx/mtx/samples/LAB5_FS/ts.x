                BOOTSEG = 0x1000
.globl begtext, begdata, begbss                      !! needed by linker

!               IMPORTS and EXPORTS

.globl _main,_prints                                 
.globl _tswitch,_running,_scheduler

.globl _int80h,_kcinth
.globl _goUmode

.globl _proc, _procsize
.globl _color

.globl _in_byte, _out_byte

.globl _inkmode
.globl _lock,_restore,_unlock
.globl _kbinth, _kbhandler
 
.text                                                !! these tell as:	
begtext:                                             !! text,data,bss segments
.data                                                !! are all the same.
begdata:
.bss
begbss:
.text                                                

        mov     ax,cs                   ! establish segments 
        mov     ds,ax                   ! we know ES,CS=0x1000. Let DS=CS  
        mov     ss,ax                   ! SS = CS ===> all point to 0x1000
        mov     es,ax

        mov     sp,#_proc               ! SP -> proc[0].kstack HIGH end
        add     sp,_procsize

        mov     ax,#0x0003
        int     #0x10

        call _main                      ! call main[] in C

! if ever return, just hang     
        mov   ax, #msg
        push  ax
        call  _prints
dead:   jmp   dead
msg:    .asciz "BACK TO ASSEMBLY AND HANG\n\r"    
	
!*************************************************************
!     KCW  added functions for MT system
!************************************************************
_tswitch:
          push   ax
          push   bx
          push   cx
          push   dx
          push   bp
          push   si
          push   di
	  pushf
	  mov	 bx, _running
 	  mov	 2[bx], sp

find:     call	 _scheduler

resume:	  mov	 bx, _running
	  mov	 sp, 2[bx]
	  popf
	  pop    di
          pop    si
          pop    bp
          pop    dx
          pop    cx
          pop    bx
          pop    ax
          ret



!These offsets are defined in struct proc
USS =   4
USP =   6

! as86 macro: parameters are ?1 ?2, etc 
! as86 -m -l listing src (generates listing with macro expansion)

         MACRO INTH
          push ax
          push bx
          push cx
          push dx
          push bp
          push si
          push di
          push es
          push ds

          push cs
          pop  ds

          inc _inkmode          ! enter Kmode : ++inkmode
          cmp _inkmode,#1       ! if inkmode == 1 ==> interrupt was in Umode
          jg  ?1                ! imode>1 : was in Kmode: bypass saving uss,usp

          ! was in Umode: save interrupted (SS,SP) into proc
	  mov si,_running   	! ready to access proc
          mov USS[si],ss        ! save SS  in proc.USS
          mov USP[si],sp        ! save SP  in proc.USP

          ! change DS,ES,SS to Kernel segment
          mov  di,ds            ! stupid !!        
          mov  es,di            ! CS=DS=SS=ES in Kmode
          mov  ss,di

          mov  sp, _running     ! sp -> running's kstack[] high end
          add  sp, _procsize

?1:       call  _?1             ! call handler in C

          br    _ireturn        ! return to interrupted point

         MEND


_int80h: INTH kcinth
_kbinth: INTH kbhandler

!*===========================================================================*
!*		_ireturn  and  goUmode()       				     *
!*===========================================================================*
! ustack contains    flag,ucs,upc, ax,bx,cx,dx,bp,si,di,es,ds
! uSS and uSP are in proc
_ireturn:
_goUmode:
        cli
        dec _inkmode            ! --inkmode
        cmp _inkmode,#0         ! inkmode==0 means was in Umode
        jg  xkmode

! restore uSS, uSP from running PROC
	mov si,_running 	! si -> proc
        mov ax,USS[si]
        mov ss,ax               ! restore SS
        mov sp,USP[si]          ! restore SP
xkmode:                         
	pop ds
	pop es
	pop di
        pop si
        pop bp
        pop dx
        pop cx
        pop bx
        pop ax 
        iret

!*===========================================================================*
!*				in_byte					     *
!*===========================================================================*
! PUBLIC unsigned in_byte[port_t port];
! Read an [unsigned] byte from the i/o port  port  and return it.

_in_byte:
        push    bp
        mov     bp,sp
        mov     dx,4[bp]
	inb     al,dx		! input 1 byte
	subb	ah,ah		! unsign extend
        pop     bp
        ret

!*===========================================================================*
!*				out_byte				     *
!*==============================================================
! out_byte[port_t port, int value];
! Write  value  [cast to a byte]  to the I/O port  port.

_out_byte:
        push    bp
        mov     bp,sp
        mov     dx,4[bp]
        mov     ax,6[bp]
	outb	dx,al   	! output 1 byte
        pop     bp
        ret


_int_off:             ! cli, return old flag register
        pushf
        cli
        pop ax
        ret

_int_on:              ! int_on(int SR)
        push bp
        mov  bp,sp

        mov  ax,4[bp] ! get SR passed in

        push ax
        popf

        pop  bp
        ret



lockvar: .word 0
!*===========================================================================*
!*				lock					     *
!*===========================================================================*
! Disable CPU interrupts.
_lock:  
	pushf			! save flags on stack
	cli			! disable interrupts
	pop lockvar		! save flags for possible restoration later
	ret			! return to caller


!*===========================================================================*
!*				unlock					     *
!*===========================================================================*
! Enable CPU interrupts.
_unlock:
	sti			! enable interrupts
	ret			! return to caller


!*===========================================================================*
!*				restore					     *
!*===========================================================================*
! Restore enable/disable bit to the value it had before last lock.
_restore:
	push lockvar		! push flags as they were before previous lock
	popf			! restore flags
	ret			! return to caller

