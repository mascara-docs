.TH @G@NEQN @MAN1EXT@ "@MDATE@" "Groff Version @VERSION@"
.SH NAME
@g@neqn \- format equations for ascii output
.SH SYNOPSIS
.B @g@neqn
[eqn options]
.SH DESCRIPTION
.B neqn
program is actually just a shell script which invokes the
.B eqn(1)
command with the ascii output device.
.SH "SEE ALSO"
.BR eqn (@MAN1EXT@)
