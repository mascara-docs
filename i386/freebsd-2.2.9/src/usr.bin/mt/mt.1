.\" Copyright (c) 1981, 1990, 1993
.\"	The Regents of the University of California.  All rights reserved.
.\"
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by the University of
.\"	California, Berkeley and its contributors.
.\" 4. Neither the name of the University nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"	@(#)mt.1	8.1 (Berkeley) 6/6/93
.\"
.Dd June 6, 1993
.Dt MT 1
.Os BSD 4
.Sh NAME
.Nm mt
.Nd magnetic tape manipulating program
.Sh SYNOPSIS
.Nm
.Op Fl f Ar tapename
.Ar command
.Op Ar count
.Sh DESCRIPTION
.Nm Mt
is used to give commands to a magnetic tape drive.
By default
.Nm
performs the requested operation once.  Operations
may be performed multiple times by specifying
.Ar count  .
Note
that
.Ar tapename
must reference a raw (not block) tape device.
.Pp
The available commands are listed below.  Only as many
characters as are required to uniquely identify a command
need be specified.
.Bl -tag -width "eof, weof"
.It Cm weof
Write
.Ar count
end-of-file marks at the current position on the tape.
.It Cm fsf
Forward space
.Ar count
files.
.It Cm fsr
Forward space
.Ar count
records.
.It Cm bsf
Back space
.Ar count
files.
.It Cm bsr
Back space
.Ar count
records.
.It Cm rewind
Rewind the tape
(Count is ignored).
.It Cm offline , rewoffl
Rewind the tape and place the tape unit off-line
(Count is ignored).
.It Cm erase
Erase the tape
(Count is ignored).
.It Cm retension
Re-tension the tape
(one full wind forth and back, Count is ignored).
.It Cm status
Print status information about the tape unit.
.It Cm blocksize
Set the block size for the tape unit.  Zero means variable-length
blocks.
.It Cm density
Set the density for the tape unit.  For the density codes, see below.
The density value could be given either numerically, or as a string,
corresponding to the
.Dq Reference
field.  If the string is abbreviated, it will be resolved in the order
shown in the table, and the first matching entry will be used.  If the
given string and the resulting canonical density name do not match
exactly, an informational message is printed about what the given
string has been taken for.
.It Cm eom
Forward space to end of recorded medium
(Count is ignored).
.It Cm eod
Forward space to end of data, identical to
.Cm eom .
.It Cm comp
Set compression mode.
(The kernel counterpart of this has not yet been reported to work
correctly.)
.El
.Pp
If a tape name is not specified, and the environment variable
.Ev TAPE
does not exist;
.Nm
uses the device
.Pa /dev/nrst0 .
.Pp
.Nm Mt
returns a 0 exit status when the operation(s) were successful,
1 if the command was unrecognized, and 2 if an operation failed.
.Pp
The different density codes are as follows:
.Pp
.Dl 0x0	default for device
.Dl 0xE	reserved for ECMA
.Bd -literal -offset indent
Value Tracks Density(bpi) Code Type  Reference     Note
0x1     9       800       NRZI  R    X3.22-1983    2
0x2     9      1600       PE    R    X3.39-1986    2
0x3     9      6250       GCR   R    X3.54-1986    2
0x5    4/9     8000       GCR   C    X3.136-1986   1
0x6     9      3200       PE    R    X3.157-1987   2
0x7     4      6400       IMFM  C    X3.116-1986   1
0x8     4      8000       GCR   CS   X3.158-1986   1
0x9    18     37871       GCR   C    X3B5/87-099   2
0xA    22      6667       MFM   C    X3B5/86-199   1
0xB     4      1600       PE    C    X3.56-1986    1
0xC    24     12690       GCR   C    HI-TC1        1,5
0xD    24     25380       GCR   C    HI-TC2        1,5
0xF    15     10000       GCR   C    QIC-120       1,5
0x10   18     10000       GCR   C    QIC-150       1,5
0x11   26     16000       GCR   C    QIC-320(525?) 1,5
0x12   30     51667       RLL   C    QIC-1350      1,5
0x13    1     61000       DDS   CS   X3B5/88-185A  4
0x14    1     43245       RLL   CS   X3.202-1991   4
0x15    1     45434       RLL   CS   ECMA TC17     4
0x16   48     10000       MFM   C    X3.193-1990   1
0x17   48     42500       MFM   C    X3B5/91-174   1
.Ed

where Code means:
.Bd -literal -offset indent
NRZI	Non Return to Zero, change on ones
GCR	Group Code Recording
PE	Phase Encoded
IMFM	Inverted Modified Frequency Modulation
MFM	Modified Frequency Modulation
DDS	Dat Data Storage
RLL	Run Length Encoding
.Ed

where Type means:
.Bd -literal -offset indent
R	Reel-to-Reel
C	Cartridge
CS	cassette
.Ed

where Notes means:
.Bd -literal -offset indent
1	Serial Recorded
2	Parallel Recorded
3	Old format know as QIC-11
4	Helical Scan
5	Not ANSI standard, rather industry standard.
.Ed

.Sh ENVIRONMENT
If the following environment variable exists, it is utilized by
.Nm mt .
.Bl -tag -width Fl
.It Ev TAPE
.Nm Mt
checks the
.Ev TAPE
environment variable if the
argument
.Ar tapename
is not given.
.Sh FILES
.Bl -tag -width /dev/*rst[0-9]*xx -compact
.It Pa /dev/*rwt*
QIC-02/QIC-36 magnetic tape interface
.It Pa /dev/*rst[0-9]*
SCSI magnetic tape interface
.El
.Sh SEE ALSO
.Xr dd 1 ,
.Xr ioctl 2 ,
.Xr mtio 4 ,
.Xr st 4 ,
.Xr wt 4 ,
.Xr environ 7
.Sh HISTORY
The
.Nm
command appeared in
.Bx 4.3 .
.Pp
Extensions regarding the
.Xr st 4
driver appeared in 386BSD 0.1 as a separate
.Xr st 1
command, and have been merged into the
.Nm
command in
.Fx 2.1 .
.Pp
The former
.Cm eof
command that used to be a synonym for
.Cm weof
has been abandoned in
.Fx 2.1
since it was often confused with
.Cm eom ,
which is fairly dangerous.
