.TH @G@PSROFF @MAN1EXT@ "@MDATE@" "Groff Version @VERSION@"
.SH NAME
@g@psroff \- sent troff to PostScript printer
.SH SYNOPSIS
.B @g@psroff
[groff options] [files ...]
.SH DESCRIPTION
The
.B psroff
program is actually just a shell script which invokes the
.B groff(1)
command 
to print the troff
.I files
to a PostScript printer.
.SH 
.SH "SEE ALSO"
.B groff(1), lpr(1), environ(7)
