// User-level page fault handler support.
// Rather than register the C page fault handler directly with the
// kernel as the page fault handler, we register the assembly language
// wrapper in pfentry.S, which in turns calls the registered C
// function.

#include <inc/lib.h>


// Assembly language pgfault entrypoint defined in lib/pfentry.S.
asmlinkage void _pgfault_upcall(void);

// Our page fault handler, which tries all user-installed page fault handlers.
asmlinkage void _pgfault_handler(struct UTrapframe *utf);

// User-installed page fault handler pointers.
static pgfault_handler_t _pgfault_handler_pointer; // backwards compatibility
#define NUSER_HANDLERS 8
static pgfault_handler_t user_handlers[NUSER_HANDLERS];

//
// Add a page fault handler function,
// returning the old page fault handler function.
//
// If no page fault handler has been set yet,
// _pgfault_handler_pointer will be 0.
// The first time we register a handler, we need to
// allocate an exception stack (one page of memory with its top
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(pgfault_handler_t handler)
{
	int r;

	if (_pgfault_handler_pointer == 0) {
		// LAB 4: Your code here.
		panic("set_pgfault_handler not implemented");
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler_pointer = handler;

	// Store handler pointer in our list of handlers.
	for (int i = 0; i < NUSER_HANDLERS; ++i)
		if (user_handlers[i] == handler || !user_handlers[i]) {
			user_handlers[i] = handler;
			return;
		}

	panic("[%08x] too many page fault handlers\n");
}

// The overall page fault handler function
// calls each user page fault handler in order.
void
_pgfault_handler(struct UTrapframe *utf)
{
	int i;
	for (i = NUSER_HANDLERS - 1; i >= 0; --i)
		if (user_handlers[i] && user_handlers[i](utf))
			return;

	panic("[%08x] unhandled page fault at va %08x from eip %08x\n",
	      sys_getenvid(), utf->utf_fault_va, utf->utf_eip);
}
