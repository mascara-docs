#!./perl

# $Header: /home/ncvs/src/gnu/usr.bin/perl/perl/t/op/Attic/fork.t,v 1.1.1.1 1994/09/10 06:27:42 gclarkii Exp $

$| = 1;
print "1..2\n";

if ($cid = fork) {
    sleep 2;
    if ($result = (kill 9, $cid)) {print "ok 2\n";} else {print "not ok 2 $result\n";}
}
else {
    $| = 1;
    print "ok 1\n";
    sleep 10;
}
