.\" Copyright (c) 1993 Christopher G. Demetriou
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. The name of the author may not be used to endorse or promote products
.\"    derived from this software without specific prior written permission
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man4/lkm.4,v 1.6.2.3 1999/09/05 11:26:57 peter Exp $
.\"
.Dd January 17, 1996
.Dt LKM 4
.Os FreeBSD 2.0
.Sh NAME
.Nm lkm
.Nd loadable kernel module facility
.Sh DESCRIPTION
Loadable kernel modules allow the system administrator to
dynamically add and remove functionality from a running system.
This ability also helps software developers to develop
new parts of the kernel without constantly rebooting to
test their changes.
.Pp
Various types of modules can be loaded into the system.
There are several defined module types, listed below, which can
be added to the system in a predefined way.  In addition, there
is a generic type, for which the module itself handles loading and
unloading.
.Pp
The
.Tn FreeBSD
system makes extensive use of loadable kernel modules, and provides
loadable versions of most filesystems, the 
.Tn NFS
client and server, all the screen-savers, and the
.Tn iBCS2
and
.Tn Linux
emulators.  Modules distributed with the system are found in the
.Pa /lkm
directory.
.Pp
The
.Nm
interface is used by performing
.Xr ioctl 2
calls on the
.Pa /dev/lkm
device.  Normally all operations involving
Loadable Kernel Modules are handled by the
.Xr modload 8 ,
.Xr modunload 8 ,
and
.Xr modstat 8
programs.  Users should never have to interact with
.Pa /dev/lkm
directly.
.Sh "MODULE TYPES"
.Bl -ohang
.It Em "System Call modules"
System calls may be replaced by loading
new ones via the
.Nm
interface.  All system calls may be
replaced, but special care should
be taken with the
.Xr ioctl 2
system call, as it is used to load and
unload modules.
.Pp
When a system call module is unloaded,
the system call which
was replaced by the loadable module
is returned to its rightful place
in the system call table.
.It Em "Virtual File System modules"
Virtual file systems may be added
via the
.Nm
interface.
.It Em "Device Driver modules"
New block and character device
drivers may be loaded into the system with
.Nm LKM .
The major problem with loading
a device driver is that the driver's
device nodes must be exist for the
devices to be accessed.  They are usually
created by instructing
.Xr modload 8
to run an appropriate program when
the driver has been successfully loaded.
.It Em "Execution classes"
Also known as image activators, execution classes are the mechanisms
by which the
.Xr execve 2
system call is able to recognize an executable file's format and load it
into memory.
.It Em "Miscellaneous modules"
Miscellaneous modules are modules
for which there are not currently
well-defined or well-used interfaces
for extension.  Users are expected
to write their own loaders to manipulate
whatever kernel data structures necessary
to enable and disable the new module
when it is loaded and unloaded.
.El
.Sh FILES
.Bl -tag -width /usr/share/examples/lkm -compact
.It Pa /dev/lkm
.Nm
interface device
.It Pa /lkm
directory containing module binaries shipped with the system
.It Pa /usr/include/sys/lkm.h
file containing definitions of module types
.It Pa /usr/share/examples/lkm
example source code implementing two of the module types
.Sh SEE ALSO
.Xr modload 8 ,
.Xr modstat 8 ,
.Xr modunload 8
.Sh BUGS
If a module fails to initialize itself correctly, and the system is
able to detect this failure, it will panic immediately.
.Pp
When system internal interfaces change, old modules often cannot
detect this, and such modules when loaded will often cause crashes or
mysterious failures.
.Sh AUTHORS
The
.Nm
facility was originally implemented by
.An Terrence R. Lambert .
Loadable filesystems were implemented by
.An Garrett Wollman ,
and loadable
execution classes were implemented by
.An David Greenman ,
.An Soren Schmidt ,
and
.An Garrett Wollman .
.Sh HISTORY
The
.Nm
facility was designed to be similar in functionality
to the loadable kernel modules facility provided by
.Tn SunOS
4.1.3.
