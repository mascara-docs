
obj/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start-0xc>:
.long MULTIBOOT_HEADER_FLAGS
.long CHECKSUM

.globl		_start
_start:
	movw	$0x1234,0x472			# warm boot
f0100000:	02 b0 ad 1b 03 00    	add    0x31bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fb                   	sti    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <_start>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 

	# Establish our own GDT in place of the boot loader's temporary GDT.
	lgdt	RELOC(mygdtdesc)		# load descriptor table
f0100015:	0f 01 15 18 10 11 00 	lgdtl  0x111018

	# Immediately reload all segment registers (including CS!)
	# with segment selectors from the new GDT.
	movl	$DATA_SEL, %eax			# Data segment selector
f010001c:	b8 10 00 00 00       	mov    $0x10,%eax
	movw	%ax,%ds				# -> DS: Data Segment
f0100021:	8e d8                	mov    %eax,%ds
	movw	%ax,%es				# -> ES: Extra Segment
f0100023:	8e c0                	mov    %eax,%es
	movw	%ax,%ss				# -> SS: Stack Segment
f0100025:	8e d0                	mov    %eax,%ss
	ljmp	$CODE_SEL,$relocated		# reload CS by jumping
f0100027:	ea 2e 00 10 f0 08 00 	ljmp   $0x8,$0xf010002e

f010002e <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0,%ebp			# nuke frame pointer
f010002e:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f0100033:	bc 00 10 11 f0       	mov    $0xf0111000,%esp

	# now to C code
	call	i386_init
f0100038:	e8 5d 00 00 00       	call   f010009a <i386_init>

f010003d <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010003d:	eb fe                	jmp    f010003d <spin>
	...

f0100040 <_Z14test_backtracei>:
#include <kern/console.h>

// Test the stack backtrace function (lab 1 only)
void
test_backtrace(int x)
{
f0100040:	53                   	push   %ebx
f0100041:	83 ec 18             	sub    $0x18,%esp
f0100044:	8b 5c 24 20          	mov    0x20(%esp),%ebx
	cprintf("entering test_backtrace %d\n", x);
f0100048:	89 5c 24 04          	mov    %ebx,0x4(%esp)
f010004c:	c7 04 24 c0 1b 10 f0 	movl   $0xf0101bc0,(%esp)
f0100053:	e8 ee 0a 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	if (x > 0)
f0100058:	85 db                	test   %ebx,%ebx
f010005a:	7e 0d                	jle    f0100069 <_Z14test_backtracei+0x29>
		test_backtrace(x-1);
f010005c:	8d 43 ff             	lea    -0x1(%ebx),%eax
f010005f:	89 04 24             	mov    %eax,(%esp)
f0100062:	e8 d9 ff ff ff       	call   f0100040 <_Z14test_backtracei>
f0100067:	eb 1c                	jmp    f0100085 <_Z14test_backtracei+0x45>
	else
		mon_backtrace(0, 0, 0);
f0100069:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
f0100070:	00 
f0100071:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0100078:	00 
f0100079:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0100080:	e8 1e 09 00 00       	call   f01009a3 <_Z13mon_backtraceiPPcP9Trapframe>
	cprintf("leaving test_backtrace %d\n", x);
f0100085:	89 5c 24 04          	mov    %ebx,0x4(%esp)
f0100089:	c7 04 24 dc 1b 10 f0 	movl   $0xf0101bdc,(%esp)
f0100090:	e8 b1 0a 00 00       	call   f0100b46 <_Z7cprintfPKcz>
}
f0100095:	83 c4 18             	add    $0x18,%esp
f0100098:	5b                   	pop    %ebx
f0100099:	c3                   	ret    

f010009a <i386_init>:

asmlinkage void
i386_init(void)
{
f010009a:	53                   	push   %ebx
f010009b:	83 ec 18             	sub    $0x18,%esp
	extern const uintptr_t sctors[], ectors[];
	const uintptr_t *ctorva;

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f010009e:	e8 f0 06 00 00       	call   f0100793 <_Z9cons_initv>

	// Then call any global constructors (e.g., defined by C++).
	// This relies on linker script magic to define the 'sctors' and
	// 'ectors' symbols; see kern/kernel.ld.
	// Call after cons_init() so we can cprintf() if necessary.
	for (ctorva = ectors; ctorva > sctors; )
f01000a3:	b8 f4 8b 10 f0       	mov    $0xf0108bf4,%eax
f01000a8:	3d f4 8b 10 f0       	cmp    $0xf0108bf4,%eax
f01000ad:	76 0f                	jbe    f01000be <i386_init+0x24>
f01000af:	89 c3                	mov    %eax,%ebx
		((void(*)()) *--ctorva)();
f01000b1:	83 eb 04             	sub    $0x4,%ebx
f01000b4:	ff 13                	call   *(%ebx)

	// Then call any global constructors (e.g., defined by C++).
	// This relies on linker script magic to define the 'sctors' and
	// 'ectors' symbols; see kern/kernel.ld.
	// Call after cons_init() so we can cprintf() if necessary.
	for (ctorva = ectors; ctorva > sctors; )
f01000b6:	81 fb f4 8b 10 f0    	cmp    $0xf0108bf4,%ebx
f01000bc:	77 f3                	ja     f01000b1 <i386_init+0x17>
		((void(*)()) *--ctorva)();

	cprintf("6828 decimal is %o octal!\n", 6828);
f01000be:	c7 44 24 04 ac 1a 00 	movl   $0x1aac,0x4(%esp)
f01000c5:	00 
f01000c6:	c7 04 24 f7 1b 10 f0 	movl   $0xf0101bf7,(%esp)
f01000cd:	e8 74 0a 00 00       	call   f0100b46 <_Z7cprintfPKcz>




	// Test the stack backtrace function (lab 1 only)
	test_backtrace(5);
f01000d2:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
f01000d9:	e8 62 ff ff ff       	call   f0100040 <_Z14test_backtracei>

	// Drop into the kernel monitor.
	while (1)
		monitor(NULL);
f01000de:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f01000e5:	e8 bf 08 00 00       	call   f01009a9 <_Z7monitorP9Trapframe>
f01000ea:	eb f2                	jmp    f01000de <i386_init+0x44>

f01000ec <_Z6_panicPKciS0_z>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt, ...)
{
f01000ec:	56                   	push   %esi
f01000ed:	53                   	push   %ebx
f01000ee:	83 ec 14             	sub    $0x14,%esp
f01000f1:	8b 74 24 28          	mov    0x28(%esp),%esi
	va_list ap;

	if (panicstr)
f01000f5:	83 3d 20 10 11 f0 00 	cmpl   $0x0,0xf0111020
f01000fc:	75 40                	jne    f010013e <_Z6_panicPKciS0_z+0x52>
		goto dead;
	panicstr = fmt;
f01000fe:	89 35 20 10 11 f0    	mov    %esi,0xf0111020

	// Be extra sure that the machine is in as reasonable state
	__asm __volatile("cli; cld");
f0100104:	fa                   	cli    
f0100105:	fc                   	cld    

	va_start(ap, fmt);
f0100106:	8d 5c 24 2c          	lea    0x2c(%esp),%ebx
	cprintf("kernel panic at %s:%d: ", file, line);
f010010a:	8b 44 24 24          	mov    0x24(%esp),%eax
f010010e:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100112:	8b 44 24 20          	mov    0x20(%esp),%eax
f0100116:	89 44 24 04          	mov    %eax,0x4(%esp)
f010011a:	c7 04 24 12 1c 10 f0 	movl   $0xf0101c12,(%esp)
f0100121:	e8 20 0a 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	vcprintf(fmt, ap);
f0100126:	89 5c 24 04          	mov    %ebx,0x4(%esp)
f010012a:	89 34 24             	mov    %esi,(%esp)
f010012d:	e8 dd 09 00 00       	call   f0100b0f <_Z8vcprintfPKcPc>
	cprintf("\n");
f0100132:	c7 04 24 4e 1c 10 f0 	movl   $0xf0101c4e,(%esp)
f0100139:	e8 08 0a 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	va_end(ap);

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f010013e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0100145:	e8 5f 08 00 00       	call   f01009a9 <_Z7monitorP9Trapframe>
f010014a:	eb f2                	jmp    f010013e <_Z6_panicPKciS0_z+0x52>

f010014c <_Z5_warnPKciS0_z>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt, ...)
{
f010014c:	53                   	push   %ebx
f010014d:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
f0100150:	8d 5c 24 2c          	lea    0x2c(%esp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f0100154:	8b 44 24 24          	mov    0x24(%esp),%eax
f0100158:	89 44 24 08          	mov    %eax,0x8(%esp)
f010015c:	8b 44 24 20          	mov    0x20(%esp),%eax
f0100160:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100164:	c7 04 24 2a 1c 10 f0 	movl   $0xf0101c2a,(%esp)
f010016b:	e8 d6 09 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	vcprintf(fmt, ap);
f0100170:	89 5c 24 04          	mov    %ebx,0x4(%esp)
f0100174:	8b 44 24 28          	mov    0x28(%esp),%eax
f0100178:	89 04 24             	mov    %eax,(%esp)
f010017b:	e8 8f 09 00 00       	call   f0100b0f <_Z8vcprintfPKcPc>
	cprintf("\n");
f0100180:	c7 04 24 4e 1c 10 f0 	movl   $0xf0101c4e,(%esp)
f0100187:	e8 ba 09 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	va_end(ap);
}
f010018c:	83 c4 18             	add    $0x18,%esp
f010018f:	5b                   	pop    %ebx
f0100190:	c3                   	ret    
	...

f01001a0 <_ZL5delayv>:

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01001a0:	ba 84 00 00 00       	mov    $0x84,%edx
f01001a5:	ec                   	in     (%dx),%al
f01001a6:	ec                   	in     (%dx),%al
f01001a7:	ec                   	in     (%dx),%al
f01001a8:	ec                   	in     (%dx),%al
{
	inb(0x84);
	inb(0x84);
	inb(0x84);
	inb(0x84);
}
f01001a9:	c3                   	ret    

f01001aa <_ZL16serial_proc_datav>:
f01001aa:	ba fd 03 00 00       	mov    $0x3fd,%edx
f01001af:	ec                   	in     (%dx),%al

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f01001b0:	b9 ff ff ff ff       	mov    $0xffffffff,%ecx
static bool serial_exists;

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f01001b5:	a8 01                	test   $0x1,%al
f01001b7:	74 06                	je     f01001bf <_ZL16serial_proc_datav+0x15>
f01001b9:	b2 f8                	mov    $0xf8,%dl
f01001bb:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f01001bc:	0f b6 c8             	movzbl %al,%ecx
}
f01001bf:	89 c8                	mov    %ecx,%eax
f01001c1:	c3                   	ret    

f01001c2 <_ZL9cons_intrPFivE>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f01001c2:	53                   	push   %ebx
f01001c3:	83 ec 08             	sub    $0x8,%esp
f01001c6:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f01001c8:	eb 28                	jmp    f01001f2 <_ZL9cons_intrPFivE+0x30>
		if (c == 0)
f01001ca:	85 c0                	test   %eax,%eax
f01001cc:	74 24                	je     f01001f2 <_ZL9cons_intrPFivE+0x30>
			continue;
		cons.buf[cons.wpos++] = c;
f01001ce:	8b 15 64 12 11 f0    	mov    0xf0111264,%edx
f01001d4:	88 82 60 10 11 f0    	mov    %al,-0xfeeefa0(%edx)
f01001da:	8d 42 01             	lea    0x1(%edx),%eax
		if (cons.wpos == CONSBUFSIZE)
f01001dd:	3d 00 02 00 00       	cmp    $0x200,%eax
			cons.wpos = 0;
f01001e2:	0f 94 c2             	sete   %dl
f01001e5:	0f b6 d2             	movzbl %dl,%edx
f01001e8:	83 ea 01             	sub    $0x1,%edx
f01001eb:	21 d0                	and    %edx,%eax
f01001ed:	a3 64 12 11 f0       	mov    %eax,0xf0111264
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f01001f2:	ff d3                	call   *%ebx
f01001f4:	83 f8 ff             	cmp    $0xffffffff,%eax
f01001f7:	75 d1                	jne    f01001ca <_ZL9cons_intrPFivE+0x8>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01001f9:	83 c4 08             	add    $0x8,%esp
f01001fc:	5b                   	pop    %ebx
f01001fd:	c3                   	ret    

f01001fe <_ZL16cga_savebuf_copyib>:
#if CRT_SAVEROWS > 0
// Copy one screen's worth of data to or from the save buffer,
// starting at line 'first_line'.
static void
cga_savebuf_copy(int first_line, bool to_screen)
{
f01001fe:	83 ec 2c             	sub    $0x2c,%esp
f0100201:	89 5c 24 1c          	mov    %ebx,0x1c(%esp)
f0100205:	89 74 24 20          	mov    %esi,0x20(%esp)
f0100209:	89 7c 24 24          	mov    %edi,0x24(%esp)
f010020d:	89 6c 24 28          	mov    %ebp,0x28(%esp)
f0100211:	89 d5                	mov    %edx,%ebp
	uint16_t *pos;
	uint16_t *end;
	uint16_t *trueend;

	// Calculate the beginning & end of the save buffer area.
	pos = crtsave_buf + (first_line % CRT_SAVEROWS) * CRT_COLS;
f0100213:	89 c2                	mov    %eax,%edx
f0100215:	c1 fa 1f             	sar    $0x1f,%edx
f0100218:	c1 ea 19             	shr    $0x19,%edx
f010021b:	01 d0                	add    %edx,%eax
f010021d:	83 e0 7f             	and    $0x7f,%eax
f0100220:	29 d0                	sub    %edx,%eax
f0100222:	8d 34 80             	lea    (%eax,%eax,4),%esi
f0100225:	c1 e6 05             	shl    $0x5,%esi
f0100228:	81 c6 80 12 11 f0    	add    $0xf0111280,%esi
	end = pos + CRT_ROWS * CRT_COLS;
f010022e:	8d be a0 0f 00 00    	lea    0xfa0(%esi),%edi
f0100234:	89 fb                	mov    %edi,%ebx
f0100236:	81 ff 80 62 11 f0    	cmp    $0xf0116280,%edi
f010023c:	76 05                	jbe    f0100243 <_ZL16cga_savebuf_copyib+0x45>
f010023e:	bb 80 62 11 f0       	mov    $0xf0116280,%ebx
	// Check for wraparound.
	trueend = MIN(end, crtsave_buf + CRT_SAVEROWS * CRT_COLS);

	// Copy the initial portion.
	if (to_screen)
f0100243:	89 e8                	mov    %ebp,%eax
f0100245:	84 c0                	test   %al,%al
f0100247:	74 1e                	je     f0100267 <_ZL16cga_savebuf_copyib+0x69>
		memmove(crt_buf, pos, (trueend - pos) * sizeof(uint16_t));
f0100249:	89 d8                	mov    %ebx,%eax
f010024b:	29 f0                	sub    %esi,%eax
f010024d:	83 e0 fe             	and    $0xfffffffe,%eax
f0100250:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100254:	89 74 24 04          	mov    %esi,0x4(%esp)
f0100258:	a1 80 62 11 f0       	mov    0xf0116280,%eax
f010025d:	89 04 24             	mov    %eax,(%esp)
f0100260:	e8 87 14 00 00       	call   f01016ec <memmove>
f0100265:	eb 1c                	jmp    f0100283 <_ZL16cga_savebuf_copyib+0x85>
	else
		memmove(pos, crt_buf, (trueend - pos) * sizeof(uint16_t));
f0100267:	89 d8                	mov    %ebx,%eax
f0100269:	29 f0                	sub    %esi,%eax
f010026b:	83 e0 fe             	and    $0xfffffffe,%eax
f010026e:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100272:	a1 80 62 11 f0       	mov    0xf0116280,%eax
f0100277:	89 44 24 04          	mov    %eax,0x4(%esp)
f010027b:	89 34 24             	mov    %esi,(%esp)
f010027e:	e8 69 14 00 00       	call   f01016ec <memmove>

	// If there was wraparound, copy the second part of the screen.
	if (end == trueend)
f0100283:	39 df                	cmp    %ebx,%edi
f0100285:	74 50                	je     f01002d7 <_ZL16cga_savebuf_copyib+0xd9>
		/* do nothing */;
	else if (to_screen)
f0100287:	89 e8                	mov    %ebp,%eax
f0100289:	84 c0                	test   %al,%al
f010028b:	74 26                	je     f01002b3 <_ZL16cga_savebuf_copyib+0xb5>
		memmove(crt_buf + (trueend - pos), crtsave_buf, (end - trueend) * sizeof(uint16_t));
f010028d:	29 df                	sub    %ebx,%edi
f010028f:	83 e7 fe             	and    $0xfffffffe,%edi
f0100292:	89 7c 24 08          	mov    %edi,0x8(%esp)
f0100296:	c7 44 24 04 80 12 11 	movl   $0xf0111280,0x4(%esp)
f010029d:	f0 
f010029e:	29 f3                	sub    %esi,%ebx
f01002a0:	83 e3 fe             	and    $0xfffffffe,%ebx
f01002a3:	03 1d 80 62 11 f0    	add    0xf0116280,%ebx
f01002a9:	89 1c 24             	mov    %ebx,(%esp)
f01002ac:	e8 3b 14 00 00       	call   f01016ec <memmove>
f01002b1:	eb 24                	jmp    f01002d7 <_ZL16cga_savebuf_copyib+0xd9>
	else
		memmove(crtsave_buf, crt_buf + (trueend - pos), (end - trueend) * sizeof(uint16_t));
f01002b3:	29 df                	sub    %ebx,%edi
f01002b5:	83 e7 fe             	and    $0xfffffffe,%edi
f01002b8:	89 7c 24 08          	mov    %edi,0x8(%esp)
f01002bc:	29 f3                	sub    %esi,%ebx
f01002be:	83 e3 fe             	and    $0xfffffffe,%ebx
f01002c1:	03 1d 80 62 11 f0    	add    0xf0116280,%ebx
f01002c7:	89 5c 24 04          	mov    %ebx,0x4(%esp)
f01002cb:	c7 04 24 80 12 11 f0 	movl   $0xf0111280,(%esp)
f01002d2:	e8 15 14 00 00       	call   f01016ec <memmove>
}
f01002d7:	8b 5c 24 1c          	mov    0x1c(%esp),%ebx
f01002db:	8b 74 24 20          	mov    0x20(%esp),%esi
f01002df:	8b 7c 24 24          	mov    0x24(%esp),%edi
f01002e3:	8b 6c 24 28          	mov    0x28(%esp),%ebp
f01002e7:	83 c4 2c             	add    $0x2c,%esp
f01002ea:	c3                   	ret    

f01002eb <_ZL9cons_putci>:
}

// output a character to the console
static void
cons_putc(int c)
{
f01002eb:	55                   	push   %ebp
f01002ec:	57                   	push   %edi
f01002ed:	56                   	push   %esi
f01002ee:	53                   	push   %ebx
f01002ef:	83 ec 1c             	sub    $0x1c,%esp
f01002f2:	89 c5                	mov    %eax,%ebp
f01002f4:	bb 01 32 00 00       	mov    $0x3201,%ebx
f01002f9:	be fd 03 00 00       	mov    $0x3fd,%esi
f01002fe:	89 f2                	mov    %esi,%edx
f0100300:	ec                   	in     (%dx),%al
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f0100301:	a8 20                	test   $0x20,%al
f0100303:	0f 85 2a 02 00 00    	jne    f0100533 <_ZL9cons_putci+0x248>
f0100309:	83 eb 01             	sub    $0x1,%ebx
f010030c:	0f 84 21 02 00 00    	je     f0100533 <_ZL9cons_putci+0x248>
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
	     i++)
		delay();
f0100312:	e8 89 fe ff ff       	call   f01001a0 <_ZL5delayv>
f0100317:	eb e5                	jmp    f01002fe <_ZL9cons_putci+0x13>
f0100319:	89 fa                	mov    %edi,%edx
f010031b:	ec                   	in     (%dx),%al
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f010031c:	84 c0                	test   %al,%al
f010031e:	66 90                	xchg   %ax,%ax
f0100320:	0f 88 26 02 00 00    	js     f010054c <_ZL9cons_putci+0x261>
f0100326:	83 eb 01             	sub    $0x1,%ebx
f0100329:	0f 84 1d 02 00 00    	je     f010054c <_ZL9cons_putci+0x261>
		delay();
f010032f:	e8 6c fe ff ff       	call   f01001a0 <_ZL5delayv>
f0100334:	eb e3                	jmp    f0100319 <_ZL9cons_putci+0x2e>
cga_putc(int c)
{
#if CRT_SAVEROWS > 0
	// unscroll if necessary
	if (crtsave_backscroll > 0) {
		cga_savebuf_copy(crtsave_pos + crtsave_size, 1);
f0100336:	0f b7 15 70 12 11 f0 	movzwl 0xf0111270,%edx
f010033d:	0f b7 05 6e 12 11 f0 	movzwl 0xf011126e,%eax
f0100344:	01 d0                	add    %edx,%eax
f0100346:	ba 01 00 00 00       	mov    $0x1,%edx
f010034b:	e8 ae fe ff ff       	call   f01001fe <_ZL16cga_savebuf_copyib>
		crtsave_backscroll = 0;
f0100350:	66 c7 05 6c 12 11 f0 	movw   $0x0,0xf011126c
f0100357:	00 00 
	}

#endif
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f0100359:	f7 c5 00 ff ff ff    	test   $0xffffff00,%ebp
f010035f:	75 06                	jne    f0100367 <_ZL9cons_putci+0x7c>
		c |= 0x0700;
f0100361:	81 cd 00 07 00 00    	or     $0x700,%ebp

	switch (c & 0xff) {
f0100367:	89 e8                	mov    %ebp,%eax
f0100369:	25 ff 00 00 00       	and    $0xff,%eax
f010036e:	83 f8 09             	cmp    $0x9,%eax
f0100371:	74 7b                	je     f01003ee <_ZL9cons_putci+0x103>
f0100373:	83 f8 09             	cmp    $0x9,%eax
f0100376:	7f 0b                	jg     f0100383 <_ZL9cons_putci+0x98>
f0100378:	83 f8 08             	cmp    $0x8,%eax
f010037b:	0f 85 a1 00 00 00    	jne    f0100422 <_ZL9cons_putci+0x137>
f0100381:	eb 15                	jmp    f0100398 <_ZL9cons_putci+0xad>
f0100383:	83 f8 0a             	cmp    $0xa,%eax
f0100386:	74 40                	je     f01003c8 <_ZL9cons_putci+0xdd>
f0100388:	83 f8 0d             	cmp    $0xd,%eax
f010038b:	90                   	nop
f010038c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0100390:	0f 85 8c 00 00 00    	jne    f0100422 <_ZL9cons_putci+0x137>
f0100396:	eb 38                	jmp    f01003d0 <_ZL9cons_putci+0xe5>
	case '\b':
		if (crt_pos > 0) {
f0100398:	0f b7 05 88 62 11 f0 	movzwl 0xf0116288,%eax
f010039f:	66 85 c0             	test   %ax,%ax
f01003a2:	0f 84 58 01 00 00    	je     f0100500 <_ZL9cons_putci+0x215>
			crt_pos--;
f01003a8:	83 e8 01             	sub    $0x1,%eax
f01003ab:	66 a3 88 62 11 f0    	mov    %ax,0xf0116288
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f01003b1:	0f b7 c0             	movzwl %ax,%eax
f01003b4:	66 81 e5 00 ff       	and    $0xff00,%bp
f01003b9:	83 cd 20             	or     $0x20,%ebp
f01003bc:	8b 15 80 62 11 f0    	mov    0xf0116280,%edx
f01003c2:	66 89 2c 42          	mov    %bp,(%edx,%eax,2)
f01003c6:	eb 77                	jmp    f010043f <_ZL9cons_putci+0x154>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f01003c8:	66 83 05 88 62 11 f0 	addw   $0x50,0xf0116288
f01003cf:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f01003d0:	0f b7 05 88 62 11 f0 	movzwl 0xf0116288,%eax
f01003d7:	69 c0 cd cc 00 00    	imul   $0xcccd,%eax,%eax
f01003dd:	c1 e8 16             	shr    $0x16,%eax
f01003e0:	8d 04 80             	lea    (%eax,%eax,4),%eax
f01003e3:	c1 e0 04             	shl    $0x4,%eax
f01003e6:	66 a3 88 62 11 f0    	mov    %ax,0xf0116288
f01003ec:	eb 51                	jmp    f010043f <_ZL9cons_putci+0x154>
		break;
	case '\t':
		cons_putc(' ');
f01003ee:	b8 20 00 00 00       	mov    $0x20,%eax
f01003f3:	e8 f3 fe ff ff       	call   f01002eb <_ZL9cons_putci>
		cons_putc(' ');
f01003f8:	b8 20 00 00 00       	mov    $0x20,%eax
f01003fd:	e8 e9 fe ff ff       	call   f01002eb <_ZL9cons_putci>
		cons_putc(' ');
f0100402:	b8 20 00 00 00       	mov    $0x20,%eax
f0100407:	e8 df fe ff ff       	call   f01002eb <_ZL9cons_putci>
		cons_putc(' ');
f010040c:	b8 20 00 00 00       	mov    $0x20,%eax
f0100411:	e8 d5 fe ff ff       	call   f01002eb <_ZL9cons_putci>
		cons_putc(' ');
f0100416:	b8 20 00 00 00       	mov    $0x20,%eax
f010041b:	e8 cb fe ff ff       	call   f01002eb <_ZL9cons_putci>
f0100420:	eb 1d                	jmp    f010043f <_ZL9cons_putci+0x154>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f0100422:	0f b7 05 88 62 11 f0 	movzwl 0xf0116288,%eax
f0100429:	0f b7 c8             	movzwl %ax,%ecx
f010042c:	8b 15 80 62 11 f0    	mov    0xf0116280,%edx
f0100432:	66 89 2c 4a          	mov    %bp,(%edx,%ecx,2)
f0100436:	83 c0 01             	add    $0x1,%eax
f0100439:	66 a3 88 62 11 f0    	mov    %ax,0xf0116288
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f010043f:	66 81 3d 88 62 11 f0 	cmpw   $0x7cf,0xf0116288
f0100446:	cf 07 
f0100448:	0f 86 b2 00 00 00    	jbe    f0100500 <_ZL9cons_putci+0x215>
		int i;

#if CRT_SAVEROWS > 0
		// Save the scrolled-back row
		if (crtsave_size == CRT_SAVEROWS - CRT_ROWS)
f010044e:	0f b7 05 6e 12 11 f0 	movzwl 0xf011126e,%eax
f0100455:	66 83 f8 67          	cmp    $0x67,%ax
f0100459:	75 15                	jne    f0100470 <_ZL9cons_putci+0x185>
			crtsave_pos = (crtsave_pos + 1) % CRT_SAVEROWS;
f010045b:	0f b7 05 70 12 11 f0 	movzwl 0xf0111270,%eax
f0100462:	83 c0 01             	add    $0x1,%eax
f0100465:	83 e0 7f             	and    $0x7f,%eax
f0100468:	66 a3 70 12 11 f0    	mov    %ax,0xf0111270
f010046e:	eb 09                	jmp    f0100479 <_ZL9cons_putci+0x18e>
		else
			crtsave_size++;
f0100470:	83 c0 01             	add    $0x1,%eax
f0100473:	66 a3 6e 12 11 f0    	mov    %ax,0xf011126e
		memmove(crtsave_buf + ((crtsave_pos + crtsave_size - 1) % CRT_SAVEROWS) * CRT_COLS, crt_buf, CRT_COLS * sizeof(uint16_t));
f0100479:	c7 44 24 08 a0 00 00 	movl   $0xa0,0x8(%esp)
f0100480:	00 
f0100481:	a1 80 62 11 f0       	mov    0xf0116280,%eax
f0100486:	89 44 24 04          	mov    %eax,0x4(%esp)
f010048a:	0f b7 15 70 12 11 f0 	movzwl 0xf0111270,%edx
f0100491:	0f b7 05 6e 12 11 f0 	movzwl 0xf011126e,%eax
f0100498:	8d 44 02 ff          	lea    -0x1(%edx,%eax,1),%eax
f010049c:	89 c2                	mov    %eax,%edx
f010049e:	c1 fa 1f             	sar    $0x1f,%edx
f01004a1:	c1 ea 19             	shr    $0x19,%edx
f01004a4:	01 d0                	add    %edx,%eax
f01004a6:	83 e0 7f             	and    $0x7f,%eax
f01004a9:	29 d0                	sub    %edx,%eax
f01004ab:	8d 04 80             	lea    (%eax,%eax,4),%eax
f01004ae:	c1 e0 05             	shl    $0x5,%eax
f01004b1:	05 80 12 11 f0       	add    $0xf0111280,%eax
f01004b6:	89 04 24             	mov    %eax,(%esp)
f01004b9:	e8 2e 12 00 00       	call   f01016ec <memmove>

#endif
		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f01004be:	a1 80 62 11 f0       	mov    0xf0116280,%eax
f01004c3:	c7 44 24 08 00 0f 00 	movl   $0xf00,0x8(%esp)
f01004ca:	00 
f01004cb:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f01004d1:	89 54 24 04          	mov    %edx,0x4(%esp)
f01004d5:	89 04 24             	mov    %eax,(%esp)
f01004d8:	e8 0f 12 00 00       	call   f01016ec <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f01004dd:	8b 15 80 62 11 f0    	mov    0xf0116280,%edx
			crtsave_size++;
		memmove(crtsave_buf + ((crtsave_pos + crtsave_size - 1) % CRT_SAVEROWS) * CRT_COLS, crt_buf, CRT_COLS * sizeof(uint16_t));

#endif
		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f01004e3:	b8 80 07 00 00       	mov    $0x780,%eax
			crt_buf[i] = 0x0700 | ' ';
f01004e8:	66 c7 04 42 20 07    	movw   $0x720,(%edx,%eax,2)
			crtsave_size++;
		memmove(crtsave_buf + ((crtsave_pos + crtsave_size - 1) % CRT_SAVEROWS) * CRT_COLS, crt_buf, CRT_COLS * sizeof(uint16_t));

#endif
		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f01004ee:	83 c0 01             	add    $0x1,%eax
f01004f1:	3d d0 07 00 00       	cmp    $0x7d0,%eax
f01004f6:	75 f0                	jne    f01004e8 <_ZL9cons_putci+0x1fd>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f01004f8:	66 83 2d 88 62 11 f0 	subw   $0x50,0xf0116288
f01004ff:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f0100500:	8b 0d 84 62 11 f0    	mov    0xf0116284,%ecx
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100506:	b8 0e 00 00 00       	mov    $0xe,%eax
f010050b:	89 ca                	mov    %ecx,%edx
f010050d:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f010050e:	0f b7 1d 88 62 11 f0 	movzwl 0xf0116288,%ebx
f0100515:	8d 71 01             	lea    0x1(%ecx),%esi
f0100518:	0f b6 c7             	movzbl %bh,%eax
f010051b:	89 f2                	mov    %esi,%edx
f010051d:	ee                   	out    %al,(%dx)
f010051e:	b8 0f 00 00 00       	mov    $0xf,%eax
f0100523:	89 ca                	mov    %ecx,%edx
f0100525:	ee                   	out    %al,(%dx)
f0100526:	89 d8                	mov    %ebx,%eax
f0100528:	89 f2                	mov    %esi,%edx
f010052a:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f010052b:	83 c4 1c             	add    $0x1c,%esp
f010052e:	5b                   	pop    %ebx
f010052f:	5e                   	pop    %esi
f0100530:	5f                   	pop    %edi
f0100531:	5d                   	pop    %ebp
f0100532:	c3                   	ret    
	for (i = 0;
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
	     i++)
		delay();

	outb(COM1 + COM_TX, c);
f0100533:	89 ee                	mov    %ebp,%esi
f0100535:	ba f8 03 00 00       	mov    $0x3f8,%edx
f010053a:	89 e8                	mov    %ebp,%eax
f010053c:	ee                   	out    %al,(%dx)
f010053d:	bb 01 32 00 00       	mov    $0x3201,%ebx

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100542:	bf 79 03 00 00       	mov    $0x379,%edi
f0100547:	e9 cd fd ff ff       	jmp    f0100319 <_ZL9cons_putci+0x2e>
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010054c:	ba 78 03 00 00       	mov    $0x378,%edx
f0100551:	89 f0                	mov    %esi,%eax
f0100553:	ee                   	out    %al,(%dx)
f0100554:	b2 7a                	mov    $0x7a,%dl
f0100556:	b8 0d 00 00 00       	mov    $0xd,%eax
f010055b:	ee                   	out    %al,(%dx)
f010055c:	b8 08 00 00 00       	mov    $0x8,%eax
f0100561:	ee                   	out    %al,(%dx)
static void
cga_putc(int c)
{
#if CRT_SAVEROWS > 0
	// unscroll if necessary
	if (crtsave_backscroll > 0) {
f0100562:	66 83 3d 6c 12 11 f0 	cmpw   $0x0,0xf011126c
f0100569:	00 
f010056a:	0f 8f c6 fd ff ff    	jg     f0100336 <_ZL9cons_putci+0x4b>
f0100570:	e9 e4 fd ff ff       	jmp    f0100359 <_ZL9cons_putci+0x6e>

f0100575 <_ZL13kbd_proc_datav>:
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f0100575:	83 ec 1c             	sub    $0x1c,%esp
f0100578:	89 5c 24 10          	mov    %ebx,0x10(%esp)
f010057c:	89 74 24 14          	mov    %esi,0x14(%esp)
f0100580:	89 7c 24 18          	mov    %edi,0x18(%esp)

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100584:	ba 64 00 00 00       	mov    $0x64,%edx
f0100589:	ec                   	in     (%dx),%al
	int c;
	uint8_t data;
	static uint32_t shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
		return -1;
f010058a:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
{
	int c;
	uint8_t data;
	static uint32_t shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
f010058f:	a8 01                	test   $0x1,%al
f0100591:	0f 84 7a 01 00 00    	je     f0100711 <_ZL13kbd_proc_datav+0x19c>
f0100597:	b2 60                	mov    $0x60,%dl
f0100599:	ec                   	in     (%dx),%al
f010059a:	89 c2                	mov    %eax,%edx
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f010059c:	3c e0                	cmp    $0xe0,%al
f010059e:	75 11                	jne    f01005b1 <_ZL13kbd_proc_datav+0x3c>
		// E0 escape character
		shift |= E0ESC;
f01005a0:	83 0d 68 12 11 f0 40 	orl    $0x40,0xf0111268
		return 0;
f01005a7:	bb 00 00 00 00       	mov    $0x0,%ebx
f01005ac:	e9 60 01 00 00       	jmp    f0100711 <_ZL13kbd_proc_datav+0x19c>
	} else if (data & 0x80) {
f01005b1:	84 c0                	test   %al,%al
f01005b3:	79 34                	jns    f01005e9 <_ZL13kbd_proc_datav+0x74>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f01005b5:	8b 0d 68 12 11 f0    	mov    0xf0111268,%ecx
f01005bb:	f6 c1 40             	test   $0x40,%cl
f01005be:	75 05                	jne    f01005c5 <_ZL13kbd_proc_datav+0x50>
f01005c0:	89 c2                	mov    %eax,%edx
f01005c2:	83 e2 7f             	and    $0x7f,%edx
		shift &= ~(shiftcode[data] | E0ESC);
f01005c5:	0f b6 d2             	movzbl %dl,%edx
f01005c8:	0f b6 82 80 1c 10 f0 	movzbl -0xfefe380(%edx),%eax
f01005cf:	83 c8 40             	or     $0x40,%eax
f01005d2:	0f b6 c0             	movzbl %al,%eax
f01005d5:	f7 d0                	not    %eax
f01005d7:	21 c1                	and    %eax,%ecx
f01005d9:	89 0d 68 12 11 f0    	mov    %ecx,0xf0111268
		return 0;
f01005df:	bb 00 00 00 00       	mov    $0x0,%ebx
f01005e4:	e9 28 01 00 00       	jmp    f0100711 <_ZL13kbd_proc_datav+0x19c>
	} else if (shift & E0ESC) {
f01005e9:	8b 0d 68 12 11 f0    	mov    0xf0111268,%ecx
f01005ef:	f6 c1 40             	test   $0x40,%cl
f01005f2:	74 0e                	je     f0100602 <_ZL13kbd_proc_datav+0x8d>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f01005f4:	89 c2                	mov    %eax,%edx
f01005f6:	83 ca 80             	or     $0xffffff80,%edx
		shift &= ~E0ESC;
f01005f9:	83 e1 bf             	and    $0xffffffbf,%ecx
f01005fc:	89 0d 68 12 11 f0    	mov    %ecx,0xf0111268
	}

	shift |= shiftcode[data];
f0100602:	0f b6 d2             	movzbl %dl,%edx
f0100605:	0f b6 82 80 1c 10 f0 	movzbl -0xfefe380(%edx),%eax
f010060c:	0b 05 68 12 11 f0    	or     0xf0111268,%eax
	shift ^= togglecode[data];
f0100612:	0f b6 8a 80 1d 10 f0 	movzbl -0xfefe280(%edx),%ecx
f0100619:	31 c8                	xor    %ecx,%eax
f010061b:	a3 68 12 11 f0       	mov    %eax,0xf0111268

	c = charcode[shift & (CTL | SHIFT)][data];
f0100620:	89 c1                	mov    %eax,%ecx
f0100622:	83 e1 03             	and    $0x3,%ecx
f0100625:	8b 1c 8d 80 1e 10 f0 	mov    -0xfefe180(,%ecx,4),%ebx
f010062c:	0f b6 1c 13          	movzbl (%ebx,%edx,1),%ebx
	if (shift & CAPSLOCK) {
f0100630:	a8 08                	test   $0x8,%al
f0100632:	74 18                	je     f010064c <_ZL13kbd_proc_datav+0xd7>
		if ('a' <= c && c <= 'z')
f0100634:	8d 53 9f             	lea    -0x61(%ebx),%edx
f0100637:	83 fa 19             	cmp    $0x19,%edx
f010063a:	77 05                	ja     f0100641 <_ZL13kbd_proc_datav+0xcc>
			c += 'A' - 'a';
f010063c:	83 eb 20             	sub    $0x20,%ebx
f010063f:	eb 0b                	jmp    f010064c <_ZL13kbd_proc_datav+0xd7>
		else if ('A' <= c && c <= 'Z')
f0100641:	8d 53 bf             	lea    -0x41(%ebx),%edx
f0100644:	83 fa 19             	cmp    $0x19,%edx
f0100647:	77 03                	ja     f010064c <_ZL13kbd_proc_datav+0xd7>
			c += 'a' - 'A';
f0100649:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
#if CRT_SAVEROWS > 0
	// Shift-PageUp and Shift-PageDown: scroll console
	if ((shift & (CTL | SHIFT)) && (c == KEY_PGUP || c == KEY_PGDN)) {
f010064c:	85 c9                	test   %ecx,%ecx
f010064e:	0f 84 98 00 00 00    	je     f01006ec <_ZL13kbd_proc_datav+0x177>
f0100654:	8d 93 1a ff ff ff    	lea    -0xe6(%ebx),%edx
f010065a:	83 fa 01             	cmp    $0x1,%edx
f010065d:	0f 87 89 00 00 00    	ja     f01006ec <_ZL13kbd_proc_datav+0x177>
		cga_scroll(c == KEY_PGUP ? -CRT_ROWS : CRT_ROWS);
f0100663:	81 fb e6 00 00 00    	cmp    $0xe6,%ebx
f0100669:	0f 95 c2             	setne  %dl
f010066c:	0f b6 d2             	movzbl %dl,%edx
f010066f:	83 ea 01             	sub    $0x1,%edx
f0100672:	83 e2 ce             	and    $0xffffffce,%edx
f0100675:	83 c2 19             	add    $0x19,%edx

#if CRT_SAVEROWS > 0
static void
cga_scroll(int delta)
{
	int new_backscroll = MAX(MIN(crtsave_backscroll - delta, crtsave_size), 0);
f0100678:	0f b7 05 6c 12 11 f0 	movzwl 0xf011126c,%eax
f010067f:	0f bf c8             	movswl %ax,%ecx
f0100682:	0f b7 3d 6e 12 11 f0 	movzwl 0xf011126e,%edi
f0100689:	89 cb                	mov    %ecx,%ebx
f010068b:	29 d3                	sub    %edx,%ebx
f010068d:	89 de                	mov    %ebx,%esi
f010068f:	39 fb                	cmp    %edi,%ebx
f0100691:	7e 02                	jle    f0100695 <_ZL13kbd_proc_datav+0x120>
f0100693:	89 fe                	mov    %edi,%esi
f0100695:	89 f2                	mov    %esi,%edx
f0100697:	c1 fa 1f             	sar    $0x1f,%edx
f010069a:	f7 d2                	not    %edx
f010069c:	21 d6                	and    %edx,%esi
	// Process special keys
#if CRT_SAVEROWS > 0
	// Shift-PageUp and Shift-PageDown: scroll console
	if ((shift & (CTL | SHIFT)) && (c == KEY_PGUP || c == KEY_PGDN)) {
		cga_scroll(c == KEY_PGUP ? -CRT_ROWS : CRT_ROWS);
		return 0;
f010069e:	bb 00 00 00 00       	mov    $0x0,%ebx
static void
cga_scroll(int delta)
{
	int new_backscroll = MAX(MIN(crtsave_backscroll - delta, crtsave_size), 0);

	if (new_backscroll == crtsave_backscroll)
f01006a3:	39 ce                	cmp    %ecx,%esi
f01006a5:	74 6a                	je     f0100711 <_ZL13kbd_proc_datav+0x19c>
		return;
	if (crtsave_backscroll == 0)
f01006a7:	66 85 c0             	test   %ax,%ax
f01006aa:	75 13                	jne    f01006bf <_ZL13kbd_proc_datav+0x14a>
		// save current screen
		cga_savebuf_copy(crtsave_pos + crtsave_size, 0);
f01006ac:	0f b7 05 70 12 11 f0 	movzwl 0xf0111270,%eax
f01006b3:	01 f8                	add    %edi,%eax
f01006b5:	ba 00 00 00 00       	mov    $0x0,%edx
f01006ba:	e8 3f fb ff ff       	call   f01001fe <_ZL16cga_savebuf_copyib>

	crtsave_backscroll = new_backscroll;
f01006bf:	66 89 35 6c 12 11 f0 	mov    %si,0xf011126c
	cga_savebuf_copy(crtsave_pos + crtsave_size - crtsave_backscroll, 1);
f01006c6:	0f b7 15 70 12 11 f0 	movzwl 0xf0111270,%edx
f01006cd:	0f b7 05 6e 12 11 f0 	movzwl 0xf011126e,%eax
f01006d4:	01 d0                	add    %edx,%eax
f01006d6:	0f bf f6             	movswl %si,%esi
f01006d9:	29 f0                	sub    %esi,%eax
f01006db:	ba 01 00 00 00       	mov    $0x1,%edx
f01006e0:	e8 19 fb ff ff       	call   f01001fe <_ZL16cga_savebuf_copyib>
	// Process special keys
#if CRT_SAVEROWS > 0
	// Shift-PageUp and Shift-PageDown: scroll console
	if ((shift & (CTL | SHIFT)) && (c == KEY_PGUP || c == KEY_PGDN)) {
		cga_scroll(c == KEY_PGUP ? -CRT_ROWS : CRT_ROWS);
		return 0;
f01006e5:	bb 00 00 00 00       	mov    $0x0,%ebx
f01006ea:	eb 25                	jmp    f0100711 <_ZL13kbd_proc_datav+0x19c>
	}
#endif
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f01006ec:	f7 d0                	not    %eax
f01006ee:	a8 06                	test   $0x6,%al
f01006f0:	75 1f                	jne    f0100711 <_ZL13kbd_proc_datav+0x19c>
f01006f2:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f01006f8:	75 17                	jne    f0100711 <_ZL13kbd_proc_datav+0x19c>
		cprintf("Rebooting!\n");
f01006fa:	c7 04 24 44 1c 10 f0 	movl   $0xf0101c44,(%esp)
f0100701:	e8 40 04 00 00       	call   f0100b46 <_Z7cprintfPKcz>
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100706:	ba 92 00 00 00       	mov    $0x92,%edx
f010070b:	b8 03 00 00 00       	mov    $0x3,%eax
f0100710:	ee                   	out    %al,(%dx)
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f0100711:	89 d8                	mov    %ebx,%eax
f0100713:	8b 5c 24 10          	mov    0x10(%esp),%ebx
f0100717:	8b 74 24 14          	mov    0x14(%esp),%esi
f010071b:	8b 7c 24 18          	mov    0x18(%esp),%edi
f010071f:	83 c4 1c             	add    $0x1c,%esp
f0100722:	c3                   	ret    

f0100723 <_Z11serial_intrv>:
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f0100723:	83 ec 0c             	sub    $0xc,%esp
	if (serial_exists)
f0100726:	80 3d 40 10 11 f0 00 	cmpb   $0x0,0xf0111040
f010072d:	74 0a                	je     f0100739 <_Z11serial_intrv+0x16>
		cons_intr(serial_proc_data);
f010072f:	b8 aa 01 10 f0       	mov    $0xf01001aa,%eax
f0100734:	e8 89 fa ff ff       	call   f01001c2 <_ZL9cons_intrPFivE>
}
f0100739:	83 c4 0c             	add    $0xc,%esp
f010073c:	c3                   	ret    

f010073d <_Z8kbd_intrv>:
	return c;
}

void
kbd_intr(void)
{
f010073d:	83 ec 0c             	sub    $0xc,%esp
	cons_intr(kbd_proc_data);
f0100740:	b8 75 05 10 f0       	mov    $0xf0100575,%eax
f0100745:	e8 78 fa ff ff       	call   f01001c2 <_ZL9cons_intrPFivE>
}
f010074a:	83 c4 0c             	add    $0xc,%esp
f010074d:	c3                   	ret    

f010074e <_Z9cons_getcv>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f010074e:	83 ec 0c             	sub    $0xc,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f0100751:	e8 cd ff ff ff       	call   f0100723 <_Z11serial_intrv>
	kbd_intr();
f0100756:	e8 e2 ff ff ff       	call   f010073d <_Z8kbd_intrv>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f010075b:	8b 15 60 12 11 f0    	mov    0xf0111260,%edx
		c = cons.buf[cons.rpos++];
		if (cons.rpos == CONSBUFSIZE)
			cons.rpos = 0;
		return c;
	}
	return 0;
f0100761:	b8 00 00 00 00       	mov    $0x0,%eax
	// (e.g., when called from the kernel monitor).
	serial_intr();
	kbd_intr();

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f0100766:	3b 15 64 12 11 f0    	cmp    0xf0111264,%edx
f010076c:	74 21                	je     f010078f <_Z9cons_getcv+0x41>
		c = cons.buf[cons.rpos++];
f010076e:	0f b6 82 60 10 11 f0 	movzbl -0xfeeefa0(%edx),%eax
f0100775:	83 c2 01             	add    $0x1,%edx
		if (cons.rpos == CONSBUFSIZE)
			cons.rpos = 0;
f0100778:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f010077e:	0f 94 c1             	sete   %cl
f0100781:	0f b6 c9             	movzbl %cl,%ecx
f0100784:	83 e9 01             	sub    $0x1,%ecx
f0100787:	21 ca                	and    %ecx,%edx
f0100789:	89 15 60 12 11 f0    	mov    %edx,0xf0111260
		return c;
	}
	return 0;
}
f010078f:	83 c4 0c             	add    $0xc,%esp
f0100792:	c3                   	ret    

f0100793 <_Z9cons_initv>:
}

// initialize the console devices
void
cons_init(void)
{
f0100793:	57                   	push   %edi
f0100794:	56                   	push   %esi
f0100795:	53                   	push   %ebx
f0100796:	83 ec 10             	sub    $0x10,%esp
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f0100799:	0f b7 15 00 80 0b f0 	movzwl 0xf00b8000,%edx
	*cp = (uint16_t) 0xA55A;
f01007a0:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f01007a7:	5a a5 
	if (*cp != 0xA55A) {
f01007a9:	0f b7 05 00 80 0b f0 	movzwl 0xf00b8000,%eax
f01007b0:	66 3d 5a a5          	cmp    $0xa55a,%ax
f01007b4:	74 11                	je     f01007c7 <_Z9cons_initv+0x34>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f01007b6:	c7 05 84 62 11 f0 b4 	movl   $0x3b4,0xf0116284
f01007bd:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f01007c0:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f01007c5:	eb 16                	jmp    f01007dd <_Z9cons_initv+0x4a>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f01007c7:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f01007ce:	c7 05 84 62 11 f0 d4 	movl   $0x3d4,0xf0116284
f01007d5:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f01007d8:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
		*cp = was;
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
f01007dd:	8b 0d 84 62 11 f0    	mov    0xf0116284,%ecx
f01007e3:	b8 0e 00 00 00       	mov    $0xe,%eax
f01007e8:	89 ca                	mov    %ecx,%edx
f01007ea:	ee                   	out    %al,(%dx)
	pos = inb(addr_6845 + 1) << 8;
f01007eb:	8d 59 01             	lea    0x1(%ecx),%ebx

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01007ee:	89 da                	mov    %ebx,%edx
f01007f0:	ec                   	in     (%dx),%al
f01007f1:	0f b6 f8             	movzbl %al,%edi
f01007f4:	c1 e7 08             	shl    $0x8,%edi
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01007f7:	b8 0f 00 00 00       	mov    $0xf,%eax
f01007fc:	89 ca                	mov    %ecx,%edx
f01007fe:	ee                   	out    %al,(%dx)

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01007ff:	89 da                	mov    %ebx,%edx
f0100801:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f0100802:	89 35 80 62 11 f0    	mov    %esi,0xf0116280

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);
f0100808:	0f b6 d8             	movzbl %al,%ebx
f010080b:	09 df                	or     %ebx,%edi

	crt_buf = (uint16_t*) cp;
	crt_pos = pos;
f010080d:	66 89 3d 88 62 11 f0 	mov    %di,0xf0116288
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100814:	be fa 03 00 00       	mov    $0x3fa,%esi
f0100819:	b8 00 00 00 00       	mov    $0x0,%eax
f010081e:	89 f2                	mov    %esi,%edx
f0100820:	ee                   	out    %al,(%dx)
f0100821:	b2 fb                	mov    $0xfb,%dl
f0100823:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
f0100828:	ee                   	out    %al,(%dx)
f0100829:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f010082e:	b8 0c 00 00 00       	mov    $0xc,%eax
f0100833:	89 da                	mov    %ebx,%edx
f0100835:	ee                   	out    %al,(%dx)
f0100836:	b2 f9                	mov    $0xf9,%dl
f0100838:	b8 00 00 00 00       	mov    $0x0,%eax
f010083d:	ee                   	out    %al,(%dx)
f010083e:	b2 fb                	mov    $0xfb,%dl
f0100840:	b8 03 00 00 00       	mov    $0x3,%eax
f0100845:	ee                   	out    %al,(%dx)
f0100846:	b2 fc                	mov    $0xfc,%dl
f0100848:	b8 00 00 00 00       	mov    $0x0,%eax
f010084d:	ee                   	out    %al,(%dx)
f010084e:	b2 f9                	mov    $0xf9,%dl
f0100850:	b8 01 00 00 00       	mov    $0x1,%eax
f0100855:	ee                   	out    %al,(%dx)

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100856:	b2 fd                	mov    $0xfd,%dl
f0100858:	ec                   	in     (%dx),%al
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f0100859:	3c ff                	cmp    $0xff,%al
f010085b:	0f 95 c1             	setne  %cl
f010085e:	88 0d 40 10 11 f0    	mov    %cl,0xf0111040
f0100864:	89 f2                	mov    %esi,%edx
f0100866:	ec                   	in     (%dx),%al
f0100867:	89 da                	mov    %ebx,%edx
f0100869:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f010086a:	84 c9                	test   %cl,%cl
f010086c:	75 0c                	jne    f010087a <_Z9cons_initv+0xe7>
		cprintf("Serial port does not exist!\n");
f010086e:	c7 04 24 50 1c 10 f0 	movl   $0xf0101c50,(%esp)
f0100875:	e8 cc 02 00 00       	call   f0100b46 <_Z7cprintfPKcz>
}
f010087a:	83 c4 10             	add    $0x10,%esp
f010087d:	5b                   	pop    %ebx
f010087e:	5e                   	pop    %esi
f010087f:	5f                   	pop    %edi
f0100880:	c3                   	ret    

f0100881 <_Z8cputchari>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f0100881:	83 ec 0c             	sub    $0xc,%esp
	cons_putc(c);
f0100884:	8b 44 24 10          	mov    0x10(%esp),%eax
f0100888:	e8 5e fa ff ff       	call   f01002eb <_ZL9cons_putci>
}
f010088d:	83 c4 0c             	add    $0xc,%esp
f0100890:	c3                   	ret    

f0100891 <_Z7getcharv>:

int
getchar(void)
{
f0100891:	83 ec 0c             	sub    $0xc,%esp
	int c;

	while ((c = cons_getc()) == 0)
f0100894:	e8 b5 fe ff ff       	call   f010074e <_Z9cons_getcv>
f0100899:	85 c0                	test   %eax,%eax
f010089b:	74 f7                	je     f0100894 <_Z7getcharv+0x3>
		/* do nothing */;
	return c;
}
f010089d:	83 c4 0c             	add    $0xc,%esp
f01008a0:	c3                   	ret    

f01008a1 <_Z6isconsi>:
int
iscons(int fdnum)
{
	// used by readline
	return 1;
}
f01008a1:	b8 01 00 00 00       	mov    $0x1,%eax
f01008a6:	c3                   	ret    
	...

f01008b0 <_Z12mon_kerninfoiPPcP9Trapframe>:
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f01008b0:	83 ec 1c             	sub    $0x1c,%esp
	extern char _start[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f01008b3:	c7 04 24 a0 21 10 f0 	movl   $0xf01021a0,(%esp)
f01008ba:	e8 87 02 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	cprintf("  _start %08x (virt)  %08x (phys)\n", _start, _start - KERNBASE);
f01008bf:	c7 44 24 08 0c 00 10 	movl   $0x10000c,0x8(%esp)
f01008c6:	00 
f01008c7:	c7 44 24 04 0c 00 10 	movl   $0xf010000c,0x4(%esp)
f01008ce:	f0 
f01008cf:	c7 04 24 2c 22 10 f0 	movl   $0xf010222c,(%esp)
f01008d6:	e8 6b 02 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f01008db:	c7 44 24 08 a5 1b 10 	movl   $0x101ba5,0x8(%esp)
f01008e2:	00 
f01008e3:	c7 44 24 04 a5 1b 10 	movl   $0xf0101ba5,0x4(%esp)
f01008ea:	f0 
f01008eb:	c7 04 24 50 22 10 f0 	movl   $0xf0102250,(%esp)
f01008f2:	e8 4f 02 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f01008f7:	c7 44 24 08 20 10 11 	movl   $0x111020,0x8(%esp)
f01008fe:	00 
f01008ff:	c7 44 24 04 20 10 11 	movl   $0xf0111020,0x4(%esp)
f0100906:	f0 
f0100907:	c7 04 24 74 22 10 f0 	movl   $0xf0102274,(%esp)
f010090e:	e8 33 02 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f0100913:	c7 44 24 08 a0 66 11 	movl   $0x1166a0,0x8(%esp)
f010091a:	00 
f010091b:	c7 44 24 04 a0 66 11 	movl   $0xf01166a0,0x4(%esp)
f0100922:	f0 
f0100923:	c7 04 24 98 22 10 f0 	movl   $0xf0102298,(%esp)
f010092a:	e8 17 02 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	cprintf("Kernel executable memory footprint: %dKB\n",
		(end-_start+1023)/1024);
f010092f:	b8 9f 6a 11 f0       	mov    $0xf0116a9f,%eax
f0100934:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
f0100939:	89 c2                	mov    %eax,%edx
f010093b:	c1 fa 1f             	sar    $0x1f,%edx
f010093e:	c1 ea 16             	shr    $0x16,%edx
f0100941:	01 d0                	add    %edx,%eax
f0100943:	c1 f8 0a             	sar    $0xa,%eax
f0100946:	89 44 24 04          	mov    %eax,0x4(%esp)
f010094a:	c7 04 24 bc 22 10 f0 	movl   $0xf01022bc,(%esp)
f0100951:	e8 f0 01 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	return 0;
}
f0100956:	b8 00 00 00 00       	mov    $0x0,%eax
f010095b:	83 c4 1c             	add    $0x1c,%esp
f010095e:	c3                   	ret    

f010095f <_Z8mon_helpiPPcP9Trapframe>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f010095f:	83 ec 1c             	sub    $0x1c,%esp
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f0100962:	c7 44 24 08 b9 21 10 	movl   $0xf01021b9,0x8(%esp)
f0100969:	f0 
f010096a:	c7 44 24 04 d7 21 10 	movl   $0xf01021d7,0x4(%esp)
f0100971:	f0 
f0100972:	c7 04 24 dc 21 10 f0 	movl   $0xf01021dc,(%esp)
f0100979:	e8 c8 01 00 00       	call   f0100b46 <_Z7cprintfPKcz>
f010097e:	c7 44 24 08 e8 22 10 	movl   $0xf01022e8,0x8(%esp)
f0100985:	f0 
f0100986:	c7 44 24 04 e5 21 10 	movl   $0xf01021e5,0x4(%esp)
f010098d:	f0 
f010098e:	c7 04 24 dc 21 10 f0 	movl   $0xf01021dc,(%esp)
f0100995:	e8 ac 01 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	return 0;
}
f010099a:	b8 00 00 00 00       	mov    $0x0,%eax
f010099f:	83 c4 1c             	add    $0x1c,%esp
f01009a2:	c3                   	ret    

f01009a3 <_Z13mon_backtraceiPPcP9Trapframe>:
int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	// Your code here.
	return 0;
}
f01009a3:	b8 00 00 00 00       	mov    $0x0,%eax
f01009a8:	c3                   	ret    

f01009a9 <_Z7monitorP9Trapframe>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f01009a9:	57                   	push   %edi
f01009aa:	56                   	push   %esi
f01009ab:	53                   	push   %ebx
f01009ac:	83 ec 50             	sub    $0x50,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f01009af:	c7 04 24 10 23 10 f0 	movl   $0xf0102310,(%esp)
f01009b6:	e8 8b 01 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	cprintf("Type 'help' for a list of commands.\n");
f01009bb:	c7 04 24 34 23 10 f0 	movl   $0xf0102334,(%esp)
f01009c2:	e8 7f 01 00 00       	call   f0100b46 <_Z7cprintfPKcz>
	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
f01009c7:	8d 7c 24 10          	lea    0x10(%esp),%edi
	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");


	while (1) {
		buf = readline("K> ");
f01009cb:	c7 04 24 ee 21 10 f0 	movl   $0xf01021ee,(%esp)
f01009d2:	e8 69 0a 00 00       	call   f0101440 <_Z8readlinePKc>
f01009d7:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f01009d9:	85 c0                	test   %eax,%eax
f01009db:	74 ee                	je     f01009cb <_Z7monitorP9Trapframe+0x22>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f01009dd:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
f01009e4:	00 
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f01009e5:	be 00 00 00 00       	mov    $0x0,%esi
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f01009ea:	0f b6 03             	movzbl (%ebx),%eax
f01009ed:	84 c0                	test   %al,%al
f01009ef:	74 65                	je     f0100a56 <_Z7monitorP9Trapframe+0xad>
f01009f1:	0f be c0             	movsbl %al,%eax
f01009f4:	89 44 24 04          	mov    %eax,0x4(%esp)
f01009f8:	c7 04 24 f2 21 10 f0 	movl   $0xf01021f2,(%esp)
f01009ff:	e8 35 0c 00 00       	call   f0101639 <_Z6strchrPKcc>
f0100a04:	85 c0                	test   %eax,%eax
f0100a06:	0f 85 d1 00 00 00    	jne    f0100add <_Z7monitorP9Trapframe+0x134>
			*buf++ = 0;
		if (*buf == 0)
f0100a0c:	80 3b 00             	cmpb   $0x0,(%ebx)
f0100a0f:	74 45                	je     f0100a56 <_Z7monitorP9Trapframe+0xad>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f0100a11:	83 fe 0f             	cmp    $0xf,%esi
f0100a14:	75 16                	jne    f0100a2c <_Z7monitorP9Trapframe+0x83>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f0100a16:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
f0100a1d:	00 
f0100a1e:	c7 04 24 f7 21 10 f0 	movl   $0xf01021f7,(%esp)
f0100a25:	e8 1c 01 00 00       	call   f0100b46 <_Z7cprintfPKcz>
f0100a2a:	eb 9f                	jmp    f01009cb <_Z7monitorP9Trapframe+0x22>
			return 0;
		}
		argv[argc++] = buf;
f0100a2c:	89 5c b4 10          	mov    %ebx,0x10(%esp,%esi,4)
f0100a30:	83 c6 01             	add    $0x1,%esi
		while (*buf && !strchr(WHITESPACE, *buf))
f0100a33:	0f b6 03             	movzbl (%ebx),%eax
f0100a36:	84 c0                	test   %al,%al
f0100a38:	74 b0                	je     f01009ea <_Z7monitorP9Trapframe+0x41>
f0100a3a:	0f be c0             	movsbl %al,%eax
f0100a3d:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100a41:	c7 04 24 f2 21 10 f0 	movl   $0xf01021f2,(%esp)
f0100a48:	e8 ec 0b 00 00       	call   f0101639 <_Z6strchrPKcc>
f0100a4d:	85 c0                	test   %eax,%eax
f0100a4f:	75 99                	jne    f01009ea <_Z7monitorP9Trapframe+0x41>
f0100a51:	e9 92 00 00 00       	jmp    f0100ae8 <_Z7monitorP9Trapframe+0x13f>
			buf++;
	}
	argv[argc] = 0;
f0100a56:	c7 44 b4 10 00 00 00 	movl   $0x0,0x10(%esp,%esi,4)
f0100a5d:	00 

	// Lookup and invoke the command
	if (argc == 0)
f0100a5e:	85 f6                	test   %esi,%esi
f0100a60:	0f 84 65 ff ff ff    	je     f01009cb <_Z7monitorP9Trapframe+0x22>
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f0100a66:	c7 44 24 04 d7 21 10 	movl   $0xf01021d7,0x4(%esp)
f0100a6d:	f0 
f0100a6e:	8b 44 24 10          	mov    0x10(%esp),%eax
f0100a72:	89 04 24             	mov    %eax,(%esp)
f0100a75:	e8 63 0b 00 00       	call   f01015dd <_Z6strcmpPKcS0_>
f0100a7a:	ba 00 00 00 00       	mov    $0x0,%edx
f0100a7f:	85 c0                	test   %eax,%eax
f0100a81:	74 1d                	je     f0100aa0 <_Z7monitorP9Trapframe+0xf7>
f0100a83:	c7 44 24 04 e5 21 10 	movl   $0xf01021e5,0x4(%esp)
f0100a8a:	f0 
f0100a8b:	8b 44 24 10          	mov    0x10(%esp),%eax
f0100a8f:	89 04 24             	mov    %eax,(%esp)
f0100a92:	e8 46 0b 00 00       	call   f01015dd <_Z6strcmpPKcS0_>
f0100a97:	85 c0                	test   %eax,%eax
f0100a99:	75 29                	jne    f0100ac4 <_Z7monitorP9Trapframe+0x11b>
f0100a9b:	ba 01 00 00 00       	mov    $0x1,%edx
			return commands[i].func(argc, argv, tf);
f0100aa0:	8d 04 12             	lea    (%edx,%edx,1),%eax
f0100aa3:	01 c2                	add    %eax,%edx
f0100aa5:	8b 44 24 60          	mov    0x60(%esp),%eax
f0100aa9:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100aad:	89 7c 24 04          	mov    %edi,0x4(%esp)
f0100ab1:	89 34 24             	mov    %esi,(%esp)
f0100ab4:	ff 14 95 64 23 10 f0 	call   *-0xfefdc9c(,%edx,4)


	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f0100abb:	85 c0                	test   %eax,%eax
f0100abd:	78 31                	js     f0100af0 <_Z7monitorP9Trapframe+0x147>
f0100abf:	e9 07 ff ff ff       	jmp    f01009cb <_Z7monitorP9Trapframe+0x22>
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f0100ac4:	8b 44 24 10          	mov    0x10(%esp),%eax
f0100ac8:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100acc:	c7 04 24 14 22 10 f0 	movl   $0xf0102214,(%esp)
f0100ad3:	e8 6e 00 00 00       	call   f0100b46 <_Z7cprintfPKcz>
f0100ad8:	e9 ee fe ff ff       	jmp    f01009cb <_Z7monitorP9Trapframe+0x22>
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f0100add:	c6 03 00             	movb   $0x0,(%ebx)
f0100ae0:	83 c3 01             	add    $0x1,%ebx
f0100ae3:	e9 02 ff ff ff       	jmp    f01009ea <_Z7monitorP9Trapframe+0x41>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f0100ae8:	83 c3 01             	add    $0x1,%ebx
f0100aeb:	e9 43 ff ff ff       	jmp    f0100a33 <_Z7monitorP9Trapframe+0x8a>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f0100af0:	83 c4 50             	add    $0x50,%esp
f0100af3:	5b                   	pop    %ebx
f0100af4:	5e                   	pop    %esi
f0100af5:	5f                   	pop    %edi
f0100af6:	c3                   	ret    

f0100af7 <_Z8read_eipv>:
// Return EIP of caller.  Does not work if inlined.
unsigned
read_eip()
{
	uint32_t callerpc;
	__asm __volatile("movl 4(%%ebp), %0" : "=r" (callerpc));
f0100af7:	8b 45 04             	mov    0x4(%ebp),%eax
	return callerpc;
}
f0100afa:	c3                   	ret    
	...

f0100afc <_ZL5putchiPv>:
#include <inc/stdarg.h>


static void
putch(int ch, void *ptr)
{
f0100afc:	83 ec 1c             	sub    $0x1c,%esp
	int *cnt = (int *) ptr;
	cputchar(ch);
f0100aff:	8b 44 24 20          	mov    0x20(%esp),%eax
f0100b03:	89 04 24             	mov    %eax,(%esp)
f0100b06:	e8 76 fd ff ff       	call   f0100881 <_Z8cputchari>
	*cnt++;
}
f0100b0b:	83 c4 1c             	add    $0x1c,%esp
f0100b0e:	c3                   	ret    

f0100b0f <_Z8vcprintfPKcPc>:

int
vcprintf(const char *fmt, va_list ap)
{
f0100b0f:	83 ec 2c             	sub    $0x2c,%esp
	int cnt = 0;
f0100b12:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
f0100b19:	00 

	vprintfmt(&putch, &cnt, fmt, ap);
f0100b1a:	8b 44 24 34          	mov    0x34(%esp),%eax
f0100b1e:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0100b22:	8b 44 24 30          	mov    0x30(%esp),%eax
f0100b26:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100b2a:	8d 44 24 1c          	lea    0x1c(%esp),%eax
f0100b2e:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100b32:	c7 04 24 fc 0a 10 f0 	movl   $0xf0100afc,(%esp)
f0100b39:	e8 31 04 00 00       	call   f0100f6f <_Z9vprintfmtPFviPvES_PKcPc>
	return cnt;
}
f0100b3e:	8b 44 24 1c          	mov    0x1c(%esp),%eax
f0100b42:	83 c4 2c             	add    $0x2c,%esp
f0100b45:	c3                   	ret    

f0100b46 <_Z7cprintfPKcz>:

int
cprintf(const char *fmt, ...)
{
f0100b46:	83 ec 1c             	sub    $0x1c,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0100b49:	8d 44 24 24          	lea    0x24(%esp),%eax
	cnt = vcprintf(fmt, ap);
f0100b4d:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100b51:	8b 44 24 20          	mov    0x20(%esp),%eax
f0100b55:	89 04 24             	mov    %eax,(%esp)
f0100b58:	e8 b2 ff ff ff       	call   f0100b0f <_Z8vcprintfPKcPc>
	va_end(ap);

	return cnt;
}
f0100b5d:	83 c4 1c             	add    $0x1c,%esp
f0100b60:	c3                   	ret    
f0100b61:	00 00                	add    %al,(%eax)
	...

f0100b64 <_ZL14stab_binsearchPK4StabPiS2_ij>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f0100b64:	55                   	push   %ebp
f0100b65:	57                   	push   %edi
f0100b66:	56                   	push   %esi
f0100b67:	53                   	push   %ebx
f0100b68:	83 ec 0c             	sub    $0xc,%esp
f0100b6b:	89 c3                	mov    %eax,%ebx
f0100b6d:	89 54 24 04          	mov    %edx,0x4(%esp)
f0100b71:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0100b75:	8b 74 24 20          	mov    0x20(%esp),%esi
	int l = *region_left, r = *region_right, any_matches = 0;
f0100b79:	8b 0a                	mov    (%edx),%ecx
f0100b7b:	8b 44 24 08          	mov    0x8(%esp),%eax
f0100b7f:	8b 00                	mov    (%eax),%eax
f0100b81:	89 04 24             	mov    %eax,(%esp)
f0100b84:	bd 00 00 00 00       	mov    $0x0,%ebp

	while (l <= r) {
f0100b89:	eb 5e                	jmp    f0100be9 <_ZL14stab_binsearchPK4StabPiS2_ij+0x85>
		int true_m = (l + r) / 2, m = true_m;
f0100b8b:	8b 04 24             	mov    (%esp),%eax
f0100b8e:	01 c8                	add    %ecx,%eax
f0100b90:	bf 02 00 00 00       	mov    $0x2,%edi
f0100b95:	99                   	cltd   
f0100b96:	f7 ff                	idiv   %edi
f0100b98:	89 c2                	mov    %eax,%edx

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0100b9a:	39 ca                	cmp    %ecx,%edx
f0100b9c:	0f 8c 97 00 00 00    	jl     f0100c39 <_ZL14stab_binsearchPK4StabPiS2_ij+0xd5>
//		left = 0, right = 657;
//		stab_binsearch(stabs, &left, &right, N_SO, 0xf0100184);
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
f0100ba2:	6b fa 0c             	imul   $0xc,%edx,%edi

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0100ba5:	0f b6 7c 3b 04       	movzbl 0x4(%ebx,%edi,1),%edi
f0100baa:	39 f7                	cmp    %esi,%edi
f0100bac:	74 72                	je     f0100c20 <_ZL14stab_binsearchPK4StabPiS2_ij+0xbc>
			m--;
f0100bae:	4a                   	dec    %edx
f0100baf:	eb e9                	jmp    f0100b9a <_ZL14stab_binsearchPK4StabPiS2_ij+0x36>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
			*region_left = m;
f0100bb1:	8b 4c 24 04          	mov    0x4(%esp),%ecx
f0100bb5:	89 11                	mov    %edx,(%ecx)
			l = true_m + 1;
f0100bb7:	8d 48 01             	lea    0x1(%eax),%ecx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100bba:	bd 01 00 00 00       	mov    $0x1,%ebp
f0100bbf:	eb 28                	jmp    f0100be9 <_ZL14stab_binsearchPK4StabPiS2_ij+0x85>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f0100bc1:	3b 7c 24 24          	cmp    0x24(%esp),%edi
f0100bc5:	76 11                	jbe    f0100bd8 <_ZL14stab_binsearchPK4StabPiS2_ij+0x74>
			*region_right = m - 1;
f0100bc7:	4d                   	dec    %ebp
f0100bc8:	89 2c 24             	mov    %ebp,(%esp)
f0100bcb:	8b 7c 24 08          	mov    0x8(%esp),%edi
f0100bcf:	89 2f                	mov    %ebp,(%edi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100bd1:	bd 01 00 00 00       	mov    $0x1,%ebp
f0100bd6:	eb 11                	jmp    f0100be9 <_ZL14stab_binsearchPK4StabPiS2_ij+0x85>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f0100bd8:	8b 44 24 04          	mov    0x4(%esp),%eax
f0100bdc:	89 28                	mov    %ebp,(%eax)
			l = m;
			addr++;
f0100bde:	ff 44 24 24          	incl   0x24(%esp)
f0100be2:	89 d1                	mov    %edx,%ecx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100be4:	bd 01 00 00 00       	mov    $0x1,%ebp
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f0100be9:	3b 0c 24             	cmp    (%esp),%ecx
f0100bec:	7e 9d                	jle    f0100b8b <_ZL14stab_binsearchPK4StabPiS2_ij+0x27>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0100bee:	85 ed                	test   %ebp,%ebp
f0100bf0:	75 0f                	jne    f0100c01 <_ZL14stab_binsearchPK4StabPiS2_ij+0x9d>
		*region_right = *region_left - 1;
f0100bf2:	8b 54 24 04          	mov    0x4(%esp),%edx
f0100bf6:	8b 02                	mov    (%edx),%eax
f0100bf8:	48                   	dec    %eax
f0100bf9:	8b 4c 24 08          	mov    0x8(%esp),%ecx
f0100bfd:	89 01                	mov    %eax,(%ecx)
f0100bff:	eb 3d                	jmp    f0100c3e <_ZL14stab_binsearchPK4StabPiS2_ij+0xda>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100c01:	8b 4c 24 08          	mov    0x8(%esp),%ecx
f0100c05:	8b 01                	mov    (%ecx),%eax
f0100c07:	8b 7c 24 04          	mov    0x4(%esp),%edi
f0100c0b:	8b 17                	mov    (%edi),%edx
f0100c0d:	39 c2                	cmp    %eax,%edx
f0100c0f:	7d 20                	jge    f0100c31 <_ZL14stab_binsearchPK4StabPiS2_ij+0xcd>
//		left = 0, right = 657;
//		stab_binsearch(stabs, &left, &right, N_SO, 0xf0100184);
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
f0100c11:	6b c8 0c             	imul   $0xc,%eax,%ecx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100c14:	0f b6 4c 0b 04       	movzbl 0x4(%ebx,%ecx,1),%ecx
f0100c19:	39 f1                	cmp    %esi,%ecx
f0100c1b:	74 14                	je     f0100c31 <_ZL14stab_binsearchPK4StabPiS2_ij+0xcd>
f0100c1d:	48                   	dec    %eax
f0100c1e:	eb ed                	jmp    f0100c0d <_ZL14stab_binsearchPK4StabPiS2_ij+0xa9>

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0100c20:	89 d5                	mov    %edx,%ebp
			continue;
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f0100c22:	6b fa 0c             	imul   $0xc,%edx,%edi
f0100c25:	8b 7c 3b 08          	mov    0x8(%ebx,%edi,1),%edi
f0100c29:	3b 7c 24 24          	cmp    0x24(%esp),%edi
f0100c2d:	72 82                	jb     f0100bb1 <_ZL14stab_binsearchPK4StabPiS2_ij+0x4d>
f0100c2f:	eb 90                	jmp    f0100bc1 <_ZL14stab_binsearchPK4StabPiS2_ij+0x5d>
		// find rightmost region containing 'addr'
		for (l = *region_right;
		     l > *region_left && stabs[l].n_type != type;
		     l--)
			/* do nothing */;
		*region_left = l;
f0100c31:	8b 54 24 04          	mov    0x4(%esp),%edx
f0100c35:	89 02                	mov    %eax,(%edx)
f0100c37:	eb 05                	jmp    f0100c3e <_ZL14stab_binsearchPK4StabPiS2_ij+0xda>

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f0100c39:	8d 48 01             	lea    0x1(%eax),%ecx
			continue;
f0100c3c:	eb ab                	jmp    f0100be9 <_ZL14stab_binsearchPK4StabPiS2_ij+0x85>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
			/* do nothing */;
		*region_left = l;
	}
}
f0100c3e:	83 c4 0c             	add    $0xc,%esp
f0100c41:	5b                   	pop    %ebx
f0100c42:	5e                   	pop    %esi
f0100c43:	5f                   	pop    %edi
f0100c44:	5d                   	pop    %ebp
f0100c45:	c3                   	ret    

f0100c46 <_Z13debuginfo_eipjP12Eipdebuginfo>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f0100c46:	83 ec 2c             	sub    $0x2c,%esp
f0100c49:	89 5c 24 20          	mov    %ebx,0x20(%esp)
f0100c4d:	89 74 24 24          	mov    %esi,0x24(%esp)
f0100c51:	89 7c 24 28          	mov    %edi,0x28(%esp)
f0100c55:	8b 74 24 30          	mov    0x30(%esp),%esi
f0100c59:	8b 5c 24 34          	mov    0x34(%esp),%ebx
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0100c5d:	c7 03 74 23 10 f0    	movl   $0xf0102374,(%ebx)
	info->eip_line = 0;
f0100c63:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	info->eip_fn_name = "<unknown>";
f0100c6a:	c7 43 08 74 23 10 f0 	movl   $0xf0102374,0x8(%ebx)
	info->eip_fn_namelen = 9;
f0100c71:	c7 43 0c 09 00 00 00 	movl   $0x9,0xc(%ebx)
	info->eip_fn_addr = addr;
f0100c78:	89 73 10             	mov    %esi,0x10(%ebx)
	info->eip_fn_narg = 0;
f0100c7b:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f0100c82:	81 fe ff ff bf ef    	cmp    $0xefbfffff,%esi
f0100c88:	76 12                	jbe    f0100c9c <_Z13debuginfo_eipjP12Eipdebuginfo+0x56>
		// Can't search for user-level addresses yet!
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0100c8a:	b8 f3 8b 10 f0       	mov    $0xf0108bf3,%eax
f0100c8f:	3d c1 68 10 f0       	cmp    $0xf01068c1,%eax
f0100c94:	0f 86 3a 01 00 00    	jbe    f0100dd4 <_Z13debuginfo_eipjP12Eipdebuginfo+0x18e>
f0100c9a:	eb 1c                	jmp    f0100cb8 <_Z13debuginfo_eipjP12Eipdebuginfo+0x72>
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
	} else {
		// Can't search for user-level addresses yet!
  	        panic("User address");
f0100c9c:	c7 44 24 08 7e 23 10 	movl   $0xf010237e,0x8(%esp)
f0100ca3:	f0 
f0100ca4:	c7 44 24 04 7f 00 00 	movl   $0x7f,0x4(%esp)
f0100cab:	00 
f0100cac:	c7 04 24 8b 23 10 f0 	movl   $0xf010238b,(%esp)
f0100cb3:	e8 34 f4 ff ff       	call   f01000ec <_Z6_panicPKciS0_z>
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f0100cb8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
		// Can't search for user-level addresses yet!
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0100cbd:	80 3d f2 8b 10 f0 00 	cmpb   $0x0,0xf0108bf2
f0100cc4:	0f 85 16 01 00 00    	jne    f0100de0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x19a>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0100cca:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
f0100cd1:	00 
	rfile = (stab_end - stabs) - 1;
f0100cd2:	b8 c0 68 10 f0       	mov    $0xf01068c0,%eax
f0100cd7:	2d ac 25 10 f0       	sub    $0xf01025ac,%eax
f0100cdc:	c1 f8 02             	sar    $0x2,%eax
f0100cdf:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
f0100ce5:	83 e8 01             	sub    $0x1,%eax
f0100ce8:	89 44 24 14          	mov    %eax,0x14(%esp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f0100cec:	89 74 24 04          	mov    %esi,0x4(%esp)
f0100cf0:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
f0100cf7:	8d 4c 24 14          	lea    0x14(%esp),%ecx
f0100cfb:	8d 54 24 10          	lea    0x10(%esp),%edx
f0100cff:	b8 ac 25 10 f0       	mov    $0xf01025ac,%eax
f0100d04:	e8 5b fe ff ff       	call   f0100b64 <_ZL14stab_binsearchPK4StabPiS2_ij>
	if (lfile == 0)
f0100d09:	8b 54 24 10          	mov    0x10(%esp),%edx
		return -1;
f0100d0d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
f0100d12:	85 d2                	test   %edx,%edx
f0100d14:	0f 84 c6 00 00 00    	je     f0100de0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x19a>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f0100d1a:	89 54 24 18          	mov    %edx,0x18(%esp)
	rfun = rfile;
f0100d1e:	8b 44 24 14          	mov    0x14(%esp),%eax
f0100d22:	89 44 24 1c          	mov    %eax,0x1c(%esp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f0100d26:	89 74 24 04          	mov    %esi,0x4(%esp)
f0100d2a:	c7 04 24 24 00 00 00 	movl   $0x24,(%esp)
f0100d31:	8d 4c 24 1c          	lea    0x1c(%esp),%ecx
f0100d35:	8d 54 24 18          	lea    0x18(%esp),%edx
f0100d39:	b8 ac 25 10 f0       	mov    $0xf01025ac,%eax
f0100d3e:	e8 21 fe ff ff       	call   f0100b64 <_ZL14stab_binsearchPK4StabPiS2_ij>

	if (lfun <= rfun) {
f0100d43:	8b 7c 24 18          	mov    0x18(%esp),%edi
f0100d47:	3b 7c 24 1c          	cmp    0x1c(%esp),%edi
f0100d4b:	7f 2e                	jg     f0100d7b <_Z13debuginfo_eipjP12Eipdebuginfo+0x135>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < (uint32_t) (stabstr_end - stabstr))
f0100d4d:	6b c7 0c             	imul   $0xc,%edi,%eax
f0100d50:	8d 90 ac 25 10 f0    	lea    -0xfefda54(%eax),%edx
f0100d56:	8b 80 ac 25 10 f0    	mov    -0xfefda54(%eax),%eax
f0100d5c:	b9 f3 8b 10 f0       	mov    $0xf0108bf3,%ecx
f0100d61:	81 e9 c1 68 10 f0    	sub    $0xf01068c1,%ecx
f0100d67:	39 c8                	cmp    %ecx,%eax
f0100d69:	73 08                	jae    f0100d73 <_Z13debuginfo_eipjP12Eipdebuginfo+0x12d>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0100d6b:	05 c1 68 10 f0       	add    $0xf01068c1,%eax
f0100d70:	89 43 08             	mov    %eax,0x8(%ebx)
		info->eip_fn_addr = stabs[lfun].n_value;
f0100d73:	8b 42 08             	mov    0x8(%edx),%eax
f0100d76:	89 43 10             	mov    %eax,0x10(%ebx)
f0100d79:	eb 07                	jmp    f0100d82 <_Z13debuginfo_eipjP12Eipdebuginfo+0x13c>
		lline = lfun;
		rline = rfun;
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f0100d7b:	89 73 10             	mov    %esi,0x10(%ebx)
		lline = lfile;
f0100d7e:	8b 7c 24 10          	mov    0x10(%esp),%edi
		rline = rfile;
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f0100d82:	c7 44 24 04 3a 00 00 	movl   $0x3a,0x4(%esp)
f0100d89:	00 
f0100d8a:	8b 43 08             	mov    0x8(%ebx),%eax
f0100d8d:	89 04 24             	mov    %eax,(%esp)
f0100d90:	e8 d6 08 00 00       	call   f010166b <_Z7strfindPKcc>
f0100d95:	2b 43 08             	sub    0x8(%ebx),%eax
f0100d98:	89 43 0c             	mov    %eax,0xc(%ebx)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0100d9b:	8b 44 24 10          	mov    0x10(%esp),%eax
f0100d9f:	39 c7                	cmp    %eax,%edi
f0100da1:	7c 38                	jl     f0100ddb <_Z13debuginfo_eipjP12Eipdebuginfo+0x195>
	       && stabs[lline].n_type != N_SOL
f0100da3:	8d 14 7f             	lea    (%edi,%edi,2),%edx
f0100da6:	8d 14 95 ac 25 10 f0 	lea    -0xfefda54(,%edx,4),%edx
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0100dad:	0f b6 4a 04          	movzbl 0x4(%edx),%ecx
f0100db1:	80 f9 84             	cmp    $0x84,%cl
f0100db4:	74 3a                	je     f0100df0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x1aa>
f0100db6:	80 f9 64             	cmp    $0x64,%cl
f0100db9:	75 06                	jne    f0100dc1 <_Z13debuginfo_eipjP12Eipdebuginfo+0x17b>
f0100dbb:	83 7a 08 00          	cmpl   $0x0,0x8(%edx)
f0100dbf:	75 2f                	jne    f0100df0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x1aa>
	       && stabs[lline].n_type != N_SOL
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
		lline--;
f0100dc1:	83 ef 01             	sub    $0x1,%edi
f0100dc4:	eb d9                	jmp    f0100d9f <_Z13debuginfo_eipjP12Eipdebuginfo+0x159>
	if (lline >= lfile
	    && stabs[lline].n_strx < (uint32_t) (stabstr_end - stabstr))
		info->eip_file = stabstr + stabs[lline].n_strx;
f0100dc6:	05 c1 68 10 f0       	add    $0xf01068c1,%eax
f0100dcb:	89 03                	mov    %eax,(%ebx)
	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	// Your code here.


	return 0;
f0100dcd:	b8 00 00 00 00       	mov    $0x0,%eax
f0100dd2:	eb 0c                	jmp    f0100de0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x19a>
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f0100dd4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100dd9:	eb 05                	jmp    f0100de0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x19a>
	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	// Your code here.


	return 0;
f0100ddb:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100de0:	8b 5c 24 20          	mov    0x20(%esp),%ebx
f0100de4:	8b 74 24 24          	mov    0x24(%esp),%esi
f0100de8:	8b 7c 24 28          	mov    0x28(%esp),%edi
f0100dec:	83 c4 2c             	add    $0x2c,%esp
f0100def:	c3                   	ret    
	while (lline >= lfile
	       && stabs[lline].n_type != N_SOL
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
		lline--;
	if (lline >= lfile
	    && stabs[lline].n_strx < (uint32_t) (stabstr_end - stabstr))
f0100df0:	6b ff 0c             	imul   $0xc,%edi,%edi
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
	       && stabs[lline].n_type != N_SOL
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
		lline--;
	if (lline >= lfile
f0100df3:	8b 87 ac 25 10 f0    	mov    -0xfefda54(%edi),%eax
f0100df9:	ba f3 8b 10 f0       	mov    $0xf0108bf3,%edx
f0100dfe:	81 ea c1 68 10 f0    	sub    $0xf01068c1,%edx
f0100e04:	39 d0                	cmp    %edx,%eax
f0100e06:	72 be                	jb     f0100dc6 <_Z13debuginfo_eipjP12Eipdebuginfo+0x180>
	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	// Your code here.


	return 0;
f0100e08:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e0d:	eb d1                	jmp    f0100de0 <_Z13debuginfo_eipjP12Eipdebuginfo+0x19a>
	...

f0100e10 <_ZL8printnumPFviPvES_yjii>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f0100e10:	55                   	push   %ebp
f0100e11:	57                   	push   %edi
f0100e12:	56                   	push   %esi
f0100e13:	53                   	push   %ebx
f0100e14:	83 ec 3c             	sub    $0x3c,%esp
f0100e17:	89 c5                	mov    %eax,%ebp
f0100e19:	89 d7                	mov    %edx,%edi
f0100e1b:	8b 44 24 50          	mov    0x50(%esp),%eax
f0100e1f:	89 44 24 2c          	mov    %eax,0x2c(%esp)
f0100e23:	8b 44 24 54          	mov    0x54(%esp),%eax
f0100e27:	89 44 24 28          	mov    %eax,0x28(%esp)
f0100e2b:	8b 5c 24 5c          	mov    0x5c(%esp),%ebx
f0100e2f:	8b 74 24 60          	mov    0x60(%esp),%esi
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f0100e33:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e38:	3b 44 24 28          	cmp    0x28(%esp),%eax
f0100e3c:	72 13                	jb     f0100e51 <_ZL8printnumPFviPvES_yjii+0x41>
f0100e3e:	8b 44 24 2c          	mov    0x2c(%esp),%eax
f0100e42:	39 44 24 58          	cmp    %eax,0x58(%esp)
f0100e46:	76 09                	jbe    f0100e51 <_ZL8printnumPFviPvES_yjii+0x41>
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0100e48:	83 eb 01             	sub    $0x1,%ebx
f0100e4b:	85 db                	test   %ebx,%ebx
f0100e4d:	7f 53                	jg     f0100ea2 <_ZL8printnumPFviPvES_yjii+0x92>
f0100e4f:	eb 5f                	jmp    f0100eb0 <_ZL8printnumPFviPvES_yjii+0xa0>
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
f0100e51:	89 74 24 10          	mov    %esi,0x10(%esp)
f0100e55:	83 eb 01             	sub    $0x1,%ebx
f0100e58:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
f0100e5c:	8b 44 24 58          	mov    0x58(%esp),%eax
f0100e60:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100e64:	8b 5c 24 08          	mov    0x8(%esp),%ebx
f0100e68:	8b 74 24 0c          	mov    0xc(%esp),%esi
f0100e6c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f0100e73:	00 
f0100e74:	8b 44 24 2c          	mov    0x2c(%esp),%eax
f0100e78:	89 04 24             	mov    %eax,(%esp)
f0100e7b:	8b 44 24 28          	mov    0x28(%esp),%eax
f0100e7f:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100e83:	e8 98 0a 00 00       	call   f0101920 <__udivdi3>
f0100e88:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0100e8c:	89 74 24 0c          	mov    %esi,0xc(%esp)
f0100e90:	89 04 24             	mov    %eax,(%esp)
f0100e93:	89 54 24 04          	mov    %edx,0x4(%esp)
f0100e97:	89 fa                	mov    %edi,%edx
f0100e99:	89 e8                	mov    %ebp,%eax
f0100e9b:	e8 70 ff ff ff       	call   f0100e10 <_ZL8printnumPFviPvES_yjii>
f0100ea0:	eb 0e                	jmp    f0100eb0 <_ZL8printnumPFviPvES_yjii+0xa0>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0100ea2:	89 7c 24 04          	mov    %edi,0x4(%esp)
f0100ea6:	89 34 24             	mov    %esi,(%esp)
f0100ea9:	ff d5                	call   *%ebp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0100eab:	83 eb 01             	sub    $0x1,%ebx
f0100eae:	75 f2                	jne    f0100ea2 <_ZL8printnumPFviPvES_yjii+0x92>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0100eb0:	89 7c 24 04          	mov    %edi,0x4(%esp)
f0100eb4:	8b 7c 24 04          	mov    0x4(%esp),%edi
f0100eb8:	8b 44 24 58          	mov    0x58(%esp),%eax
f0100ebc:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100ec0:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f0100ec7:	00 
f0100ec8:	8b 44 24 2c          	mov    0x2c(%esp),%eax
f0100ecc:	89 04 24             	mov    %eax,(%esp)
f0100ecf:	8b 44 24 28          	mov    0x28(%esp),%eax
f0100ed3:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100ed7:	e8 54 0b 00 00       	call   f0101a30 <__umoddi3>
f0100edc:	89 7c 24 04          	mov    %edi,0x4(%esp)
f0100ee0:	0f be 80 99 23 10 f0 	movsbl -0xfefdc67(%eax),%eax
f0100ee7:	89 04 24             	mov    %eax,(%esp)
f0100eea:	ff d5                	call   *%ebp
}
f0100eec:	83 c4 3c             	add    $0x3c,%esp
f0100eef:	5b                   	pop    %ebx
f0100ef0:	5e                   	pop    %esi
f0100ef1:	5f                   	pop    %edi
f0100ef2:	5d                   	pop    %ebp
f0100ef3:	c3                   	ret    

f0100ef4 <_ZL7getuintPPci>:
// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0100ef4:	83 fa 01             	cmp    $0x1,%edx
f0100ef7:	7e 0d                	jle    f0100f06 <_ZL7getuintPPci+0x12>
		return va_arg(*ap, unsigned long long);
f0100ef9:	8b 10                	mov    (%eax),%edx
f0100efb:	8d 4a 08             	lea    0x8(%edx),%ecx
f0100efe:	89 08                	mov    %ecx,(%eax)
f0100f00:	8b 02                	mov    (%edx),%eax
f0100f02:	8b 52 04             	mov    0x4(%edx),%edx
f0100f05:	c3                   	ret    
	else if (lflag)
f0100f06:	85 d2                	test   %edx,%edx
f0100f08:	74 0f                	je     f0100f19 <_ZL7getuintPPci+0x25>
		return va_arg(*ap, unsigned long);
f0100f0a:	8b 10                	mov    (%eax),%edx
f0100f0c:	8d 4a 04             	lea    0x4(%edx),%ecx
f0100f0f:	89 08                	mov    %ecx,(%eax)
f0100f11:	8b 02                	mov    (%edx),%eax
f0100f13:	ba 00 00 00 00       	mov    $0x0,%edx
f0100f18:	c3                   	ret    
	else
		return va_arg(*ap, unsigned int);
f0100f19:	8b 10                	mov    (%eax),%edx
f0100f1b:	8d 4a 04             	lea    0x4(%edx),%ecx
f0100f1e:	89 08                	mov    %ecx,(%eax)
f0100f20:	8b 02                	mov    (%edx),%eax
f0100f22:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0100f27:	c3                   	ret    

f0100f28 <_ZL11sprintputchiPv>:
	int cnt;
};

static void
sprintputch(int ch, void *ptr)
{
f0100f28:	8b 44 24 08          	mov    0x8(%esp),%eax
	struct sprintbuf *b = (struct sprintbuf *) ptr;
	b->cnt++;
f0100f2c:	83 40 08 01          	addl   $0x1,0x8(%eax)
	if (b->buf < b->ebuf)
f0100f30:	8b 10                	mov    (%eax),%edx
f0100f32:	3b 50 04             	cmp    0x4(%eax),%edx
f0100f35:	73 0b                	jae    f0100f42 <_ZL11sprintputchiPv+0x1a>
		*b->buf++ = ch;
f0100f37:	8b 4c 24 04          	mov    0x4(%esp),%ecx
f0100f3b:	88 0a                	mov    %cl,(%edx)
f0100f3d:	83 c2 01             	add    $0x1,%edx
f0100f40:	89 10                	mov    %edx,(%eax)
f0100f42:	f3 c3                	repz ret 

f0100f44 <_Z8printfmtPFviPvES_PKcz>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f0100f44:	83 ec 1c             	sub    $0x1c,%esp
	va_list ap;

	va_start(ap, fmt);
f0100f47:	8d 44 24 2c          	lea    0x2c(%esp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f0100f4b:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0100f4f:	8b 44 24 28          	mov    0x28(%esp),%eax
f0100f53:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100f57:	8b 44 24 24          	mov    0x24(%esp),%eax
f0100f5b:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100f5f:	8b 44 24 20          	mov    0x20(%esp),%eax
f0100f63:	89 04 24             	mov    %eax,(%esp)
f0100f66:	e8 04 00 00 00       	call   f0100f6f <_Z9vprintfmtPFviPvES_PKcPc>
	va_end(ap);
}
f0100f6b:	83 c4 1c             	add    $0x1c,%esp
f0100f6e:	c3                   	ret    

f0100f6f <_Z9vprintfmtPFviPvES_PKcPc>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f0100f6f:	55                   	push   %ebp
f0100f70:	57                   	push   %edi
f0100f71:	56                   	push   %esi
f0100f72:	53                   	push   %ebx
f0100f73:	83 ec 4c             	sub    $0x4c,%esp
f0100f76:	8b 6c 24 60          	mov    0x60(%esp),%ebp
f0100f7a:	8b 5c 24 64          	mov    0x64(%esp),%ebx
			base = 16;
			goto number;

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f0100f7e:	89 6c 24 28          	mov    %ebp,0x28(%esp)
f0100f82:	89 dd                	mov    %ebx,%ebp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0100f84:	8b 7c 24 68          	mov    0x68(%esp),%edi
f0100f88:	0f b6 07             	movzbl (%edi),%eax
f0100f8b:	89 fb                	mov    %edi,%ebx
f0100f8d:	83 c3 01             	add    $0x1,%ebx
f0100f90:	83 f8 25             	cmp    $0x25,%eax
f0100f93:	74 2a                	je     f0100fbf <_Z9vprintfmtPFviPvES_PKcPc+0x50>
			if (ch == '\0')
f0100f95:	85 c0                	test   %eax,%eax
f0100f97:	75 0e                	jne    f0100fa7 <_Z9vprintfmtPFviPvES_PKcPc+0x38>
f0100f99:	e9 08 04 00 00       	jmp    f01013a6 <_Z9vprintfmtPFviPvES_PKcPc+0x437>
f0100f9e:	85 c0                	test   %eax,%eax
f0100fa0:	75 09                	jne    f0100fab <_Z9vprintfmtPFviPvES_PKcPc+0x3c>
f0100fa2:	e9 ff 03 00 00       	jmp    f01013a6 <_Z9vprintfmtPFviPvES_PKcPc+0x437>
f0100fa7:	8b 74 24 28          	mov    0x28(%esp),%esi
				return;
			putch(ch, putdat);
f0100fab:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f0100faf:	89 04 24             	mov    %eax,(%esp)
f0100fb2:	ff d6                	call   *%esi
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0100fb4:	0f b6 03             	movzbl (%ebx),%eax
f0100fb7:	83 c3 01             	add    $0x1,%ebx
f0100fba:	83 f8 25             	cmp    $0x25,%eax
f0100fbd:	75 df                	jne    f0100f9e <_Z9vprintfmtPFviPvES_PKcPc+0x2f>
			precision = va_arg(ap, int);
			goto process_precision;

		case '.':
			if (width < 0)
				width = 0;
f0100fbf:	c6 44 24 30 20       	movb   $0x20,0x30(%esp)
f0100fc4:	bf 00 00 00 00       	mov    $0x0,%edi
f0100fc9:	be ff ff ff ff       	mov    $0xffffffff,%esi
f0100fce:	c7 44 24 2c ff ff ff 	movl   $0xffffffff,0x2c(%esp)
f0100fd5:	ff 
f0100fd6:	b9 00 00 00 00       	mov    $0x0,%ecx
f0100fdb:	89 7c 24 34          	mov    %edi,0x34(%esp)
f0100fdf:	eb 2d                	jmp    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100fe1:	8b 5c 24 68          	mov    0x68(%esp),%ebx

		// flag to pad on the right
		case '-':
			padc = '-';
f0100fe5:	c6 44 24 30 2d       	movb   $0x2d,0x30(%esp)
f0100fea:	eb 22                	jmp    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100fec:	8b 5c 24 68          	mov    0x68(%esp),%ebx
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f0100ff0:	c6 44 24 30 30       	movb   $0x30,0x30(%esp)
f0100ff5:	eb 17                	jmp    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100ff7:	8b 5c 24 68          	mov    0x68(%esp),%ebx
			precision = va_arg(ap, int);
			goto process_precision;

		case '.':
			if (width < 0)
				width = 0;
f0100ffb:	c7 44 24 2c 00 00 00 	movl   $0x0,0x2c(%esp)
f0101002:	00 
f0101003:	eb 09                	jmp    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
				width = precision, precision = -1;
f0101005:	89 74 24 2c          	mov    %esi,0x2c(%esp)
f0101009:	be ff ff ff ff       	mov    $0xffffffff,%esi
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010100e:	0f b6 03             	movzbl (%ebx),%eax
f0101011:	0f b6 d0             	movzbl %al,%edx
f0101014:	8d 7b 01             	lea    0x1(%ebx),%edi
f0101017:	89 7c 24 68          	mov    %edi,0x68(%esp)
f010101b:	83 e8 23             	sub    $0x23,%eax
f010101e:	3c 55                	cmp    $0x55,%al
f0101020:	0f 87 30 03 00 00    	ja     f0101356 <_Z9vprintfmtPFviPvES_PKcPc+0x3e7>
f0101026:	0f b6 c0             	movzbl %al,%eax
f0101029:	ff 24 85 28 24 10 f0 	jmp    *-0xfefdbd8(,%eax,4)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f0101030:	8d 72 d0             	lea    -0x30(%edx),%esi
				ch = *fmt;
f0101033:	0f be 43 01          	movsbl 0x1(%ebx),%eax
				if (ch < '0' || ch > '9')
f0101037:	8d 50 d0             	lea    -0x30(%eax),%edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010103a:	8b 5c 24 68          	mov    0x68(%esp),%ebx
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
f010103e:	83 fa 09             	cmp    $0x9,%edx
f0101041:	77 53                	ja     f0101096 <_Z9vprintfmtPFviPvES_PKcPc+0x127>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101043:	8b 7c 24 34          	mov    0x34(%esp),%edi
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0101047:	83 c3 01             	add    $0x1,%ebx
				precision = precision * 10 + ch - '0';
f010104a:	8d 14 b6             	lea    (%esi,%esi,4),%edx
f010104d:	8d 74 50 d0          	lea    -0x30(%eax,%edx,2),%esi
				ch = *fmt;
f0101051:	0f be 03             	movsbl (%ebx),%eax
				if (ch < '0' || ch > '9')
f0101054:	8d 50 d0             	lea    -0x30(%eax),%edx
f0101057:	83 fa 09             	cmp    $0x9,%edx
f010105a:	76 eb                	jbe    f0101047 <_Z9vprintfmtPFviPvES_PKcPc+0xd8>
f010105c:	89 7c 24 34          	mov    %edi,0x34(%esp)
f0101060:	eb 34                	jmp    f0101096 <_Z9vprintfmtPFviPvES_PKcPc+0x127>
					break;
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f0101062:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f0101066:	8d 50 04             	lea    0x4(%eax),%edx
f0101069:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f010106d:	8b 30                	mov    (%eax),%esi
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010106f:	8b 5c 24 68          	mov    0x68(%esp),%ebx
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f0101073:	eb 21                	jmp    f0101096 <_Z9vprintfmtPFviPvES_PKcPc+0x127>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101075:	8b 5c 24 68          	mov    0x68(%esp),%ebx
		case '*':
			precision = va_arg(ap, int);
			goto process_precision;

		case '.':
			if (width < 0)
f0101079:	83 7c 24 2c 00       	cmpl   $0x0,0x2c(%esp)
f010107e:	79 8e                	jns    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>
f0101080:	e9 72 ff ff ff       	jmp    f0100ff7 <_Z9vprintfmtPFviPvES_PKcPc+0x88>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101085:	8b 5c 24 68          	mov    0x68(%esp),%ebx
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f0101089:	c7 44 24 34 01 00 00 	movl   $0x1,0x34(%esp)
f0101090:	00 
			goto reswitch;
f0101091:	e9 78 ff ff ff       	jmp    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>

		process_precision:
			if (width < 0)
f0101096:	83 7c 24 2c 00       	cmpl   $0x0,0x2c(%esp)
f010109b:	0f 89 6d ff ff ff    	jns    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>
f01010a1:	e9 5f ff ff ff       	jmp    f0101005 <_Z9vprintfmtPFviPvES_PKcPc+0x96>
				width = precision, precision = -1;
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f01010a6:	83 c1 01             	add    $0x1,%ecx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f01010a9:	8b 5c 24 68          	mov    0x68(%esp),%ebx
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f01010ad:	e9 5c ff ff ff       	jmp    f010100e <_Z9vprintfmtPFviPvES_PKcPc+0x9f>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f01010b2:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f01010b6:	8d 50 04             	lea    0x4(%eax),%edx
f01010b9:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f01010bd:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01010c1:	8b 00                	mov    (%eax),%eax
f01010c3:	89 04 24             	mov    %eax,(%esp)
f01010c6:	ff 54 24 28          	call   *0x28(%esp)
			break;
f01010ca:	e9 b5 fe ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>

		// error message
		case 'e':
			err = va_arg(ap, int);
f01010cf:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f01010d3:	8d 50 04             	lea    0x4(%eax),%edx
f01010d6:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f01010da:	8b 00                	mov    (%eax),%eax
f01010dc:	89 c2                	mov    %eax,%edx
f01010de:	c1 fa 1f             	sar    $0x1f,%edx
f01010e1:	31 d0                	xor    %edx,%eax
f01010e3:	29 d0                	sub    %edx,%eax
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
f01010e5:	83 f8 06             	cmp    $0x6,%eax
f01010e8:	7f 0f                	jg     f01010f9 <_Z9vprintfmtPFviPvES_PKcPc+0x18a>
f01010ea:	8b 14 85 80 25 10 f0 	mov    -0xfefda80(,%eax,4),%edx
f01010f1:	85 d2                	test   %edx,%edx
f01010f3:	0f 85 8c 02 00 00    	jne    f0101385 <_Z9vprintfmtPFviPvES_PKcPc+0x416>
				printfmt(putch, putdat, "error %d", err);
f01010f9:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01010fd:	c7 44 24 08 b1 23 10 	movl   $0xf01023b1,0x8(%esp)
f0101104:	f0 
f0101105:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f0101109:	8b 7c 24 28          	mov    0x28(%esp),%edi
f010110d:	89 3c 24             	mov    %edi,(%esp)
f0101110:	e8 2f fe ff ff       	call   f0100f44 <_Z8printfmtPFviPvES_PKcz>
f0101115:	e9 6a fe ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>
f010111a:	8b 7c 24 34          	mov    0x34(%esp),%edi
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010111e:	89 f1                	mov    %esi,%ecx
f0101120:	8b 44 24 2c          	mov    0x2c(%esp),%eax
f0101124:	89 44 24 34          	mov    %eax,0x34(%esp)
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0101128:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f010112c:	8d 50 04             	lea    0x4(%eax),%edx
f010112f:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f0101133:	8b 18                	mov    (%eax),%ebx
f0101135:	85 db                	test   %ebx,%ebx
f0101137:	75 05                	jne    f010113e <_Z9vprintfmtPFviPvES_PKcPc+0x1cf>
				p = "(null)";
f0101139:	bb aa 23 10 f0       	mov    $0xf01023aa,%ebx
			if (width > 0 && padc != '-')
f010113e:	83 7c 24 34 00       	cmpl   $0x0,0x34(%esp)
f0101143:	0f 8e 83 00 00 00    	jle    f01011cc <_Z9vprintfmtPFviPvES_PKcPc+0x25d>
f0101149:	80 7c 24 30 2d       	cmpb   $0x2d,0x30(%esp)
f010114e:	74 7c                	je     f01011cc <_Z9vprintfmtPFviPvES_PKcPc+0x25d>
				for (width -= strnlen(p, precision); width > 0; width--)
f0101150:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0101154:	89 1c 24             	mov    %ebx,(%esp)
f0101157:	e8 dd 03 00 00       	call   f0101539 <_Z7strnlenPKcj>
f010115c:	8b 54 24 34          	mov    0x34(%esp),%edx
f0101160:	29 c2                	sub    %eax,%edx
f0101162:	89 54 24 2c          	mov    %edx,0x2c(%esp)
f0101166:	85 d2                	test   %edx,%edx
f0101168:	7e 62                	jle    f01011cc <_Z9vprintfmtPFviPvES_PKcPc+0x25d>
					putch(padc, putdat);
f010116a:	0f be 4c 24 30       	movsbl 0x30(%esp),%ecx
f010116f:	89 5c 24 34          	mov    %ebx,0x34(%esp)
f0101173:	89 d3                	mov    %edx,%ebx
f0101175:	89 7c 24 38          	mov    %edi,0x38(%esp)
f0101179:	89 74 24 3c          	mov    %esi,0x3c(%esp)
f010117d:	8b 7c 24 28          	mov    0x28(%esp),%edi
f0101181:	89 ce                	mov    %ecx,%esi
f0101183:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f0101187:	89 34 24             	mov    %esi,(%esp)
f010118a:	ff d7                	call   *%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f010118c:	83 eb 01             	sub    $0x1,%ebx
f010118f:	75 f2                	jne    f0101183 <_Z9vprintfmtPFviPvES_PKcPc+0x214>
f0101191:	89 5c 24 2c          	mov    %ebx,0x2c(%esp)
f0101195:	8b 5c 24 34          	mov    0x34(%esp),%ebx
f0101199:	8b 7c 24 38          	mov    0x38(%esp),%edi
f010119d:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f01011a1:	eb 29                	jmp    f01011cc <_Z9vprintfmtPFviPvES_PKcPc+0x25d>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f01011a3:	8d 50 e0             	lea    -0x20(%eax),%edx
f01011a6:	83 fa 5e             	cmp    $0x5e,%edx
f01011a9:	76 11                	jbe    f01011bc <_Z9vprintfmtPFviPvES_PKcPc+0x24d>
					putch('?', putdat);
f01011ab:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01011af:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
f01011b6:	ff 54 24 28          	call   *0x28(%esp)
f01011ba:	eb 0b                	jmp    f01011c7 <_Z9vprintfmtPFviPvES_PKcPc+0x258>
				else
					putch(ch, putdat);
f01011bc:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01011c0:	89 04 24             	mov    %eax,(%esp)
f01011c3:	ff 54 24 28          	call   *0x28(%esp)
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f01011c7:	83 6c 24 2c 01       	subl   $0x1,0x2c(%esp)
f01011cc:	0f be 03             	movsbl (%ebx),%eax
f01011cf:	83 c3 01             	add    $0x1,%ebx
f01011d2:	85 c0                	test   %eax,%eax
f01011d4:	74 0f                	je     f01011e5 <_Z9vprintfmtPFviPvES_PKcPc+0x276>
f01011d6:	85 f6                	test   %esi,%esi
f01011d8:	78 05                	js     f01011df <_Z9vprintfmtPFviPvES_PKcPc+0x270>
f01011da:	83 ee 01             	sub    $0x1,%esi
f01011dd:	78 0c                	js     f01011eb <_Z9vprintfmtPFviPvES_PKcPc+0x27c>
				if (altflag && (ch < ' ' || ch > '~'))
f01011df:	85 ff                	test   %edi,%edi
f01011e1:	75 c0                	jne    f01011a3 <_Z9vprintfmtPFviPvES_PKcPc+0x234>
f01011e3:	eb d7                	jmp    f01011bc <_Z9vprintfmtPFviPvES_PKcPc+0x24d>
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f01011e5:	8b 44 24 2c          	mov    0x2c(%esp),%eax
f01011e9:	eb 04                	jmp    f01011ef <_Z9vprintfmtPFviPvES_PKcPc+0x280>
f01011eb:	8b 44 24 2c          	mov    0x2c(%esp),%eax
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f01011ef:	85 c0                	test   %eax,%eax
f01011f1:	0f 8e 8d fd ff ff    	jle    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>
f01011f7:	8b 5c 24 2c          	mov    0x2c(%esp),%ebx
f01011fb:	8b 74 24 28          	mov    0x28(%esp),%esi
				putch(' ', putdat);
f01011ff:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f0101203:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f010120a:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f010120c:	83 eb 01             	sub    $0x1,%ebx
f010120f:	85 db                	test   %ebx,%ebx
f0101211:	7f ec                	jg     f01011ff <_Z9vprintfmtPFviPvES_PKcPc+0x290>
f0101213:	e9 6c fd ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0101218:	83 f9 01             	cmp    $0x1,%ecx
f010121b:	7e 12                	jle    f010122f <_Z9vprintfmtPFviPvES_PKcPc+0x2c0>
		return va_arg(*ap, long long);
f010121d:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f0101221:	8d 50 08             	lea    0x8(%eax),%edx
f0101224:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f0101228:	8b 30                	mov    (%eax),%esi
f010122a:	8b 78 04             	mov    0x4(%eax),%edi
f010122d:	eb 2a                	jmp    f0101259 <_Z9vprintfmtPFviPvES_PKcPc+0x2ea>
	else if (lflag)
f010122f:	85 c9                	test   %ecx,%ecx
f0101231:	74 14                	je     f0101247 <_Z9vprintfmtPFviPvES_PKcPc+0x2d8>
		return va_arg(*ap, long);
f0101233:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f0101237:	8d 50 04             	lea    0x4(%eax),%edx
f010123a:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f010123e:	8b 30                	mov    (%eax),%esi
f0101240:	89 f7                	mov    %esi,%edi
f0101242:	c1 ff 1f             	sar    $0x1f,%edi
f0101245:	eb 12                	jmp    f0101259 <_Z9vprintfmtPFviPvES_PKcPc+0x2ea>
	else
		return va_arg(*ap, int);
f0101247:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f010124b:	8d 50 04             	lea    0x4(%eax),%edx
f010124e:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f0101252:	8b 30                	mov    (%eax),%esi
f0101254:	89 f7                	mov    %esi,%edi
f0101256:	c1 ff 1f             	sar    $0x1f,%edi
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
f0101259:	85 ff                	test   %edi,%edi
f010125b:	78 0e                	js     f010126b <_Z9vprintfmtPFviPvES_PKcPc+0x2fc>
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f010125d:	89 f0                	mov    %esi,%eax
f010125f:	89 fa                	mov    %edi,%edx
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f0101261:	bb 0a 00 00 00       	mov    $0xa,%ebx
f0101266:	e9 af 00 00 00       	jmp    f010131a <_Z9vprintfmtPFviPvES_PKcPc+0x3ab>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
				putch('-', putdat);
f010126b:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f010126f:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
f0101276:	ff 54 24 28          	call   *0x28(%esp)
				num = -(long long) num;
f010127a:	89 f0                	mov    %esi,%eax
f010127c:	89 fa                	mov    %edi,%edx
f010127e:	f7 d8                	neg    %eax
f0101280:	83 d2 00             	adc    $0x0,%edx
f0101283:	f7 da                	neg    %edx
			}
			base = 10;
f0101285:	bb 0a 00 00 00       	mov    $0xa,%ebx
f010128a:	e9 8b 00 00 00       	jmp    f010131a <_Z9vprintfmtPFviPvES_PKcPc+0x3ab>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f010128f:	89 ca                	mov    %ecx,%edx
f0101291:	8d 44 24 6c          	lea    0x6c(%esp),%eax
f0101295:	e8 5a fc ff ff       	call   f0100ef4 <_ZL7getuintPPci>
			base = 10;
f010129a:	bb 0a 00 00 00       	mov    $0xa,%ebx
			goto number;
f010129f:	eb 79                	jmp    f010131a <_Z9vprintfmtPFviPvES_PKcPc+0x3ab>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
f01012a1:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01012a5:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
f01012ac:	ff 54 24 28          	call   *0x28(%esp)
			putch('X', putdat);
f01012b0:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01012b4:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
f01012bb:	ff 54 24 28          	call   *0x28(%esp)
			putch('X', putdat);
f01012bf:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01012c3:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
f01012ca:	ff 54 24 28          	call   *0x28(%esp)
			break;
f01012ce:	e9 b1 fc ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>

		// pointer
		case 'p':
			putch('0', putdat);
f01012d3:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01012d7:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
f01012de:	ff 54 24 28          	call   *0x28(%esp)
			putch('x', putdat);
f01012e2:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f01012e6:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
f01012ed:	ff 54 24 28          	call   *0x28(%esp)
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f01012f1:	8b 44 24 6c          	mov    0x6c(%esp),%eax
f01012f5:	8d 50 04             	lea    0x4(%eax),%edx
f01012f8:	89 54 24 6c          	mov    %edx,0x6c(%esp)
f01012fc:	8b 00                	mov    (%eax),%eax
f01012fe:	ba 00 00 00 00       	mov    $0x0,%edx
			base = 16;
f0101303:	bb 10 00 00 00       	mov    $0x10,%ebx
			goto number;
f0101308:	eb 10                	jmp    f010131a <_Z9vprintfmtPFviPvES_PKcPc+0x3ab>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f010130a:	89 ca                	mov    %ecx,%edx
f010130c:	8d 44 24 6c          	lea    0x6c(%esp),%eax
f0101310:	e8 df fb ff ff       	call   f0100ef4 <_ZL7getuintPPci>
			base = 16;
f0101315:	bb 10 00 00 00       	mov    $0x10,%ebx
		number:
			printnum(putch, putdat, num, base, width, padc);
f010131a:	0f be 4c 24 30       	movsbl 0x30(%esp),%ecx
f010131f:	89 4c 24 10          	mov    %ecx,0x10(%esp)
f0101323:	8b 7c 24 2c          	mov    0x2c(%esp),%edi
f0101327:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f010132b:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f010132f:	89 04 24             	mov    %eax,(%esp)
f0101332:	89 54 24 04          	mov    %edx,0x4(%esp)
f0101336:	89 ea                	mov    %ebp,%edx
f0101338:	8b 44 24 28          	mov    0x28(%esp),%eax
f010133c:	e8 cf fa ff ff       	call   f0100e10 <_ZL8printnumPFviPvES_yjii>
			break;
f0101341:	e9 3e fc ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0101346:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f010134a:	89 14 24             	mov    %edx,(%esp)
f010134d:	ff 54 24 28          	call   *0x28(%esp)
			break;
f0101351:	e9 2e fc ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0101356:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f010135a:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
f0101361:	ff 54 24 28          	call   *0x28(%esp)
			for (fmt--; fmt[-1] != '%'; fmt--)
f0101365:	89 5c 24 68          	mov    %ebx,0x68(%esp)
f0101369:	80 7b ff 25          	cmpb   $0x25,-0x1(%ebx)
f010136d:	0f 84 11 fc ff ff    	je     f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>
f0101373:	83 eb 01             	sub    $0x1,%ebx
f0101376:	80 7b ff 25          	cmpb   $0x25,-0x1(%ebx)
f010137a:	75 f7                	jne    f0101373 <_Z9vprintfmtPFviPvES_PKcPc+0x404>
f010137c:	89 5c 24 68          	mov    %ebx,0x68(%esp)
f0101380:	e9 ff fb ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
f0101385:	89 54 24 0c          	mov    %edx,0xc(%esp)
f0101389:	c7 44 24 08 ba 23 10 	movl   $0xf01023ba,0x8(%esp)
f0101390:	f0 
f0101391:	89 6c 24 04          	mov    %ebp,0x4(%esp)
f0101395:	8b 7c 24 28          	mov    0x28(%esp),%edi
f0101399:	89 3c 24             	mov    %edi,(%esp)
f010139c:	e8 a3 fb ff ff       	call   f0100f44 <_Z8printfmtPFviPvES_PKcz>
f01013a1:	e9 de fb ff ff       	jmp    f0100f84 <_Z9vprintfmtPFviPvES_PKcPc+0x15>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
f01013a6:	83 c4 4c             	add    $0x4c,%esp
f01013a9:	5b                   	pop    %ebx
f01013aa:	5e                   	pop    %esi
f01013ab:	5f                   	pop    %edi
f01013ac:	5d                   	pop    %ebp
f01013ad:	c3                   	ret    

f01013ae <_Z9vsnprintfPciPKcS_>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f01013ae:	83 ec 2c             	sub    $0x2c,%esp
f01013b1:	8b 44 24 30          	mov    0x30(%esp),%eax
f01013b5:	8b 54 24 34          	mov    0x34(%esp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f01013b9:	c7 44 24 1c 00 00 00 	movl   $0x0,0x1c(%esp)
f01013c0:	00 
f01013c1:	89 44 24 14          	mov    %eax,0x14(%esp)
f01013c5:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f01013c9:	89 4c 24 18          	mov    %ecx,0x18(%esp)

	if (buf == NULL || n < 1)
f01013cd:	85 c0                	test   %eax,%eax
f01013cf:	74 35                	je     f0101406 <_Z9vsnprintfPciPKcS_+0x58>
f01013d1:	85 d2                	test   %edx,%edx
f01013d3:	7e 31                	jle    f0101406 <_Z9vsnprintfPciPKcS_+0x58>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt(sprintputch, &b, fmt, ap);
f01013d5:	8b 44 24 3c          	mov    0x3c(%esp),%eax
f01013d9:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01013dd:	8b 44 24 38          	mov    0x38(%esp),%eax
f01013e1:	89 44 24 08          	mov    %eax,0x8(%esp)
f01013e5:	8d 44 24 14          	lea    0x14(%esp),%eax
f01013e9:	89 44 24 04          	mov    %eax,0x4(%esp)
f01013ed:	c7 04 24 28 0f 10 f0 	movl   $0xf0100f28,(%esp)
f01013f4:	e8 76 fb ff ff       	call   f0100f6f <_Z9vprintfmtPFviPvES_PKcPc>

	// null terminate the buffer
	*b.buf = '\0';
f01013f9:	8b 44 24 14          	mov    0x14(%esp),%eax
f01013fd:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f0101400:	8b 44 24 1c          	mov    0x1c(%esp),%eax
f0101404:	eb 05                	jmp    f010140b <_Z9vsnprintfPciPKcS_+0x5d>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f0101406:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f010140b:	83 c4 2c             	add    $0x2c,%esp
f010140e:	c3                   	ret    

f010140f <_Z8snprintfPciPKcz>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f010140f:	83 ec 1c             	sub    $0x1c,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f0101412:	8d 44 24 2c          	lea    0x2c(%esp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f0101416:	89 44 24 0c          	mov    %eax,0xc(%esp)
f010141a:	8b 44 24 28          	mov    0x28(%esp),%eax
f010141e:	89 44 24 08          	mov    %eax,0x8(%esp)
f0101422:	8b 44 24 24          	mov    0x24(%esp),%eax
f0101426:	89 44 24 04          	mov    %eax,0x4(%esp)
f010142a:	8b 44 24 20          	mov    0x20(%esp),%eax
f010142e:	89 04 24             	mov    %eax,(%esp)
f0101431:	e8 78 ff ff ff       	call   f01013ae <_Z9vsnprintfPciPKcS_>
	va_end(ap);

	return rc;
}
f0101436:	83 c4 1c             	add    $0x1c,%esp
f0101439:	c3                   	ret    
f010143a:	00 00                	add    %al,(%eax)
f010143c:	00 00                	add    %al,(%eax)
	...

f0101440 <_Z8readlinePKc>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f0101440:	57                   	push   %edi
f0101441:	56                   	push   %esi
f0101442:	53                   	push   %ebx
f0101443:	83 ec 10             	sub    $0x10,%esp
f0101446:	8b 44 24 20          	mov    0x20(%esp),%eax
	int i, c, echoing;

	if (prompt != NULL)
f010144a:	85 c0                	test   %eax,%eax
f010144c:	74 10                	je     f010145e <_Z8readlinePKc+0x1e>
		cprintf("%s", prompt);
f010144e:	89 44 24 04          	mov    %eax,0x4(%esp)
f0101452:	c7 04 24 ba 23 10 f0 	movl   $0xf01023ba,(%esp)
f0101459:	e8 e8 f6 ff ff       	call   f0100b46 <_Z7cprintfPKcz>

	i = 0;
	echoing = iscons(0) > 0;
f010145e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0101465:	e8 37 f4 ff ff       	call   f01008a1 <_Z6isconsi>
f010146a:	85 c0                	test   %eax,%eax
f010146c:	0f 9f c0             	setg   %al
f010146f:	0f b6 c0             	movzbl %al,%eax
f0101472:	89 c7                	mov    %eax,%edi
	int i, c, echoing;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
f0101474:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0) > 0;
	while (1) {
		c = getchar();
f0101479:	e8 13 f4 ff ff       	call   f0100891 <_Z7getcharv>
f010147e:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f0101480:	85 c0                	test   %eax,%eax
f0101482:	79 17                	jns    f010149b <_Z8readlinePKc+0x5b>
			cprintf("read error: %e\n", c);
f0101484:	89 44 24 04          	mov    %eax,0x4(%esp)
f0101488:	c7 04 24 9c 25 10 f0 	movl   $0xf010259c,(%esp)
f010148f:	e8 b2 f6 ff ff       	call   f0100b46 <_Z7cprintfPKcz>
			return NULL;
f0101494:	b8 00 00 00 00       	mov    $0x0,%eax
f0101499:	eb 6f                	jmp    f010150a <_Z8readlinePKc+0xca>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f010149b:	83 f8 08             	cmp    $0x8,%eax
f010149e:	74 05                	je     f01014a5 <_Z8readlinePKc+0x65>
f01014a0:	83 f8 7f             	cmp    $0x7f,%eax
f01014a3:	75 19                	jne    f01014be <_Z8readlinePKc+0x7e>
f01014a5:	85 f6                	test   %esi,%esi
f01014a7:	7e 15                	jle    f01014be <_Z8readlinePKc+0x7e>
			if (echoing)
f01014a9:	85 ff                	test   %edi,%edi
f01014ab:	74 0c                	je     f01014b9 <_Z8readlinePKc+0x79>
				cputchar('\b');
f01014ad:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
f01014b4:	e8 c8 f3 ff ff       	call   f0100881 <_Z8cputchari>
			i--;
f01014b9:	83 ee 01             	sub    $0x1,%esi
f01014bc:	eb bb                	jmp    f0101479 <_Z8readlinePKc+0x39>
		} else if (c >= ' ' && i < BUFLEN-1) {
f01014be:	83 fb 1f             	cmp    $0x1f,%ebx
f01014c1:	7e 1f                	jle    f01014e2 <_Z8readlinePKc+0xa2>
f01014c3:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f01014c9:	7f 17                	jg     f01014e2 <_Z8readlinePKc+0xa2>
			if (echoing)
f01014cb:	85 ff                	test   %edi,%edi
f01014cd:	74 08                	je     f01014d7 <_Z8readlinePKc+0x97>
				cputchar(c);
f01014cf:	89 1c 24             	mov    %ebx,(%esp)
f01014d2:	e8 aa f3 ff ff       	call   f0100881 <_Z8cputchari>
			buf[i++] = c;
f01014d7:	88 9e a0 62 11 f0    	mov    %bl,-0xfee9d60(%esi)
f01014dd:	83 c6 01             	add    $0x1,%esi
f01014e0:	eb 97                	jmp    f0101479 <_Z8readlinePKc+0x39>
		} else if (c == '\n' || c == '\r') {
f01014e2:	83 fb 0a             	cmp    $0xa,%ebx
f01014e5:	74 05                	je     f01014ec <_Z8readlinePKc+0xac>
f01014e7:	83 fb 0d             	cmp    $0xd,%ebx
f01014ea:	75 8d                	jne    f0101479 <_Z8readlinePKc+0x39>
			if (echoing)
f01014ec:	85 ff                	test   %edi,%edi
f01014ee:	66 90                	xchg   %ax,%ax
f01014f0:	74 0c                	je     f01014fe <_Z8readlinePKc+0xbe>
				cputchar('\n');
f01014f2:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
f01014f9:	e8 83 f3 ff ff       	call   f0100881 <_Z8cputchari>
			buf[i] = 0;
f01014fe:	c6 86 a0 62 11 f0 00 	movb   $0x0,-0xfee9d60(%esi)
			return buf;
f0101505:	b8 a0 62 11 f0       	mov    $0xf01162a0,%eax
		}
	}
}
f010150a:	83 c4 10             	add    $0x10,%esp
f010150d:	5b                   	pop    %ebx
f010150e:	5e                   	pop    %esi
f010150f:	5f                   	pop    %edi
f0101510:	c3                   	ret    
	...

f0101520 <_Z6strlenPKc>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f0101520:	8b 54 24 04          	mov    0x4(%esp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f0101524:	b8 00 00 00 00       	mov    $0x0,%eax
f0101529:	80 3a 00             	cmpb   $0x0,(%edx)
f010152c:	74 09                	je     f0101537 <_Z6strlenPKc+0x17>
		n++;
f010152e:	83 c0 01             	add    $0x1,%eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f0101531:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f0101535:	75 f7                	jne    f010152e <_Z6strlenPKc+0xe>
		n++;
	return n;
}
f0101537:	f3 c3                	repz ret 

f0101539 <_Z7strnlenPKcj>:

int
strnlen(const char *s, size_t size)
{
f0101539:	8b 4c 24 04          	mov    0x4(%esp),%ecx
		n++;
	return n;
}

int
strnlen(const char *s, size_t size)
f010153d:	8b 54 24 08          	mov    0x8(%esp),%edx
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0101541:	b8 00 00 00 00       	mov    $0x0,%eax
f0101546:	39 d0                	cmp    %edx,%eax
f0101548:	74 0b                	je     f0101555 <_Z7strnlenPKcj+0x1c>
f010154a:	80 3c 01 00          	cmpb   $0x0,(%ecx,%eax,1)
f010154e:	74 05                	je     f0101555 <_Z7strnlenPKcj+0x1c>
		n++;
f0101550:	83 c0 01             	add    $0x1,%eax
f0101553:	eb f1                	jmp    f0101546 <_Z7strnlenPKcj+0xd>
	return n;
}
f0101555:	f3 c3                	repz ret 

f0101557 <_Z6strcpyPcPKc>:

char *
strcpy(char *dst, const char *src)
{
f0101557:	53                   	push   %ebx
f0101558:	8b 44 24 08          	mov    0x8(%esp),%eax
f010155c:	8b 5c 24 0c          	mov    0xc(%esp),%ebx
	char *ret = dst;

	while ((*dst++ = *src++) != '\0')
f0101560:	ba 00 00 00 00       	mov    $0x0,%edx
f0101565:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
f0101569:	88 0c 10             	mov    %cl,(%eax,%edx,1)
f010156c:	83 c2 01             	add    $0x1,%edx
f010156f:	84 c9                	test   %cl,%cl
f0101571:	75 f2                	jne    f0101565 <_Z6strcpyPcPKc+0xe>
		/* do nothing */;
	return ret;
}
f0101573:	5b                   	pop    %ebx
f0101574:	c3                   	ret    

f0101575 <_Z7strncpyPcPKcj>:

char *
strncpy(char *dst, const char *src, size_t size)
{
f0101575:	56                   	push   %esi
f0101576:	53                   	push   %ebx
f0101577:	8b 44 24 0c          	mov    0xc(%esp),%eax
f010157b:	8b 54 24 10          	mov    0x10(%esp),%edx
f010157f:	8b 74 24 14          	mov    0x14(%esp),%esi
	size_t i;
	char *ret = dst;

	for (i = 0; i < size; i++) {
f0101583:	85 f6                	test   %esi,%esi
f0101585:	74 18                	je     f010159f <_Z7strncpyPcPKcj+0x2a>
f0101587:	b9 00 00 00 00       	mov    $0x0,%ecx
		*dst++ = *src;
f010158c:	0f b6 1a             	movzbl (%edx),%ebx
f010158f:	88 1c 08             	mov    %bl,(%eax,%ecx,1)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f0101592:	80 3a 01             	cmpb   $0x1,(%edx)
f0101595:	83 da ff             	sbb    $0xffffffff,%edx
strncpy(char *dst, const char *src, size_t size)
{
	size_t i;
	char *ret = dst;

	for (i = 0; i < size; i++) {
f0101598:	83 c1 01             	add    $0x1,%ecx
f010159b:	39 f1                	cmp    %esi,%ecx
f010159d:	75 ed                	jne    f010158c <_Z7strncpyPcPKcj+0x17>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f010159f:	5b                   	pop    %ebx
f01015a0:	5e                   	pop    %esi
f01015a1:	c3                   	ret    

f01015a2 <_Z7strlcpyPcPKcj>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f01015a2:	56                   	push   %esi
f01015a3:	53                   	push   %ebx
f01015a4:	8b 74 24 0c          	mov    0xc(%esp),%esi
f01015a8:	8b 4c 24 10          	mov    0x10(%esp),%ecx
f01015ac:	8b 54 24 14          	mov    0x14(%esp),%edx
	char *dst_in = dst;

	if (size > 0) {
f01015b0:	89 f0                	mov    %esi,%eax
f01015b2:	85 d2                	test   %edx,%edx
f01015b4:	74 17                	je     f01015cd <_Z7strlcpyPcPKcj+0x2b>
		while (--size > 0 && *src != '\0')
f01015b6:	83 ea 01             	sub    $0x1,%edx
f01015b9:	74 17                	je     f01015d2 <_Z7strlcpyPcPKcj+0x30>
f01015bb:	80 39 00             	cmpb   $0x0,(%ecx)
f01015be:	74 16                	je     f01015d6 <_Z7strlcpyPcPKcj+0x34>
			*dst++ = *src++;
f01015c0:	0f b6 19             	movzbl (%ecx),%ebx
f01015c3:	88 18                	mov    %bl,(%eax)
f01015c5:	83 c0 01             	add    $0x1,%eax
f01015c8:	83 c1 01             	add    $0x1,%ecx
f01015cb:	eb e9                	jmp    f01015b6 <_Z7strlcpyPcPKcj+0x14>
		*dst = '\0';
	}
	return dst - dst_in;
f01015cd:	29 f0                	sub    %esi,%eax
}
f01015cf:	5b                   	pop    %ebx
f01015d0:	5e                   	pop    %esi
f01015d1:	c3                   	ret    
strlcpy(char *dst, const char *src, size_t size)
{
	char *dst_in = dst;

	if (size > 0) {
		while (--size > 0 && *src != '\0')
f01015d2:	89 c2                	mov    %eax,%edx
f01015d4:	eb 02                	jmp    f01015d8 <_Z7strlcpyPcPKcj+0x36>
f01015d6:	89 c2                	mov    %eax,%edx
			*dst++ = *src++;
		*dst = '\0';
f01015d8:	c6 02 00             	movb   $0x0,(%edx)
f01015db:	eb f0                	jmp    f01015cd <_Z7strlcpyPcPKcj+0x2b>

f01015dd <_Z6strcmpPKcS0_>:
	return dst - dst_in;
}

int
strcmp(const char *p, const char *q)
{
f01015dd:	8b 4c 24 04          	mov    0x4(%esp),%ecx
f01015e1:	8b 54 24 08          	mov    0x8(%esp),%edx
	while (*p && *p == *q)
f01015e5:	0f b6 01             	movzbl (%ecx),%eax
f01015e8:	84 c0                	test   %al,%al
f01015ea:	74 0c                	je     f01015f8 <_Z6strcmpPKcS0_+0x1b>
f01015ec:	3a 02                	cmp    (%edx),%al
f01015ee:	75 08                	jne    f01015f8 <_Z6strcmpPKcS0_+0x1b>
		p++, q++;
f01015f0:	83 c1 01             	add    $0x1,%ecx
f01015f3:	83 c2 01             	add    $0x1,%edx
f01015f6:	eb ed                	jmp    f01015e5 <_Z6strcmpPKcS0_+0x8>
	return (unsigned char) *p - (unsigned char) *q;
f01015f8:	0f b6 c0             	movzbl %al,%eax
f01015fb:	0f b6 12             	movzbl (%edx),%edx
f01015fe:	29 d0                	sub    %edx,%eax
}
f0101600:	c3                   	ret    

f0101601 <_Z7strncmpPKcS0_j>:

int
strncmp(const char *p, const char *q, size_t n)
{
f0101601:	53                   	push   %ebx
f0101602:	8b 4c 24 08          	mov    0x8(%esp),%ecx
f0101606:	8b 5c 24 0c          	mov    0xc(%esp),%ebx
f010160a:	8b 54 24 10          	mov    0x10(%esp),%edx
	while (n > 0 && *p && *p == *q)
f010160e:	85 d2                	test   %edx,%edx
f0101610:	74 16                	je     f0101628 <_Z7strncmpPKcS0_j+0x27>
f0101612:	0f b6 01             	movzbl (%ecx),%eax
f0101615:	84 c0                	test   %al,%al
f0101617:	74 16                	je     f010162f <_Z7strncmpPKcS0_j+0x2e>
f0101619:	3a 03                	cmp    (%ebx),%al
f010161b:	75 12                	jne    f010162f <_Z7strncmpPKcS0_j+0x2e>
		n--, p++, q++;
f010161d:	83 ea 01             	sub    $0x1,%edx
f0101620:	83 c1 01             	add    $0x1,%ecx
f0101623:	83 c3 01             	add    $0x1,%ebx
f0101626:	eb e6                	jmp    f010160e <_Z7strncmpPKcS0_j+0xd>
	if (n == 0)
		return 0;
f0101628:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (unsigned char) *p - (unsigned char) *q;
}
f010162d:	5b                   	pop    %ebx
f010162e:	c3                   	ret    
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (unsigned char) *p - (unsigned char) *q;
f010162f:	0f b6 01             	movzbl (%ecx),%eax
f0101632:	0f b6 13             	movzbl (%ebx),%edx
f0101635:	29 d0                	sub    %edx,%eax
f0101637:	eb f4                	jmp    f010162d <_Z7strncmpPKcS0_j+0x2c>

f0101639 <_Z6strchrPKcc>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f0101639:	8b 44 24 04          	mov    0x4(%esp),%eax
f010163d:	0f b6 4c 24 08       	movzbl 0x8(%esp),%ecx
	for (; *s; s++)
f0101642:	0f b6 10             	movzbl (%eax),%edx
f0101645:	84 d2                	test   %dl,%dl
f0101647:	74 1b                	je     f0101664 <_Z6strchrPKcc+0x2b>
		if (*s == c)
f0101649:	38 ca                	cmp    %cl,%dl
f010164b:	75 09                	jne    f0101656 <_Z6strchrPKcc+0x1d>
f010164d:	f3 c3                	repz ret 
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f010164f:	83 c0 01             	add    $0x1,%eax
		if (*s == c)
f0101652:	38 ca                	cmp    %cl,%dl
f0101654:	74 13                	je     f0101669 <_Z6strchrPKcc+0x30>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f0101656:	0f b6 50 01          	movzbl 0x1(%eax),%edx
f010165a:	84 d2                	test   %dl,%dl
f010165c:	75 f1                	jne    f010164f <_Z6strchrPKcc+0x16>
		if (*s == c)
			return (char *) s;
	return 0;
f010165e:	b8 00 00 00 00       	mov    $0x0,%eax
f0101663:	c3                   	ret    
f0101664:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0101669:	f3 c3                	repz ret 

f010166b <_Z7strfindPKcc>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f010166b:	8b 44 24 04          	mov    0x4(%esp),%eax
f010166f:	0f b6 4c 24 08       	movzbl 0x8(%esp),%ecx
	for (; *s; s++)
f0101674:	0f b6 10             	movzbl (%eax),%edx
f0101677:	84 d2                	test   %dl,%dl
f0101679:	74 14                	je     f010168f <_Z7strfindPKcc+0x24>
		if (*s == c)
f010167b:	38 ca                	cmp    %cl,%dl
f010167d:	75 06                	jne    f0101685 <_Z7strfindPKcc+0x1a>
f010167f:	f3 c3                	repz ret 
f0101681:	38 ca                	cmp    %cl,%dl
f0101683:	74 0a                	je     f010168f <_Z7strfindPKcc+0x24>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f0101685:	83 c0 01             	add    $0x1,%eax
f0101688:	0f b6 10             	movzbl (%eax),%edx
f010168b:	84 d2                	test   %dl,%dl
f010168d:	75 f2                	jne    f0101681 <_Z7strfindPKcc+0x16>
		if (*s == c)
			break;
	return (char *) s;
}
f010168f:	f3 c3                	repz ret 

f0101691 <memset>:

asmlinkage void *
memset(void *v, int c, size_t n)
{
f0101691:	83 ec 0c             	sub    $0xc,%esp
f0101694:	89 1c 24             	mov    %ebx,(%esp)
f0101697:	89 74 24 04          	mov    %esi,0x4(%esp)
f010169b:	89 7c 24 08          	mov    %edi,0x8(%esp)
f010169f:	8b 7c 24 10          	mov    0x10(%esp),%edi
f01016a3:	8b 44 24 14          	mov    0x14(%esp),%eax
f01016a7:	8b 4c 24 18          	mov    0x18(%esp),%ecx
#if ASM
	if ((uintptr_t) v % 4 == 0 && n % 4 == 0) {
f01016ab:	f7 c7 03 00 00 00    	test   $0x3,%edi
f01016b1:	75 25                	jne    f01016d8 <memset+0x47>
f01016b3:	f6 c1 03             	test   $0x3,%cl
f01016b6:	75 20                	jne    f01016d8 <memset+0x47>
		c &= 0xFF;
f01016b8:	0f b6 d0             	movzbl %al,%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f01016bb:	89 d3                	mov    %edx,%ebx
f01016bd:	c1 e3 08             	shl    $0x8,%ebx
f01016c0:	89 d6                	mov    %edx,%esi
f01016c2:	c1 e6 18             	shl    $0x18,%esi
f01016c5:	89 d0                	mov    %edx,%eax
f01016c7:	c1 e0 10             	shl    $0x10,%eax
f01016ca:	09 f0                	or     %esi,%eax
f01016cc:	09 d0                	or     %edx,%eax
f01016ce:	09 d8                	or     %ebx,%eax
		asm volatile("cld; rep stosl\n"
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
f01016d0:	c1 e9 02             	shr    $0x2,%ecx
f01016d3:	fc                   	cld    
f01016d4:	f3 ab                	rep stos %eax,%es:(%edi)
f01016d6:	eb 03                	jmp    f01016db <memset+0x4a>
	} else
		asm volatile("cld; rep stosb\n"
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
f01016d8:	fc                   	cld    
f01016d9:	f3 aa                	rep stos %al,%es:(%edi)
	while (n-- > 0)
		*p++ = c;
#endif

	return v;
}
f01016db:	89 f8                	mov    %edi,%eax
f01016dd:	8b 1c 24             	mov    (%esp),%ebx
f01016e0:	8b 74 24 04          	mov    0x4(%esp),%esi
f01016e4:	8b 7c 24 08          	mov    0x8(%esp),%edi
f01016e8:	83 c4 0c             	add    $0xc,%esp
f01016eb:	c3                   	ret    

f01016ec <memmove>:

asmlinkage void *
memmove(void *dst, const void *src, size_t n)
{
f01016ec:	83 ec 08             	sub    $0x8,%esp
f01016ef:	89 34 24             	mov    %esi,(%esp)
f01016f2:	89 7c 24 04          	mov    %edi,0x4(%esp)
f01016f6:	8b 44 24 0c          	mov    0xc(%esp),%eax
f01016fa:	8b 74 24 10          	mov    0x10(%esp),%esi
f01016fe:	8b 4c 24 14          	mov    0x14(%esp),%ecx
#if ASM
	const char *s = (const char *) src;
	char *d = (char *) dst;

	if (s < d && s + n > d) {
f0101702:	39 c6                	cmp    %eax,%esi
f0101704:	73 36                	jae    f010173c <memmove+0x50>
f0101706:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f0101709:	39 d0                	cmp    %edx,%eax
f010170b:	73 2f                	jae    f010173c <memmove+0x50>
		s += n;
		d += n;
f010170d:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((uintptr_t) s % 4 == 0 && (uintptr_t) d % 4 == 0
f0101710:	f6 c2 03             	test   $0x3,%dl
f0101713:	75 1b                	jne    f0101730 <memmove+0x44>
f0101715:	f7 c7 03 00 00 00    	test   $0x3,%edi
f010171b:	75 13                	jne    f0101730 <memmove+0x44>
f010171d:	f6 c1 03             	test   $0x3,%cl
f0101720:	75 0e                	jne    f0101730 <memmove+0x44>
		    && n % 4 == 0)
			asm volatile("std; rep movsl\n"
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
f0101722:	83 ef 04             	sub    $0x4,%edi
f0101725:	8d 72 fc             	lea    -0x4(%edx),%esi
f0101728:	c1 e9 02             	shr    $0x2,%ecx
f010172b:	fd                   	std    
f010172c:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f010172e:	eb 09                	jmp    f0101739 <memmove+0x4d>
		else
			asm volatile("std; rep movsb\n"
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
f0101730:	83 ef 01             	sub    $0x1,%edi
f0101733:	8d 72 ff             	lea    -0x1(%edx),%esi
f0101736:	fd                   	std    
f0101737:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f0101739:	fc                   	cld    
f010173a:	eb 20                	jmp    f010175c <memmove+0x70>
	} else {
		if ((uintptr_t) s % 4 == 0 && (uintptr_t) d % 4 == 0
f010173c:	f7 c6 03 00 00 00    	test   $0x3,%esi
f0101742:	75 13                	jne    f0101757 <memmove+0x6b>
f0101744:	a8 03                	test   $0x3,%al
f0101746:	75 0f                	jne    f0101757 <memmove+0x6b>
f0101748:	f6 c1 03             	test   $0x3,%cl
f010174b:	75 0a                	jne    f0101757 <memmove+0x6b>
		    && n % 4 == 0)
			asm volatile("cld; rep movsl\n"
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
f010174d:	c1 e9 02             	shr    $0x2,%ecx
f0101750:	89 c7                	mov    %eax,%edi
f0101752:	fc                   	cld    
f0101753:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f0101755:	eb 05                	jmp    f010175c <memmove+0x70>
		else
			asm volatile("cld; rep movsb\n"
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
f0101757:	89 c7                	mov    %eax,%edi
f0101759:	fc                   	cld    
f010175a:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
			*d++ = *s++;

#endif

	return dst;
}
f010175c:	8b 34 24             	mov    (%esp),%esi
f010175f:	8b 7c 24 04          	mov    0x4(%esp),%edi
f0101763:	83 c4 08             	add    $0x8,%esp
f0101766:	c3                   	ret    

f0101767 <memcpy>:

asmlinkage void *
memcpy(void *dst, const void *src, size_t n)
{
f0101767:	83 ec 08             	sub    $0x8,%esp
f010176a:	89 34 24             	mov    %esi,(%esp)
f010176d:	89 7c 24 04          	mov    %edi,0x4(%esp)
f0101771:	8b 44 24 0c          	mov    0xc(%esp),%eax
f0101775:	8b 74 24 10          	mov    0x10(%esp),%esi
f0101779:	8b 4c 24 14          	mov    0x14(%esp),%ecx
#if ASM
	const char *s = (const char *) src;
	char *d = (char *) dst;

	if ((uintptr_t) s % 4 == 0 && (uintptr_t) d % 4 == 0
f010177d:	f7 c6 03 00 00 00    	test   $0x3,%esi
f0101783:	75 13                	jne    f0101798 <memcpy+0x31>
f0101785:	a8 03                	test   $0x3,%al
f0101787:	75 0f                	jne    f0101798 <memcpy+0x31>
f0101789:	f6 c1 03             	test   $0x3,%cl
f010178c:	75 0a                	jne    f0101798 <memcpy+0x31>
	    && n % 4 == 0)
		asm volatile("cld; rep movsl\n"
			:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
f010178e:	c1 e9 02             	shr    $0x2,%ecx
f0101791:	89 c7                	mov    %eax,%edi
f0101793:	fc                   	cld    
f0101794:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f0101796:	eb 05                	jmp    f010179d <memcpy+0x36>
	else
		asm volatile("cld; rep movsb\n"
			:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
f0101798:	89 c7                	mov    %eax,%edi
f010179a:	fc                   	cld    
f010179b:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
	while (n-- > 0)
		*d++ = *s++;
#endif

	return dst;
}
f010179d:	8b 34 24             	mov    (%esp),%esi
f01017a0:	8b 7c 24 04          	mov    0x4(%esp),%edi
f01017a4:	83 c4 08             	add    $0x8,%esp
f01017a7:	c3                   	ret    

f01017a8 <memcmp>:

asmlinkage int
memcmp(const void *v1, const void *v2, size_t n)
{
f01017a8:	57                   	push   %edi
f01017a9:	56                   	push   %esi
f01017aa:	53                   	push   %ebx
f01017ab:	8b 5c 24 10          	mov    0x10(%esp),%ebx
f01017af:	8b 74 24 14          	mov    0x14(%esp),%esi
f01017b3:	8b 7c 24 18          	mov    0x18(%esp),%edi
		if (*s1 != *s2)
			return *s1 - *s2;
		s1++, s2++;
	}

	return 0;
f01017b7:	b8 00 00 00 00       	mov    $0x0,%eax
memcmp(const void *v1, const void *v2, size_t n)
{
	const unsigned char *s1 = (const unsigned char *) v1;
	const unsigned char *s2 = (const unsigned char *) v2;

	while (n-- > 0) {
f01017bc:	85 ff                	test   %edi,%edi
f01017be:	74 37                	je     f01017f7 <memcmp+0x4f>
		if (*s1 != *s2)
f01017c0:	0f b6 03             	movzbl (%ebx),%eax
f01017c3:	0f b6 0e             	movzbl (%esi),%ecx
memcmp(const void *v1, const void *v2, size_t n)
{
	const unsigned char *s1 = (const unsigned char *) v1;
	const unsigned char *s2 = (const unsigned char *) v2;

	while (n-- > 0) {
f01017c6:	83 ef 01             	sub    $0x1,%edi
f01017c9:	ba 00 00 00 00       	mov    $0x0,%edx
		if (*s1 != *s2)
f01017ce:	38 c8                	cmp    %cl,%al
f01017d0:	74 1c                	je     f01017ee <memcmp+0x46>
f01017d2:	eb 10                	jmp    f01017e4 <memcmp+0x3c>
f01017d4:	0f b6 44 13 01       	movzbl 0x1(%ebx,%edx,1),%eax
f01017d9:	83 c2 01             	add    $0x1,%edx
f01017dc:	0f b6 0c 16          	movzbl (%esi,%edx,1),%ecx
f01017e0:	38 c8                	cmp    %cl,%al
f01017e2:	74 0a                	je     f01017ee <memcmp+0x46>
			return *s1 - *s2;
f01017e4:	0f b6 c0             	movzbl %al,%eax
f01017e7:	0f b6 c9             	movzbl %cl,%ecx
f01017ea:	29 c8                	sub    %ecx,%eax
f01017ec:	eb 09                	jmp    f01017f7 <memcmp+0x4f>
memcmp(const void *v1, const void *v2, size_t n)
{
	const unsigned char *s1 = (const unsigned char *) v1;
	const unsigned char *s2 = (const unsigned char *) v2;

	while (n-- > 0) {
f01017ee:	39 fa                	cmp    %edi,%edx
f01017f0:	75 e2                	jne    f01017d4 <memcmp+0x2c>
		if (*s1 != *s2)
			return *s1 - *s2;
		s1++, s2++;
	}

	return 0;
f01017f2:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01017f7:	5b                   	pop    %ebx
f01017f8:	5e                   	pop    %esi
f01017f9:	5f                   	pop    %edi
f01017fa:	c3                   	ret    

f01017fb <memfind>:

asmlinkage void *
memfind(const void *v, int c, size_t n)
{
f01017fb:	53                   	push   %ebx
f01017fc:	8b 5c 24 08          	mov    0x8(%esp),%ebx
	const unsigned char *s = (const unsigned char *) v;
	const unsigned char *ends = s + n;
f0101800:	89 da                	mov    %ebx,%edx
f0101802:	03 54 24 10          	add    0x10(%esp),%edx
}

asmlinkage void *
memfind(const void *v, int c, size_t n)
{
	const unsigned char *s = (const unsigned char *) v;
f0101806:	89 d8                	mov    %ebx,%eax
	const unsigned char *ends = s + n;

	for (; s < ends; s++)
f0101808:	39 d3                	cmp    %edx,%ebx
f010180a:	73 16                	jae    f0101822 <memfind+0x27>
		if (*s == (unsigned char) c)
f010180c:	0f b6 4c 24 0c       	movzbl 0xc(%esp),%ecx
f0101811:	38 0b                	cmp    %cl,(%ebx)
f0101813:	75 06                	jne    f010181b <memfind+0x20>
f0101815:	eb 0b                	jmp    f0101822 <memfind+0x27>
f0101817:	38 08                	cmp    %cl,(%eax)
f0101819:	74 07                	je     f0101822 <memfind+0x27>
memfind(const void *v, int c, size_t n)
{
	const unsigned char *s = (const unsigned char *) v;
	const unsigned char *ends = s + n;

	for (; s < ends; s++)
f010181b:	83 c0 01             	add    $0x1,%eax
f010181e:	39 d0                	cmp    %edx,%eax
f0101820:	75 f5                	jne    f0101817 <memfind+0x1c>
		if (*s == (unsigned char) c)
			break;
	return (void *) s;
}
f0101822:	5b                   	pop    %ebx
f0101823:	c3                   	ret    

f0101824 <_Z6strtolPKcPPci>:

long
strtol(const char *s, char **endptr, int base)
{
f0101824:	55                   	push   %ebp
f0101825:	57                   	push   %edi
f0101826:	56                   	push   %esi
f0101827:	53                   	push   %ebx
f0101828:	8b 54 24 14          	mov    0x14(%esp),%edx
f010182c:	8b 74 24 18          	mov    0x18(%esp),%esi
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0101830:	0f b6 02             	movzbl (%edx),%eax
f0101833:	3c 20                	cmp    $0x20,%al
f0101835:	74 04                	je     f010183b <_Z6strtolPKcPPci+0x17>
f0101837:	3c 09                	cmp    $0x9,%al
f0101839:	75 0e                	jne    f0101849 <_Z6strtolPKcPPci+0x25>
		s++;
f010183b:	83 c2 01             	add    $0x1,%edx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f010183e:	0f b6 02             	movzbl (%edx),%eax
f0101841:	3c 20                	cmp    $0x20,%al
f0101843:	74 f6                	je     f010183b <_Z6strtolPKcPPci+0x17>
f0101845:	3c 09                	cmp    $0x9,%al
f0101847:	74 f2                	je     f010183b <_Z6strtolPKcPPci+0x17>
		s++;

	// plus/minus sign
	if (*s == '+')
f0101849:	3c 2b                	cmp    $0x2b,%al
f010184b:	75 0a                	jne    f0101857 <_Z6strtolPKcPPci+0x33>
		s++;
f010184d:	83 c2 01             	add    $0x1,%edx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f0101850:	bf 00 00 00 00       	mov    $0x0,%edi
f0101855:	eb 10                	jmp    f0101867 <_Z6strtolPKcPPci+0x43>
f0101857:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f010185c:	3c 2d                	cmp    $0x2d,%al
f010185e:	75 07                	jne    f0101867 <_Z6strtolPKcPPci+0x43>
		s++, neg = 1;
f0101860:	83 c2 01             	add    $0x1,%edx
f0101863:	66 bf 01 00          	mov    $0x1,%di

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0101867:	83 7c 24 1c 00       	cmpl   $0x0,0x1c(%esp)
f010186c:	0f 94 c0             	sete   %al
f010186f:	74 07                	je     f0101878 <_Z6strtolPKcPPci+0x54>
f0101871:	83 7c 24 1c 10       	cmpl   $0x10,0x1c(%esp)
f0101876:	75 18                	jne    f0101890 <_Z6strtolPKcPPci+0x6c>
f0101878:	80 3a 30             	cmpb   $0x30,(%edx)
f010187b:	75 13                	jne    f0101890 <_Z6strtolPKcPPci+0x6c>
f010187d:	80 7a 01 78          	cmpb   $0x78,0x1(%edx)
f0101881:	75 0d                	jne    f0101890 <_Z6strtolPKcPPci+0x6c>
		s += 2, base = 16;
f0101883:	83 c2 02             	add    $0x2,%edx
f0101886:	c7 44 24 1c 10 00 00 	movl   $0x10,0x1c(%esp)
f010188d:	00 
f010188e:	eb 1c                	jmp    f01018ac <_Z6strtolPKcPPci+0x88>
	else if (base == 0 && s[0] == '0')
f0101890:	84 c0                	test   %al,%al
f0101892:	74 18                	je     f01018ac <_Z6strtolPKcPPci+0x88>
		s++, base = 8;
	else if (base == 0)
		base = 10;
f0101894:	c7 44 24 1c 0a 00 00 	movl   $0xa,0x1c(%esp)
f010189b:	00 
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
		s += 2, base = 16;
	else if (base == 0 && s[0] == '0')
f010189c:	80 3a 30             	cmpb   $0x30,(%edx)
f010189f:	75 0b                	jne    f01018ac <_Z6strtolPKcPPci+0x88>
		s++, base = 8;
f01018a1:	83 c2 01             	add    $0x1,%edx
f01018a4:	c7 44 24 1c 08 00 00 	movl   $0x8,0x1c(%esp)
f01018ab:	00 
	else if (base == 0)
		base = 10;
f01018ac:	b8 00 00 00 00       	mov    $0x0,%eax

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f01018b1:	0f b6 0a             	movzbl (%edx),%ecx
f01018b4:	8d 69 d0             	lea    -0x30(%ecx),%ebp
f01018b7:	89 eb                	mov    %ebp,%ebx
f01018b9:	80 fb 09             	cmp    $0x9,%bl
f01018bc:	77 08                	ja     f01018c6 <_Z6strtolPKcPPci+0xa2>
			dig = *s - '0';
f01018be:	0f be c9             	movsbl %cl,%ecx
f01018c1:	83 e9 30             	sub    $0x30,%ecx
f01018c4:	eb 22                	jmp    f01018e8 <_Z6strtolPKcPPci+0xc4>
		else if (*s >= 'a' && *s <= 'z')
f01018c6:	8d 69 9f             	lea    -0x61(%ecx),%ebp
f01018c9:	89 eb                	mov    %ebp,%ebx
f01018cb:	80 fb 19             	cmp    $0x19,%bl
f01018ce:	77 08                	ja     f01018d8 <_Z6strtolPKcPPci+0xb4>
			dig = *s - 'a' + 10;
f01018d0:	0f be c9             	movsbl %cl,%ecx
f01018d3:	83 e9 57             	sub    $0x57,%ecx
f01018d6:	eb 10                	jmp    f01018e8 <_Z6strtolPKcPPci+0xc4>
		else if (*s >= 'A' && *s <= 'Z')
f01018d8:	8d 69 bf             	lea    -0x41(%ecx),%ebp
f01018db:	89 eb                	mov    %ebp,%ebx
f01018dd:	80 fb 19             	cmp    $0x19,%bl
f01018e0:	77 18                	ja     f01018fa <_Z6strtolPKcPPci+0xd6>
			dig = *s - 'A' + 10;
f01018e2:	0f be c9             	movsbl %cl,%ecx
f01018e5:	83 e9 37             	sub    $0x37,%ecx
		else
			break;
		if (dig >= base)
f01018e8:	3b 4c 24 1c          	cmp    0x1c(%esp),%ecx
f01018ec:	7d 10                	jge    f01018fe <_Z6strtolPKcPPci+0xda>
			break;
		s++, val = (val * base) + dig;
f01018ee:	83 c2 01             	add    $0x1,%edx
f01018f1:	0f af 44 24 1c       	imul   0x1c(%esp),%eax
f01018f6:	01 c8                	add    %ecx,%eax
		s++, base = 8;
	else if (base == 0)
		base = 10;

	// digits
	while (1) {
f01018f8:	eb b7                	jmp    f01018b1 <_Z6strtolPKcPPci+0x8d>

		if (*s >= '0' && *s <= '9')
			dig = *s - '0';
		else if (*s >= 'a' && *s <= 'z')
			dig = *s - 'a' + 10;
		else if (*s >= 'A' && *s <= 'Z')
f01018fa:	89 c1                	mov    %eax,%ecx
f01018fc:	eb 02                	jmp    f0101900 <_Z6strtolPKcPPci+0xdc>
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
f01018fe:	89 c1                	mov    %eax,%ecx
			break;
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
f0101900:	85 f6                	test   %esi,%esi
f0101902:	74 02                	je     f0101906 <_Z6strtolPKcPPci+0xe2>
		*endptr = (char *) s;
f0101904:	89 16                	mov    %edx,(%esi)
	return (neg ? -val : val);
f0101906:	85 ff                	test   %edi,%edi
f0101908:	74 04                	je     f010190e <_Z6strtolPKcPPci+0xea>
f010190a:	89 c8                	mov    %ecx,%eax
f010190c:	f7 d8                	neg    %eax
}
f010190e:	5b                   	pop    %ebx
f010190f:	5e                   	pop    %esi
f0101910:	5f                   	pop    %edi
f0101911:	5d                   	pop    %ebp
f0101912:	c3                   	ret    
	...

f0101920 <__udivdi3>:
f0101920:	83 ec 1c             	sub    $0x1c,%esp
f0101923:	8b 44 24 2c          	mov    0x2c(%esp),%eax
f0101927:	89 74 24 10          	mov    %esi,0x10(%esp)
f010192b:	8b 4c 24 28          	mov    0x28(%esp),%ecx
f010192f:	8b 74 24 20          	mov    0x20(%esp),%esi
f0101933:	89 7c 24 14          	mov    %edi,0x14(%esp)
f0101937:	8b 7c 24 24          	mov    0x24(%esp),%edi
f010193b:	85 c0                	test   %eax,%eax
f010193d:	89 6c 24 18          	mov    %ebp,0x18(%esp)
f0101941:	89 cd                	mov    %ecx,%ebp
f0101943:	89 74 24 08          	mov    %esi,0x8(%esp)
f0101947:	75 37                	jne    f0101980 <__udivdi3+0x60>
f0101949:	39 f9                	cmp    %edi,%ecx
f010194b:	77 5b                	ja     f01019a8 <__udivdi3+0x88>
f010194d:	85 c9                	test   %ecx,%ecx
f010194f:	75 0b                	jne    f010195c <__udivdi3+0x3c>
f0101951:	b8 01 00 00 00       	mov    $0x1,%eax
f0101956:	31 d2                	xor    %edx,%edx
f0101958:	f7 f1                	div    %ecx
f010195a:	89 c1                	mov    %eax,%ecx
f010195c:	89 f8                	mov    %edi,%eax
f010195e:	31 d2                	xor    %edx,%edx
f0101960:	f7 f1                	div    %ecx
f0101962:	89 c7                	mov    %eax,%edi
f0101964:	89 f0                	mov    %esi,%eax
f0101966:	f7 f1                	div    %ecx
f0101968:	89 fa                	mov    %edi,%edx
f010196a:	89 c6                	mov    %eax,%esi
f010196c:	89 f0                	mov    %esi,%eax
f010196e:	8b 74 24 10          	mov    0x10(%esp),%esi
f0101972:	8b 7c 24 14          	mov    0x14(%esp),%edi
f0101976:	8b 6c 24 18          	mov    0x18(%esp),%ebp
f010197a:	83 c4 1c             	add    $0x1c,%esp
f010197d:	c3                   	ret    
f010197e:	66 90                	xchg   %ax,%ax
f0101980:	31 d2                	xor    %edx,%edx
f0101982:	31 f6                	xor    %esi,%esi
f0101984:	39 f8                	cmp    %edi,%eax
f0101986:	77 e4                	ja     f010196c <__udivdi3+0x4c>
f0101988:	0f bd c8             	bsr    %eax,%ecx
f010198b:	83 f1 1f             	xor    $0x1f,%ecx
f010198e:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0101992:	75 24                	jne    f01019b8 <__udivdi3+0x98>
f0101994:	3b 6c 24 08          	cmp    0x8(%esp),%ebp
f0101998:	76 04                	jbe    f010199e <__udivdi3+0x7e>
f010199a:	39 f8                	cmp    %edi,%eax
f010199c:	73 ce                	jae    f010196c <__udivdi3+0x4c>
f010199e:	31 d2                	xor    %edx,%edx
f01019a0:	be 01 00 00 00       	mov    $0x1,%esi
f01019a5:	eb c5                	jmp    f010196c <__udivdi3+0x4c>
f01019a7:	90                   	nop
f01019a8:	89 f0                	mov    %esi,%eax
f01019aa:	89 fa                	mov    %edi,%edx
f01019ac:	f7 f1                	div    %ecx
f01019ae:	31 d2                	xor    %edx,%edx
f01019b0:	89 c6                	mov    %eax,%esi
f01019b2:	eb b8                	jmp    f010196c <__udivdi3+0x4c>
f01019b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f01019b8:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f01019bd:	89 c2                	mov    %eax,%edx
f01019bf:	b8 20 00 00 00       	mov    $0x20,%eax
f01019c4:	2b 44 24 04          	sub    0x4(%esp),%eax
f01019c8:	89 ee                	mov    %ebp,%esi
f01019ca:	d3 e2                	shl    %cl,%edx
f01019cc:	89 c1                	mov    %eax,%ecx
f01019ce:	d3 ee                	shr    %cl,%esi
f01019d0:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f01019d5:	09 d6                	or     %edx,%esi
f01019d7:	89 fa                	mov    %edi,%edx
f01019d9:	89 74 24 0c          	mov    %esi,0xc(%esp)
f01019dd:	8b 74 24 08          	mov    0x8(%esp),%esi
f01019e1:	d3 e5                	shl    %cl,%ebp
f01019e3:	89 c1                	mov    %eax,%ecx
f01019e5:	d3 ea                	shr    %cl,%edx
f01019e7:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f01019ec:	d3 e7                	shl    %cl,%edi
f01019ee:	89 c1                	mov    %eax,%ecx
f01019f0:	d3 ee                	shr    %cl,%esi
f01019f2:	09 fe                	or     %edi,%esi
f01019f4:	89 f0                	mov    %esi,%eax
f01019f6:	f7 74 24 0c          	divl   0xc(%esp)
f01019fa:	89 d7                	mov    %edx,%edi
f01019fc:	89 c6                	mov    %eax,%esi
f01019fe:	f7 e5                	mul    %ebp
f0101a00:	39 d7                	cmp    %edx,%edi
f0101a02:	72 13                	jb     f0101a17 <__udivdi3+0xf7>
f0101a04:	8b 6c 24 08          	mov    0x8(%esp),%ebp
f0101a08:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101a0d:	d3 e5                	shl    %cl,%ebp
f0101a0f:	39 c5                	cmp    %eax,%ebp
f0101a11:	73 07                	jae    f0101a1a <__udivdi3+0xfa>
f0101a13:	39 d7                	cmp    %edx,%edi
f0101a15:	75 03                	jne    f0101a1a <__udivdi3+0xfa>
f0101a17:	83 ee 01             	sub    $0x1,%esi
f0101a1a:	31 d2                	xor    %edx,%edx
f0101a1c:	e9 4b ff ff ff       	jmp    f010196c <__udivdi3+0x4c>
	...

f0101a30 <__umoddi3>:
f0101a30:	83 ec 1c             	sub    $0x1c,%esp
f0101a33:	89 6c 24 18          	mov    %ebp,0x18(%esp)
f0101a37:	8b 6c 24 2c          	mov    0x2c(%esp),%ebp
f0101a3b:	8b 44 24 20          	mov    0x20(%esp),%eax
f0101a3f:	89 74 24 10          	mov    %esi,0x10(%esp)
f0101a43:	8b 4c 24 28          	mov    0x28(%esp),%ecx
f0101a47:	8b 74 24 24          	mov    0x24(%esp),%esi
f0101a4b:	85 ed                	test   %ebp,%ebp
f0101a4d:	89 7c 24 14          	mov    %edi,0x14(%esp)
f0101a51:	89 44 24 08          	mov    %eax,0x8(%esp)
f0101a55:	89 cf                	mov    %ecx,%edi
f0101a57:	89 04 24             	mov    %eax,(%esp)
f0101a5a:	89 f2                	mov    %esi,%edx
f0101a5c:	75 1a                	jne    f0101a78 <__umoddi3+0x48>
f0101a5e:	39 f1                	cmp    %esi,%ecx
f0101a60:	76 4e                	jbe    f0101ab0 <__umoddi3+0x80>
f0101a62:	f7 f1                	div    %ecx
f0101a64:	89 d0                	mov    %edx,%eax
f0101a66:	31 d2                	xor    %edx,%edx
f0101a68:	8b 74 24 10          	mov    0x10(%esp),%esi
f0101a6c:	8b 7c 24 14          	mov    0x14(%esp),%edi
f0101a70:	8b 6c 24 18          	mov    0x18(%esp),%ebp
f0101a74:	83 c4 1c             	add    $0x1c,%esp
f0101a77:	c3                   	ret    
f0101a78:	39 f5                	cmp    %esi,%ebp
f0101a7a:	77 54                	ja     f0101ad0 <__umoddi3+0xa0>
f0101a7c:	0f bd c5             	bsr    %ebp,%eax
f0101a7f:	83 f0 1f             	xor    $0x1f,%eax
f0101a82:	89 44 24 04          	mov    %eax,0x4(%esp)
f0101a86:	75 60                	jne    f0101ae8 <__umoddi3+0xb8>
f0101a88:	3b 0c 24             	cmp    (%esp),%ecx
f0101a8b:	0f 87 07 01 00 00    	ja     f0101b98 <__umoddi3+0x168>
f0101a91:	89 f2                	mov    %esi,%edx
f0101a93:	8b 34 24             	mov    (%esp),%esi
f0101a96:	29 ce                	sub    %ecx,%esi
f0101a98:	19 ea                	sbb    %ebp,%edx
f0101a9a:	89 34 24             	mov    %esi,(%esp)
f0101a9d:	8b 04 24             	mov    (%esp),%eax
f0101aa0:	8b 74 24 10          	mov    0x10(%esp),%esi
f0101aa4:	8b 7c 24 14          	mov    0x14(%esp),%edi
f0101aa8:	8b 6c 24 18          	mov    0x18(%esp),%ebp
f0101aac:	83 c4 1c             	add    $0x1c,%esp
f0101aaf:	c3                   	ret    
f0101ab0:	85 c9                	test   %ecx,%ecx
f0101ab2:	75 0b                	jne    f0101abf <__umoddi3+0x8f>
f0101ab4:	b8 01 00 00 00       	mov    $0x1,%eax
f0101ab9:	31 d2                	xor    %edx,%edx
f0101abb:	f7 f1                	div    %ecx
f0101abd:	89 c1                	mov    %eax,%ecx
f0101abf:	89 f0                	mov    %esi,%eax
f0101ac1:	31 d2                	xor    %edx,%edx
f0101ac3:	f7 f1                	div    %ecx
f0101ac5:	8b 04 24             	mov    (%esp),%eax
f0101ac8:	f7 f1                	div    %ecx
f0101aca:	eb 98                	jmp    f0101a64 <__umoddi3+0x34>
f0101acc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0101ad0:	89 f2                	mov    %esi,%edx
f0101ad2:	8b 74 24 10          	mov    0x10(%esp),%esi
f0101ad6:	8b 7c 24 14          	mov    0x14(%esp),%edi
f0101ada:	8b 6c 24 18          	mov    0x18(%esp),%ebp
f0101ade:	83 c4 1c             	add    $0x1c,%esp
f0101ae1:	c3                   	ret    
f0101ae2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
f0101ae8:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101aed:	89 e8                	mov    %ebp,%eax
f0101aef:	bd 20 00 00 00       	mov    $0x20,%ebp
f0101af4:	2b 6c 24 04          	sub    0x4(%esp),%ebp
f0101af8:	89 fa                	mov    %edi,%edx
f0101afa:	d3 e0                	shl    %cl,%eax
f0101afc:	89 e9                	mov    %ebp,%ecx
f0101afe:	d3 ea                	shr    %cl,%edx
f0101b00:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101b05:	09 c2                	or     %eax,%edx
f0101b07:	8b 44 24 08          	mov    0x8(%esp),%eax
f0101b0b:	89 14 24             	mov    %edx,(%esp)
f0101b0e:	89 f2                	mov    %esi,%edx
f0101b10:	d3 e7                	shl    %cl,%edi
f0101b12:	89 e9                	mov    %ebp,%ecx
f0101b14:	d3 ea                	shr    %cl,%edx
f0101b16:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101b1b:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0101b1f:	d3 e6                	shl    %cl,%esi
f0101b21:	89 e9                	mov    %ebp,%ecx
f0101b23:	d3 e8                	shr    %cl,%eax
f0101b25:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101b2a:	09 f0                	or     %esi,%eax
f0101b2c:	8b 74 24 08          	mov    0x8(%esp),%esi
f0101b30:	f7 34 24             	divl   (%esp)
f0101b33:	d3 e6                	shl    %cl,%esi
f0101b35:	89 74 24 08          	mov    %esi,0x8(%esp)
f0101b39:	89 d6                	mov    %edx,%esi
f0101b3b:	f7 e7                	mul    %edi
f0101b3d:	39 d6                	cmp    %edx,%esi
f0101b3f:	89 c1                	mov    %eax,%ecx
f0101b41:	89 d7                	mov    %edx,%edi
f0101b43:	72 3f                	jb     f0101b84 <__umoddi3+0x154>
f0101b45:	39 44 24 08          	cmp    %eax,0x8(%esp)
f0101b49:	72 35                	jb     f0101b80 <__umoddi3+0x150>
f0101b4b:	8b 44 24 08          	mov    0x8(%esp),%eax
f0101b4f:	29 c8                	sub    %ecx,%eax
f0101b51:	19 fe                	sbb    %edi,%esi
f0101b53:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101b58:	89 f2                	mov    %esi,%edx
f0101b5a:	d3 e8                	shr    %cl,%eax
f0101b5c:	89 e9                	mov    %ebp,%ecx
f0101b5e:	d3 e2                	shl    %cl,%edx
f0101b60:	0f b6 4c 24 04       	movzbl 0x4(%esp),%ecx
f0101b65:	09 d0                	or     %edx,%eax
f0101b67:	89 f2                	mov    %esi,%edx
f0101b69:	d3 ea                	shr    %cl,%edx
f0101b6b:	8b 74 24 10          	mov    0x10(%esp),%esi
f0101b6f:	8b 7c 24 14          	mov    0x14(%esp),%edi
f0101b73:	8b 6c 24 18          	mov    0x18(%esp),%ebp
f0101b77:	83 c4 1c             	add    $0x1c,%esp
f0101b7a:	c3                   	ret    
f0101b7b:	90                   	nop
f0101b7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0101b80:	39 d6                	cmp    %edx,%esi
f0101b82:	75 c7                	jne    f0101b4b <__umoddi3+0x11b>
f0101b84:	89 d7                	mov    %edx,%edi
f0101b86:	89 c1                	mov    %eax,%ecx
f0101b88:	2b 4c 24 0c          	sub    0xc(%esp),%ecx
f0101b8c:	1b 3c 24             	sbb    (%esp),%edi
f0101b8f:	eb ba                	jmp    f0101b4b <__umoddi3+0x11b>
f0101b91:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f0101b98:	39 f5                	cmp    %esi,%ebp
f0101b9a:	0f 82 f1 fe ff ff    	jb     f0101a91 <__umoddi3+0x61>
f0101ba0:	e9 f8 fe ff ff       	jmp    f0101a9d <__umoddi3+0x6d>
