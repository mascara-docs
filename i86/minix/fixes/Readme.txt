204fixes.txt 
These are fixes for Minix 2.0.4
asw revised 2006-06-17

Descriptions are listed here newest first: 

man0606.tar.Z (posted 2006-06-17)
-------------
This archive contains new man pages for Minix 2 and Minix 3: 
ftp.1		(updated for ftp 1.01, released Feb 2005)
http_status.5
httpd.8		(updated for httpd 0.995, released May 2006)
httpd.conf.5
mtools.1
tcpd.8
urlget.1
All of the programs documented by these pages were part of the standard
Minix 2 distribution, except for httpd. However, httpd has been 
distributed (without man pages) with Minix 3.1.0 and later Minix releases.

204ether.tar.Z (posted 2005-03-29)
--------------
This adds support for AMD Lance ethernet adapters. This adapter is
emulated by VMWare, this makes networked Minix on VMWare possible. 

pci_tbl.tar.Z (updated 2005-04-15)
-------------
This adds support for a number of PCI devices.  It duplicates the
changes to pci_table.c included in the FIX.TAZ archive, and adds
support for additional items and comments regarding all changes.  Note
that there have been numerous updates to this patch, be sure you have
the latest. The contents of pci_tbl.tar.Z are also available for
separate download:
pci_table.cdif 
pci_table.txt 

FTPWARNING.txt (2005-03-25)
--------------
Read this if you use an ftp server (ftpd) on a Minix system.

ftpd200.tar.Z (update added to fixes directory 2005-03-20)
-------------
The ftpd provided with Minix 2.0.4 (and earlier releases) has a
security vulnerability and should be replaced with version 2.00.  See
the ftpd200.txt file here and the Security Advisory at
http://minix1.hampshire.edu/news/ftpsecadv.html and
http://minix1.bio.umass.edu/news/ftpsecadv.html. 

manfixes.tar.Z (2005-01-16)
------------
This archive contains updated Minix 2.0.4 man pages: corrections of
minor errors, some just typos and some more important, in ash.1,
eject.1, elvis.1, flex.1, flexdoc.1, fsck.1, join.1, makewhatis.1,
tar.1, and substantially revised versions of ps.1 and touch.1.  (The
ps.1 page is the same as in the psfix.tar.Z file posted earlier).

psfix.tar.Z (2004-12-15)
-----------
This contains a new ps.1 man page, as well as a cleaned-up ps.c which
has a few new comments and from which a few unnecessary variables have
been removed.  It is not functionally different from the previous
version. 

whatisfx.tar.Z (2004-12-12)
--------------
Changes to the way grep (1) parses regular expressions caused the
whatis (1) script to fail to function correctly.  These changes were
introduced with the grep version distributed with Minix 2.0.3.  The
problem with whatis was noticed soon after the patches for grep were
announced, but somehow were overlooked.  This package contains a simple
patch for /usr/src/commands/scripts/whatis.sh that makes it compatible
with grep again.

tzfix.tar.Z (2004-10-31)
-----------
When the change from summer to winter time is supposed to occur on the
last Sunday in a month (i.e., as in the EST5EDT,M4.1.0/2,M10.5.0/2 time
zone of the eastern USA) it was being erroneously detected by Minix as
taking place on the fourth Sunday in months with 5 Sundays.  This
package contains a patch for /usr/src/lib/ansi/misc.c to correct this.

dp8390fx.tar.Z (2004-08-08)
--------------
This fixes a minor error which may have existed in the DP8390 support
for a long time but which only was discovered after problems were seen
using Minix with the emulated NE2000 support provided by the Bochs
emulator.  Details are in the file dp8390.tx written by Giovanni
Falzoni, who also provided the patch.
 
dp8390.txt 04 tape blocks
dp8390.cdiff 04 tape blocks

cronfix.tar.Z (2004-05-08)
-------------
The patches in here fix a couple of errors in the cron and crontab
commands.  The errors have been present since the Minix 2.0.3 release.
Read cronfix.txt for more information Both patches are from Michael
Temari. Unpack the archive in /usr/src/commands/cron.

cronfix.txt 02 tape blocks
crontab.cdiff 01 tape blocks
tab.cdiff 02 tape blocks
crontab.c 013 tape blocks
tab.c 046 tape blocks

FIX.TAZ (2003-11-30)
-------
This was released almost immediately upon the release of Minix 2.0.4 in
November 2003, and is found in the 2.0.4/src directory.  I have linked
it here only to draw attention to it -- if you carefully installed
Minix 2.0.4 according to the top-level documentation page you probably
already installed these patches as the last part of the installation
process.  However, if you are using the DOSMinix prepackaged Minix
system these are not installed.  These patches are needed only if you
will use the RTL8139 ethernet adapter support.

src/kernel/rtl8139.c 0164 tape blocks
src/kernel/pci_table.c 021 tape blocks
src/kernel/pci.c 071 tape blocks
