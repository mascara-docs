#
# $FreeBSD: src/gnu/lib/libgmp/Makefile.inc,v 1.1.2.1 1999/09/05 11:05:17 peter Exp $
#

SHLIB_MAJOR=	3
SHLIB_MINOR=	0

.if exists(${.OBJDIR})
CFLAGS+=	-I${.OBJDIR}
.else
CFLAGS+=	-I${.CURDIR}
.endif
