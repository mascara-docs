#!./perl

# $Header: /home/ncvs/src/gnu/usr.bin/perl/perl/t/base/Attic/if.t,v 1.1.1.1 1994/09/10 06:27:39 gclarkii Exp $

print "1..2\n";

# first test to see if we can run the tests.

$x = 'test';
if ($x eq $x) { print "ok 1\n"; } else { print "not ok 1\n";}
if ($x ne $x) { print "not ok 2\n"; } else { print "ok 2\n";}
