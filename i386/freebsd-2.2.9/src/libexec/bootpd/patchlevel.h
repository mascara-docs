/* 
 * patchlevel.h 
 *
 * $FreeBSD: src/libexec/bootpd/patchlevel.h,v 1.2.2.1 1999/09/05 11:20:10 peter Exp $
 */

#define VERSION 	"2.4"
#define PATCHLEVEL	3
