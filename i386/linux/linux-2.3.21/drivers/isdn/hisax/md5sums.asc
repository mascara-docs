-----BEGIN PGP SIGNED MESSAGE-----

# This are valid md5sums for certificated HiSax driver.
# The certification is valid only if the md5sums of all files match.
# The certification is valid only for ELSA QuickStep cards and
# Eicon Technology Diva 2.01 PCI cards in the moment.
# Read ../../../Documentation/isdn/HiSax.cert for more informations.
# 
0cc164fadd4ec0e2983ec9735e209cbd  isac.c
5fe8cb5526c78c91f61b0a94a423ea5d  isdnl1.c
3b9522e8bf9e1c3e7848d729fc3dc05d  isdnl2.c
f4184a50e35e5b568608e6cb7a693319  isdnl3.c
ef70f4269fdc2ca15100f9b776afaa0d  tei.c
65be616dd9d0e06c788d4fdd0fe5fe0a  callc.c
bf9605b36429898f7be6630034e83230  cert.c
97c5e31c2739665b9c2976a30ce0b357  l3dss1.c
b674eee9314a7cc413971c84003cf1d2  l3_1tr6.c
51b2ef1efb221bb09fd08ab28bd2c565  elsa.c
24cda374da44b57f6a1bb215424267b5  diva.c
# end of md5sums

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3i
Charset: noconv

iQCVAwUBN8RgFjpxHvX/mS9tAQFzFQP/dOgnppDIm5ug1hnlWjQ/0BVurKEEJ64r
DYDHwkcog+0gVE/EB1A7WUDqpFEnj52OZeoVinCfdVuVjP8IkrAJ8dCONsnXjBXz
pzM+FunP1LFxuv2TVM0f642j98JxS8rObGWH8ZwY36P2QfNp47zorO2F9WvdCkuz
sxJUtMUOlQ8=
=8uEP
-----END PGP SIGNATURE-----
