# $FreeBSD: src/usr.sbin/pkg_install/Makefile.inc,v 1.3.8.2 1999/09/05 11:40:42 peter Exp $

.if exists(${.OBJDIR}/../lib)
LIBINSTALL=	${.OBJDIR}/../lib/libinstall.a
.else
LIBINSTALL=	${.CURDIR}/../lib/libinstall.a
.endif

# Inherit BINDIR from one level up.
.include "../Makefile.inc"
