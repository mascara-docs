#	from $NetBSD: Makefile.inc,v 1.7 1995/02/27 13:06:20 cgd Exp $
# $FreeBSD: src/lib/libc/nls/Makefile.inc,v 1.2.6.2 1999/09/05 11:16:37 peter Exp $

.PATH: ${.CURDIR}/../libc/nls

SRCS+=	catclose.c catgets.c catopen.c msgcat.c

# Only build man pages with libc.
.if ${LIB} == "c"
MAN3+=  nls/catclose.3 nls/catgets.3 nls/catopen.3
.endif
