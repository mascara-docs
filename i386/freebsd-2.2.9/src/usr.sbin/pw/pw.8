.\" Copyright (C) 1996
.\" David L. Nugent.  All rights reserved.
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY DAVID L. NUGENT AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL DAVID L. NUGENT OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/usr.sbin/pw/pw.8,v 1.1.1.1.2.7 1999/09/05 11:41:09 peter Exp $
.\"
.Dd December 9, 1996
.Dt PW 8
.Os
.Sh NAME
.Nm pw
.Nd create, remove, modify & display system users and groups
.Sh SYNOPSIS
.Nm pw
.Ar useradd
.Op name|uid
.Op Fl C Ar config
.Op Fl q
.Op Fl n Ar name
.Op Fl u Ar uid
.Op Fl c Ar comment
.Op Fl d Ar dir
.Op Fl e Ar date
.Op Fl p Ar date
.Op Fl g Ar group
.Op Fl G Ar grouplist
.Op Fl m
.Op Fl k Ar dir
.Op Fl s Ar shell
.Op Fl o
.Op Fl L Ar class
.Op Fl h Ar fd
.Op Fl N
.Op Fl P
.Nm pw
.Ar useradd
.Op name|uid
.Op Fl D
.Op Fl C Ar config
.Op Fl q
.Op Fl b Ar dir
.Op Fl e Ar days
.Op Fl p Ar days
.Op Fl g Ar group
.Op Fl G Ar grouplist
.Op Fl k Ar dir
.Op Fl u Ar min,max
.Op Fl i Ar min,max
.Op Fl w Ar method
.Op Fl s Ar shell
.Nm pw
.Ar userdel
.Op name|uid
.Op Fl n Ar name
.Op Fl u Ar uid
.Op Fl r
.Nm pw
.Ar usermod
.Op name|uid
.Op Fl C Ar config
.Op Fl q
.Op Fl n Ar name
.Op Fl u Ar uid
.Op Fl c Ar comment
.Op Fl d Ar dir
.Op Fl e Ar date
.Op Fl p Ar date
.Op Fl g Ar group
.Op Fl G Ar grouplist
.Op Fl l Ar name
.Op Fl m
.Op Fl k Ar dir
.Op Fl w Ar method
.Op Fl s Ar shell
.Op Fl L Ar class
.Op Fl h Ar fd
.Op Fl N
.Op Fl P
.Nm pw
.Ar usershow
.Op name|uid
.Op Fl n Ar name
.Op Fl u Ar uid
.Op Fl F
.Op Fl P
.Op Fl a
.Nm pw
.Ar usernext
.Op Fl C Ar config
.Op Fl q
.Nm pw
.Ar groupadd
.Op group|gid
.Op Fl C Ar config
.Op Fl q
.Op Fl n Ar group
.Op Fl g Ar gid
.Op Fl M Ar members
.Op Fl o
.Op Fl h Ar fd
.Op Fl N
.Op Fl P
.Nm pw
.Ar groupdel
.Op group|gid
.Op Fl n Ar name
.Op Fl g Ar gid
.Nm pw
.Ar groupmod
.Op group|gid
.Op Fl C Ar config
.Op Fl q
.Op Fl F
.Op Fl n Ar name
.Op Fl g Ar gid
.Op Fl l Ar name
.Op Fl M Ar members
.Op Fl m Ar newmembers
.Op Fl h Ar fd
.Op Fl N
.Op Fl P
.Nm pw
.Ar groupshow
.Op group|gid
.Op Fl n Ar name
.Op Fl g Ar gid
.Op Fl F
.Op Fl P
.Op Fl a
.Nm pw
.Ar groupnext
.Op Fl C Ar config
.Op Fl q
.Sh DESCRIPTION
.Nm Pw
is a command-line based editor for the system
.Ar user
and
.Ar group
files, allowing the superuser an easy to use and standardized way of adding,
modifying and removing users and groups.
Note that
.Nm
only operates on the local user and group files; NIS users and groups must be
maintained on the NIS server.
.Nm Pw
handles updating the 
.Pa passwd , 
.Pa master.passwd , 
.Pa group
and the secure and insecure
password database files, and must be run as root.
.Pp
The first one or two keywords provided on
.Xr pw 8 's
command line provide the context for the remainder of the arguments.
One of the keywords
.Ar user
and
.Ar group
may be combined or provided separately with
.Ar add ,
.Ar del ,
.Ar mod ,
.Ar show ,
or
.Ar next ,
and may be specified in either order (ie. showuser, usershow, show user and user show
are all considered to be the same thing).
This flexibility is useful for interactive scripts which call
.Nm
for the actual user and group database manipulation.
Following these keywords, you may optionally specify the user or group name or numeric
id as an alternative to using the
.Fl n Ar name ,
.Fl u Ar uid ,
.Fl g Ar gid
options.
.Pp
The following flags are common to all modes of operation:
.Pp
.Bl -tag -width "-G grouplist"
.It Fl C Ar config
By default,
.Nm
reads the file
.Pa /etc/pw.conf
to obtain policy information on how new user accounts and groups are to be created,
and the
.Fl C
option specifies a different configuration file.
Most of the contents in the configuration file may be overridden via command line
options, but it may be more useful to set up standard information for addition of
new accounts in the configuration file.
.It Fl q
Use of this option causes
.Nm
to suppress error messages, which may be useful in interactive environments where it
is preferable to interpret status codes returned by
.Nm
rather than messing up a carefully formatted display.
.It Fl N
This option is available in add and modify operations, and causes
.Nm
to skip updating the user/group databases and instead print the result
of the operation without actually performing it.
You may use the
.Fl P
option to switch between standard passwd and readable formats.
.El
.Pp
.Sh USER OPTIONS
The following options apply to the
.Ar useradd ,
and
.Ar usermod ,
commands:
.Pp
.Bl -tag -width "-G grouplist"
.It Fl n Ar name
Specify the user/account name.
.It Fl u Ar uid
Specify the user/account numeric id.
.Pp
Usually, you need only to provide one or the other of these options, as the account
name will imply the uid, and vice versa.
Also, you may provide either the account or userid immediately after the
.Ar useradd ,
.Ar userdel ,
.Ar usermod
or
.Ar usershow
keyword on the command line without the need to use
.Ql Fl n
or
.Ql Fl u .
There are times, however, were you need to provide both.
For example, when changing the uid of an existing user with
.Ar usermod ,
or overriding the default uid when creating a new account.
If you wish
.Nm
to automatically allocate the uid to a new user on
.Ar useradd ,
then you should
.Em not
use the
.Ql Fl u
option.
.El
.Pp
Options available with both
.Ar useradd
and
.Ar usermod
are:
.Bl -tag -width "-G grouplist"
.It Fl c Ar comment
This field sets the contents of the passwd GECOS field, which normally contains up
to four comma-separated fields containing the user's full name, office or location,
work and home phone numbers.
These sub-fields are used by convention only, however, and are optional.
If this field is to contain spaces, you need to quote the comment itself with double
quotes
.Ql \&" .
Avoid using commas in this field as these are used as sub-field separators, and the
colon
.Ql \&:
character also cannot be used as this is the field separator in the passwd file.
.It Fl d Ar dir
This option sets the account's home directory.
Normally, you will only use this if the home directory is to be different from the
default (which is determined from pw.conf, which specifies the base home directory
- normally
.Pa /home
- with the account name as a subdirectory).
.It Fl e Ar date
Set the account's expiration date. 
Format of the date is either a UNIX time in decimal, or a date in
.Ql \& dd-mmm-yy[yy]
format, where dd is the day, mmm is the month, either in numeric or alphabetic format
('Jan', 'Feb', etc) and year is either a two or four digit year.
This option also accepts a relative date in the form
.Ql \&+n[mhdwoy]
where
.Ql \&n
is a decimal, octal (leading 0) or hexadecimal (leading 0x) digit followed by the
number of Minutes, Hours, Days, Weeks, Months or Years from the current date at
which the expiry date is to be set.
.It Fl p Ar date
Set the account's password expiration date.
This field is identical to the account expiration date option, except that it
applies to forced password changes.
The same formats are accepted as with the account expiration option.
.It Fl g Ar group
Set the account's primary group to the given group.
.Ar group
may be either the group name or its corresponding group id number.
.It Fl G Ar grouplist
Sets the additional groups to which an account belongs.
.Ar grouplist
is a comma-separated list or group names or group ids.
When adding a user, the user's name is added to the group lists in
.Pa /etc/group ,
and when editing a user, the user's name is also added to the group lists, and
removed from any groups not specified in
.Ar grouplist .
Note: a user should not be added to their primary group in
.Pa /etc/group .
Also, group membership changes do not take effect immediately for current logins,
only logins subsequent to the change.
.It Fl L Ar class
This option sets the login class for the user being created.
See
.Xr login.conf 5
for more information on user classes.
.It Fl m
This option instructs
.Nm
to attempt to create the user's home directory.
While primarily useful when adding a new account with
.Ar useradd ,
this may also be of use when moving an existing user's home directory elsewhere on
the filesystem.
The new home directory is populated with the contents of the
.Ar skeleton
directory, which typically contains a set of shell configuration files that the
user may personalize to taste.
When
.Ql Fl m
is used on an account with
.Ar usermod ,
any existing configuration files in the user's home directory are
.Em not
overwritten with the prototype files.
.Pp
When a user's home directory is created, it will be default be as a subdirectory of the
.Ar basehome
directory specified with the
.Ql Fl b Ar dir
option (see below), and will be named the same as the account.
This may be overridden with the
.Ql Fl d Ar dir
option on the command line, if desired.
.It Fl k Ar dir
Set the
.Ar skeleton
subdirectory, from which the basic startup and configuration files are copied when
the user's home directory is created.
This option only has meaning when used with
.Ql Fl D
(see below) or
.Ql Fl m .
.It Fl s Ar shell
Set or changes the user's login shell to
.Ar shell .
If the path to the shell program is omitted,
.Nm
searches the
.Ar shellpath
specified in
.Pa /etc/pw.conf
and fills it in as appropriate.
Note that unless you have a specific reason to do so, you should avoid
specifying the path - this will allow
.Nm
to validate that the program exists and is executable.
Specifying a full path (or supplying a blank "" shell) avoids this check
and allows for such entries as
.Pa /nonexistent
that should be set for accounts not intended for interactive login.
.It Fl L Ar class
Set the
.Em class
field in the user's passwd record.
This field is not currently used, but will be in the future used to specify a
.Em termcap
entry like tag (see
.Xr passwd 5
for details).
.It Fl h Ar fd
This option provides a special interface by which interactive scripts can
set an account password using
.Nm pw .
Because the command line and environment are fundamental insecure mechanisms
by which programs can accept information,
.Nm
will only allow setting of account and group passwords via a file descriptor
(usually a pipe between an interactive script and the program).
.Ar sh ,
.Ar bash ,
.Ar ksh
and
.Ar perl
all posses mechanisms by which this can be done.
Alternatively,
.Nm
will prompt for the user's password if
.Ql Fl h Ar 0
is given, nominating
.Em stdin
as the file descriptor on which to read the password.
Note that this password will be read once and once only and is intended
for use by a script or similar rather than interactive use.
If you wish to have new password confirmation along the lines of
.Xr passwd 1 ,
this must be implemented as part of the interactive script that calls
.Nm pw .
.Pp
If a value of
.Ql \&-
is given as the argument
.Ar fd ,
then the password will be set to
.Ql \&* ,
rendering the account inaccessible via passworded login.
.El
.Pp
It is possible to use
.Ar useradd
to create a new account that duplicates an existing user id.
While this is normally considered an error and will be rejected, the
.Ql Fl o
option overrides the check for duplicates and allows the duplication of
the user id.
This may be useful if you allow the same user to login under
different contexts (different group allocations, different home
directory, different shell) while providing basically the same
permissions for access to the user's files in each account.
.Pp
The
.Ar useradd
command also has the ability to set new user and group defaults by using the
.Ql Fl D
option.
Instead of adding a new user,
.Nm
writes a new set of defaults to its configuration file,
.Pa /etc/pw.conf .
When using the
.Ql Fl D
option, you must not use either
.Ql Fl n Ar name
or
.Ql Fl u Ar uid
or an error will result.
Use of
.Ql Fl D
changes the meaning of several command line switches in the
.Ar useradd
command.
These are:
.Bl -tag -width "-G grouplist"
.It Fl D
Set default values in
.Pa /etc/pw.conf
configuration file, or a different named configuration file if the
.Ql Fl C Ar config
option is used.
.It Fl b Ar dir
Set the root directory in which user home directories are created.
The default value for this is
.Pa /home ,
but it may be set elsewhere as desired.
.It Fl e Ar days
Set the default account expiration period in days.
Unlike use without
.Ql Fl D ,
the argument must be numeric, which specifies the number of days after creation when
the account is to expire.
A value of 0 suppresses automatic calculation of the expiry date.
.It Fl p Ar days
Set the default password expiration period in days.
.It Fl g Ar group
Set the default group for new users.
If a blank group is specified using
.Ql Fl g Ar \&"" ,
then new users will be allocated their own private primary group (a new group created
with the same name as their login name).
If a group is supplied, either its name or uid may be given as an argument.
.It Fl G Ar grouplist
Set the default groups in which new users are made members.
This is a separate set of groups from the primary group, and you should avoid
nominating the same group as both the primary and in extra groups.
In other words, these extra groups determine membership in groups
.Em other than
the primary group.
.Ar grouplist
is a comma-separated list of group names or ids, or a mixture of both, and are always
stored in
.Pa /etc/pw.conf
by their symbolic names.
.It Fl L Ar class
This option sets the default login class for new users.
.It Fl k Ar dir
Set the default
.Em skeleton
directory, from which prototype shell and other initialization files are copied when
.Nm
creates a user's home directory.
.It Fl u Ar min,max , Fl i Ar min,max
These options set the minimum and maximum user and group ids allocated for new accounts
and groups created by
.Nm pw .
The default values for each is 1000 minimum and 32000 maximum.
.Ar min
and
.Ar max
are both numbers, where max must be greater than min, and both must be between 0
and 32767.
In general, user and group ids less than 100 are reserved for use by the system,
and numbers greater than 32000 may also be reserved for special purposes (used by
some system daemons).
.It Fl w Ar method
The
.Ql Fl w
option sets the default method used to set passwords for newly created user accounts.
.Ar method
is one of:
.Pp
.Bl -tag -width random -offset indent -compact
.It no
disable login on newly created accounts
.It yes
force the password to be the account name
.It none
force a blank password
.It random
generate a random password
.El
.Pp
The
.Ql \&random
or
.Ql \&no
methods are the most secure; in the former case,
.Nm
generates a password and prints it to stdout, which is suitable where you issue
users with passwords to access their accounts rather than having the user nominate
their own (possibly poorly chosen) password.
The
.Ql \&no
method requires that the superuser use
.Xr passwd 1
to render the account accessible with a password.
.El
.Pp
The
.Ar userdel
command has only three valid options. The
.Ql Fl n Ar name
and
.Ql Fl u Ar uid
options have already been covered above.
The additional option is:
.Bl -tag -width "-G grouplist"
.It Fl r
This tells
.Nm
to remove the user's home directory and all of its contents.
.Nm Pw
errs on the side of caution when removing files from the system.
Firstly, it will not do so if the uid of the account being removed is also used by
another account on the system, and the 'home' directory in the password file is
a valid path that commences with the character
.Ql \&/ .
Secondly, it will only remove files and directories that are actually owned by
the user, or symbolic links owned by anyone under the user's home directory.
Finally, after deleting all contents owned by the user only empty directories
will be removed.
If any additional cleanup work is required, this is left to the administrator.
.El
.Pp
Mail spool files and crontabs are always removed when an account is deleted as these
are unconditionally attached to the user name.
Jobs queued for processing by
.Ar at
are also removed if the user's uid is unique (not also used by another account on the
system).
.Pp
The
.Ar usershow
command allows viewing of an account in one of two formats.
By default, the format is identical to the format used in
.Pa /etc/master.passwd
with the password field replaced with a
.Ql \&* .
If the
.Ql Fl P
option is used, then
.Nm
outputs the account details in a more human readable form.
The
.Ql Fl a
option lists all users currently on file.
.Pp
The command
.Ar usernext
returns the next available user and group ids separated by a colon.
This is normally of interest only to interactive scripts or front-ends
that use
.Nm pw .
.Pp
.Sh GROUP OPTIONS
The
.Ql Fl C Ar config
and
.Ql Fl q
options (explained at the start of the previous section) are available
with the group manipulation commands.
Other common options to all group-related commands are:
.Bl -tag -width "-m newmembers"
.It Fl n Ar name
Specify the group name.
.It Fl g Ar gid
Specify the group numeric id.
.Pp
As with the account name and id fields, you will usually only need
to supply one of these, as the group name implies the uid and vice
versa.
You will only need to use both when setting a specific group id
against a new group or when changing the uid of an existing group.
.It Fl M Ar memberlist
This option provides an alternative way to add existing users to a
new group (in groupadd) or replace an existing membership list (in
groupmod).
.Ar memberlist
is a comma separated list of valid and existing user names or uids.
.It Fl m Ar newmembers
Similar to
.Op M ,
this option allows the
.Em addition
of existing users to a group without first replacing the existing list of
members.
Login names or user ids may be used, and duplicated users are automatically
and silently eliminated.
.El
.Pp
.Ar groupadd
also has a
.Ql Fl o
option that allows allocation of an existing group id to new group.
The default action is to reject an attempt to add a group, and this option overrides
the check for duplicate group ids.
There is rarely any need to duplicate a group id.
.Pp
The
.Ar groupmod
command adds one additonal option:
.Pp
.Bl -tag -width "-m newmembers"
.It Fl l Ar name
This option allows changing of an existing group name to
.Ql \&name .
The new name must not already exist, and any attempt to duplicate an existing group
name will be rejected.
.El
.Pp
Options for
.Ar groupshow
are the same as for
.Ar usershow ,
with the
.Ql Fl g Ar gid
replacing
.Ql Fl u Ar uid
to specify the group id.
.Pp
The command
.Ar groupnext
returns the next available group id on standard output.
.Sh DIAGNOSTICS
.Nm Pw
returns EXIT_SUCCESS on successful operation, otherwise one of the
following exit codes defined by
.Xr sysexits 3
as follows:
.Bl -tag -width xxxx
.It EX_USAGE
.Bl -bullet -compact
.It
Command line syntax errors (invalid keyword, unknown option).
.El
.It EX_NOPERM
.Bl -bullet -compact
.It
Attempting to run one of the update modes as non-root.
.El
.It EX_OSERR
.Bl -bullet -compact
.It
Memory allocation error.
.It
Read error from password file descriptor.
.El
.It EX_DATAERR
.Bl -bullet -compact
.It
Bad or invalid data provided or missing on the command line or
via the password flie descriptor.
.It
Attempted to remove, rename root account or change its uid.
.El
.It EX_OSFILE
.Bl -bullet -compact
.It
Skeleton directory is invalid or does not exist.
.It
Base home directory is invalid or does not exist.
.It
Invalid or non-existant shell specified.
.El
.It EX_NOUSER
.Bl -bullet -compact
.It
User, user id, group or group id specified does not exist.
.It
User or group recorded added or modified unexpectedly disappeared.
.El
.It EX_SOFTWARE
.Bl -bullet -compact
.It
No more group or user ids available within specified range.
.El
.It EX_IOERR
.Bl -bullet -compact
.It
Unable to rewrite configuration file.
.It
Error updating group or user database files.
.It
Update error for passwd or group database files.
.El
.It EX_CONFIG
.Bl -bullet -compact
.It
No base home directory configured.
.El
.El
.Pp
.Sh NOTES
For a summary of options available with each command, you can use
.Dl pw [command] help
For example,
.Dl pw useradd help
lists all available options for the useradd operation.
.Sh FILES
.Bl -tag -width /etc/master.passwd.new -compact
.It Pa /etc/master.passwd
The user database
.It Pa /etc/passwd 
A Version 7 format password file
.It Pa /etc/login.conf
The user capabilities database
.It Pa /etc/group
The group database
.It Pa /etc/master.passwd.new
Temporary copy of the master password file
.It Pa /etc/passwd.new
Temporary copy of the Version 7 password file
.It Pa /etc/group.new
Temporary copy of the group file
.It Pa /etc/pw.conf
Pw default options file
.El
.Sh SEE ALSO
.Xr chpass 1 ,
.Xr passwd 1 ,
.Xr group 5 ,
.Xr login.conf 5 ,
.Xr passwd 5 ,
.Xr pw.conf 5 ,
.Xr pwd_mkdb 8 ,
.Xr vipw 8
.Sh HISTORY
.Nm Pw
was written to mimic many of the options used in the SYSV
.Em shadow
support suite, but is modified for passwd and group fields specific to
the
.Bx 4.4
operating system, and combines all of the major elements
into a single command.
