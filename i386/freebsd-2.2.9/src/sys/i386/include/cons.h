/*
 * Console support headers should be in <machine/cons.h> since MI software
 * needs to access these functions.  In the mean time, just include the
 * header where it sits.
 *
 * $FreeBSD: src/sys/i386/include/cons.h,v 1.2.12.1 1999/09/05 08:11:42 peter Exp $
 */
#include <i386/i386/cons.h>
