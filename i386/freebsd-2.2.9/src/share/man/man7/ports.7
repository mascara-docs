.\"
.\" Copyright (c) 1997 David E. O'Brien
.\"
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man7/ports.7,v 1.1.2.6 1999/09/05 11:27:09 peter Exp $
.\"
.Dd January 25, 1998
.Dt PORTS 7
.Os FreeBSD 2.2
.Sh NAME
.Nm ports
.Nd contributed applications
.Sh DESCRIPTION
The
.Nm FreeBSD Ports Collection
offers a simple way for users and
administrators to install applications.
Each 
.Em port
contains any patches necessary to make the original
application source code compile and run on BSD.  Compiling an
application is as simple as typing
.Ic make build
in the port directory!  The 
.Ql Pa Makefile
automatically fetches the
application source code, either from a local disk or via ftp, unpacks it
on your system, applies the patches, and compiles it.  If all goes well,
simply type
.Ic make install
to install the application.
.Pp
For more information about using ports, see
.Nm The Ports Collection
(file:/usr/share/doc/handbook/ports.html --or--
http://www.freebsd.org/handbook/ports.html).
For information about creating new ports, see
.Nm Porting applications
(file:/usr/share/doc/handbook/porting.html --or--
http://www.freebsd.org/handbook/porting.html).
Both are part of the FreeBSD Handbook.
.Pp
.Sh TARGETS
.Pp
Some of the targets work recursively through subdirectories.
This lets you, for example, install all of the biology
ports.  The targets that do this are
.Ar build , checksum , clean , configure , extract , fetch , install ,
and
.Ar package .
.Pp
The following targets will be run automatically by each proceeding
target in order.  That is,
.Ar build
will be run
.Pq if necessary
by
.Ar install ,
and so on all the way to
.Ar fetch .
You will usually only target
.Ar install .
.Bl -tag -width configure
.It Ar fetch
Fetch all of the files needed to build this port from the site(s)
listed in MASTER_SITES and PATCH_SITES.  See
.Ev FETCH_CMD
and
.Ev MASTER_SITE_OVERRIDE .
.It Ar checksum
Verify that the fetched distfile matches the one the port was tested against.
Defining
.Ev NO_CHECKSUM
will skip this step.
.It Ar depends
Install
.Pq or compile if only compilation is necessary
any dependencies of the current port.  When called by the
.Ar extract
or
.Ar fetch
targets, this is run in piecemeal as
.Ar fetch-depends ,
.Ar build-depends ,
etc.  Defining
.Ev NO_DEPENDS
will skip this step.
.It Ar extract
Expand the distfile into a work directory.
.It Ar patch
Apply any patches that are necessary for the port.
.It Ar configure
Configure the port.  Some ports will ask you questions during
this stage.  See
.Ev INTERACTIVE
and
.Ev BATCH .
.It Ar build
Build the the port.  This is the same as calling the
.Ar all
target.
.It Ar install
Install the the port and register it with the package system.  This
is all you really need to do.
.El
.Pp
The following targets are not run during the normal install process.
.Bl -tag -width fetch-list
.It Ar fetch-list
Show list of files needed to be fetched in order to build the port.
.It Ar depends-list package-depends
Print a list of all the compile and run dependencies, and dependencies
of those dependencies.
.It Ar clean
Remove the expanded source code.  This recurses to dependencies unless
.Ev NOCLEANDEPENDS
is defined.
.It Ar distclean
Remove the port's distfile(s) and perform the
.Ar clean
operation.  The
.Sq clean
portion recurses to dependencies unless
.Ev NOCLEANDEPENDS
is defined, but the
.Sq distclean
portion never recurses
.Pq this is perhaps a bug .
.It Ar reinstall
Use this to restore a port after using
.Xr pkg_delete 1
when you should have used
.Ar deinstall .
.It Ar deinstall
Remove an installed port from the system, similar to
.Xr pkg_delete 1 .
.It Ar package
Make a binary package for the port.  The port will be installed if it
hasn't already been.  The package is a .tgz file that you can use to
install the port on other machines with
.Xr pkg_add 1 .
If the directory specified by
.Ev PACKAGES
does not exist the package will be put into the current directory.
See
.Ev PKGREPOSITORY
and
.Ev PKGFILE .
.It Ar readmes
Create a port's
.Pa README.html .
You may need to
.Xr fetch 1
ftp://ftp.freebsd.org/pub/FreeBSD/FreeBSD-current/ports/templates/README.port
and set
.Ev TEMPLATES
to its directory.  This is intended for would-be release engineers -- most
people can just read
.Pa pkg/COMMENT
and
.Pa pkg/DESCR .
.El
.Sh ENVIRONMENT VARIABLES
You can change all of these.
.Bl -tag -width MASTER_SITES
.It Ev PORTSDIR
Location of the ports tree.  This is
.Pa /usr/ports
on
.\" .Fx
FreeBSD
and
.\" .Ox ,
OpenBSD
and
.Pa /usr/pkgsrc
on
.Nx .
.It Ev DISTDIR
Where to find/put distfiles, normally
.Pa distfiles/
in
.Ev PORTSDIR .
.It Ev PACKAGES
Used only for the
.Ar package
target; the base directory for the packages tree, normally
.Pa packages/
in
.Ev PORTSDIR .
If this directory exists, the package tree will be (partially) constructed.
This directory does not have to exist; if it doesn't packages will be
placed into the current directory, or you can define one of
.Bl -tag -width PKGREPOSITORY
.It Ev PKGREPOSITORY
Directory to put the package in.
.It Ev PKGFILE
The full path to the package.
.El
.It Ev PREFIX
Where to install things in general
.Po
usually
.Pa /usr/local
or
.Pa /usr/X11R6
.Pc
.It Ev MASTER_SITES
Primary sites for distribution files if not found locally.
.It Ev PATCH_SITES
Primary location(s) for distribution patch files if not found
locally.
.It Ev MASTER_SITE_FREEBSD
If set, go to the master FreeBSD site for all files.
.It Ev MASTER_SITE_OVERRIDE
Try going to this site for all files and patches, first.
.It Ev NOCLEANDEPENDS
If defined, don't let
.Sq clean
recurse to dependencies.
.It Ev FETCH_CMD
Command to use to fetch files.  Normally
.Xr fetch 1 .
.It Ev FORCE_PKG_REGISTER
If set, overwrite any existing package registration on the system.
.It Ev MOTIFLIB
Location of libXm.{a,so}.
.It Ev PATCH_DEBUG
If defined, display verbose output when applying each patch.
.It Ev INTERACTIVE
If defined, only operate on a port if it requires interaction.
.It Ev BATCH
If defined, only operate on a port if it can be installed 100% automatically.
.El
.Sh FILES
.Bl -tag -width /usr/ports/xxxx -compact
.It Pa /usr/ports
The default ports directory (FreeBSD and OpenBSD).
.It Pa /usr/pkgsrc
The default ports directory (NetBSD).
.It Pa /usr/share/mk/bsd.port.mk
The big Kahuna.
.Sh SEE ALSO
.Xr make 1 ,
.Xr pkg_add 1 ,
.Xr pkg_create 1 ,
.Xr pkg_delete 1 ,
.Xr pkg_info 1 .
.Pp
The FreeBSD handbook
.Pp
http://www.FreeBSD.ORG/ports
.Pq searchable index of all ports
.Sh AUTHORS
This man page was originated by
.An David O'Brien .
The ports collection is maintained by
.An Satoshi Asami
and the awesome ports team.
.Sh HISTORY
.Nm The Ports Collection
appeared in
.Fx 1.0 .
.Sh BUGS
Ports documentation is split over four places ---
.Pa /usr/share/mk/bsd.port.mk ,
the
.Dq Ports Collection
section of the handbook, the
.Dq Porting Existing Software
section of the handbook, and
.Xr ports 7 .
.Pp
This man page is too long.
