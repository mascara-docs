:
# $FreeBSD: src/release/floppies/fixit/verbatim/.profile,v 1.1.4.1 1999/09/05 11:20:48 peter Exp $
PATH=/stand
BLOCKSIZE=K
PS1="Fixit# "

echo '+---------------------------------------------------+'
echo '| You are now running from a FreeBSD "fixit" floppy |'
echo '+---------------------------------------------------+'
echo
echo 'Good Luck!'
echo

export PATH BLOCKSIZE PS1

