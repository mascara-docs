#ifndef _ASM_KMAP_TYPES_H
#define _ASM_KMAP_TYPES_H

enum km_type {
	KM_READ,
	KM_WRITE,
	KM_TYPE_NR,
};

#endif
