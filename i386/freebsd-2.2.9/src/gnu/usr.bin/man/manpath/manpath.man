.\" Man page for manpath
.\"
.\" Copyright (c) 1990, 1991, John W. Eaton.
.\"
.\" You may distribute under the terms of the GNU General Public
.\" License as specified in the README file that comes with the man 1.0
.\" distribution.  
.\"
.\" John W. Eaton
.\" jwe@che.utexas.edu
.\" Department of Chemical Engineering
.\" The University of Texas at Austin
.\" Austin, Texas  78712
.\"
.Dd Jan 5, 1991
.Dt MANPATH 1
.Os
.Sh NAME
.Nm manpath
.Nd determine user's search path for man pages
.Sh SYNOPSIS
.Nm
.Op Fl q
.Sh DESCRIPTION
.Nm manpath
tries to determine the user's manpath from a set of system
defaults and the user's
.Ev PATH ,
echoing the result to the standard output.  Warnings and errors are
written to the standard error.
If a directory in the user's path is not listed in the manpath.config
file, manpath looks for the subdirectories man or MAN.  If they exist,
they are added to the search path.
.Pp
.Nm
is used by
.Nm man
to determine the search path, so user's normally don't need to set the
.Ev MANPATH
environment variable directly.
.Pp
The options are as follows:
.Bl -tag -width Ds
.It Fl q
Operate quietly.  Only echo the final manpath.
.El
.Sh ENVIRONMENT
.Bl -tag -width MANPATH -compact
.It Ev MANPATH
If
.Ev MANPATH
is set,
.Nm manpath
echoes its value on the standard output and issues a warning on the
standard error.
.El
.Sh FILES
.Bl -tag -width %manpath_config_file% -compact
.It Pa %manpath_config_file%
System configuration file.
.El
.Sh SEE ALSO
.Xr apropos 1 ,
.Xr whatis 1 ,
.Xr man 1
.Sh BUGS
None known.
