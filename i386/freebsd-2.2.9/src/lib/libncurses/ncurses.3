.TH ncurses 3 ""
.ds n 5
.ds d @TERMINFO@
.SH NAME
\fBncurses\fR - CRT screen handling and optimization package
.SH SYNOPSIS
\fB#include <ncurses.h>\fR
.br
.SH DESCRIPTION
The \fBncurses\fR library routines give the user a terminal-independent
method of updating character screens with reasonable optimization.

The \fBncurses\fR routines emulate the \fBcurses\fR(3) library of System V
Release 4 UNIX, but is freely redistributable in source form.  Differences from
the SVr4 curses are described in the BUGS sections of individual man pages.
There are only few of these, and as ncurses matures they will become fewer
still.

A program using these routines must be linked with the \fB-lncurses\fR option,
or (if they have been generated) with one of the debugging libraries
\fB-ldcurses\fR or \fB-lpcurses\fR.  The dcurses library generates trace logs
that describe curses actions; the pcurses library supports profiling.

The \fBncurses\fR package supports: overall screen, window and pad
manipulation; output to windows and pads; reading terminal input; control over
terminal and \fBncurses\fR input and output options; environment query
routines; color manipulation; use of soft label keys; terminfo capabilities;
and access to low-level \fBncurses\fR routines.

To initialize the routines, the routine \fBinitscr\fR or \fBnewterm\fR
must be called before any of the other routines that deal with windows
and screens are used.  The routine \fBendwin\fR must be called before
exiting.  To get character-at-a-time input without echoing (most
interactive, screen oriented programs want this), the following
sequence should be used:

      \fBinitscr(); cbreak(); noecho();\fR

Most programs would additionally use the sequence:

      \fBnonl(); intrflush(stdscr,FALSE); keypad(stdscr,TRUE);\fR

Before a \fBncurses\fR program is run, the tab stops of the terminal
should be set and its initialization strings, if defined, must be
output.  This can be done by executing the \fBtput init\fR command
after the shell environment variable \fBTERM\fR has been exported.
[See \fBterminfo\fR(\*n) for further details.]

The \fBncurses\fR library permits manipulation of data structures,
called \fIwindows\fR, which can be thought of as two-dimensional
arrays of characters representing all or part of a CRT screen.  A
default window called \fBstdscr\fR, which is the size of the terminal
screen, is supplied.  Others may be created with \fBnewwin\fR.

Windows are referred to by variables declared as \fBWINDOW *\fR.
These data structures are manipulated with routines described in manual section 3
pages (whose names begin "curs_").  Among which the most basic
routines are \fBmove\fR and \fBaddch\fR.  More general versions of
these routines are included with names beginning with \fBw\fR,
allowing the user to specify a window.  The routines not beginning
with \fBw\fR affect \fBstdscr\fR.)

After using routines to manipulate a window, \fBrefresh\fR is called,
telling \fBncurses\fR to make the user's CRT screen look like
\fBstdscr\fR.  The characters in a window are actually of type
\fBchtype\fR, (character and attribute data) so that other information
about the character may also be stored with each character.

Special windows called \fIpads\fR may also be manipulated.  These are windows
which are not constrained to the size of the screen and whose contents need not
be completely displayed.  See curs_pad(3) for more information.

In addition to drawing characters on the screen, video attributes and colors
may be supported, causing the characters to show up in such modes as
underlined, in reverse video, or in color on terminals that support such
display enhancements.  Line drawing characters may be specified to be output.
On input, \fBncurses\fR is also able to translate arrow and function keys that
transmit escape sequences into single values.  The video attributes, line
drawing characters, and input values use names, defined in \fB<ncurses.h>\fR,
such as \fBA_REVERSE\fR, \fBACS_HLINE\fR, and \fBKEY_LEFT\fR.

If the environment variables \fBLINES\fR and \fBCOLUMNS\fR are set, or if the
program is executing in a window environment, line and column information in
the environment will override information read by \fIterminfo\fR.  This would
effect a program running in an AT&T 630 layer, for example, where the size of a
screen is changeable.

If the environment variable \fBTERMINFO\fR is defined, any program using
\fBncurses\fR checks for a local terminal definition before checking in the
standard place.  For example, if \fBTERM\fR is set to \fBatt4424\fR, then the
compiled terminal definition is found in

      \fB\*d/a/att4424\fR.

(The \fBa\fR is copied from the first letter of \fBatt4424\fR to avoid
creation of huge directories.)  However, if \fBTERMINFO\fR is set to
\fB$HOME/myterms\fR, \fBncurses\fR first checks

      \fB$HOME/myterms/a/att4424\fR,

and if that fails, it then checks

      \fB\*d/a/att4424\fR.

This is useful for developing experimental definitions or when write
permission in \fB\*d\fR is not available.

The integer variables \fBLINES\fR and \fBCOLS\fR are defined in
\fB<ncurses.h>\fR and will be filled in by \fBinitscr\fR with the size of the
screen.  The constants \fBTRUE\fR and \fBFALSE\fR have the values \fB1\fR and
\fB0\fR, respectively.

The \fBncurses\fR routines also define the \fBWINDOW *\fR variable \fBcurscr\fR
which is used for certain low-level operations like clearing and redrawing a
screen containing garbage.  The \fBcurscr\fR can be used in only a few
routines.

.SS Routine and Argument Names
Many \fBncurses\fR routines have two or more versions.  The routines prefixed
with \fBw\fR require a window argument.  The routines prefixed with \fBp\fR
require a pad argument.  Those without a prefix generally use \fBstdscr\fR.

The routines prefixed with \fBmv\fR require a \fIy\fR and \fIx\fR
coordinate to move to before performing the appropriate action.  The
\fBmv\fR routines imply a call to \fBmove\fR before the call to the
other routine.  The coordinate \fIy\fR always refers to the row (of
the window), and \fIx\fR always refers to the column.  The upper
left-hand corner is always (0,0), not (1,1).

The routines prefixed with \fBmvw\fR take both a window argument and
\fIx\fR and \fIy\fR coordinates.  The window argument is always
specified before the coordinates.

In each case, \fIwin\fR is the window affected, and \fIpad\fR is the
pad affected; \fIwin\fR and \fIpad\fR are always pointers to type
\fBWINDOW\fR.

Option setting routines require a Boolean flag \fIbf\fR with the value
\fBTRUE\fR or \fBFALSE\fR; \fIbf\fR is always of type \fBbool\fR.  The
variables \fIch\fR and \fIattrs\fR below are always of type
\fBchtype\fR.  The types \fBWINDOW\fR, \fBSCREEN\fR, \fBbool\fR, and
\fBchtype\fR are defined in \fB<ncurses.h>\fR.  The type \fBTERMINAL\fR
is defined in \fB<term.h>\fR.  All other arguments are integers.

.SS Routine Name Index
The following table lists each \fBncurses\fR routine and the name of
the manual page on which it is described.

.nf 
\fBncurses\fR Routine Name    Manual Page Name
___________________________________________
addch                  curs_addch(3)
addchnstr              curs_addchstr(3)
addchstr               curs_addchstr(3)
addnstr                curs_addstr(3)
addstr                 curs_addstr(3)
attroff                curs_attr(3)
attron                 curs_attr(3)
attrset                curs_attr(3)
baudrate               curs_termattrs(3)
beep                   curs_beep(3)
bkgd                   curs_bkgd(3)
bkgdset                curs_bkgd(3)
border                 curs_border(3)
box                    curs_border(3)
can_change_color       curs_color(3)
cbreak                 curs_inopts(3)
clear                  curs_clear(3)
clearok                curs_outopts(3)
clrtobot               curs_clear(3)
clrtoeol               curs_clear(3)
color_content          curs_color(3)
copywin                curs_overlay(3)
curs_set               curs_kernel(3)
def_prog_mode          curs_kernel(3)
def_shell_mode         curs_kernel(3)
del_curterm            curs_terminfo(\*n)
delay_output           curs_util(3)
delch                  curs_delch(3)
deleteln               curs_deleteln(3)
delscreen              curs_initscr(3)
delwin                 curs_window(3)
derwin                 curs_window(3)
doupdate               curs_refresh(3)
dupwin                 curs_window(3)
echo                   curs_inopts(3)
echochar               curs_addch(3)
endwin                 curs_initscr(3)
erase                  curs_clear(3)
erasechar              curs_termattrs(3)
filter                 curs_util(3)
flash                  curs_beep(3)
flushinp               curs_util(3)
getbegyx               curs_getyx(3)
getch                  curs_getch(3)
getmaxyx               curs_getyx(3)
getparyx               curs_getyx(3)
getstr                 curs_getstr(3)
getsyx                 curs_kernel(3)
getwin                 curs_util(3)
getyx                  curs_getyx(3)
halfdelay              curs_inopts(3)
has_colors             curs_color(3)
has_ic                 curs_termattrs(3)
has_il                 curs_termattrs(3)
hline                  curs_border(3)
idcok                  curs_outopts(3)
idlok                  curs_outopts(3)
immedok                curs_outopts(3)
inch                   curs_inch(3)
inchnstr               curs_inchstr(3)
inchstr                curs_inchstr(3)
init_color             curs_color(3)
init_pair              curs_color(3)
initscr                curs_initscr(3)
innstr                 curs_instr(3)
insch                  curs_insch(3)
insdelln               curs_deleteln(3)
insertln               curs_deleteln(3)
insnstr                curs_insstr(3)
insstr                 curs_insstr(3)
instr                  curs_instr(3)
intrflush              curs_inopts(3)
is_linetouched         curs_touch(3)
is_wintouched          curs_touch(3)
isendwin               curs_initscr(3)
keyname                curs_util(3)
keypad                 curs_inopts(3)
killchar               curs_termattrs(3)
leaveok                curs_outopts(3)
longname               curs_termattrs(3)
meta                   curs_inopts(3)
move                   curs_move(3)
mvaddch                curs_addch(3)
mvaddchnstr            curs_addchstr(3)
mvaddchstr             curs_addchstr(3)
mvaddnstr              curs_addstr(3)
mvaddstr               curs_addstr(3)
mvcur                  curs_terminfo(\*n)
mvdelch                curs_delch(3)
mvderwin               curs_window(3)
mvgetch                curs_getch(3)
mvgetstr               curs_getstr(3)
mvinch                 curs_inch(3)
mvinchnstr             curs_inchstr(3)
mvinchstr              curs_inchstr(3)
mvinnstr               curs_instr(3)
mvinsch                curs_insch(3)
mvinsnstr              curs_insstr(3)
mvinsstr               curs_insstr(3)
mvinstr                curs_instr(3)
mvprintw               curs_printw(3)
mvscanw                curs_scanw(3)
mvwaddch               curs_addch(3)
mvwaddchnstr           curs_addchstr(3)
mvwaddchstr            curs_addchstr(3)
mvwaddnstr             curs_addstr(3)
mvwaddstr              curs_addstr(3)
mvwdelch               curs_delch(3)
mvwgetch               curs_getch(3)
mvwgetstr              curs_getstr(3)
mvwin                  curs_window(3)
mvwinch                curs_inch(3)
mvwinchnstr            curs_inchstr(3)
mvwinchstr             curs_inchstr(3)
mvwinnstr              curs_instr(3)
mvwinsch               curs_insch(3)
mvwinsnstr             curs_insstr(3)
mvwinsstr              curs_insstr(3)
mvwinstr               curs_instr(3)
mvwprintw              curs_printw(3)
mvwscanw               curs_scanw(3)
napms                  curs_kernel(3)
newpad                 curs_pad(3)
newterm                curs_initscr(3)
newwin                 curs_window(3)
nl                     curs_outopts(3)
nocbreak               curs_inopts(3)
nodelay                curs_inopts(3)
noecho                 curs_inopts(3)
nonl                   curs_outopts(3)
noqiflush              curs_inopts(3)
noraw                  curs_inopts(3)
notimeout              curs_inopts(3)
overlay                curs_overlay(3)
overwrite              curs_overlay(3)
pair_content           curs_color(3)
pechochar              curs_pad(3)
pnoutrefresh           curs_pad(3)
prefresh               curs_pad(3)
printw                 curs_printw(3)
putp                   curs_terminfo(\*n)
putwin                 curs_util(3)
qiflush                curs_inopts(3)
raw                    curs_inopts(3)
redrawwin              curs_refresh(3)
refresh                curs_refresh(3)
reset_prog_mode        curs_kernel(3)
reset_shell_mode       curs_kernel(3)
resetty                curs_kernel(3)
restartterm            curs_terminfo(\*n)
ripoffline             curs_kernel(3)
savetty                curs_kernel(3)
scanw                  curs_scanw(3)
scr_dump               curs_scr_dmp(3)
scr_init               curs_scr_dmp(3)
scr_restore            curs_scr_dmp(3)
scr_set                curs_scr_dmp(3)
scrl                   curs_scroll(3)
scroll                 curs_scroll(3)
scrollok               curs_outopts(3)
set_curterm            curs_terminfo(\*n)
set_term               curs_initscr(3)
setscrreg              curs_outopts(3)
setsyx                 curs_kernel(3)
setterm                curs_terminfo(\*n)
setupterm              curs_terminfo(\*n)
slk_attroff            curs_slk(3)
slk_attron             curs_slk(3)
slk_attrset            curs_slk(3)
slk_clear              curs_slk(3)
slk_init               curs_slk(3)
slk_label              curs_slk(3)
slk_noutrefresh        curs_slk(3)
slk_refresh            curs_slk(3)
slk_restore            curs_slk(3)
slk_set                curs_slk(3)
slk_touch              curs_slk(3)
standend               curs_attr(3)
standout               curs_attr(3)
start_color            curs_color(3)
subpad                 curs_pad(3)
subwin                 curs_window(3)
syncok                 curs_window(3)
termattrs              curs_termattrs(3)
termname               curs_termattrs(3)
tigetflag              curs_terminfo(\*n)
tigetnum               curs_terminfo(\*n)
tigetstr               curs_terminfo(\*n)
timeout                curs_inopts(3)
touchline              curs_touch(3)
touchwin               curs_touch(3)
tparm                  curs_terminfo(\*n)
tputs                  curs_terminfo(\*n)
typeahead              curs_inopts(3)
unctrl                 curs_util(3)
ungetch                curs_getch(3)
untouchwin             curs_touch(3)
use_env                curs_util(3)
vidattr                curs_terminfo(\*n)
vidputs                curs_terminfo(\*n)
vline                  curs_border(3)
vwprintw               curs_printw(3)
vwscanw                curs_scanw(3)
waddch                 curs_addch(3)
waddchnstr             curs_addchstr(3)
waddchstr              curs_addchstr(3)
waddnstr               curs_addstr(3)
waddstr                curs_addstr(3)
wattroff               curs_attr(3)
wattron                curs_attr(3)
wattrset               curs_attr(3)
wbkgd                  curs_bkgd(3)
wbkgdset               curs_bkgd(3)
wborder                curs_border(3)
wclear                 curs_clear(3)
wclrtobot              curs_clear(3)
wclrtoeol              curs_clear(3)
wcursyncup             curs_window(3)
wdelch                 curs_delch(3)
wdeleteln              curs_deleteln(3)
wechochar              curs_addch(3)
werase                 curs_clear(3)
wgetch                 curs_getch(3)
wgetnstr               curs_getstr(3)
wgetstr                curs_getstr(3)
whline                 curs_border(3)
winch                  curs_inch(3)
winchnstr              curs_inchstr(3)
winchstr               curs_inchstr(3)
winnstr                curs_instr(3)
winsch                 curs_insch(3)
winsdelln              curs_deleteln(3)
winsertln              curs_deleteln(3)
winsnstr               curs_insstr(3)
winsstr                curs_insstr(3)
winstr                 curs_instr(3)
wmove                  curs_move(3)
wnoutrefresh           curs_refresh(3)
wprintw                curs_printw(3)
wredrawln              curs_refresh(3)
wrefresh               curs_refresh(3)
wscanw                 curs_scanw(3)
wscrl                  curs_scroll(3)
wsetscrreg             curs_outopts(3)
wstandend              curs_attr(3)
wstandout              curs_attr(3)
wsyncdown              curs_window(3)
wsyncup                curs_window(3)
wtimeout               curs_inopts(3)
wtouchln               curs_touch(3)
wvline                 curs_border(3)
.fi
.SH RETURN VALUE
Routines that return an integer return \fBERR\fR upon failure and an
integer value other than \fBERR\fR upon successful completion, unless
otherwise noted in the routine descriptions.

All macros return the value of the \fBw\fR version, except \fBsetscrreg\fR,
\fBwsetscrreg\fR, \fBgetyx\fR, \fBgetbegyx\fR, \fBgetmaxyx\fR.  The return
values of \fBsetscrreg\fR, \fBwsetscrreg\fR, \fBgetyx\fR, \fBgetbegyx\fR, and
\fBgetmaxyx\fR are undefined (\fIi\fR.\fIe\fR., these should not be used as the
right-hand side of assignment statements).

Routines that return pointers return \fBNULL\fR on error.
.SH SEE ALSO
\fBterminfo\fR(5) and section 3 pages whose names begin "curs_" for detailed routine
descriptions.
.SH NOTES
The header file \fB<ncurses.h>\fR automatically includes the header files
\fB<stdio.h>\fR and \fB<unctrl.h>\fR.
.\"#
.\"# The following sets edit modes for GNU EMACS
.\"# Local Variables:
.\"# mode:nroff
.\"# fill-column:79
.\"# End:
