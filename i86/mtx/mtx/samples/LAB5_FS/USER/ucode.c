// ucode.c file

char *cmd[]={"getpid", "ps", "chname", "kmode", "switch", "wait", "exit", 
             "fork", "exec", "sh", 0};

int show_menu()
{
   printf("************************* Menu ***************************\n");
   printf("*  ps  chname  kmode  switch  wait  exit  fork  exec  sh *\n");
   /*         1     2      3       4      5     6    7     8     9 */
   printf("**********************************************************\n");
}

int find_cmd(name) char *name;
{
   int i;   char *p;
   i = 0;   p = cmd[0];
   while (p){
         if (strcmp(p, name)==0)
            return i;
         i++;  p = cmd[i];
   } 
   return(-1);
}


int getpid()
{
   return syscall(0,0,0);
}

int ps()
{
   return syscall(1, 0, 0);
}

int chname()
{
    char s[64];
    printf("\ninput new name : ");
    gets(s);
    return syscall(2, s, 0);
}

int kmode()
{
    printf("kmode : enter Kmode via INT 80\n");
    printf("proc %d going K mode ....\n", getpid());
        return syscall(3, 0, 0);
    printf("proc %d back from Kernel\n", getpid());
}    

int uswitch()
{
    printf("proc %d enter Kernel to switch proc\n", getpid());
        return syscall(4,0,0);
    printf("proc %d back from Kernel\n", getpid());
}

int uwait()
{
    int child, exitValue;
    printf("proc %d enter Kernel to wait for a child to die\n", getpid());
    child = syscall(5, &exitValue, 0);
    printf("proc %d back from wait, dead child=%d", getpid(), child);
    if (child>=0)
        printf("exitValue=%d", exitValue);
    printf("\n"); 
} 

int uexit()
{
   char exitValue;
   printf("\nenter an exitValue (0-9) : ");
   exitValue=getc() - '0';
   printf("enter kernel to die with exitValue=%d\n");
        syscall(6,exitValue,0);
}

int ufork()
{
  int child;
  child = syscall(7,0,0,0);
  if (child)
    printf("parent % return form fork, child=%d\n", getpid(), child);
  else
    printf("child %d return from fork, child=%d\n", getpid(), child);
}

int uexec()
{
  int r;
  char filename[32];
  printf("\nenter exec filename : ");
  gets(filename);
  r = syscall(8,filename,0,0);
  printf("exec failed\n");
}



/******** simple MTX syscalls ********/

int wait(status) int *status;
{
  return syscall(5,status, 0,0);
}

int exit(value) int value;
{
  return syscall(6,value,0,0);
}

int fork()
{
  return syscall(7,0,0,0);
}

int exec(name) char *name;
{
  return syscall(8,name,0,0);
}

// putc() syscall #10 to MTX kernel ==> putc() in VID.c
int putc(c) char c;
{
  return syscall(10,c,0,0);
}

int getc()
{
  return syscall(11,0,0,0) & 0x7F;
}

int invalid(name) char *name;
{
    printf("Invalid command : %s\n", name);
}



/******** syscalls to file system in MTX kernel *********/
char pathname[64];

int mkdir(name) char *name;
{
   return syscall(21, name, 30);
}

int rmdir(name) char *name;
{
   return syscall(22, name, 30);
}

int creat(name) char *name;
{
   return syscall(23, name, 30);
}

int rm(name) char *name;
{
   return syscall(24, name, 30);
}


int chdir(name) char *name;
{
   return syscall(25, name, 30);
}

int pwd()
{
    int r; 
    char cwd[64];
    r = syscall(26, cwd, 0);
    cwd[r]=0;
    printf("%s\n",cwd);
}

int getcwd(cwdname) char *cwdname;
{
    return syscall(26, cwdname, 0);
}

int stat(filename, sPtr) char *filename; struct stat *sPtr;
{   
   return syscall(27, filename, sPtr);
}

int open(file, mode) char *file; int mode;
{
    return syscall(28, file, mode);
}

int close(fd) int fd;
{
    return syscall(29, fd);
}

int read(fd, buf, nbytes) int fd, nbytes; char *buf;
{
    return syscall(30, fd, buf, nbytes);
}

int write(fd, buf, nbytes) int fd, nbytes; char *buf;
{
    return syscall(31, fd, buf, nbytes);
}

long lseek(fd, offset, ww) int fd; long offset; int ww;
{
    return syscall(32, fd, (long)offset, ww);
}

int chmod(mode) int mode;
{
  return syscall(33, mode,0,0);
}

int chown(uid) int uid;
{
  return syscall(34,uid,0,0);
}

int dup(oldfd) int oldfd;
{
   return syscall(35, oldfd, 0);
}

int dup2(oldfd,newfd) int oldfd, newfd;
{
   return syscall(36, oldfd, newfd);
}

