.Dd
.Dt ZIC 8
.Os
.Sh NAME
.Nm zic
.Nd timezone compiler
.Sh SYNOPSIS
.Nm zic
.Op Fl v
.Op Fl d Ar directory
.Op Fl l Ar localtime
.Op Fl p Ar posixrules
.Op Fl L Ar leapsecondfilename
.Op Fl s
.Op Fl y Ar command
.Op Ar filename ...
.Sh DESCRIPTION
.Nm Zic
reads text from the file(s) named on the command line
and creates the time conversion information files specified in this input.
If a
.Ar filename
is
.Em - ,
the standard input is read.
.Pp
The following options are available:
.Bl -tag -width indent
.It Fl d Ar directory
Create time conversion information files in the named directory rather than
in the standard directory named below.
.It Fl l  Ar timezone
Use the given
.Ar time zone
as local time.
.Nm Zic
will act as if the input contained a link line of the form
.sp
.ti +.5i
Link	\fItimezone\fP		localtime
.It Fl p Ar timezone
Use the given
.Ar "time zone" Ns 's
rules when handling POSIX-format
time zone environment variables.
.Nm Zic
will act as if the input contained a link line of the form
.sp
.ti +.5i
Link	\fItimezone\fP		posixrules
.It Fl L Ar leapsecondfilename
Read leap second information from the file with the given name.
If this option is not used,
no leap second information appears in output files.
.It Fl v
Complain if a year that appears in a data file is outside the range
of years representable by
.Xr time 2
values.
.It Fl s
Limit time values stored in output files to values that are the same
whether they're taken to be signed or unsigned.
You can use this option to generate SVVS-compatible files.
.It Fl y Ar command
Use the given
.Ar command
rather than
.Em yearistype
when checking year types (see below).
.El
.Pp
Input lines are made up of fields.
Fields are separated from one another by any number of white space characters.
Leading and trailing white space on input lines is ignored.
An unquoted sharp character (#) in the input introduces a comment which extends
to the end of the line the sharp character appears on.
White space characters and sharp characters may be enclosed in double quotes
(") if they're to be used as part of a field.
Any line that is blank (after comment stripping) is ignored.
Non-blank lines are expected to be of one of three types:
rule lines, zone lines, and link lines.
.Pp
A rule line has the form
.nf
.ti +.5i
.ta \w'Rule\0\0'u +\w'NAME\0\0'u +\w'FROM\0\0'u +\w'1973\0\0'u +\w'TYPE\0\0'u +\w'Apr\0\0'u +\w'lastSun\0\0'u +\w'2:00\0\0'u +\w'SAVE\0\0'u
.sp
Rule	NAME	FROM	TO	TYPE	IN	ON	AT	SAVE	LETTER/S
.sp
For example:
.ti +.5i
.sp
Rule	US	1967	1973	\-	Apr	lastSun	2:00	1:00	D
.sp
.fi
The fields that make up a rule line are:
.Bl -tag -width indent
.It NAME
Give the (arbitrary) name of the set of rules this rule is part of.
.It FROM
Give the first year in which the rule applies.
Any integer year can be supplied; the Gregorian calendar is assumed.
The word
.Em minimum
(or an abbreviation) means the minimum year representable as an integer.
The word
.Em maximum
(or an abbreviation) means the maximum year representable as an integer.
Rules can describe times that are not representable as time values,
with the unrepresentable times ignored; this allows rules to be portable
among hosts with differing time value types.
.It TO
Give the final year in which the rule applies.
In addition to
.Em minimum
and
.Em maximum
(as above),
the word
.Em only
(or an abbreviation)
may be used to repeat the value of the
.Em FROM
field.
.It TYPE
Give the type of year in which the rule applies.
If
.Em TYPE
is
.Em \-
then the rule applies in all years between
.Em FROM
and
.Em TO
inclusive.
If
.Em TYPE
is something else, then
.Nm
executes the command
.ti +.5i
\fByearistype\fP \fIyear\fP \fItype\fP
.br
to check the type of a year:
an exit status of zero is taken to mean that the year is of the given type;
an exit status of one is taken to mean that the year is not of the given type.
.It IN
Name the month in which the rule takes effect.
Month names may be abbreviated.
.It ON
Give the day on which the rule takes effect.
Recognized forms include:
.nf
.in +.5i
.sp
.ta \w'Sun<=25\0\0'u
5	the fifth of the month
lastSun	the last Sunday in the month
lastMon	the last Monday in the month
Sun>=8	first Sunday on or after the eighth
Sun<=25	last Sunday on or before the 25th
.fi
.in -.5i
.sp
Names of days of the week may be abbreviated or spelled out in full.
Note that there must be no spaces within the
.Em ON
field.
.It AT
Give the time of day at which the rule takes effect.
Recognized forms include:
.nf
.in +.5i
.sp
.ta \w'1:28:13\0\0'u
2	time in hours
2:00	time in hours and minutes
15:00	24-hour format time (for times after noon)
1:28:14	time in hours, minutes, and seconds
.fi
.in -.5i
.sp
Any of these forms may be followed by the letter
.Em w
if the given time is local
.Sq "wall clock"
time,
.Em s
if the given time is local
.Sq standard
time, or
.Em u
(or
.Em g
or
.Em z )
if the given time is universal time;
in the absence of an indicator,
wall clock time is assumed.
.It SAVE
Give the amount of time to be added to local standard time when the rule is in
effect.
This field has the same format as the
.Em AT
field
(although, of course, the
.Em w
and
.Em s
suffixes are not used).
.It LETTER/S
Give the
.Sq "variable part"
(for example, the
.Sq S
or
.Sq D
in
.Sq EST
or
.Sq EDT )
of time zone abbreviations to be used when this rule is in effect.
If this field is
.Em \- ,
the variable part is null.
.El
.Pp
A zone line has the form
.sp
.nf
.ti +.5i
.ta \w'Zone\0\0'u +\w'Australia/Adelaide\0\0'u +\w'GMTOFF\0\0'u +\w'RULES/SAVE\0\0'u +\w'FORMAT\0\0'u
Zone	NAME	GMTOFF	RULES/SAVE	FORMAT	[UNTIL]
.sp
For example:
.sp
.ti +.5i
Zone	Australia/Adelaide	9:30	Aus	CST	1971 Oct 31 2:00
.sp
.fi
The fields that make up a zone line are:
.Bl -tag -width indent
.It NAME
The name of the time zone.
This is the name used in creating the time conversion information file for the
zone.
.It GMTOFF
The amount of time to add to GMT to get standard time in this zone.
This field has the same format as the
.Em AT
and
.Em SAVE
fields of rule lines;
begin the field with a minus sign if time must be subtracted from GMT.
.It RULES/SAVE
The name of the rule(s) that apply in the time zone or,
alternately, an amount of time to add to local standard time.
If this field is
.Em \-
then standard time always applies in the time zone.
.It FORMAT
The format for time zone abbreviations in this time zone.
The pair of characters
.Em %s
is used to show where the
.Sq "variable part"
of the time zone abbreviation goes.
Alternately,
a slash (/)
separates standard and daylight abbreviations.
.It UNTIL
The time at which the GMT offset or the rule(s) change for a location.
It is specified as a year, a month, a day, and a time of day.
If this is specified,
the time zone information is generated from the given GMT offset
and rule change until the time specified.
.Pp
The next line must be a
.Sq continuation
line; this has the same form as a zone line except that the
string
.Sq Zone
and the name are omitted, as the continuation line will
place information starting at the time specified as the
.Em UNTIL
field in the previous line in the file used by the previous line.
Continuation lines may contain an
.Em UNTIL
field, just as zone lines do, indicating that the next line is a further
continuation.
.El
.Pp
A link line has the form
.sp
.nf
.ti +.5i
.ta \w'Link\0\0'u +\w'Europe/Istanbul\0\0'u
Link	LINK-FROM	LINK-TO
.sp
For example:
.sp
.ti +.5i
Link	Europe/Istanbul	Asia/Istanbul
.sp
.fi
The
.Em LINK-FROM
field should appear as the
.Em NAME
field in some zone line;
the
.Em LINK-TO
field is used as an alternate name for that zone.
.Pp
Except for continuation lines,
lines may appear in any order in the input.
.Pp
Lines in the file that describes leap seconds have the following form:
.nf
.ti +.5i
.ta \w'Leap\0\0'u +\w'YEAR\0\0'u +\w'MONTH\0\0'u +\w'DAY\0\0'u +\w'HH:MM:SS\0\0'u +\w'CORR\0\0'u
.sp
Leap	YEAR	MONTH	DAY	HH:MM:SS	CORR	R/S
.sp
For example:
.ti +.5i
.sp
Leap	1974	Dec	31	23:59:60	+	S
.sp
.fi
The
.Em YEAR ,
.Em MONTH ,
.Em DAY ,
and
.Em HH:MM:SS
fields tell when the leap second happened.
The
.Em CORR
field
should be
.Sq +
if a second was added
or
.Sq -
if a second was skipped.
.\" There's no need to document the following, since it's impossible for more
.\" than one leap second to be inserted or deleted at a time.
.\" The C Standard is in error in suggesting the possibility.
.\" See Terry J Quinn, The BIPM and the accurate measure of time,
.\" Proc IEEE 79, 7 (July 1991), 894-905.
.\"	or
.\"	.q ++
.\"	if two seconds were added
.\"	or
.\"	.q --
.\"	if two seconds were skipped.
The
.Em R/S
field
should be (an abbreviation of)
.Sq Stationary
if the leap second time given by the other fields should be interpreted as GMT
or
(an abbreviation of)
.Sq Rolling
if the leap second time given by the other fields should be interpreted as
local wall clock time.
.Sh NOTE
For areas with more than two types of local time,
you may need to use local standard time in the
.Em AT
field of the earliest transition time's rule to ensure that
the earliest transition time recorded in the compiled file is correct.
.Sh FILE
.Bl -tag -width /usr/share/zoneinfo -compact
/usr/share/zoneinfo	standard directory used for created files
.El
.Sh "SEE ALSO"
.Xr ctime 3 ,
.Xr tzfile 5 ,
.Xr zdump 8
.\" @(#)zic.8	7.12
