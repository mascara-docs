.\" Copyright (c) 1991, 1993
.\"	The Regents of the University of California.  All rights reserved.
.\"
.\" This code is derived from software written and contributed
.\" to Berkeley by William Jolitz.
.\"
.\" Almost completely rewritten for FreeBSD 2.1 by Joerg Wunsch.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by the University of
.\"	California, Berkeley and its contributors.
.\" 4. Neither the name of the University nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"     @(#)boot_i386.8	8.2 (Berkeley) 4/19/94
.\"
.\" $FreeBSD: src/sbin/reboot/boot_i386.8,v 1.4.2.5 1999/09/05 11:24:10 peter Exp $
.\"
.Dd April 19, 1994
.Dt BOOT 8 i386
.Os
.Sh NAME
.Nm boot
.Nd
system bootstrapping procedures
.Sh DESCRIPTION
.Sy Power fail and crash recovery.
Normally, the system will reboot itself at power-up or after crashes.
An automatic consistency check of the file systems will be performed,
and unless this fails, the system will resume multi-user operations.
.Pp
.Sy Cold starts.
Most 386
.Tn "PC AT"
clones attempt to boot the floppy disk drive 0 (otherwise known as
drive A:) first, and failing that, attempt to boot the hard disk
drive 0 (otherwise known as drive C:,
or (confusingly) hard disk drive 1, or drive 0x80 in the BIOS).
Some BIOSes let you change this default sequence or may include a CD-ROM
drive as a boot device.
After the boot blocks have been loaded,
you should see a prompt similar to the following:
.Bd -literal
>> FreeBSD BOOT @ 0x10000: 640/7168 k of memory, internal console
Boot default: 0:wd(0,a)kernel

boot:
.Ed
.Pp
(You may see some tips printed on the screen too.)
.Pp
The automatic boot will attempt to load
.Pa /kernel
from partition
.Ql a
of either the floppy or the hard disk.
This boot may be aborted by typing any character on the keyboard
at the
.Ql boot:
prompt.  At this time, the following input will be accepted:
.Bl -tag -width 10x
.It \&?
Give a short listing of the files in the root directory of the default
boot device, as a hint about available boot files.
.It Op bios_drive:interface(unit,part) Op filename Op Fl aCcDdghPrsv
Specify boot file and flags.
.Bl -tag -width 10x -compact
.It bios_drive
The drive number as recognized by the BIOS. 
0 for the first drive, 1 for the second drive, etc.
.It interface
The type of controller to boot from.  Note that the controller is required
to have BIOS support since the BIOS services are used to load the
boot file image.
.Pp
The supported interfaces are:
.Bl -tag -width "wdXX" -compact
.It wd
ST506, IDE, ESDI, RLL disks on a WD100[2367] or lookalike
controller
.It fd
5 1/4" or 3 1/2" High density floppies
.It sd
SCSI disk on any supported SCSI controller
.\".It cd
.\"boot from CDROM
.El
.It unit
The unit number of the drive on the interface being used.
0 for the first drive, 1 for the second drive, etc.
.It part
The partition letter inside the BSD portion of the disk.  See
.Xr disklabel 8  .
By convention, only partition
.Ql a
contains a bootable image.  If sliced disks are used
.Pq Dq fdisk partitions ,
only the first BSD slice can be used to boot from.  The partition
letter always refers to the first slice.
.It filename
The pathname of the file to boot (relative to the root directory
on the specified partition).  Defaults to
.Pa kernel .
Symbolic links are not supported (hard links are).
.It Fl acCdDghPrsv
Boot flags:
.Bl -tag -width "-CXX" -compact
.It Fl a
during kernel initialization,
ask for the device to mount as as the root file system.
.It Fl C
boot from CDROM.
.It Fl c
run UserConfig to modify hardware parameters for the loaded
kernel.  If the kernel was built with the USERCONFIG_BOOT option,
remain in UserConfig regardless of any
.Ic quit
commands present in the script.
.It Fl D
toggle single and dual console configurations.  In the single
configuration the console will be either the internal display
or the serial port, depending on the state of the 
.Fl h
option below.  In the dual console configuration, 
both the internal display and the serial port will become the console 
at the same time, regardless of the state of the 
.Fl h
option.  However, the dual console configuration takes effect only during
the boot prompt.  Once the kernel is loaded, the console specified 
by the
.Fl h
option becomes the only console.
.It Fl d
enter the DDB kernel debugger
.Pq see Xr ddb 4
as early as possible in kernel initialization.
.It Fl g
use the GDB remote debugging protocol.
.It Fl h
toggle internal and serial consoles.  You can use this to switch 
console devices.  For instance, if you boot from the internal console, 
you can use the
.Fl h
option to force the kernel to use the serial port as its 
console device.  Alternatively, if you boot from the serial port, 
you can use this option to force the kernel to use the internal display 
as the console instead.
.It Fl P
probe the keyboard.  If no keyboard is found, the
.Fl D
and
.Fl h
options are automatically set.
.It Fl r
use the statically configured default for the device containing the
root file system
.Pq see Xr config 8 .
Normally, the root file system is on the device
that the kernel was loaded from.
.It Fl s
boot into single-user mode; if the console is marked as
.Dq insecure
.Pq see Xr ttys 5 ,
the root password must be entered
.It Fl v
be verbose during device probing (and later).
.El
.El
.El
.Pp
You may put a BIOS drive number, a controller type, a unit number,
a partition, a kernel file name and the 
.Fl D,
.Fl h
or
.Fl P
options in 
.Pa /boot.config
to set defaults.  Write them in one line just as you type at the
.Ql boot:
prompt.
.Sh FILES
.Bl -tag -width /kernel.old.config -compact
.It Pa /boot.config
parameters for the boot loader (optional)
.It Pa /boot.help
help messages
.It Pa /kernel
default kernel
.It Pa /kernel.config
parameters for default kernel (optional)
.It Pa /kernel.old
typical non-default kernel (optional)
.It Pa /kernel.old.config
parameters for non-default kernel (optional)
.\" .It Pa /boot
.\" system bootstrap
.El
.Sh SEE ALSO
.Xr ddb 4 ,
.Xr ttys 5 ,
.Xr config 8 ,
.Xr disklabel 8 ,
.Xr halt 8 ,
.Xr reboot 8 ,
.Xr shutdown 8
.Sh BUGS
The disklabel format used by this version of
.Bx
is quite
different from that of other architectures.
.Pp
The boot flags are not very self-explanatory, and the alphabet has
too few characters to implement every potentially useful boot option.
