.\"
.\" Copyright (c) 1998, Luigi Rizzo
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man4/man4.i386/pcm.4,v 1.1.2.1 1999/09/05 11:27:02 peter Exp $
.\"
.Dd June 3, 1998
.Dt PCM 4 i386
.Os FreeBSD
.Sh NAME
.Nm pcm
.Nd FreeBSD audio device driver
.Sh SYNOPSIS
.Cd "device pcm0 at isa? port? tty irq 5 drq 1 flags 0x15 vector pcmintr"
.Sh DESCRIPTION
The
.Nm pcm
driver provides support for varioust ISA sound cards that are compatible
with the WSS/MSS specs, or with the SBPro and SB16. Only audio
capture/playback is supported by the driver, and true full duplex
operation is available on most cards.
.Pp
Apart from the usual parameters, the flags field is used to specify
the secondary DMA channel (generally used for capture in full duplex
cards). Flags are set to 0 for cards not using a secondary DMA
channel, or to 0x10 + C to specify channel C.
.Pp
PnP audio cards are also supported using the
.Nm pnp
support which is available in FreeBSD starting from release 2.2.6.
See the
.Xr pnp 4
manpage for more information. In particular, remember that unit
numbers for (recognized)
PnP cards are assigned after the legacy ISA devices, and that the
actual resources (port, irq and drq) used by the driver are read from
the PnP configuration and not from the "device pcm0" line.
So, if you have the following line in your kernel config file:
.Pp
.Cd "device pcm0 ..."
.Pp
your first PnP audio card will be unit #1, i.e. it will be accessible
as /dev/audio1, /dev/dsp1, etc.
Many application default to using /dev/audio, so you can create
symlinks from /dev/audio etc to the correct device entries as follows:
.Pp
.Cd cd /dev
.Cd rm audio dsp dspW mixer
.Cd ./MAKEDEV snd1
.Cd ln -s audio1 audio
.Cd ln -s dsp1 dsp
.Cd ln -s dspW1 dspW
.Cd ln -s mixer1 mixer
.Pp
The driver works best with WSS/MSS cards, which have a very clean
architecture and an orthogonal set of features. They also happen to be
among the cheapest audio cards on the market.
Other cards such as SB and ESS have a more complex internal
architecture, and often no documentation available. As a consequence,
support for these cards is slightly worse.
.Pp
The driver does its best to recognize the installed harware and drive
it correctly, so that you don't have to give too many details in the
kernel config files. For PnP cards this is actually easy since they
identify themselves. For legacy ISA cards, the driver first looks for
MSS cards at addresses 0x530 and 0x604, then for SB
cards at 0x220 and 0x240 (obviously, unless overridden in the kernel
config file by specifying an address).

.Sh IOCTL
The driver supports most of the Voxware ioctls(), and most
applications work unmodified (including popular mpeg players and linux
binaries). A few
differences exist (the most important one is the ability to use
memory-mapped access to the audio buffers). As a consequence, some
applications may need to be recompiled with a slightly modified
audio module.  See /usr/include/machine/soundcard.h for a complete
list of the supported ioctls.

.Sh SUPPORTED CARDS
.Pp
Below we include a list of supported codecs/cards, including, if
possible, the pnp configuration information where applicable
(we give default parameters, your actual resources may vary).

.Bl -tag -width 2m  % begin list
.It CS4237, CS4236, CS4232, CS4231
.Cd "pnp 1 0 os enable port0 0x534 port2 0x220 irq0 5 drq0 1 drq1 3"
.Pp
All these cards work perfectly in full duplex using the MSS mode.
This chipset is used, among others, on the A/Open AW35 and AW32, on
some Intel motherboards, and (the CS4231) on some non-PnP cards.
.Pp
The CS4232 is reported as buggy in the Voxware documentation but
I am not sure if this is true. On one of my Intel motherboards,
capture does not work simply because the capture DMA channel is
not wired to the ISA DMA controller.

.It GUSPnP
.Cd "pnp 1 0 os enable port0 0x220 port1 0x320 port2 0x32c irq0 5 drq0 7 drq1 5"
.Pp
Supported in full duplex using the MSS mode. The GUSPnP does not use a
real CS4231, and I suspect a bug in the emulation when operating
with mu-law format. Since the card is discontinued, I did not bother
to include special code to set the card in "Mode3" (where mu-law is
known to work) and rather I use U8 format internally, and do the
conversion in software within the driver. This loses resolution,
so you should use 16-bit modes with this card when possible.

.It Yamaha OPL-SAx
.Cd "pnp 1 0 os enable port0 0x220 port1 0x530 port2 0x388 port3 0x370 irq0 5 drq0 1 drq1 3"
.Pp
Works perfectly in all modes. This chip is used in several PnP cards,
but also (in non-PnP mode) on motherboards and laptops (e.g. the
Toshiba Libretto).

.It OPTi931
.Cd "pnp 1 1 os enable port0 0x534 port2 0x220 port3 0xe0d irq0 10 drq0 1 drq1 6"
.Pp
The chip is buggy, but the driver has many workarounds to make it work
in full duplex because for some time these were the only full duplex
cards I could find. u-law formats uses U8 format internally because of
a bug in the chip.

.It SB16, Vibra16C, and old SB16/AWExx cards
.Cd "pnp 1 0 os enable port0 0x220 irq0 5 drq0 1 drq1 5"
.Pp
These codecs have limited full duplex capabilities, by doing 8-bit
in one direction and 16-bit in the other one. The driver supports this
mode of operation but keep in mind that it is not a supported mode of
operation from CreativeLabs.

.It Vibra16X and newer SB16/AWExx cards
.Cd "pnp 1 0 os enable port0 0x220 irq0 5 drq0 1 drq1 3"
.Pp
Recently CreativeLabs has changed the codec (DSP) and the new one is
somewhat different from the old one. The most apparent difference is
the use of two 8-bit DMA channels, which seems to have broken our full
duplex support. Maybe one can achieve full duplex by making use of the
wavetable to play audio, but since this driver does not support the
wavetable you have to stick with half duplex.

.It SBPro and clones
This is the default mode of operation for most other cards. We have
only limited (and possibly buggy) support for them: 8-bit, half
duplex, even if the hardware in some cases (e.g. ESS chips) might do more.
.El
.Pp
.Sh DIAGNOSTICS AND TROUBLESHOOTING
.Bl -tag -width 2m
.It "This is XXX but LDN Y is disabled"
.Pp
This means that the bios has left the PnP device disabled, and you
have to enable it manually booting with "-c" and giving the pnp
configuration shown above.
.It "pcmX: unit not configured, perhaps you want pcmY ?"
This means that you are using the wrong unit. Generally this happens
when you are using a PnP card without creating the symlinks to the
correct unit.  Re-create the symlinks (and possibly also the device
entries) in the /dev directory to point to the right one.

.It "timeout flushing dbuf_out ..."
This means a problem in the configuration of the card (specifically,
in the DMA channel) or (much less likely) in the driver, which has
not recognized the card correctly. Check the DMA channel used for
capture.

.It capture does not work
This usually happens when the input dma channel is misconfigured.

.El
.Sh BUGS
Due to lack of documentation,
SB16 support is not very good. Also, mixer support is not complete,
and some features of your cards (e.g. global volume control) might not
be supported on all devices.
.Sh HISTORY
The
.Nm pcm
device driver first appeared in
.Fx 2.2.6
.Sh AUTHOR
The
.Nm
device driver and this manual page were written by Luigi Rizzo
(luigi@iet.unipi.it)
