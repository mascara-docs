#	from @(#)Makefile.inc	8.1 (Berkeley) 6/4/93
# $FreeBSD: src/lib/libc/db/mpool/Makefile.inc,v 1.1.1.1.8.2 1999/09/05 11:16:26 peter Exp $

.PATH: ${.CURDIR}/../libc/db/mpool

SRCS+=	mpool.c
