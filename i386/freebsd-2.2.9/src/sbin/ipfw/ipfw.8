.Dd July 20, 1996
.Dt IPFW 8 SMM
.Os FreeBSD
.Sh NAME
.Nm ipfw
.Nd controlling utility for IP firewall
.Sh SYNOPSIS
.Nm ipfw
.Oo
.Fl q
.Oc
.Ar file
.Nm ipfw
.Oo
.Fl f
|
.Fl q
.Oc
flush
.Nm ipfw
.Oo
.Fl q
.Oc
zero
.Op Ar number ...
.Nm ipfw
delete
.Ar number ...
.Nm ipfw
.Op Fl aftN
list
.Op Ar number ...
.Nm ipfw
.Oo
.Fl ftN
.Oc
show
.Op Ar number ...
.Nm ipfw
.Oo
.Fl q
.Oc
add
.Op Ar number
.Ar action 
.Op log
.Ar proto
from
.Ar src
to
.Ar dst
.Op via Ar name | ipno
.Op Ar options
.Sh DESCRIPTION
If used as shown in the first synopsis line, the
.Ar file
will be read line by line and applied as arguments to the 
.Nm
command.
.Pp
The
.Nm
code works by going through the rule-list for each packet,
until a match is found.
All rules have two associated counters, a packet count and
a byte count.
These counters are updated when a packet matches the rule.
.Pp
The rules are ordered by a ``line-number'' from 1 to 65534 that is used
to order and delete rules. Rules are tried in increasing order, and the
first rule that matches a packet applies.
Multiple rules may share the same number and apply in
the order in which they were added.
.Pp
If a rule is added without a number, it is numbered 100 higher
than the previous rule. If the highest defined rule number is
greater than 65434, new rules are appended to the last rule.
.Pp
The delete operation deletes the first rule with number
.Ar number ,
if any.
.Pp
The list command prints out the current rule set.
.Pp
The show command is equivalent to `ipfw -a list'.
.Pp
The zero operation zeroes the counters associated with rule number
.Ar number .
.Pp
The flush operation removes all rules.
.Pp
Any command beginning with a '#', or being all blank, is ignored.
.Pp
One rule is always present:
.Bd -literal -offset center
65535 deny all from any to any
.Ed
.Pp
This rule is the default policy, i.e., don't allow anything at all.
Your job in setting up rules is to modify this policy to match your
needs.
.Pp
The following options are available:
.Bl -tag -width flag
.It Fl a
While listing, show counter values.  See also ``show'' command.
.It Fl f
Don't ask for confirmation for commands that can cause problems if misused
(ie; flush).
.Ar Note ,
if there is no tty associated with the process, this is implied.
.It Fl q
While adding, zeroing or flushing, be quiet about actions (implies
.Fl f Ns ).
This is useful for adjusting rules by executing multiple ipfw commands in a
script (e.g. sh /etc/rc.firewall), or by processing a file of many ipfw rules,
across a remote login session.  If a flush is performed in normal
(verbose) mode, it prints a message.  Because all rules are flushed, the
message cannot be delivered to the login session, the login session is
closed and the remainder of the ruleset is not processed.  Access to the
console is required to recover. 
.It Fl t
While listing, show last match timestamp.
.It Fl N
Try to resolve addresses and service names in output.
.El
.Pp
.Ar action :
.Bl -hang -offset flag -width 1234567890123456
.It Ar allow
Allow packets that match rule.
The search terminates. Aliases are
.Ar pass ,
.Ar permit ,
and
.Ar accept .
.It Ar deny
Discard packets that match this rule.
The search terminates.
.Ar Drop
is an alias for
.Ar deny .
.It Ar reject
(Deprecated.) Discard packets that match this rule, and try to send an ICMP
host unreachable notice.
The search terminates.
.It Ar unreach code
Discard packets that match this rule, and try to send an ICMP
unreachable notice with code
.Ar code ,
where
.Ar code
is a number from zero to 255, or one of these aliases:
.Ar net ,
.Ar host ,
.Ar protocol ,
.Ar port ,
.Ar needfrag ,
.Ar srcfail ,
.Ar net-unknown ,
.Ar host-unknown ,
.Ar isolated ,
.Ar net-prohib ,
.Ar host-prohib ,
.Ar tosnet ,
.Ar toshost ,
.Ar filter-prohib ,
.Ar host-precedence ,
or
.Ar precedence-cutoff .
The search terminates.
.It Ar reset
TCP packets only. Discard packets that match this rule,
and try to send a TCP reset (RST) notice.
The search terminates.
.It Ar count
Update counters for all packets that match rule.
The search continues with the next rule.
.It Ar divert port
Divert packets that match this rule to the
.Xr divert 4
socket bound to port
.Ar port .
The search terminates.
.It Ar tee port
Send a copy of packets matching this rule to the
.Xr divert 4
socket bound to port
.Ar port .
The search continues with the next rule.
.It Ar skipto number
Skip all subsequent rules numbered less than
.Ar number .
The search continues with the first rule numbered
.Ar number
or higher.
.El
.Pp
If a packet matches more than one
.Ar divert
and/or
.Ar tee
rule, all but the last are ignored.
.Pp
If the kernel was compiled with
.Dv IPFIREWALL_VERBOSE ,
then when a packet matches a rule with the ``log''
keyword a message will be printed on the console.
If the kernel was compiled with the
.Dv IPFIREWALL_VERBOSE_LIMIT
option, then logging will cease after the number of packets
specified by the option are received for that particular
chain entry.  Logging may then be re-enabled by clearing
the packet counter for that entry.
.Pp
Console logging and the log limit are adjustable dynamically
through the
.Xr sysctl 8
interface.
.Pp
.Ar proto :
.Bl -hang -offset flag -width 1234567890123456
.It Ar ip
All packets match. The alias
.Ar all
has the same effect.
.It Ar tcp
Only TCP packets match.
.It Ar udp
Only UDP packets match.
.It Ar icmp
Only ICMP packets match.
.It Ar <number|name>
Only packets for the specified protocol matches (see
.Pa /etc/protocols
for a complete list).
.El
.Pp
.Ar src 
and
.Ar dst :
.Bl -hang -offset flag
.It Ar <address/mask>
.Op Ar ports
.El
.Pp
The
.Em <address/mask>
may be specified as:
.Bl -hang -offset flag -width 1234567890123456
.It Ar ipno
An ipnumber of the form 1.2.3.4.
Only this exact ip number match the rule.
.It Ar ipno/bits
An ipnumber with a mask width of the form 1.2.3.4/24.
In this case all ip numbers from 1.2.3.0 to 1.2.3.255 will match.
.It Ar ipno:mask
An ipnumber with a mask width of the form 1.2.3.4:255.255.240.0.
In this case all ip numbers from 1.2.0.0 to 1.2.15.255 will match.
.El
.Pp
The sense of the match can be inverted by preceding an address with the
``not'' modifier, causing all other addresses to be matched instead. This
does not affect the selection of port numbers.
.Pp
With the TCP and UDP protocols, optional
.Em ports
may be specified as:
.Pp
.Bl -hang -offset flag
.It Ns {port|port-port} Ns Op ,port Ns Op ,...
.El
.Pp
Service names (from 
.Pa /etc/services )
may be used instead of numeric port values.
A range may only be specified as the first value,
and the length of the port list is limited to
.Dv IP_FW_MAX_PORTS
(as defined in 
.Pa /usr/src/sys/netinet/ip_fw.h )
ports.
.Pp
Fragmented packets which have a non-zero offset (i.e. not the first
fragment) will never match a rule which has one or more port
specifications.  See the
.Ar frag
option for details on matching fragmented packets.
.Pp
Rules can apply to packets when they are incoming, or outgoing, or both.
The
.Ar in
keyword indicates the rule should only match incoming packets.
The
.Ar out
keyword indicates the rule should only match outgoing packets.
.Pp
To match packets going through a certain interface, specify
the interface using
.Ar via :
.Bl -hang -offset flag -width 1234567890123456
.It Ar via ifX
Packet must be going through interface
.Ar ifX.
.It Ar via if*
Packet must be going through interface
.Ar ifX ,
where X is any unit number.
.It Ar via any
Packet must be going through
.Em some
interface.
.It Ar via ipno
Packet must be going through the interface having IP address
.Ar ipno .
.El
.Pp
The
.Ar via
keyword causes the interface to always be checked.
If
.Ar recv
or
.Ar xmit
is used instead of
.Ar via ,
then the only receive or transmit interface (respectively) is checked.
By specifying both, it is possible to match packets based on both receive
and transmit interface, e.g.:
.Pp
.Dl "ipfw add 100 deny ip from any to any out recv ed0 xmit ed1"
.Pp
The
.Ar recv
interface can be tested on either incoming or outgoing packets, while the
.Ar xmit
interface can only be tested on outgoing packets. So
.Ar out
is required (and
.Ar in
invalid) whenever
.Ar xmit
is used. Specifying
.Ar via
together with
.Ar xmit
or
.Ar recv
is invalid.
.Pp
A packet may not have a receive or transmit interface: packets originating
from the local host have no receive interface. while packets destined for
the local host have no transmit interface.
.Pp
Additional
.Ar options :
.Bl -hang -offset flag -width 1234567890123456
.It frag
Matches if the packet is a fragment and this is not the first fragment
of the datagram.
.Ar frag
may not be used in conjunction with either
.Ar tcpflags
or TCP/UDP port specifications.
.It in
Matches if this packet was on the way in.
.It out
Matches if this packet was on the way out.
.It ipoptions Ar spec
Matches if the IP header contains the comma separated list of 
options specified in
.Ar spec .
The supported IP options are:
.Ar ssrr 
(strict source route),
.Ar lsrr 
(loose source route),
.Ar rr 
(record packet route), and
.Ar ts 
(timestamp).
The absence of a particular option may be denoted
with a ``!''.
.It established
Matches packets that have the RST or ACK bits set.
TCP packets only.
.It setup
Matches packets that have the SYN bit set but no ACK bit.
TCP packets only.
.It tcpflags Ar spec
Matches if the TCP header contains the comma separated list of
flags specified in
.Ar spec .
The supported TCP flags are:
.Ar fin ,
.Ar syn ,
.Ar rst ,
.Ar psh ,
.Ar ack ,
and
.Ar urg .
The absence of a particular flag may be denoted
with a ``!''.
A rule which contains a
.Ar tcpflags
specification can never match a fragmented packet which has
a non-zero offset.  See the
.Ar frag
option for details on matching fragmented packets.
.It icmptypes Ar types
Matches if the ICMP type is in the list
.Ar types .
The list may be specified as any combination of ranges
or individual types separated by commas.
.El
.Sh CHECKLIST
Here are some important points to consider when designing your
rules:
.Bl -bullet -hang -offset flag 
.It 
Remember that you filter both packets going in and out.
Most connections need packets going in both directions.
.It
Remember to test very carefully.
It is a good idea to be near the console when doing this.
.It
Don't forget the loopback interface.
.El
.Sh FINE POINTS
There is one kind of packet that the firewall will always discard,
that is an IP fragment with a fragment offset of one.
This is a valid packet, but it only has one use, to try to circumvent
firewalls.
.Pp
If you are logged in over a network, loading the LKM version of
.Nm
is probably not as straightforward as you would think.
I recommend this command line:
.Bd -literal -offset center
modload /lkm/ipfw_mod.o && \e
ipfw add 32000 allow all from any to any
.Ed
.Pp
Along the same lines, doing an
.Bd -literal -offset center
ipfw flush
.Ed
.Pp
in similar surroundings is also a bad idea.
.Sh PACKET DIVERSION
A divert socket bound to the specified port will receive all packets diverted
to that port; see
.Xr divert 4 .
If no socket is bound to the destination port, or if the kernel
wasn't compiled with divert socket support, diverted packets are dropped.
.Sh EXAMPLES
This command adds an entry which denies all tcp packets from
.Em hacker.evil.org
to the telnet port of
.Em wolf.tambov.su
from being forwarded by the host:
.Pp
.Dl ipfw add deny tcp from hacker.evil.org to wolf.tambov.su 23
.Pp 
This one disallows any connection from the entire hackers network to
my host:
.Pp
.Dl ipfw addf deny all from 123.45.67.0/24 to my.host.org
.Pp
Here is a good usage of the list command to see accounting records
and timestamp information:
.Pp
.Dl ipfw -at l
.Pp
or in short form without timestamps:
.Pp
.Dl ipfw -a l
.Pp
This rule diverts all incoming packets from 192.168.2.0/24 to divert port 5000:
.Pp
.Dl ipfw divert 5000 all from 192.168.2.0/24 to any in
.Sh SEE ALSO
.Xr ip 4 ,
.Xr ipfirewall 4 ,
.Xr divert 4 ,
.Xr protocols 5 ,
.Xr services 5 ,
.Xr reboot 8 ,
.Xr syslogd 8 ,
.Xr sysctl 8
.Sh BUGS
.Pp
.Em WARNING!!WARNING!!WARNING!!WARNING!!WARNING!!WARNING!!WARNING!!
.Pp
This program can put your computer in rather unusable state. When
using it for the first time, work on the console of the computer, and
do
.Em NOT
do anything you don't understand.
.Pp
When manipulating/adding chain entries, service and protocol names are
not accepted.
.Pp
Incoming packet fragments diverted by
.Ar divert
are reassembled before delivery to the socket, whereas fragments diverted via
.Ar tee
are not.
.Pp
Port aliases containing dashes cannot be first in a list.
.Pp
The ``tee'' action is unimplemented.
.Sh AUTHORS
.An Ugen J. S. Antsilevich ,
.An Poul-Henning Kamp ,
.An Alex Nash ,
.An Archie Cobbs .
.An Archie Cobbs .
API based upon code written by
.An Daniel Boulet
for BSDI.
.Sh HISTORY
.Nm Ipfw
first appeared in
.Fx 2.0 .
