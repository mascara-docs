#	@(#)Makefile.inc	8.1 (Berkeley) 6/5/93
# $FreeBSD: src/share/Makefile.inc,v 1.1.1.1.8.2 1999/09/05 11:26:24 peter Exp $

BINDIR=	${SHAREDIR}
BINOWN=	${SHAREOWN}
BINGRP=	${SHAREGRP}
