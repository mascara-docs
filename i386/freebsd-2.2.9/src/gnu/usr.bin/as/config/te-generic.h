/*
 * This file is te-generic.h and is intended to be a template for
 * target environment specific header files.
 *
 * It is my intent that this file will evolve into a file suitable for config,
 * compile, and copying as an aid for testing and porting.  xoxorich.
 */
/*
 * $FreeBSD: src/gnu/usr.bin/as/config/te-generic.h,v 1.1.10.1 1999/09/05 11:06:07 peter Exp $
 */


#define TE_GENERIC 1

/* these define interfaces */
#include "obj-format.h"

/*
 * Local Variables:
 * comment-column: 0
 * fill-column: 131
 * End:
 */

/* end of te-generic.h */
