/* $FreeBSD: src/usr.sbin/pppd/patchlevel.h,v 1.3.2.2 1999/09/05 11:41:05 peter Exp $ */
#define	PATCHLEVEL	5

#define VERSION		"2.3"
#define IMPLEMENTATION	""
#define DATE		"4 May 1998"
