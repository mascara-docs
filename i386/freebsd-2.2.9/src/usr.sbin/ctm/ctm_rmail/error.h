extern	void	err_set_log(char *log_file);
extern	void	err_prog_name(char *name);
extern	void	err(char *fmt, ...);
