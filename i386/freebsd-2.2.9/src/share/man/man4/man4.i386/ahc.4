.\"
.\" Copyright (c) 1995, 1996, 1997
.\" 	Justin T. Gibbs.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. The name of the author may not be used to endorse or promote products
.\"    derived from this software withough specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man4/man4.i386/ahc.4,v 1.6.2.7 1999/09/05 11:27:01 peter Exp $
.\"
.Dd April 20, 1996
.Dt AHC 4 i386
.Os FreeBSD
.Sh NAME
.Nm ahc
.Nd Adaptec VL/EISA/PCI SCSI host adapter driver
.Sh SYNOPSIS
For one or more VL/EISA cards:
.Cd controller eisa0
.Cd controller ahc0
.Pp
For one or more PCI cards:
.Cd controller pci0
.Cd controller ahc0
.Pp
To enable SCB paging:
.Cd options AHC_SCBPAGING_ENABLE
.Pp
To enable tagged queueing:
.Cd options AHC_TAGENABLE
.Pp
To allow PCI adapters to use memory mapped I/O if enabled:
.Cd options AHC_ALLOW_MEMIO
.Pp
For one or more SCSI busses:
.Cd controller scbus0 at ahc0
.Sh DESCRIPTION
This driver provides access to the
.Tn SCSI
bus(es) connected to Adaptec 
274x, 284x, 2940, 3940, or controllers based on the
.Tn AIC7770,
.Tn AIC7850,
.Tn AIC7860,
.Tn AIC7870,
or
.Tn AIC7880
host adapter chips.
Features include support for twin and wide busses,
ultra
.Tn SCSI,
two active commands at a time per non-tagged queueing target,
tagged queuing,
and SCB paging.
.Pp
The number of concurrent transactions allowed is chip dependent
and ranges from 3 to 16.
On PCI adapters,
this number can be increased with the SCB paging option.
SCB paging implements an algorithm to 'page-out' transactions
that are in the disconnected state so that the freed space in
the controller's memory can be used to start additional transactions.
On the aic7880 and aic7870,
this increases the maximum number of outstanding transactions from 16 to 255.
On the aic7850 and aic7860 controllers, this maximum rises from 3 to 8.
During the hardware probe,
a diagnostic showing the ratio of hardware supported 'slots' to number
of transactions is printed.
SCB paging is enabled with the
.Dq Dv AHC_SCBPAGING_ENABLE
configuration option.
This option will likely be removed and become the default behavior for
adapters that support it,
in the near future.
.Pp
Tagged queueing is enabled with the
.Dq Dv AHC_TAGENABLE
configuration option.
Tagged queueing allows multiple transactions to be queued at the device
level instead of the host level,
allowing the device to re-order I/O to minimize seeks,
seek distance, 
and to increase throughput.
Tagged queueing can have a significant impact on performance for seek
bound applications and should be enabled for most configurations.
Unfortunately, some devices that claim to support tagged queueing fail
miserably when it is used.
The only reason tagged queueing remains as a controller option is as a
stop gap measure until a mechanism to detect these broken devices and to
control this feature on a per device basis is in place.
.Pp
Memory mapped I/O can be enabled with the
.Dq Dv AHC_ALLOW_MEMIO
configuration option.
Memory mapped I/O is more efficient than the alternative, programmed I/O.
Most PCI BIOSes will map devices so that either technique for communicating
with the card is available.
In some cases,
usually when the PCI device is sitting behind a PCI->PCI bridge,
the BIOS fails to properly initialize the chip for memory mapped I/O.
The symptom of this problem is usually a system hang if memory mapped I/O
is attempted.
Most modern motherboards perform the initialization correctly and work fine
with this option enabled.
.Pp
Per target configuration performed in the 
.Tn SCSI-Select
menu, accessible at boot
in 
.No non- Ns Tn EISA
models,
or through an 
.Tn EISA
configuration utility for 
.Tn EISA
models,
is honored by this driver with the stipulation that the 
.Tn BIOS
must be enabled for 
.Tn EISA
adaptors.  This includes synchronous/asynchronous transfers,
maximum synchronous negotiation rate,
disconnection,
and the host adapter's SCSI ID.
.Pp
Note that I/O addresses are determined automatically by the probe routines,
but care should be taken when using a 284x
.Pq Tn VESA No local bus controller
in an
.Tn EISA 
system.  Ensure that the jumpers setting the I/O area for the 284x match the 
.Tn EISA
slot into which the card is inserted to prevent conflicts with other
.Tn EISA
cards.
.Sh BUGS
Some Quantum drives (at least the Empire 2100 and 1080s) will not run on an
.Tn AIC7870
Rev B in synchronous mode at 10MHz.  Controllers with this problem have a
42 MHz clock crystal on them and run slightly above 10MHz.  This causes the
drive much confusion.  Setting a maximum synchronous negotiation rate of 8MHz
in the 
.Tn SCSI-Select
utility
will allow normal function.
.Sh SEE ALSO
.Xr aha 4 ,
.Xr ahb 4 ,
.Xr cd 4 ,
.Xr scsi 4 ,
.Xr sd 4 ,
.Xr st 4
.Sh AUTHORS
The
.Nm
driver was written by
.An Justin Gibbs .
The
.Tn AIC7xxx
sequencer-code assembler was
written by
.An John Aycock .
.Sh HISTORY
The
.Nm
driver appeared in
.Fx 2.1 .
