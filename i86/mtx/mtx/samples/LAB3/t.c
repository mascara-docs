/************ main.c file **********************************/
#define NPROC     8        
#define SSIZE  1024                /* kstack int size */

#define DEAD      0                /* proc status     */
#define READY     1      

typedef struct proc{
    struct proc *next;   
           int  ksp;               /* saved sp; offset = 2 */
           int  pid;
           int  status;            /* READY|DEAD, etc */
           int  kstack[SSIZE];     // kmode stack of task
}PROC;


PROC proc[NPROC], *running, *readyQueue;
int  procSize = sizeof(PROC);

/****************************************************************
 Initialize the proc's as shown:
        running ---> proc[0];
        readyQueue -> proc[1] --> proc[2] ... --> proc[NPROC-1] --> NULL
        Each proc's kstack contains:
        retPC, ax, bx, cx, dx, bp, si, di, flag;  all 2 bytes
*****************************************************************/

#include "io.c"

int body();  

int initialize()
{
  int i, j;
  PROC *p;

  for (i=1; i < NPROC; i++){
    p = &proc[i];
    p->next = &proc[i+1];
    p->pid = i;
    p->status = READY;
    
    for (j=1; j<10; j++)
         p->kstack[SSIZE - j] = 0;         // all saved registers = 0
    p->kstack[SSIZE-1]=(int)body;          // called tswitch() from body
    p->ksp = &(p->kstack[SSIZE-9]);        // ksp -> kstack top
  }
  proc[NPROC-1].next = 0;

  running = &proc[0];
  running->pid = 0;
  running->status = READY;
  readyQueue = &proc[1];
  ps();
  printf("initialization complete\n");
}

int ps()
{
  PROC *p = readyQueue;

  while(p){
    printf("%d -> ", p->pid);
    p = p->next;
  }
  printf("\n");
}

char *gasp[4]={
     "Oh! You are killing me .......\n",
     "Oh! I am dying ...............\n", 
     "Oh! I am a goner .............\n", 
     "Bye! Bye! World...............\n",      
};

int grave(){
  printf("\n*****************************************\n"); 
  printf("proc %d %s\n", running->pid, gasp[(running->pid) % 4]);
  printf("*****************************************\n");
  running->status = DEAD;

  tswitch();   /* journey of no return */        
}

int body()
{  char c;
   while(1){
     ps(); 
      printf("I am Proc %d ", running->pid);
       printf("Input a char : [s|q] ");
       c=getc();
       printf("%c\n", c);
       switch(c){
            case 's': tswitch(); break;
	    case 'q': grave();   break;
            default :            break;  
       }
   }
}


main()
{
   printf("\nWelcome to the 460 Multitasking System\n");
   initialize(); 
   printf("P0 switch to P1\n"); 
   tswitch();
   printf("back to main\n");
}


int scheduler()
{
  PROC *p;

  if (running->status != DEAD){
    p = readyQueue;

    while (p->next)
      p = p->next;
    p->next = running;
    running->next = 0;
  }
  running = readyQueue;
  readyQueue = running->next;

  printf("\n-----------------------------\n");
  printf("next running proc = %d\n", running->pid);
  printf("-----------------------------\n");
}


