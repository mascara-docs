/* $Header: /home/ncvs/src/gnu/usr.bin/patch/version.h,v 1.2 1995/05/30 05:02:40 rgrimes Exp $
 *
 * $Log: version.h,v $
 * Revision 1.2  1995/05/30 05:02:40  rgrimes
 * Remove trailing whitespace.
 *
 * Revision 1.1.1.1  1993/06/19  14:21:52  paul
 * b-maked patch-2.10
 *
 * Revision 2.0  86/09/17  15:40:14  lwall
 * Baseline for netwide release.
 *
 */

void version();
