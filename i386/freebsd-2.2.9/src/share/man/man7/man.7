.\" Copyright (c) 1998.
.\"	The FreeBSD Project.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by the University of
.\"	California, Berkeley and its contributors.
.\" 4. Neither the name of the University nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man7/man.7,v 1.1.2.1 1999/09/05 11:27:09 peter Exp $
.\"
.Dd November 30, 1998
.Os
.Dt MAN 7
.Sh NAME
.Nm man
.Nd quick reference guide for the
.Nm \-man
macro package
.Sh SYNOPSIS
.Nm groff
.Fl m Ns Ar an
.Ar
.Sh DESCRIPTION
The
.Nm \-man
package is a set of macros used to format
.Ux
manual pages.  On
.Bx
systems, the use of
.Nm
is deprecated and the more expressive
.Nm mdoc
package is recommended in its place.
.Sh USAGE
.Ss Conventions
.Nm
macros are named using one or two upper case alphabetic characters.
Following regular
.Xr troff 1
convention, each macro request starts with a 
.Li "."
as the first character of a line.  Arguments to macro requests
expecting printable text may consist of zero to six words.  Some macros will
process the next input line if no arguments are supplied.  For
example, a
.Li ".I"
request on a line by itself will cause the next input line to be set
in italics.
Whitespace characters may be embedded in an argument by enclosing
it in quotes.  Type font and size are reset to their defaults before
each paragraph and after processing font size and face changing macros.
.Ss Indentation
The prevailing indent distance is remembered between successive
indented paragraphs and is reset to the default on reaching a
non-indented paragraph.  Default units for indents are 
.Dq ens .
.Ss Preprocessing
The 
.Xr man 1
program is conventionally used to format and display manual pages.  If
the first line of the manual page source starts with the literal string
.Li \&\'\e"
.\" " bring emacs's font-lock mode back in sync ...
then the remaining letters on the line indicate preprocessors that
need to be run prior to formatting with
.Xr troff 1 .
Supported preprocessing directives are:
.Bl -column "Letter" "Preprocessor" -offset indent
.It Em Letter Ta Em Preprocessor
.It e Ta Xr eqn 1
.It g Ta Xr grap 1
.It p Ta Xr pic 1
.It r Ta Xr refer 1
.It t Ta Xr tbl 1
.It v Ta Xr vgrind 1
.El
.Ss Available Strings
The
.Nm
package has the following predefined strings:
.Bl -column "String" "XXXXXXXXXXXXXXXXXXXXXXXXXXXX" -offset indent
.It Em String Ta Em Description
.It "\e*R" Ta "registration symbol"
.It "\e*S" Ta "change to default font size"
.It "\e*(Tm" Ta "trademark symbol"
.It "\e*(lq" Ta "left quote"
.It "\e*(rq" Ta "right quote"
.El
.Pp
.Ss Available Macros
The available macros are presented in alphabetical order.
.Bl -tag -width "XXX XX"
.It ".B" Op Ar words
typeset
.Ar words
using a bold face.  Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".BI" Op Ar words
join
.Ar words
alternating bold and italic faces.  Does not cause a line break.  If
no arguments are given the next text line is processed.
.It ".BR" Op Ar words
join
.Ar words
alternating bold and roman faces.  Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".DT"
restore the default tab spacing of 0.5 inches.  Does not cause a line
break.
.It ".HP" Op Ar indent
Begin a paragraph with a hanging indent and sets the prevailing indent
to
.Ar indent .
This request forces a line break.  If
.Ar indent
is not specified, the value of the prevailing indent is used.
.It ".I" Op Ar words
typeset
.Ar words
using an italic face.  Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".IB" Op Ar words
join 
.Ar words
alternating italic and bold faces.  Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".IP" Op Ar tag Op Ar indent
Begin an indented paragraph with tag
.Ar tag
and prevailing indent set to
.Ar indent .
If 
.Ar tag
is not specified it is taken to be the null string
.Qq "" .
If
.Ar indent
is not specified it is taken to be the prevailing indent.
.It ".IR" Op Ar words
join
.Ar words
alternating italic and roman faces. Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".LP"
begin a left-aligned paragraph.  The prevailing indent is set to the
default.  This request forces a line break.
.It "\&.P"
aliased to \&.LP.
.It ".PD" Op Ar distance
set the vertical distance between paragraphs to
.Ar distance .
If argument
.Ar distance
is not specified a value of 0.4v is used.
.It ".PP"
aliased to \&.LP.
.It ".RE"
end of a relative indent (see \&.RS below).  This request forces a
line break and restores the prevailing indent to its previous value.
.It ".RB" Op Ar words
join 
.Ar words
alternating roman and bold faces.  Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".RI" Op Ar words
join
.Ar words
alternating roman and italic faces.  Does not cause a line break.  If no
arguments are given the next text line is processed.
.It ".RS" Op Ar indent
start a relative indent, increasing the indentation by
.Ar indent .
If argument
.Ar indent
is not specified, the value of the prevailing indent is used.
.It ".SB" Op Ar words
typeset
.Ar words
using a bold face after reducing the font size by 1 point.
Does not cause a line break.  If no arguments are given the next text
line is processed.
.It ".SH" Op Ar words
specifies a section heading.  This request forces a line break.
It resets the prevailing indent and margins to their defaults.
.It ".SM" Op Ar words
typeset
.Ar words
after reducing the font size by 1 point.  Does not cause a line break.
If no arguments are given the next text line is processed.
.It ".SS" Op Ar words
specifies a section subheading.  This request forces a line
break.  If no arguments are given the next text line is processed.
It resets the prevailing indent and margins to their defaults.
.It ".TH" Ar name Ar section Ar date Xo
.Op Ar footer Op Ar center
.Xc
Begin reference page
.Ar name
belonging to section
.Ar section .
The third argument
.Ar date ,
is the date of the most recent change.  If present,
.Ar footer
specifies the left page footer text and 
.Ar center
specifies the center header text.  This request must the very first
request in the manual page.
.It ".TP" Op Ar indent
begin an indented paragraph with the tag specified in the next text
line.  If argument
.Ar indent
is given, it specifies the new value of the prevailing indent.
This request forces a line break.
.El
.Sh PAGE STRUCTURE
Most manual pages follow the general structure outlined below:
.Bl -tag -width ".SH NAME"
.It ".TH" Ar title Op Ar section-number
The very first macro request in a manual page has to be the \&.TH
request which establishes the name and title of the manual page.  The
\&.TH request also establishes the number of the manual page section.
.It ".SH NAME"
The name, or list of names, by which the command is called, followed
by a dash and a one-line summary of the action performed.  This
section should not contain any
.Nm troff
commands or escapes, or any macro requests.  This section is used to
generate the database used by the
.Xr whatis 1
command.
.It ".SH SYNOPSIS"
A brief summary of the usage of the command or function being
described.
.Bl -tag -width "Commands"
.It Commands
The syntax of the command and its arguments as would be typed on the
command line.  Words that have to be typed exactly as printed are to
be presented in bold face.  Arguments are indicated by the use of an
italic face.  Arguments and command names so indicated should not be
capitalized, even when starting a sentence.
.Pp
Syntactic symbols used should appear in roman face:
.Bl -tag -width "XXX"
.It "[]"
square brackets are used to indicate optional arguments.
.It "|"
vertical bars are used to indicate a one of many exclusive choice.
Only one item from a list separated by vertical bars is to be selected.
.It "..."
an ellipsis following an argument is used to indicate that the
arguments can be repeated.  When an ellipsis follows a bracketed set,
the expression within the brackets can be repeated.
.El
.It Functions
Required data declarations or
.Li "#include"
directives are to be shown first, followed by the function declaration.
.El
.It ".SH DESCRIPTION"
An overview of the command or functions external behavior, including
its interactions with files or data, how standard input, standard
output and standard error are handled.  Internals and implementation
details are not normally specified.  The question answered by this 
section is "what does it do?" or "what is it for?".
.Pp
Literal text, filenames and references to items that appear elsewhere
in the reference manuals should be presented using a constant width
face. Arguments should be presented using an italic face.
.It ".SH OPTIONS"
The list of options together with a description of how each affects
the commands operation.
.It ".SH USAGE"
This section is optional and contains a detailed description of the
subcommands and input grammar understood by the command.
.It ".SH RETURN VALUES"
The list of return values a library routine could return to the caller,
with the conditions that cause these values to be returned.
.It ".SH EXIT STATUS"
The list of values returned as the exit status of the command, with
the conditions that cause these values to be returned.
.It ".SH FILES"
The list of files associated with the command or function.
.It ".SH SEE ALSO"
A comma separated list of related manual pages followed by references
to other published documentation.
.It ".SH DIAGNOSTICS"
A list of diagnostic messages with corresponding explanations.
.It ".SH BUGS"
Known defects and limitations, if any.
.El
.Sh FILES
.Bl -tag -width "/usr/share/lib/tmac/tmac.groff_an"
.It "/usr/share/lib/tmac/tmac.an"
Initial file defining the
.Nm
package.
.It "/usr/share/lib/tmac/tmac.groff_an"
.Nm groff
source for macro definitions.
.It "/usr/share/lib/tmac/man.local"
local modifications to the
.Nm
package.
.El
.Sh SEE ALSO
.Xr apropos 1 ,
.Xr groff 1 ,
.Xr man 1 ,
.Xr nroff 1 ,
.Xr troff 1 ,
.Xr whatis 1 ,
.Xr mdoc 7 ,
.Xr mdoc.samples 7
.Sh HISTORY
This manual page was written by
.An "Joseph Koshy"
.Ad Aq jkoshy@freebsd.org .
