.\" $FreeBSD: src/usr.bin/file/file.1,v 1.6.2.3 1999/09/05 11:32:08 peter Exp $
.Dd July 30, 1997
.Dt FILE 1 "Copyright but distributable"
.Os
.Sh NAME
.Nm file
.Nd determine file type
.Sh SYNOPSIS
.Nm file
.Op Fl vczL
.Op Fl f Ar namefile
.Op Fl m Ar magicfiles
.Ar
.Sh DESCRIPTION
This manual page documents version 3.22 of the
.Nm
command.
.Nm File
tests each argument in an attempt to classify it.
There are three sets of tests, performed in this order:
filesystem tests, magic number tests, and language tests.
The
.Em first
test that succeeds causes the file type to be printed.
.Pp
The type printed will usually contain one of the words
.Em text
(the file contains only
.Tn ASCII
characters and is probably safe to read on an
.Tn ASCII
terminal),
.Em executable
(the file contains the result of compiling a program
in a form understandable to some 
.Ux
kernel or another),
or
.Em data
meaning anything else (data is usually `binary' or non-printable).
Exceptions are well-known file formats (core files, tar archives)
that are known to contain binary data.
When modifying the file
.Pa /usr/share/misc/magic
or the program itself, 
.Em "preserve these keywords" .
.Pp
People depend on knowing that all the readable files in a directory
have the word ``text'' printed.
Don't do as Berkeley did \- change ``shell commands text''
to ``shell script''.
.Pp
The filesystem tests are based on examining the return from a
.Xr stat 2
system call.
The program checks to see if the file is empty,
or if it's some sort of special file.
Any known file types appropriate to the system you are running on
(sockets, symbolic links, or named pipes (FIFOs) on those systems that
implement them)
are intuited if they are defined in
the system header file
.Pa sys/stat.h  .
.Pp
The magic number tests are used to check for files with data in
particular fixed formats.
The canonical example of this is a binary executable (compiled program)
.Pa a.out
file, whose format is defined in 
.Pa a.out.h
and possibly
.Pa exec.h
in the standard include directory.
These files have a `magic number' stored in a particular place
near the beginning of the file that tells the
.Ux
operating system
that the file is a binary executable, and which of several types thereof.
The concept of `magic number' has been applied by extension to data files.
Any file with some invariant identifier at a small fixed
offset into the file can usually be described in this way.
The information in these files is read from the magic file
.Pa /usr/share/misc/magic .
.Pp
If an argument appears to be an
.Tn ASCII
file,
.Nm
attempts to guess its language.
The language tests look for particular strings (cf
.Pa names.h )
that can appear anywhere in the first few blocks of a file.
For example, the keyword
.Em .br
indicates that the file is most likely a
.Xr troff 1
input file, just as the keyword 
.Em struct
indicates a C program.
These tests are less reliable than the previous
two groups, so they are performed last.
The language test routines also test for some miscellany
(such as 
.Xr tar 1
archives) and determine whether an unknown file should be
labelled as `ascii text' or `data'. 
.Sh OPTIONS
.Bl -tag -width indent
.It Fl v
Print the version of the program and exit.
.It Fl m Ar list
Specify an alternate
.Ar list
of files containing magic numbers.
This can be a single file, or a colon-separated list of files.
.It Fl z
Try to look inside compressed files.
.It Fl c
Cause a checking printout of the parsed form of the magic file.
This is usually used in conjunction with 
.Fl m
to debug a new magic file before installing it.
.It Fl f Ar namefile
Read the names of the files to be examined from 
.Ar namefile
(one per line) 
before the argument list.
Either 
.Ar namefile
or at least one filename argument must be present;
to test the standard input, use ``-'' as a filename argument.
.It Fl L
Cause symlinks to be followed, as the like-named option in
.Xr ls 1 .
(on systems that support symbolic links).
.El
.Sh FILES
.Bl -tag -width /usr/share/misc/magic -compact
.It Pa /usr/share/misc/magic
default list of magic numbers (used to be
.Pa /etc/magic
in previous versions of
.Bx Free )
.El
.Sh ENVIRONMENT
The environment variable
.Em MAGIC
can be used to set the default magic number files.
.Sh SEE ALSO
.Xr od 1 ,
.Xr strings 1 ,
.Xr magic 5
.Sh STANDARDS CONFORMANCE
This program is believed to exceed the System V Interface Definition
of FILE(CMD), as near as one can determine from the vague language
contained therein. 
Its behaviour is mostly compatible with the System V program of the same name.
This version knows more magic, however, so it will produce
different (albeit more accurate) output in many cases. 
.Pp
The one significant difference 
between this version and System V
is that this version treats any white space
as a delimiter, so that spaces in pattern strings must be escaped.
For example,
.br
>10	string	language impress\ 	(imPRESS data)
.br
in an existing magic file would have to be changed to
.br
>10	string	language\e impress	(imPRESS data)
.br
In addition, in this version, if a pattern string contains a backslash,
it must be escaped.  For example
.br
0	string		\ebegindata	Andrew Toolkit document
.br
in an existing magic file would have to be changed to
.br
0	string		\e\ebegindata	Andrew Toolkit document
.br
.Pp
SunOS releases 3.2 and later from Sun Microsystems include a
.Xr file 1
command derived from the System V one, but with some extensions.
My version differs from Sun's only in minor ways.
It includes the extension of the `&' operator, used as,
for example,
.br
>16	long&0x7fffffff	>0		not stripped
.Sh MAGIC DIRECTORY
The magic file entries have been collected from various sources,
mainly USENET, and contributed by various authors.
.An Christos Zoulas
(address below) will collect additional
or corrected magic file entries.
A consolidation of magic file entries 
will be distributed periodically.
.Pp
The order of entries in the magic file is significant.
Depending on what system you are using, the order that
they are put together may be incorrect.
If your old
.Nm
command uses a magic file,
keep the old magic file around for comparison purposes
(rename it to 
.Pa /usr/share/misc/magic.orig Ns ).
.Sh HISTORY
There has been a 
.Nm
command in every
.Ux
since at least Research Version 6
(man page dated January, 1975).
The System V version introduced one significant major change:
the external list of magic number types.
This slowed the program down slightly but made it a lot more flexible.
.Pp
This program, based on the System V version,
was written by
.An Ian Darwin
without looking at anybody else's source code.
.Pp
.An John Gilmore
revised the code extensively, making it better than
the first version.
.An Geoff Collyer
found several inadequacies
and provided some magic file entries.
The program has undergone continued evolution since.
.Sh AUTHORS
Written by
.An Ian F. Darwin Aq ian@sq.com ,
UUCP address {utzoo | ihnp4}!darwin!ian,
postal address: P.O. Box 603, Station F, Toronto, Ontario, CANADA M4Y 2L8.
.Pp
Altered by
.An Rob McMahon Aq cudcv@warwick.ac.uk ,
1989, to extend the `&' operator
from simple `x&y != 0' to `x&y op z'.
.Pp
Altered by
.An Guy Harris Aq guy@auspex.com ,
1993, to:
.Bl -item -offset indent
.It
put the ``old-style'' `&'
operator back the way it was, because
.Bl -enum -offset indent
.It
Rob McMahon's change broke the
previous style of usage,
.It
The SunOS ``new-style'' `&' operator, which this version of
.Nm
supports, also handles `x&y op z',
.It
Rob's change wasn't documented in any case;
.El
.It
put in multiple levels of `>';
.It
put in ``beshort'', ``leshort'', etc. keywords to look at numbers in the
file in a specific byte order, rather than in the native byte order of
the process running
.Nm file .
.El
.Pp
Changes by
.An Ian Darwin
and various authors including
.An Christos Zoulas Aq christos@deshaw.com ,
1990-1992.
.Sh LEGAL NOTICE
Copyright (c) Ian F. Darwin, Toronto, Canada,
1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993.
.Pp
This software is not subject to and may not be made subject to any
license of the American Telephone and Telegraph Company, Sun
Microsystems Inc., Digital Equipment Inc., Lotus Development Inc., the
Regents of the University of California, The X Consortium or MIT, or
The Free Software Foundation.
.Pp
This software is not subject to any export provision of the United States
Department of Commerce, and may be exported to any country or planet.
.Pp
Permission is granted to anyone to use this software for any purpose on
any computer system, and to alter it and redistribute it freely, subject
to the following restrictions:
.Pp 
.Bl -enum -offset indent
.It
The author is not responsible for the consequences of use of this
software, no matter how awful, even if they arise from flaws in it;
.It
The origin of this software must not be misrepresented, either by
explicit claim or by omission.  Since few users ever read sources,
credits must appear in the documentation;
.It
Altered versions must be plainly marked as such, and must not be
misrepresented as being the original software.  Since few users
ever read sources, credits must appear in the documentation;
.It
This notice may not be removed or altered.
.El
.Pp
A few support files (
.Fn getopt ,
.Fn strtok )
distributed with this package
are by
.An Henry Spencer
and are subject to the same terms as above.
.Pp
A few simple support files (
.Fn strtol ,
.Fn strchr )
distributed with this package
are in the public domain; they are so marked.
.Pp
The files
.Pa tar.h
and
.Pa is_tar.c
were written by
.An John Gilmore
from his public-domain
.Nm tar
program, and are not covered by the above restrictions.
.Sh BUGS
There must be a better way to automate the construction of the Magic
file from all the glop in Magdir. What is it?
Better yet, the magic file should be compiled into binary (say,
.Xr ndbm 3
or, better yet, fixed-length
.Tn ASCII
strings for use in heterogenous network environments) for faster startup.
Then the program would run as fast as the Version 7 program of the same name,
with the flexibility of the System V version.
.Pp
.Nm File
uses several algorithms that favor speed over accuracy,
thus it can be misled about the contents of
.Tn ASCII
files.
.Pp
The support for
.Tn ASCII
files (primarily for programming languages)
is simplistic, inefficient and requires recompilation to update.
.Pp
There should be an ``else'' clause to follow a series of continuation lines.
.Pp
The magic file and keywords should have regular expression support.
Their use of
.Tn ASCII TAB
as a field delimiter is ugly and makes
it hard to edit the files, but is entrenched.
.Pp
It might be advisable to allow upper-case letters in keywords
for e.g.,
.Xr troff 1
commands vs man page macros.
Regular expression support would make this easy.
.Pp
The program doesn't grok \s-2FORTRAN\s0.
It should be able to figure \s-2FORTRAN\s0 by seeing some keywords which 
appear indented at the start of line.
Regular expression support would make this easy.
.Pp
The list of keywords in 
.Em ascmagic
probably belongs in the Magic file.
This could be done by using some keyword like `*' for the offset value.
.Pp
Another optimization would be to sort
the magic file so that we can just run down all the
tests for the first byte, first word, first long, etc, once we
have fetched it.  Complain about conflicts in the magic file entries.
Make a rule that the magic entries sort based on file offset rather
than position within the magic file?
.Pp
The program should provide a way to give an estimate 
of ``how good'' a guess is.
We end up removing guesses (e.g. ``From '' as first 5 chars of file) because
they are not as good as other guesses (e.g. ``Newsgroups:'' versus
"Return-Path:").  Still, if the others don't pan out, it should be
possible to use the first guess.  
.Pp
This program is slower than some vendors'
.Nm
commands.
.Pp
This manual page, and particularly this section, is too long.
.Sh AVAILABILITY
You can obtain the original author's latest version by anonymous FTP
on
.Em ftp.deshaw.com
in the directory
.Pa /pub/file/file-X.YY.tar.gz
