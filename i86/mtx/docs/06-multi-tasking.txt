460 Notes #3


               CS460 NOTES on Multitasking

1. An Example Program:

============== main.c file ======================
typedef struct proc{
        struct proc *next;
               int  ksp;
}PROC;

PROC mainProc, *running;
int  procSize = sizeof(PROC);

prints(s) char *s;
{ YOUR prints() function }

main()
{
  running = &mainProc;
  prints("call tswitch()\n\r");
     tswitch();
  prints("back to main()\n\r");
}

int scheduler()
{
   prints("in scheduler\n\r");
}

================= s.s file ==================
.globl begtext, begdata, begbss                      ! needed by linker
.globl _main, _getc, _putc
.globl _tswitch, _running, _scheduler

.text                                                ! these tell as:
begtext:                                             ! text,data,bss segments
.data                                                ! are all the same.
begdata:
.bss
begbss:
.text

start:
        mov     ax,cs                   ! establish segments
        mov     ds,ax                   ! we know ES,CS=0x1000. Let DS=CS
        mov     ss,ax                   ! SS = CS ===> all point to 0x1000
        mov     es,ax
        mov     sp,#0                   ! SP = 64KB

        call _main                      ! call main[] in C

dead:   jmp dead

_getc:
        xorb   ah,ah
        int    0x16
        ret

_putc:
        push   bp
        mov    bp,sp
        movb   al,4[bp]
        movb   ah,#14
        mov    bx,#0x000B
        int    0x10
        pop    bp
        ret


_tswitch:
SAVE:   push ax
        push bx
        push cx
        push dx
        push bp
        push si
        push di
        pushf
        mov   bx, _running
        mov   2[bx], sp

FIND:   call _scheduler

RESUME: mov   bx, _running
        mov   sp, 2[bx]
        popf
        pop  di
        pop  si
        pop  bp
        pop  dx
        pop  cx
        pop  bx
        pop  ax
        ret
=============================================
          UNDER Linux:
   as86 -o s.o s.s
   bcc  -c main.c
   ld86 -d s.o main.o /usr/lib/bcc/libc.a
   mount /dev/fd0 /fd0
   cp a.out /fd0/boot/mtx
   umount /fd0

   Use YOUR MTX booter of LAB#1 (OR mtx_booter from the samples/LAB3/)

   dd if=mtx_booter of=/dev/fd0

   Boot mtx from FD to run the program.
   It does practically NOTHING.

   However, it is the basis of all multitasking systems. Note that in
   scheduler() we may let running -> a different PROC structure.
   Then tswitch() would "resume" that PROC's saved context.

   -------------------------------------------------------------------------
   To see this, let's define a newPORC structure, together with a new stack
   area,  int newStack[1024].

   Although newProc never existed before, we may PRETEND that it was running
   before and it gave up CPU by calling tswitch().

   When calling tswitch(), it saved returnPC on its stack in newStack[1023].
   Then, it executed SAVE, whcih saved CPU registers ax to flag
                  retPC  ax, bx, cx, dx, bp, si, di, flag;
   in newStack[]:  -1    -2  -3  -4  -5  -6  -7  -8   -9

   and  newProc.savedSp -> newStack[1024-9].
   Then, it called secheduler(), in which it gave up CPU (to our mainProc).

   What should be the retPC?  It can be the entry address of any executable
   code. For example, a function    body(){ ........}
   What should be the values of the "saved" registers? They can be anything,
   except for flag, which must be 0. So, we may set all of them to 0.
   -------------------------------------------------------------------------

   Now, in scheduler(), if we let running point to newProc,  then the RESUME
   part of tswitch() will "resume" newProc, causing it to "return" to body().

   Changing CPU's execution environment (i.e. CPU register contents, hence
   stack area and "point of execution") from one "activity" to that of another
   is called CONTEXT SWITCHING.

2. A Simple Multi-tasking System

  (s.s, t.c and mk are in ~samples/LAB2/; Download and test run)


!----------------- s.s file -----------------------------------------------
.globl begtext, begdata, begbss                      ! needed by linker

.globl _tswitch,_getc,_putc                          ! EXPORT these
.globl _main,_running,_scheduler,_proc,_procSize     ! IMPORT these

.text                                                ! these tell as:
begtext:                                             ! text,data,bss segments
.data                                                ! are all the same.
begdata:
.bss
begbss:
.text

start:
        mov     ax,cs                   ! establish segments
        mov     ds,ax                   ! Let DS,SS,ES = CS=0x1000.
        mov     ss,ax
        mov     es,ax

        mov     sp,#_proc               ! sp -> proc[0]
        add     sp,_procSize            ! sp -> proc[0]'s HIGH END

        call _main                      ! call main() in C

dead:   jmp dead                        ! loop if main() ever returns


_tswitch:
SAVE:   push ax
        push bx
        push cx
        push dx
        push bp
        push si
        push di
        pushf
        mov   bx, _running
        mov   2[bx], sp

FIND:   call _scheduler

RESUME: mov   bx, _running
        mov   sp, 2[bx]
        popf
        pop  di
        pop  si
        pop  bp
        pop  dx
        pop  cx
        pop  bx
        pop  ax
        ret


_getc:
        xorb   ah,ah
        int    0x16
        ret

_putc:
        push   bp
        mov    bp,sp
        movb   al,4[bp]
        movb   ah,#14
        mov    bx,#0x000B   ! CYAN color
        int    0x10
        pop    bp
        ret
!----------------- end of s.s file ---------------------------


/************ t.c file **********************************/
#define NPROC     9
#define SSIZE  1024                /* kstack int size */

#define DEAD      0                /* proc status     */
#define READY     1

typedef struct proc{
    struct proc *next;
           int  ksp;               /* saved sp; offset = 2 */
           int  pid;
           int  status;            /* READY|DEAD, etc */
           int  kstack[SSIZE];     // kmode stack of task
}PROC;


#include "io.c"   /**** USE YOUR OWN io.c with printf() here *****/

PROC proc[NPROC], *running;

int  procSize = sizeof(PROC);

/****************************************************************
 Initialize the proc's as shown:
        running ---> proc[0] -> proc[1];

        proc[1] to proc[N-1] form a circular list:

        proc[1] --> proc[2] ... --> proc[NPROC-1] -->
          ^                                         |
          |<---------------------------------------<-

        Each proc's kstack contains:
        retPC, ax, bx, cx, dx, bp, si, di, flag;  all 2 bytes
*****************************************************************/

int body();

int initialize()
{
  int i, j;
  PROC *p;

  for (i=0; i < NPROC; i++){
    p = &proc[i];
    p->next = &proc[i+1];
    p->pid = i;
    p->status = READY;

    if (i){     // initialize kstack[ ] of proc[1] to proc[N-1]
      for (j=1; j<10; j++)
          p->kstack[SSIZE - j] = 0;          // all saved registers = 0
      p->kstack[SSIZE-1]=(int)body;          // called tswitch() from body
      p->ksp = &(p->kstack[SSIZE-9]);        // ksp -> kstack top
    }
  }
  running = &proc[0];
  proc[NPROC-1].next = &proc[1];
  printf("initialization complete\n");
}

char *gasp[NPROC]={
     "Oh! You are killing me .......\n",
     "Oh! I am dying ...............\n",
     "Oh! I am a goner .............\n",
     "Bye! Bye! World...............\n",
};

int grave(){
  printf("\n*****************************************\n");
  printf("Task %d %s\n", running->pid,gasp[(running->pid) % 4]);
  printf("*****************************************\n");
  running->status = DEAD;

  tswitch();   /* journey of no return */
}

int ps()
{
  PROC *p;

  printf("running = %d\n", running->pid);

  p = running;
  p = p->next;
  printf("readyProcs = ");
  while(p != running && p->status==READY){
    printf("%d -> ", p->pid);
    p = p->next;
  }
  printf("\n");
}

int body()
{  char c;
   while(1){
      ps();
      printf("I am Proc %d in body()\n", running->pid);
      printf("Input a char : [s|q] ");
       c=getc();
       switch(c){
            case 's': tswitch(); break;
            case 'q': grave();   break;
            default :            break;
       }
   }
}


main()
{
 printf("\nWelcome to the 460 Multitasking System\n");
   initialize();
   printf("P0 switch to P1\n");
   tswitch();
 printf("P0 resumes: all dead, happy ending\n");
}


int scheduler()
{
    PROC *p;
    p = running->next;

    while (p->status != READY && p != running)
      p = p->next;

    if (p == running)
       running = &proc[0];
    else
       running = p;

    printf("\n-----------------------------\n");
    printf("next running proc = %d\n", running->pid);
    printf("-----------------------------\n");
}

//=======================================================================
# mk file

   as86 -o s.o s.s
   bcc  -c t.c
   ld86 -d s.o t.o /usr/lib/bcc/libc.a

#  mount /dev/fd0 /fd0
   mount -o loop /root/dosemu/mtximage /fd0
   cp a.out /fd0/boot/mtx
   umount /fd0

#   Use YOUR MTX booter of LAB#1 to boot up MTX


