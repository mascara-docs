.\" $FreeBSD: src/lib/libc/gen/getvfsent.3,v 1.4.2.4 1999/09/05 11:16:27 peter Exp $
.\"	Written by Garrett A. Wollman, September 1994.
.\"	This manual page is in the public domain.
.\"
.Dd September 24, 1994
.Dt GETVFSBYNAME 3
.Os
.Sh NAME
.Nm getvfsbyname ,
.Nm getvfsbytype ,
.Nm getvfsent ,
.Nm setvfsent ,
.Nm endvfsent ,
.Nm vfsisloadable ,
.Nm vfsload
.Nd manage virtual filesystem modules
.Sh SYNOPSIS
.Fd #include <sys/param.h>
.Fd #include <sys/mount.h>
.Ft struct vfsconf *
.Fn getvfsbyname "const char *name"
.Ft struct vfsconf *
.Fn getvfsbytype "int type"
.Ft struct vfsconf *
.Fn getvfsent "void"
.Ft void
.Fn setvfsent "int cachelist"
.Ft void
.Fn endvfsent "void"
.Ft int
.Fn vfsisloadable "const char *name"
.Ft int
.Fn vfsload "const char *name"
.Sh DESCRIPTION
The
.Fn getvfsbyname ,
.Fn getvfsbytype ,
and
.Fn getvfsent
functions provide convenient access to a list of installed virtual
filesystem modules managed by the kernel.
.Fn getvfsbyname
and
.Fn getvfsbytype
provide direct access to a single filesystem's information;
.Fn getvfsent
steps through the list of filesystems one at a time.  All three
functions return a null pointer when no data is available.  The fields
in a
.Dq Li struct vfsconf
are as follows:
.Pp
.Bl -tag -compact -width vfc_refcount
.It vfc_name
the name of the filesystem
.It vfc_index
the filesystem type number assigned by the kernel and used in calls to
.Xr mount 2
.It vfc_refcount
the number of references to this filesystem
(usually the number of mounts, but one greater for filesystems which
cannot be unloaded or which are statically linked into the kernel)
.It vfc_flags
flag bits, of which none are currently defined
.El
.Pp
The
.Fn setvfsent
and
.Fn endvfsent
functions are used to control caching of the filesystem list, which is
obtained in toto from the kernel via
.Xr sysctl 3 .
If the
.Fa cachelist
parameter to
.Fn setvfsent
is non-zero, the list will be retrieved only once, upon the first call
to one of the retrieval functions, until
.Fn endvfsent
is called to clear the cache.  In general,
.Fn setvfsent 1
should be called by programs using the
.Fn getvfsent
function, and
.Fn setvfsent 0
(which is also the default state)
should be called by programs using the
.Fn vfsload
function.
.Pp
The
.Fn vfsisloadable
function returns a non-zero value if a later call to
.Fn vfsload name
is likely to succeed.  We say ``likely'' because the conditions
checked by
.Fn vfsisloadable
are only a small subset of the conditions necessary for
.Fn vfsload
to succeed.  In particular,
.Fn vfsisloadable
checks that
.Pa /dev/lkm
is present and can be opened for writing, and that
.Pa Ns Fa name Ns _mod.o
can be found in one of the directories designated for LKMs.
.Pp
The
.Fn vfsload
function attempts to load a kernel module implementing filesystem
.Fa name .
It returns zero if the filesystem module was successfully located and
loaded, or non-zero otherwise.  It should only be called in the
following circumstances:
.Bl -enum
.It
.Fn getvfsbyname name
has been called and returned a null pointer.
.It
.Fn vfsisloadable name
has been called and returned a non-zero value.
.It
sufficient temporary file space is available 
.Em and writable
in one of
.No Ns \&${ Ns Ev TMPDIR Ns \&} ,
.Pa /var/tmp ,
or
.Pa /tmp .
.El
.Pp
Here is an example, taken from the source to
.Xr mount_cd9660 8 :
.Bd -literal -offset indent

struct vfsconf *vfc;

/* setup code here */

vfc = getvfsbyname("cd9660");
if (!vfc && vfsisloadable("cd9660")) {
	if (vfsload("cd9660")) {
		err(EX_OSERR, "vfsload(cd9660)");
	}
	endvfsent();    /* flush cache */
	vfc = getvfsbyname("cd9660");
}
if (!vfc)
	errx(EX_OSERR, "cd9660 filesystem not available");

if (mount(vfc->vfc_index, dir, mntflags, &args) < 0)
	err(EX_OSERR, "%s", dev);

.Ed
.Sh RETURN VALUES
The
.Fn getvfsbyname ,
.Fn getvfsbytype ,
and
.Fn getvfsent
routines all return a pointer to the same static data structure when
they succeed, and return a null pointer when they fail.  On failure,
.Va errno
may be set to one of the values documented for
.Xr sysctl 3
or
.Xr malloc 3 ,
if a failure of that function was the cause; otherwise
.Va errno
will be unmodified.
.Pp
The
.Fn vfsload
function returns a non-zero value on failure, or zero on success.  If
.Fn vfsload
fails,
.Va errno
may be set to one of the values documented for
.Xr fork 2 ,
.Xr waitpid 2 ,
.Xr chdir 2 ,
or
.Xr execlp 3 ,
or the return status of the
.Xr modload 8
program, which is not currently particularly useful but eventually
will be.  In addition, if
.Xr modload 8
exits on a signal,
.Fn vfsload
will fail and set
.Va errno
to
.Er EINVAL .
.Sh ENVIRONMENT
.Bl -tag -compact -width TMPDIRx
.It Ev TMPDIR
location for temporary file created by
.Xr modload 8
on behalf of
.Fn vfsload .
.It Ev LKMDIR
alternate directory for
.Fn vfsisloadable
and
.Fn vfsload
to search for loadable modules.
.Sh SEE ALSO
.Xr mount 2 ,
.Xr modload 8 ,
.Xr mount 8
.Sh BUGS
The return value of the
.Xr modload 8
command isn't particularly useful when interpreted as an
.Va errno
value (or anything else, for that matter).
.Sh AUTHOR
The loadable filesystem support was written by Garrett A. Wollman,
based on generic loadable kernel module support by Terry Lambert.
.Sh HISTORY
The
.Fn getvfsbyname
family of functions first appeared in
.Fx 2.0 .
