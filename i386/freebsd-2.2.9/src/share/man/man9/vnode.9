.\" -*- nroff -*-
.\"
.\" Copyright (c) 1996 Doug Rabson
.\"
.\" All rights reserved.
.\"
.\" This program is free software.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man9/vnode.9,v 1.1.2.1 1999/09/05 11:27:13 peter Exp $
.\"
.Dd July 24, 1996
.Os
.Dt vnode 9
.Sh NAME
.Nm vnode
.Nd internal representation of a file or directory
.Sh SYNOPSIS
.Fd #include <sys/vnode.h>
.Pp
.Bd -literal
/*
 * Vnode types.  VNON means no type.
 */
enum vtype	{ VNON, VREG, VDIR, VBLK, VCHR, VLNK, VSOCK, VFIFO, VBAD };

/*
 * Vnode tag types.
 * These are for the benefit of external programs only (e.g., pstat)
 * and should NEVER be inspected by the kernel.
 */
enum vtagtype	{
	VT_NON, VT_UFS, VT_NFS, VT_MFS, VT_PC, VT_LFS, VT_LOFS, VT_FDESC,
	VT_PORTAL, VT_NULL, VT_UMAP, VT_KERNFS, VT_PROCFS, VT_AFS, VT_ISOFS,
	VT_UNION, VT_MSDOSFS, VT_DEVFS
};

/*
 * Each underlying filesystem allocates its own private area and hangs
 * it from v_data.  If non-null, this area is freed in getnewvnode().
 */
LIST_HEAD(buflists, buf);

typedef	int 	vop_t __P((void *));

struct vnode {
	u_long	v_flag;			/* vnode flags (see below) */
	int	v_usecount;		/* reference count of users */
	int	v_writecount;		/* reference count of writers */
	int	v_holdcnt;		/* page & buffer references */
	daddr_t	v_lastr;		/* last read (read-ahead) */
	u_long	v_id;			/* capability identifier */
	struct	mount *v_mount;		/* ptr to vfs we are in */
	vop_t	**v_op;			/* vnode operations vector */
	TAILQ_ENTRY(vnode) v_freelist;	/* vnode freelist */
	LIST_ENTRY(vnode) v_mntvnodes;	/* vnodes for mount point */
	struct	buflists v_cleanblkhd;	/* clean blocklist head */
	struct	buflists v_dirtyblkhd;	/* dirty blocklist head */
	long	v_numoutput;		/* num of writes in progress */
	enum	vtype v_type;		/* vnode type */
	union {
		struct mount	*vu_mountedhere;/* ptr to mounted vfs (VDIR) */
		struct socket	*vu_socket;	/* unix ipc (VSOCK) */
		struct specinfo	*vu_specinfo;	/* device (VCHR, VBLK) */
		struct fifoinfo	*vu_fifoinfo;	/* fifo (VFIFO) */
	} v_un;
	struct	nqlease *v_lease;	/* Soft reference to lease */
	daddr_t	v_lastw;		/* last write (write cluster) */
	daddr_t	v_cstart;		/* start block of cluster */
	daddr_t	v_lasta;		/* last allocation */
	int	v_clen;			/* length of current cluster */
	int	v_ralen;		/* Read-ahead length */
	int	v_usage;		/* Vnode usage counter */
	daddr_t	v_maxra;		/* last readahead block */
	void	*v_object;		/* Place to store VM object */
	enum	vtagtype v_tag;		/* type of underlying data */
	void 	*v_data;		/* private data for fs */
};
#define	v_mountedhere	v_un.vu_mountedhere
#define	v_socket	v_un.vu_socket
#define	v_specinfo	v_un.vu_specinfo
#define	v_fifoinfo	v_un.vu_fifoinfo

/*
 * Vnode flags.
 */
#define	VROOT	0x0001	/* root of its file system */
#define	VTEXT	0x0002	/* vnode is a pure text prototype */
#define	VSYSTEM	0x0004	/* vnode being used by kernel */
#define	VOLOCK	0x0008	/* vnode is locked waiting for an object */
#define	VOWANT	0x0010	/* a process is waiting for VOLOCK */
#define	VXLOCK	0x0100	/* vnode is locked to change underlying type */
#define	VXWANT	0x0200	/* process is waiting for vnode */
#define	VBWAIT	0x0400	/* waiting for output to complete */
#define	VALIASED 0x0800	/* vnode has an alias */
#define	VDIROP	0x1000	/* LFS: vnode is involved in a directory op */
#define	VVMIO	0x2000	/* VMIO flag */
#define	VNINACT	0x4000	/* LFS: skip ufs_inactive() in lfs_vunref */
#define	VAGE	0x8000	/* Insert vnode at head of free list */
.Ed
.Sh DESCRIPTION
The vnode is the focus of all file activity in UNIX.  There is a
unique vnode allocated for each active file, each current directory,
each mounted-on file, text file, and the root.
.Pp
Each vnode has two reference counts,
.Dv v_usecount
and
.Dv v_writecount .
The first is the number of clients within the kernel which are
using this vnode.  This count is maintained by
.Xr vref 9 ,
.Xr vrele 9 and
.Xr vput 9 .
When the
.Dv v_usecount
of a vnode reaches zero then the vnode may be reused for another
file, possibly in another filesystem.
The second is a count of the number of clients which are writing into
the file.  It is maintained by the
.Xr open 2
and
.Xr close 2
system calls.
.Pp
Any call which returns a vnode (e.g.
.Xr VFS_GET 9 ,
.Xr VOP_LOOKUP 9
etc.)
will increase the
.Dv v_usecount
of the vnode by one.  When the caller is finished with the vnode, it
should release this reference by calling
.Xr vrele 9
(or
.Xr vput 9
if the vnode is locked).
.Pp
Other commonly used members of the vnode structure are
.Dv v_id
which is used to maintain consistency in the name cache,
.Dv v_mount
which points at the filesystem which owns the vnode,
.Dv v_type
which contains the type of object the vnode represents and
.Dv v_data
which is used by filesystems to store filesystem specific data with
the vnode.
The
.Dv v_op
field is used by the
.Dv VOP_*
macros to call functions in the filesystem which implement the vnode's
functionality.
.Sh SEE ALSO
.Xr VFS 9
.Sh AUTHORS
This man page was written by Doug Rabson.

