.\"
.\" Copyright (c) 1997 Joerg Wunsch
.\"
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man9/uio.9,v 1.2.2.2 1999/09/05 11:27:13 peter Exp $
.\" "
.Dd February 2, 1997
.Os
.Dt UIO 9
.Sh NAME
.Nm uio ,
.Nm uiomove
.Nd device driver IO routines
.Sh SYNOPSIS
.Fd #include <sys/types.h>
.Fd #include <sys/cdefs.h>
.Fd #include <sys/uio.h>
.Pp
.Bd -literal
struct uio {
	struct	iovec *uio_iov;
	int	uio_iovcnt;
	off_t	uio_offset;
	int	uio_resid;
	enum	uio_seg uio_segflg;
	enum	uio_rw uio_rw;
	struct	proc *uio_procp;
};
.Ed
.Ft int
.Fn uiomove "caddr_t buf" "int howmuch" "struct uio *uiop"
.Sh DESCRIPTION
The function
.Fn uiomove
is used to handle transfer of data between buffers and IO vectors
that might possibly also cross the user/kernel space boundary.
.Pp
As a result of any
.Xr read 2 ,
.Xr write 2 ,
.Xr readv 2 ,
or
.Xr writev 2
system call that is being passed to a character-device driver, the
appropriate driver
.Em read
or
.Em write
entry will be called with a pointer to a
.Fa "struct uio"
being passed.  The transfer request is encoded in this structure.
The driver itself should use
.Fn uiomove
to get at the data in this structure.
.Pp
The fields in the uio structure are:
.Bl -tag -width "uio_iovcntXXXX" -compact
.It Dv uio_iov
The array of IO vectors to be processed.  In the case of scatter/gather
IO, this will be more than one vector.
.It Dv uio_iovcnt
The number of IO vectors present.
.It Dv uio_offset
The offset into the device.
.It Dv uio_resid
The number of bytes to process.
.It Dv uio_segflg
One of the following flags:
.Bl -tag -width "UIO_USERISPACEX" -compact
.It Dv UIO_USERSPACE
The IO vector points into a process's address space.
.It Dv UIO_SYSSPACE
The IO vector points into the kernel address space.
.It Dv UIO_USERISPACE
The IO vector points into the instruction area of a process's address
space.
.It Dv UIO_NOCOPY
Don't copy, already in object.
.El
.It Dv uio_rw uio_rw
The direction of the desired transfer, either
.Dv UIO_READ ,
or
.Dv UIO_WRITE .
.It Dv uio_procp
The pointer to a
.Li struct proc
for the associated process; used if
.Dv uio_segflg
indicates that the transfer is to be made from/to a process's address
space.
.El
.Sh EXAMPLES
The idea is that the driver maintains a private buffer for its data,
and processes the request in chunks of maximal the size of this
buffer.  Note that the buffer handling below is very simplified and
won't work (the buffer pointer is not being advanced in case of a
partial read), it's just here to demonstrate the uio handling.
.Bd -literal
/* MIN() can be found there: */
#include <sys/param.h>

#define BUFSIZE 512
static char buffer[BUFSIZE];

static int data_available;	/* amount of data that can be read */

static int
fooread(dev_t dev, struct uio *uio, int flag)
{
	int rv, amnt;

	while (uio->uio_resid > 0) {
		if (data_available > 0) {
			amnt = MIN(uio->uio_resid, data_available);
			if ((rv = uiomove((caddr_t)buffer, amnt, uio))
			    != 0)
				goto error;
			data_available -= amnt;
		} else {
			tsleep(...);	/* wait for a better time */
		}
	}
	return 0;
error:
	/* do error cleanup here */
	return rv;
}
			
.Ed
.Sh RETURN VALUES
.Fn uiomove
can return
.Er EFAULT
from the invoked
.Xr copyin 9
or
.Xr copyout 9
in case the transfer was to/from a process's address space.
.Sh SEE ALSO
.Xr read 2 ,
.Xr readv 2 ,
.Xr write 2 ,
.Xr writev 2 ,
.Xr copyin 9 ,
.Xr copyout 9 ,
.Xr sleep 9
.Sh HISTORY
The uio mechanism appeared in some early version of
.Ux .
.Sh AUTHORS
This man page has been written by
.ie t J\(:org Wunsch.
.el Joerg Wunsch.
