.\" Copyright (c) 1980, 1990, 1993
.\"	The Regents of the University of California.  All rights reserved.
.\"
.\" This code is derived from software contributed to Berkeley by
.\" the Institute of Electrical and Electronics Engineers, Inc.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by the University of
.\"	California, Berkeley and its contributors.
.\" 4. Neither the name of the University nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"     @(#)date.1	8.3 (Berkeley) 4/28/95
.\" $FreeBSD: src/bin/date/date.1,v 1.8.2.11 1999/09/05 10:59:37 peter Exp $
.\"
.Dd November 17, 1993
.Dt DATE 1
.Os
.Sh NAME
.Nm date
.Nd display or set date and time
.Sh SYNOPSIS
.Nm date
.Op Fl nu
.Op Fl d Ar dst
.Op Fl r Ar seconds
.Op Fl t Ar minutes_west
.Op Fl v Ns Ar [+|-]val Ns Op ymwdHM
.Ar ...
.Op Fl f Ar fmt Ar date | [[[[yy]mm]dd]HH]MM[\&.ss]
.Op Cm + Ns Ar format
.Sh DESCRIPTION
.Nm Date
displays the current date and time when invoked without arguments.
Providing arguments will format the date and time in a user-defined
way or set the date.
Only the superuser may set the date.
.Pp
The options are as follows:
.Bl -tag -width Ds
.It Fl d
Set the kernel's value for daylight savings time.
If
.Ar dst
is non-zero, future calls
to
.Xr gettimeofday 2
will return a non-zero
.Ql tz_dsttime  .
.It Fl f
Use
.Ar fmt
as the format string to parse the date provided rather than using
the default
.Ar [[[[yy]mm]dd]HH]MM[.ss]
format.  Parsing is done using
.Xr strptime 3 .
.It Fl n
The utility
.Xr timed 8
is used to synchronize the clocks on groups of machines.
By default, if
.Xr timed
is running,
.Nm
will set the time on all of the machines in the local group.
The
.Fl n
option stops
.Nm
from setting the time for other than the current machine.
.It Fl r
Print out the date and time in
.Ar seconds
from the Epoch.
.It Fl t
Set the kernel's value for minutes west of
.Tn GMT .
.Ar Minutes_west
specifies the number of minutes returned in
.Ql tz_minuteswest  
by future calls to
.Xr gettimeofday 2 .
.It Fl u
Display or set the date in
.Tn UCT
(universal) time.
.It Fl v
Adjust the minute, hour, month day, week day, month or year according to
.Ar val .
If
.Ar val
is preceded with a plus or minus sign, the date is adjusted forwards
or backwards according to the remaining string, otherwise the relevant
part of the date is set.  The date can be adjusted as many times as
required using these flags.  Flags are processed in the order given.
.Pp
Minutes are in the range 0-59, hours are in the range 1-12, month days
are in the range 1-31, week days are in the range 0-6 (Sun-Sat), months
are in the range 1-12 (Jan-Dec) and years are in the range 80-38 or
1980-2038.
.Pp
If
.Ar val
is numeric, one of either
.Ar y ,
.Ar m ,
.Ar w ,
.Ar d ,
.Ar H
or
.Ar M
must be used to specify which part of the date is to be adjusted.
.Pp
The week day or month may be specified using a name rather than a
number.  If a name is used with the plus (or minus) sign, the date
will be put forwards (or backwards) to the next (previous) date that
matches the given week day or month.  This will not adjust the date
if the given week day or month is the same as the current one.
.Pp
Refer to the examples below for further details.
.El
.Pp
An operand with a leading plus (``+'') sign signals a user-defined format
string which specifies the format in which to display the date and time.
The format string may contain any of the conversion specifications described
in the 
.Xr strftime 3
manual page, as well as any arbitrary text.
A <newline> character is always output after the characters specified by
the format string.
The format string for the default display is:
.Bd -literal -offset indent
``+%+''
.Ed
.Pp
If an operand does not have a leading plus sign, it is interpreted as
a value for setting the system's notion of the current date and time.
The canonical representation for setting the date and time is:
.Pp
.Bl -tag -width Ds -compact -offset indent
.It Ar yy
Year in abbreviated form (e.g. 89 for 1989, 06 for 2006).
.It Ar mm
Numeric month.
A number from 1 to 12.
.It Ar dd
Day, a number from 1 to 31.
.It Ar HH
Hour, a number from 0 to 23.
.It Ar MM
Minutes, a number from 0 to 59.
.It Ar .ss
Seconds, a number from 0 to 61 (59 plus a maximum of two leap seconds).
.El
.Pp
Everything but the minutes is optional.
.Pp
Time changes for Daylight Saving and Standard time and leap seconds
and years are handled automatically.
.Sh EXAMPLES
The command:
.Bd -literal -offset indent
date "+DATE: %Y-%m-%d%nTIME: %H:%M:%S"
.Ed
.Pp
will display:
.Bd -literal -offset indent
DATE: 1987-11-21
TIME: 13:36:16
.Ed
.Pp
The command:
.Bd -literal -offset indent
date -v1m -v+1y
.Ed
.Pp
will display:
.Bd -literal -offset indent
Sun Jan  4 03:15:24 GMT 1998
.Ed
.Pp
(where it is currently Mon Aug  4 04:15:24 BST 1997).
.Pp
The command:
.Bd -literal -offset indent
date -v1d -v3m -v0y -v-1d
.Ed
.Pp
will display the last day of February in the year 2000:
.Bd -literal -offset indent
Tue Feb 29 03:18:00 GMT 2000
.Ed
.Pp
The command:
.Bd -literal -offset indent
date -v1d -v+1m -v-1d -v-fri
.Ed
.Pp
will display the last Friday of the month:
.Bd -literal -offset indent
Fri Aug 29 04:31:11 BST 1997
.Ed
.Pp
(where it is currently Mon Aug  4 04:31:11 BST 1997).
.Pp
The command:
.Bd -literal -offset indent
date 8506131627
.Ed
.Pp
sets the date to
.Dq Li "June 13, 1985, 4:27 PM" .
.Pp
The command:
.Bd -literal -offset indent
date 1432
.Ed
.Pp
sets the time to
.Li "2:32 PM" ,
without modifying the date.
.Sh ENVIRONMENT
The execution of
.Nm
is affected by the following environment variables:
.Bl -tag -width Ds
.It Ev TZ
The timezone to use when displaying dates.
The normal format is a pathname relative to
.Pa /usr/share/zoneinfo .
For example, the command
.Dq TZ=America/Los_Angeles date
displays the current time in California.
See
.Xr environ 7
for more information.
.El
.Sh FILES
.Bl -tag -width /var/log/messages -compact
.It Pa /var/log/wtmp
a record of date resets and time changes
.It Pa /var/log/messages
a record of the user setting the time
.El
.Sh SEE ALSO
.Xr gettimeofday 2 ,
.Xr strftime 3 ,
.Xr strptime 3 ,
.Xr utmp 5 ,
.Xr timed 8
.Rs
.%T "TSP: The Time Synchronization Protocol for UNIX 4.3BSD"
.%A R. Gusella
.%A S. Zatti
.Re
.Sh DIAGNOSTICS
Exit status is 0 on success, 1 if unable to set the date, and 2
if able to set the local date, but unable to set it globally.
.Pp
Occasionally, when
.Xr timed
synchronizes the time on many hosts, the setting of a new time value may
require more than a few seconds.
On these occasions,
.Nm
prints:
.Ql Network time being set .
The message
.Ql Communication error with timed
occurs when the communication
between
.Nm
and
.Xr timed
fails.
.Sh BUGS
The system attempts to keep the date in a format closely compatible
with
.Tn VMS .
.Tn VMS ,
however, uses local time (rather than
.Tn GMT )
and does not understand daylight-savings time.
Thus, if you use both
.Tn UNIX
and
.Tn VMS ,
.Tn VMS
will be running on
.Tn GMT .
.Sh STANDARDS
The
.Nm
command is expected to be compatible with
.St -p1003.2 .
.Sh HISTORY
A
.Nm
command appeared in
.At v1 .
