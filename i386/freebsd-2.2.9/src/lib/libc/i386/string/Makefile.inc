#	@(#)Makefile.inc	8.1 (Berkeley) 6/4/93
# $FreeBSD: src/lib/libc/i386/string/Makefile.inc,v 1.4.6.2 1999/09/05 11:16:32 peter Exp $

SRCS+=	bcmp.S bcopy.S bzero.S ffs.S index.S memchr.S memcmp.S \
	memmove.S memcpy.S memset.S \
	rindex.S strcat.S strchr.S strcmp.S strcpy.S strcspn.c \
	strlen.S strncat.c strncmp.S strncpy.c strpbrk.c strsep.c \
	strspn.c strrchr.S strstr.c swab.S
