#!/bin/sh

verbose=false

if [ "x$1" = "x-v" ]
then
	verbose=true
	out=/dev/stdout
	err=/dev/stderr
else
	out=/dev/null
	err=/dev/null
fi

pts=5
timeout=30

runbochs() {
	# Find the address of the kernel readline function,
	# which the kernel monitor uses to read commands interactively.
	brkaddr=`grep readline obj/kern/kernel.sym | sed -e's/ .*$//g'`
	#echo "brkaddr $brkaddr"

	# Run Bochs, setting a breakpoint at readline(),
	# and feeding in appropriate commands to run, then quit.
	(
		echo vbreak 0x8:0x$brkaddr
		echo c
		echo die
		echo quit
	) | (
		ulimit -t $timeout
		bochs -q 'display_library: nogui' \
			'parport1: enabled=1, file="bochs.out"' \
			>$out 2>$err
	)
}



gmake
runbochs

score=0

# echo -n "Page directory: "
awk 'BEGIN{printf("Page directory: ");}' </dev/null	# goddamn suns can't echo -n.
 if grep "check_boot_pgdir() succeeded!" bochs.out >/dev/null
 then
	score=`echo 20+$score | bc`
	echo OK
 else
	echo WRONG
 fi

# echo -n "Page management: "
awk 'BEGIN{printf("Page management: ");}' </dev/null
 if grep "page_check() succeeded!" bochs.out >/dev/null
 then
	score=`echo 30+$score | bc`
	echo OK
 else
	echo WRONG
 fi

echo "Score: $score/50"


