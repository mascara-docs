# $FreeBSD: src/gnu/usr.bin/texinfo/Makefile.inc,v 1.5.2.2 1999/09/05 11:07:26 peter Exp $

TXIDIR=	${.CURDIR}/../../../../contrib/texinfo

.if exists(${.OBJDIR}/../libtxi)
LIBTXI=	${.OBJDIR}/../libtxi/libtxi.a
.else
LIBTXI=	${.CURDIR}/../libtxi/libtxi.a
.endif

.include "../../Makefile.inc"
