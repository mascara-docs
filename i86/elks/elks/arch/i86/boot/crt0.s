# 1 "boot/crt0.S"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "boot/crt0.S"
! enable the autoconfig tool




# 1 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/config.h" 1




# 1 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/autoconf.h" 1

















# 28 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/autoconf.h"









































# 86 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/autoconf.h"





























# 123 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/autoconf.h"







# 5 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/config.h" 2

# 1 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/kdev_t.h" 1






















# 32 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/kdev_t.h"








# 51 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/kdev_t.h"



# 6 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/config.h" 2

# 1 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/major.h" 1













# 29 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/major.h"



# 41 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/major.h"









# 7 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/config.h" 2











# 27 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/config.h"






























# 65 "/home/mvlad/src/projects/mascara/docs/elks/elks/elks/include/linuxmt/config.h"






# 6 "boot/crt0.S" 2

!	Assembler boot strap hooks. This is called by setup

	.text
	.globl _main
	.extern	_start_kernel
	.extern _arch_boot

! note: this next instruction is part of the kernel restart fix for
! protexted mode. it must be 3 bytes long.

	.extern _kernel_restarted
	br _kernel_restarted
	
!	Setup passes these on the stack	
!	Setup patched to pass parameters in registers to avoid clobbering the
!	kernel when using the 286pmode extender.

_main:
# 34 "boot/crt0.S"
	mov	__endtext, bx
	mov	__enddata, si
	add	si, dx
	mov	__endbss, si

       

	mov	ax,ds           ! in ROMCODE stack is ready placed
	mov	ss,ax
	mov	sp,#_bootstack


!  overwrite start of main with a jmp to kernel_restarted()
!  this will give is a call stack trace instead of the "timer bug" message
!  no longer nessecary due to pmode fix. -AJB

!	.extern _redirect_main
!	call	_redirect_main

	call	_arch_boot
	call	_start_kernel	! Break point if it returns
	int	3

!	Segment beginnings	

	.data
	.globl __endtext
	.globl __enddata
	.globl __endbss
	

	.zerow	384
_bootstack:


! 768 byte boot stack. Print sp in wake_up and you'll see that more than
! 512 bytes of stack are used! 
! Must be in data as its in use when we wipe the BSS

__endtext:
	.word	0

__enddata:
	.word	0

__endbss:
	.word	0
	.bss

__sbss:
