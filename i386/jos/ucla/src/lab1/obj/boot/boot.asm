
obj/boot/boot.out:     file format elf32-i386


Disassembly of section .text:

00007c00 <_text>:
    7c00:	fa                   	cli    
    7c01:	fc                   	cld    
    7c02:	31 c0                	xor    %eax,%eax
    7c04:	8e d8                	mov    %eax,%ds
    7c06:	8e c0                	mov    %eax,%es
    7c08:	8e d0                	mov    %eax,%ss

00007c0a <seta20.1>:
    7c0a:	e4 64                	in     $0x64,%al
    7c0c:	a8 02                	test   $0x2,%al
    7c0e:	75 fa                	jne    7c0a <seta20.1>
    7c10:	b0 d1                	mov    $0xd1,%al
    7c12:	e6 64                	out    %al,$0x64

00007c14 <seta20.2>:
    7c14:	e4 64                	in     $0x64,%al
    7c16:	a8 02                	test   $0x2,%al
    7c18:	75 fa                	jne    7c14 <seta20.2>
    7c1a:	b0 df                	mov    $0xdf,%al
    7c1c:	e6 60                	out    %al,$0x60
    7c1e:	0f 01 16             	lgdtl  (%esi)
    7c21:	64                   	fs
    7c22:	7c 0f                	jl     7c33 <protcseg+0x1>
    7c24:	20 c0                	and    %al,%al
    7c26:	66 83 c8 01          	or     $0x1,%ax
    7c2a:	0f 22 c0             	mov    %eax,%cr0
    7c2d:	ea 32 7c 08 00 66 b8 	ljmp   $0xb866,$0x87c32

00007c32 <protcseg>:
    7c32:	66 b8 10 00          	mov    $0x10,%ax
    7c36:	8e d8                	mov    %eax,%ds
    7c38:	8e c0                	mov    %eax,%es
    7c3a:	8e e0                	mov    %eax,%fs
    7c3c:	8e e8                	mov    %eax,%gs
    7c3e:	8e d0                	mov    %eax,%ss
    7c40:	bc 00 7c 00 00       	mov    $0x7c00,%esp
    7c45:	e8 da 00 00 00       	call   7d24 <bootmain>

00007c4a <spin>:
    7c4a:	eb fe                	jmp    7c4a <spin>

00007c4c <gdt>:
	...
    7c54:	ff                   	(bad)  
    7c55:	ff 00                	incl   (%eax)
    7c57:	00 00                	add    %al,(%eax)
    7c59:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf
    7c60:	00 92 cf 00 17 00    	add    %dl,0x1700cf(%edx)

00007c64 <gdtdesc>:
    7c64:	17                   	pop    %ss
    7c65:	00 4c 7c 00          	add    %cl,0x0(%esp,%edi,2)
    7c69:	00 00                	add    %al,(%eax)
	...

00007c6c <waitdisk>:
    7c6c:	55                   	push   %ebp
    7c6d:	ba f7 01 00 00       	mov    $0x1f7,%edx
    7c72:	89 e5                	mov    %esp,%ebp
    7c74:	ec                   	in     (%dx),%al
    7c75:	25 c0 00 00 00       	and    $0xc0,%eax
    7c7a:	83 f8 40             	cmp    $0x40,%eax
    7c7d:	75 f5                	jne    7c74 <waitdisk+0x8>
    7c7f:	5d                   	pop    %ebp
    7c80:	c3                   	ret    

00007c81 <readsect>:
    7c81:	55                   	push   %ebp
    7c82:	89 e5                	mov    %esp,%ebp
    7c84:	57                   	push   %edi
    7c85:	8b 7d 0c             	mov    0xc(%ebp),%edi
    7c88:	e8 df ff ff ff       	call   7c6c <waitdisk>
    7c8d:	ba f2 01 00 00       	mov    $0x1f2,%edx
    7c92:	b0 01                	mov    $0x1,%al
    7c94:	ee                   	out    %al,(%dx)
    7c95:	b2 f3                	mov    $0xf3,%dl
    7c97:	89 f8                	mov    %edi,%eax
    7c99:	ee                   	out    %al,(%dx)
    7c9a:	89 f8                	mov    %edi,%eax
    7c9c:	b2 f4                	mov    $0xf4,%dl
    7c9e:	c1 e8 08             	shr    $0x8,%eax
    7ca1:	ee                   	out    %al,(%dx)
    7ca2:	89 f8                	mov    %edi,%eax
    7ca4:	b2 f5                	mov    $0xf5,%dl
    7ca6:	c1 e8 10             	shr    $0x10,%eax
    7ca9:	ee                   	out    %al,(%dx)
    7caa:	c1 ef 18             	shr    $0x18,%edi
    7cad:	b2 f6                	mov    $0xf6,%dl
    7caf:	89 f8                	mov    %edi,%eax
    7cb1:	83 c8 e0             	or     $0xffffffe0,%eax
    7cb4:	ee                   	out    %al,(%dx)
    7cb5:	b0 20                	mov    $0x20,%al
    7cb7:	b2 f7                	mov    $0xf7,%dl
    7cb9:	ee                   	out    %al,(%dx)
    7cba:	e8 ad ff ff ff       	call   7c6c <waitdisk>
    7cbf:	8b 7d 08             	mov    0x8(%ebp),%edi
    7cc2:	b9 80 00 00 00       	mov    $0x80,%ecx
    7cc7:	ba f0 01 00 00       	mov    $0x1f0,%edx
    7ccc:	fc                   	cld    
    7ccd:	f2 6d                	repnz insl (%dx),%es:(%edi)
    7ccf:	5f                   	pop    %edi
    7cd0:	5d                   	pop    %ebp
    7cd1:	c3                   	ret    

00007cd2 <readseg>:
    7cd2:	55                   	push   %ebp
    7cd3:	89 e5                	mov    %esp,%ebp
    7cd5:	57                   	push   %edi
    7cd6:	56                   	push   %esi
    7cd7:	53                   	push   %ebx
    7cd8:	51                   	push   %ecx
    7cd9:	8b 75 08             	mov    0x8(%ebp),%esi
    7cdc:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    7cdf:	8b 7d 14             	mov    0x14(%ebp),%edi
    7ce2:	89 f0                	mov    %esi,%eax
    7ce4:	81 e6 00 fe ff 00    	and    $0xfffe00,%esi
    7cea:	25 ff ff ff 00       	and    $0xffffff,%eax
    7cef:	01 c3                	add    %eax,%ebx
    7cf1:	03 45 10             	add    0x10(%ebp),%eax
    7cf4:	c1 ef 09             	shr    $0x9,%edi
    7cf7:	47                   	inc    %edi
    7cf8:	89 45 f0             	mov    %eax,-0x10(%ebp)
    7cfb:	eb 10                	jmp    7d0d <readseg+0x3b>
    7cfd:	57                   	push   %edi
    7cfe:	47                   	inc    %edi
    7cff:	56                   	push   %esi
    7d00:	81 c6 00 02 00 00    	add    $0x200,%esi
    7d06:	e8 76 ff ff ff       	call   7c81 <readsect>
    7d0b:	58                   	pop    %eax
    7d0c:	5a                   	pop    %edx
    7d0d:	39 de                	cmp    %ebx,%esi
    7d0f:	72 ec                	jb     7cfd <readseg+0x2b>
    7d11:	eb 04                	jmp    7d17 <readseg+0x45>
    7d13:	c6 03 00             	movb   $0x0,(%ebx)
    7d16:	43                   	inc    %ebx
    7d17:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
    7d1a:	72 f7                	jb     7d13 <readseg+0x41>
    7d1c:	8d 65 f4             	lea    -0xc(%ebp),%esp
    7d1f:	5b                   	pop    %ebx
    7d20:	5e                   	pop    %esi
    7d21:	5f                   	pop    %edi
    7d22:	5d                   	pop    %ebp
    7d23:	c3                   	ret    

00007d24 <bootmain>:
    7d24:	55                   	push   %ebp
    7d25:	89 e5                	mov    %esp,%ebp
    7d27:	56                   	push   %esi
    7d28:	53                   	push   %ebx
    7d29:	6a 00                	push   $0x0
    7d2b:	68 00 10 00 00       	push   $0x1000
    7d30:	68 00 10 00 00       	push   $0x1000
    7d35:	68 00 00 01 00       	push   $0x10000
    7d3a:	e8 93 ff ff ff       	call   7cd2 <readseg>
    7d3f:	83 c4 10             	add    $0x10,%esp
    7d42:	81 3d 00 00 01 00 7f 	cmpl   $0x464c457f,0x10000
    7d49:	45 4c 46 
    7d4c:	75 42                	jne    7d90 <bootmain+0x6c>
    7d4e:	8b 1d 1c 00 01 00    	mov    0x1001c,%ebx
    7d54:	0f b7 05 2c 00 01 00 	movzwl 0x1002c,%eax
    7d5b:	81 c3 00 00 01 00    	add    $0x10000,%ebx
    7d61:	c1 e0 05             	shl    $0x5,%eax
    7d64:	8d 34 03             	lea    (%ebx,%eax,1),%esi
    7d67:	eb 17                	jmp    7d80 <bootmain+0x5c>
    7d69:	ff 73 04             	pushl  0x4(%ebx)
    7d6c:	ff 73 14             	pushl  0x14(%ebx)
    7d6f:	ff 73 10             	pushl  0x10(%ebx)
    7d72:	ff 73 08             	pushl  0x8(%ebx)
    7d75:	83 c3 20             	add    $0x20,%ebx
    7d78:	e8 55 ff ff ff       	call   7cd2 <readseg>
    7d7d:	83 c4 10             	add    $0x10,%esp
    7d80:	39 f3                	cmp    %esi,%ebx
    7d82:	72 e5                	jb     7d69 <bootmain+0x45>
    7d84:	a1 18 00 01 00       	mov    0x10018,%eax
    7d89:	25 ff ff ff 00       	and    $0xffffff,%eax
    7d8e:	ff d0                	call   *%eax
    7d90:	ba 00 8a 00 00       	mov    $0x8a00,%edx
    7d95:	b8 00 8a ff ff       	mov    $0xffff8a00,%eax
    7d9a:	66 ef                	out    %ax,(%dx)
    7d9c:	b8 00 8e ff ff       	mov    $0xffff8e00,%eax
    7da1:	66 ef                	out    %ax,(%dx)
    7da3:	8d 65 f8             	lea    -0x8(%ebp),%esp
    7da6:	5b                   	pop    %ebx
    7da7:	5e                   	pop    %esi
    7da8:	5d                   	pop    %ebp
    7da9:	c3                   	ret    
