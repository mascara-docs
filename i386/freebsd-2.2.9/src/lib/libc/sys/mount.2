.\" Copyright (c) 1980, 1989, 1993
.\"	The Regents of the University of California.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by the University of
.\"	California, Berkeley and its contributors.
.\" 4. Neither the name of the University nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"     @(#)mount.2	8.2 (Berkeley) 12/11/93
.\"
.Dd December 11, 1993
.Dt MOUNT 2
.Os BSD 4
.Sh NAME
.Nm mount ,
.Nm unmount
.Nd mount or dismount a filesystem
.Sh SYNOPSIS
.Fd #include <sys/param.h>
.Fd #include <sys/mount.h>
.Ft int
.Fn mount "int type" "const char *dir" "int flags" "caddr_t data"
.Ft int
.Fn unmount "const char *dir" "int flags"
.Sh DESCRIPTION
The
.Fn mount
function grafts
a filesystem object onto the system file tree
at the point
.Ar dir .
The argument
.Ar data
describes the filesystem object to be mounted.
The argument
.Ar type
tells the kernel how to interpret
.Ar data
(See
.Ar type
below).
The contents of the filesystem
become available through the new mount point
.Ar dir .
Any files in
.Ar dir
at the time
of a successful mount are swept under the carpet so to speak, and
are unavailable until the filesystem is unmounted.
.Pp
The following
.Ar flags
may be specified to
suppress default semantics which affect filesystem access.
.Bl -tag -width MNT_SYNCHRONOUS
.It Dv MNT_RDONLY
The filesystem should be treated as read-only;
Even the super-user may not write on it.
.It Dv MNT_NOEXEC
Do not allow files to be executed from the filesystem.
.It Dv MNT_NOSUID
Do not honor setuid or setgid bits on files when executing them.
.It Dv MNT_NOATIME
Disable update of file access times.
.It Dv MNT_NODEV
Do not interpret special files on the filesystem.
.It Dv MNT_SYNCHRONOUS
All I/O to the filesystem should be done synchronously.
.It Dv MNT_ASYNC
All I/O to the filesystem should be done asynchronously.
.It Dv MNT_WANTRDWR
Upgrade a mounted read-only filesystem to read-write if MNT_UPDATE
is also specified.
.It Dv MNT_FORCE
Force a read-write mount even if the filesystem appears to be unclean.
Dangerous.
.El
.Pp
The flag
.Dv MNT_UPDATE
indicates that the mount command is being applied 
to an already mounted filesystem.
This allows the mount flags to be changed without requiring
that the filesystem be unmounted and remounted.
Some filesystems may not allow all flags to be changed.
For example,
many filesystems will not allow a change from read-write to read-only.
.Pp
The flag
.Dv MNT_RELOAD
causes the vfs subsystem to update its data structures pertaining to
the specified already mounted filesystem.
.Pp
The
.Fa type
argument defines the type of the filesystem.
The types of filesystems known to the system are defined in
.Aq Pa sys/mount.h .
.Fa Data
is a pointer to a structure that contains the type
specific arguments to mount.
The currently supported types of filesystems and
their type specific data are:
.Pp
.Bd -literal -compact
/*
 * Export arguments for local filesystem mount calls.
 */
struct export_args {
	int	ex_flags;		/* export related flags */
	uid_t	ex_root;		/* mapping for root uid */
	struct	ucred ex_anon;		/* mapping for anonymous user */
	struct	sockaddr *ex_addr;	/* net address to which exported */
	int	ex_addrlen;		/* and the net address length */
	struct	sockaddr *ex_mask;	/* mask of valid bits in saddr */
	int	ex_masklen;		/* and the smask length */
};
.Ed
.Pp
.Dv MOUNT_UFS
.Pp
.Bd -literal -compact
/*
 * Arguments to mount UFS-bases filesystems
 */
struct ufs_args {
      char	*fspec;               /* Block special file to mount */
      struct	export_args export;   /* network export information */
};
.Ed
.Pp
.Dv MOUNT_NFS
.Pp
.Bd -literal -compact
#ifdef NFS
/*
 * Arguments to mount NFS
 */
struct nfs_args {
	struct sockaddr	*addr;		/* file server address */
	int		addrlen;	/* length of address */
	int		sotype;		/* Socket type */
	int		proto;		/* and Protocol */
	u_char		*fh;		/* File handle to be mounted */
	int		fhsize;		/* Size, in bytes, of fh */
	int		flags;		/* flags */
	int		wsize;		/* write size in bytes */
	int		rsize;		/* read size in bytes */
	int		readdirsize;	/* readdir size in bytes */
	int		timeo;		/* initial timeout in .1 secs */
	int		retrans;	/* times to retry send */
	int		maxgrouplist;	/* Max. size of group list */
	int		readahead;	/* # of blocks to readahead */
	int		leaseterm;	/* Term (sec) of lease */
	int		deadthresh;	/* Retrans threshold */
	char		*hostname;	/* server's name */
};

/*
 * NFS mount option flags
 */
#define	NFSMNT_SOFT		0x00000001  /* soft mount (hard is default) */
#define	NFSMNT_WSIZE		0x00000002  /* set write size */
#define	NFSMNT_RSIZE		0x00000004  /* set read size */
#define	NFSMNT_TIMEO		0x00000008  /* set initial timeout */
#define	NFSMNT_RETRANS		0x00000010  /* set number of request retries */
#define	NFSMNT_MAXGRPS		0x00000020  /* set maximum grouplist size */
#define	NFSMNT_INT		0x00000040  /* allow interrupts on hard mount */
#define	NFSMNT_NOCONN		0x00000080  /* Don't Connect the socket */
#define	NFSMNT_NQNFS		0x00000100  /* Use Nqnfs protocol */
#define	NFSMNT_NFSV3		0x00000200  /* Use NFS Version 3 protocol */
#define	NFSMNT_KERB		0x00000400  /* Use Kerberos authentication */
#define	NFSMNT_DUMBTIMR		0x00000800  /* Don't estimate rtt dynamically */
#define	NFSMNT_LEASETERM	0x00001000  /* set lease term (nqnfs) */
#define	NFSMNT_READAHEAD	0x00002000  /* set read ahead */
#define	NFSMNT_DEADTHRESH	0x00004000  /* set dead server retry thresh */
#define	NFSMNT_RESVPORT		0x00008000  /* Allocate a reserved port */
#define	NFSMNT_RDIRPLUS		0x00010000  /* Use Readdirplus for V3 */
#define	NFSMNT_READDIRSIZE	0x00020000  /* Set readdir size */
#define	NFSMNT_INTERNAL		0xfffc0000  /* Bits set internally */
#define NFSMNT_HASWRITEVERF	0x00040000  /* Has write verifier for V3 */
#define NFSMNT_GOTPATHCONF	0x00080000  /* Got the V3 pathconf info */
#define NFSMNT_GOTFSINFO	0x00100000  /* Got the V3 fsinfo */
#define	NFSMNT_MNTD		0x00200000  /* Mnt server for mnt point */
#define	NFSMNT_DISMINPROG	0x00400000  /* Dismount in progress */
#define	NFSMNT_DISMNT		0x00800000  /* Dismounted */
#define	NFSMNT_SNDLOCK		0x01000000  /* Send socket lock */
#define	NFSMNT_WANTSND		0x02000000  /* Want above */
#define	NFSMNT_RCVLOCK		0x04000000  /* Rcv socket lock */
#define	NFSMNT_WANTRCV		0x08000000  /* Want above */
#define	NFSMNT_WAITAUTH		0x10000000  /* Wait for authentication */
#define	NFSMNT_HASAUTH		0x20000000  /* Has authenticator */
#define	NFSMNT_WANTAUTH		0x40000000  /* Wants an authenticator */
#define	NFSMNT_AUTHERR		0x80000000  /* Authentication error */
#endif /* NFS */
.Ed
.Pp
.Dv MOUNT_MFS
.Pp
.Bd -literal -compact
/*
 * Arguments to mount MFS
 */
struct mfs_args {
      char	*fspec; /* name to export for statfs */
      struct	export_args export; /* if exported MFSes are supported */
      caddr_t	base;   /* base of file system in memory */
      u_long	size;   /* size of file system */
};
.Ed
.Pp
.Dv MOUNT_MSDOS
.Pp
.Bd -literal -compact
#ifdef MSDOSFS
/*
 *  Arguments to mount MSDOS filesystems.
 */
struct msdosfs_args {
	char	*fspec;		/* blocks special holding the fs to mount */
	struct	export_args export;	/* network export information */
	uid_t	uid;		/* uid that owns msdosfs files */
	gid_t	gid;		/* gid that owns msdosfs files */
	mode_t	mask;		/* mask to be applied for msdosfs perms */
	int	flags;		/* see below */
	int magic;		/* version number */
	u_int16_t u2w[128];     /* Local->Unicode table */
	u_int8_t  ul[128];      /* Local upper->lower table */
	u_int8_t  lu[128];      /* Local lower->upper table */
	u_int8_t  d2u[128];     /* DOS->local table */
	u_int8_t  u2d[128];     /* Local->DOS table */
};
#endif
.Ed
.Pp
.Dv MOUNT_CD9660
.Pp
.Bd -literal -compact
#ifdef CD9660
/*
 * Arguments to mount ISO 9660 filesystems.
 */
struct iso_args {
	char *fspec;			/* block special device to mount */
	struct	export_args export;	/* network export info */
	int flags;			/* mounting flags, see below */

};
#define ISOFSMNT_NORRIP	0x00000001 /* disable Rock Ridge Ext.*/
#define ISOFSMNT_GENS	0x00000002 /* enable generation numbers */
#define ISOFSMNT_EXTATT	0x00000004 /* enable extended attributes */
#endif /* CD9660 */
.Ed
.Pp
The
.Fn umount
function call disassociates the filesystem from the specified
mount point
.Fa dir .
.Pp
The
.Fa flags
argument may specify
.Dv MNT_FORCE
to specify that the filesystem should be forcibly unmounted or made read-only
(if MNT_UPDATE and MNT_RDONLY are also specified)
even if files are still active.
Active special devices continue to work,
but any further accesses to any other active files result in errors
even if the filesystem is later remounted.
.Sh RETURN VALUES
The
.Fn mount
returns the value 0 if the mount was successful, otherwise -1 is returned
and the variable
.Va errno
is set to indicate the error.
.Pp
The
.Fn umount
function returns the value 0 if the umount succeeded; otherwise -1 is returned
and the variable
.Va errno
is set to indicate the error.
.Sh ERRORS
The
.Fn mount
function will fail when one of the following occurs:
.Bl -tag -width [ENOTBLK]
.It Bq Er EPERM
The caller is not the super-user.
.It Bq Er ENAMETOOLONG
A component of a pathname exceeded 255 characters,
or the entire length of a path name exceeded 1023 characters.
.It Bq Er ELOOP
Too many symbolic links were encountered in translating a pathname.
.It Bq Er ENOENT
A component of
.Fa dir
does not exist.
.It Bq Er ENOTDIR
A component of
.Ar name
is not a directory,
or a path prefix of
.Ar special
is not a directory.
.It Bq Er EBUSY
Another process currently holds a reference to
.Fa dir .
.It Bq Er EFAULT
.Fa Dir
points outside the process's allocated address space.
.El
.Pp
The following errors can occur for a
.Em ufs
filesystem mount:
.Bl -tag -width [ENOTBLK]
.It Bq Er ENODEV
A component of ufs_args
.Ar fspec
does not exist.
.It Bq Er ENOTBLK
.Ar Fspec
is not a block device.
.It Bq Er ENXIO
The major device number of 
.Ar fspec
is out of range (this indicates no device driver exists
for the associated hardware).
.It Bq Er EBUSY
.Ar Fspec
is already mounted.
.It Bq Er EMFILE
No space remains in the mount table.
.It Bq Er EINVAL
The super block for the filesystem had a bad magic
number or an out of range block size.
.It Bq Er ENOMEM
Not enough memory was available to read the cylinder
group information for the filesystem.
.It Bq Er EIO
An I/O error occurred while reading the super block or
cylinder group information.
.It Bq Er EFAULT
.Ar Fspec
points outside the process's allocated address space.
.El
.Pp
The following errors can occur for a
.Em nfs
filesystem mount:
.Bl -tag -width [ENOTBLK]
.It Bq Er ETIMEDOUT
.Em Nfs
timed out trying to contact the server.
.It Bq Er EFAULT
Some part of the information described by nfs_args
points outside the process's allocated address space.
.El
.Pp
The following errors can occur for a
.Em mfs
filesystem mount:
.Bl -tag -width [ENOTBLK]
.It Bq Er EMFILE
No space remains in the mount table.
.It Bq Er EINVAL
The super block for the filesystem had a bad magic
number or an out of range block size.
.It Bq Er ENOMEM
Not enough memory was available to read the cylinder
group information for the filesystem.
.It Bq Er EIO
A paging error occurred while reading the super block or
cylinder group information.
.It Bq Er EFAULT
.Em Name
points outside the process's allocated address space.
.El
.Pp
The
.Fn umount
function may fail with one of the following errors:
.Bl -tag -width [ENOTBLK]
.It Bq Er EPERM
The caller is not the super-user.
.It Bq Er ENOTDIR
A component of the path is not a directory.
.It Bq Er EINVAL
The pathname contains a character with the high-order bit set.
.It Bq Er ENAMETOOLONG
A component of a pathname exceeded 255 characters,
or an entire path name exceeded 1023 characters.
.It Bq Er ELOOP
Too many symbolic links were encountered in translating the pathname.
.It Bq Er EINVAL
The requested directory is not in the mount table.
.It Bq Er EBUSY
A process is holding a reference to a file located
on the filesystem.
.It Bq Er EIO
An I/O error occurred while writing cached filesystem information.
.It Bq Er EFAULT
.Fa Dir
points outside the process's allocated address space.
.El
.Pp
A
.Em ufs
or
.Em mfs
mount can also fail if the maximum number of filesystems are currently
mounted.
.Sh SEE ALSO
.Xr mfs 8 ,
.Xr mount 8 ,
.Xr umount 8
.Sh BUGS
Some of the error codes need translation to more obvious messages.
.Sh HISTORY
.Fn Mount
and
.Fn umount
function calls appeared in
.At v6 .
