.\"
.\" Copyright (c) 1993 Paul Kranenburg
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"      This product includes software developed by Paul Kranenburg.
.\" 3. The name of the author may not be used to endorse or promote products
.\"    derived from this software without specific prior written permission
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/gnu/usr.bin/ld/ldconfig/ldconfig.8,v 1.9.2.5 1999/09/05 11:06:44 peter Exp $
.\"
.Dd October 3, 1993
.Dt LDCONFIG 8
.Os FreeBSD
.Sh NAME
.Nm ldconfig
.Nd configure the shared library cache
.Sh SYNOPSIS
.Nm ldconfig
.Op Fl Rmrsv
.Op Fl f Ar hints_file
.Op Ar directory | file Ar ...
.Sh DESCRIPTION
.Nm
is used to prepare a set of
.Dq hints
for use by the run-time linker
.Xr ld.so 1
to facilitate quick lookup of shared libraries available in multiple
directories.  It scans a set of built-in system directories and any
.Ar directories
specified on the command line (in the given order) looking for shared
libraries and stores the results in the file
.Pa /var/run/ld.so.hints
to forestall the overhead that would otherwise result from the
directory search operations
.Xr ld.so 1
would have to perform to load the required shared libraries.
.Pp
Files named on the command line are expected to contain directories
to scan for shared libraries.  Each directory's pathname must start on a new
line.  Blank lines and lines starting with the comment character
.Ql \&#
are ignored.  A standard name for this file is
.Xr /etc/ld.so.conf.
.Pp
The shared libraries so found will be automatically available for loading
if needed by the program being prepared for execution. This obviates the need
for storing search paths within the executable.
.Pp
The
.Ev LD_LIBRARY_PATH
environment variable can be used to override the use of
directories (or the order thereof) from the cache or to specify additional
directories where shared libraries might be found.
.Ev LD_LIBRARY_PATH
is a
.Sq \:
separated list of directory paths which are searched by
.Xr ld.so 1
when it needs to load a shared library. It can be viewed as the run-time
equivalent of the
.Fl L
switch of
.Xr ld 1 .
.Pp
.Nm Ldconfig
is typically run as part of the boot sequence.
.Pp
The following options recognized by
.Nm ldconfig:
.Bl -tag -width indent
.It Fl R
Rescan the previously configured directories.  This opens the previous hints
file and fetches the directory list from the header.  Any additional pathnames
on the command line are also processed.
.It Fl f Ar hints_file
Read and/or update the specified hints file, instead of
.Pa /var/run/ld.so.hints .
This option is provided primarily for testing.
.It Fl m
Instead of replacing the contents of the hints file
with those found in the directories specified,
.Dq merge
in new entries.
Directories recorded in the hints file by previous runs of
.Nm
are also rescanned for new shared libraries.
.It Fl r
List the current contents of the hints file
on the standard output. The hints file is not modified.  The list of
directories stored in the hints file is included.
.It Fl s
Do not scan the built-in system directory
.Pq Dq /usr/lib
for shared libraries.
.It Fl v
Switch on verbose mode.
.Sh Security
Special care must be taken when loading shared libraries into the address
space of
.Ev set-user-Id
programs. Whenever such a program is run,
.Nm ld.so
will only load shared libraries from the hints
file. In particular, the
.Ev LD_LIBRARY_PATH
is not used to search for libraries. Thus, the role of ldconfig is dual. In
addition to building a set of hints for quick lookup, it also serves to
specify the trusted collection of directories from which shared objects can
be safely loaded. It is presumed that the set of directories specified to
.Nm ldconfig
are under control of the system's administrator.
.Sh FILES
.Bl -tag -width /var/run/ld.so.hintsxxx -compact
.It Pa /var/run/ld.so.hints
Default
.Dq hints
file.
.Sh SEE ALSO
.Xr ld 1 ,
.Xr link 5
.Sh HISTORY
A
.Nm
utility first appeared in SunOS 4.0, it appeared in its current form
in FreeBSD 1.1.
