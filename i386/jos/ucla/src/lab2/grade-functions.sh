verbose=false

if [ "x$1" = "x-v" ]
then
	verbose=true
	out=/dev/stdout
	err=/dev/stderr
else
	out=/dev/null
	err=/dev/null
fi

if gmake --version >/dev/null 2>&1; then make=gmake; else make=make; fi

#
# QEMU
#

timeout=30
preservefs=n
qemu=`$make -s --no-print-directory print-qemu`
gdbport=`$make -s --no-print-directory print-gdbport`
qemugdb=`$make -s --no-print-directory print-qemugdb`
cxxfilt=`$make -s --no-print-directory print-c++filt`
brkfn=readline
keystrokes=

echo_n () {
	# suns can't echo -n, and Mac OS X can't echo "x\c"
	# assume argument has no doublequotes
	awk 'BEGIN { printf("'"$*"'"); }' </dev/null
}

psleep () {
	# solaris "sleep" doesn't take fractions
	perl -e "select(undef, undef, undef, $1);"
}

# Run QEMU with serial output redirected to jos.out.  If $brkfn is
# non-empty, wait until $brkfn is reached or $timeout expires, then
# kill QEMU.
run () {
	qemuextra=
	if [ -n "$brkfn" ]; then
		qemuextra="-monitor null -S $qemugdb"
	else
		qemuextra="-monitor telnet::$gdbport,server,nowait"
	fi

	t0=`date +%s.%N 2>/dev/null`
	rm -f jos.out
	(
		ulimit -t $timeout
		exec $qemu -nographic $qemuopts -serial file:jos.out -no-reboot $qemuextra
	) >$out 2>$err &
	PID=$!

	# Wait for QEMU to start
	sleep 1

	if [ -n "$brkfn" ]; then
		# Find the address of the kernel $brkfn function,
		# which is typically what the kernel monitor uses to
		# read commands interactively.
		brkaddr=`grep " $brkfn\$" obj/kernel.sym | sed -e's/ .*$//g'`
		if [ -z "$brkaddr" ]; then
			# Perhaps the symbol was mangled.
			brkaddr=`$cxxfilt < obj/kernel.sym | grep " $brkfn(" | sed -e's/ .*$//g'`
		fi

		(
			echo "target remote localhost:$gdbport"
			echo "br *0x$brkaddr"
			echo c
		) > jos.in
		gdb -batch -nx -x jos.in > /dev/null 2>&1
	else
		# Wait until "Welcome to the JOS kernel monitor" is printed.
		while ! grep -l -q "^Welcome to the JOS kernel monitor" jos.out >/dev/null 2>&1; do
			psleep 0.1
		done

		(
		while [ -n "$keystrokes" ]; do
			firstchar=`echo "$keystrokes" | sed -e 's/^\(.\).*/\1/'`
			keystrokes=`echo "$keystrokes" | sed -e 's/^.//'`
			if [ "$firstchar" = ';' ]; then
				echo "sendkey ret"
			elif [ "$firstchar" = ' ' ]; then
				echo "sendkey spc"
			else
				echo "sendkey $firstchar"
			fi
			psleep 0.05
		done

		echo "quit"
		psleep 0.05
		) | telnet localhost $gdbport >/dev/null 2>$err
	fi

	# Make sure QEMU is dead.  On OS X, exiting gdb
	# doesn't always exit QEMU.
	kill $PID > /dev/null 2>&1
}

#
# Scoring
#

pts=5
part=0
partpos=0
total=0
totalpos=0

showpart () {
	echo "Part $1 score: $part/$partpos"
	echo
	total=`expr $total + $part`
	totalpos=`expr $totalpos + $partpos`
	part=0
	partpos=0
}

showfinal () {
	total=`expr $total + $part`
	totalpos=`expr $totalpos + $partpos`
	echo "Score: $total/$totalpos"
	if [ $total -lt $totalpos ]; then
		exit 1
	fi
}

passfailmsg () {
	msg="$1"
	shift
	if [ $# -gt 0 ]; then
		msg="$msg,"
	fi

	t1=`date +%s.%N 2>/dev/null`
	time=`echo "scale=1; ($t1-$t0)/1" | sed 's/.N/.0/g' | bc 2>/dev/null`

	echo $msg "$@" "(${time}s)"
}

pass () {
	passfailmsg OK "$@"
	part=`expr $part + $pts`
	partpos=`expr $partpos + $pts`
}

fail () {
	passfailmsg WRONG "$@"
	partpos=`expr $partpos + $pts`
}

