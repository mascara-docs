# $FreeBSD: src/etc/root/dot.login,v 1.14.2.4 1999/09/05 11:02:13 peter Exp $
#
# csh .login file
#

set path = (/sbin /bin /usr/sbin /usr/bin /usr/games /usr/local/bin /usr/X11R6/bin $HOME/bin)
setenv MANPATH "/usr/share/man:/usr/X11R6/man:/usr/local/man"

# Interviews settings
#setenv CPU "FREEBSD"
#set path = ($path /usr/local/interviews/bin/$CPU)
#setenv MANPATH "${MANPATH}:/usr/local/interviews/man"

# 8-bit locale (Germany)
#setenv LANG de_DE.ISO_8859-1

# A righteous umask
umask 22

[ -x /usr/games/fortune ] && /usr/games/fortune
