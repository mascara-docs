/* $FreeBSD: src/lib/libskey/pathnames.h,v 1.2.8.1 1999/09/05 11:17:05 peter Exp $ (FreeBSD) */

#include <paths.h>

#define _PATH_SKEYACCESS        "/etc/skey.access"
#define	_PATH_SKEYFILE		"/etc/skeykeys"
