.\"	$NetBSD: ccd.4,v 1.5 1995/10/09 06:09:09 thorpej Exp $
.\"
.\" Copyright (c) 1994 Jason Downs.
.\" Copyright (c) 1994, 1995 Jason R. Thorpe.
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed for the NetBSD Project
.\"	by Jason Downs and Jason R. Thorpe.
.\" 4. Neither the name of the author nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.Dd August 9, 1995
.Dt CCD 4
.Os NetBSD
.Sh NAME
.Nm ccd
.Nd Concatenated Disk Driver
.Sh SYNOPSIS
.Cd "pseudo-device ccd 4"
.Sh DESCRIPTION
The
.Nm
driver provides the capability of combining one or more disks/partitions
into one virtual disk.
.Pp
This document assumes that you're familiar with how to generate kernels,
how to properly configure disks and pseudo-devices in a kernel
configuration file, and how to partition disks.
.Pp
Note that the
.Sq raw
partitions of the disks
.Pa should not
be combined.  The kernel will only allow component partitions of type
FS_BSDFFS (type
.Dq 4.2BSD
as shown as
.Xr disklabel 8 ).
.Pp
In order to compile in support for the ccd, you must add a line similar
to the following to your kernel configuration file:
.Bd -unfilled -offset indent
pseudo-device	ccd	4	# concatenated disk devices
.Ed
.Pp
The count argument is how many
.Nm ccds
memory is allocated for a boot time.  In this example, no more than 4
.Nm ccds
may be configured.
.Pp
A
.Nm ccd
may be either serially concatenated or interleaved.  To serially
concatenate the partitions, specify the interleave factor of 0.
.Pp
There is a run-time utility that is used for configuring
.Nm ccds .
See
.Xr ccdconfig 8
for more information.
.Ss The Interleave Factor
If a
.Nm ccd
is interleaved correctly, a
.Dq striping
effect is achieved, which can increase sequential read/write
performance.  The interleave factor is expressed in units of DEV_BSIZE
(usually 512 bytes).  For large writes, the optimum interleave factor
is typically the size of a track, while for large reads, it is about a
quarter of a track. (Note that this changes greatly depending on the
number and speed of disks.)  For instance, with eight 7,200 RPM drives
on two Fast-Wide SCSI buses, this translates to about 128 for writes
and 32 for reads.
.Pp
The best performance is achieved if all component disks have the same
geometry and size.  Optimum striping cannot occur with different
disk types.
.Pp
For random-access oriented workloads, such as news servers, a larger
interleave factor (e.g., 65,536) is more desirable.  Note that there
isn't much
.Nm ccd
can do to speed up applications that are seek-time limited.  Larger
interleave factors will at least reduce the chance of having to seek
two disk-heads to read one directory or a file.
.Ss Disk Mirroring
You can configure the
.Nm ccd
to
.Dq mirror
any even number of disks.  See
.Xr ccdconfig 8
for how to specify the necessary flags.  In an event of a disk
failure, you can use
.Xr dd 1
to recover the failed disk.
.Pp
Note that a one-disk
.Nm ccd
is not the same as the original partition.  In particular, this means
if you have a filesystem on a two-disk mirrored
.Nm ccd
and one of the disks fail, you cannot mount and use the remaining
partition as itself; you have to configure it as a one-disk
.Nm ccd.
.Sh WARNINGS
If just one (or more) of the disks in a
.Nm ccd
fails, the entire
file system will be lost unless you are mirroring the disks.
.Sh FILES
/dev/{,r}ccd* - ccd device special files.
.Pp
.Sh HISTORY
The concatenated disk driver was originally written at the University of
Utah.
.Sh SEE ALSO
.Xr dd 1 ,
.Xr ccdconfig 8 ,
.Xr config 8 ,
.Xr disklabel 8 ,
.Xr fsck 8 ,
.Xr MAKEDEV 8 ,
.Xr mount 8 ,
.Xr newfs 8 .
