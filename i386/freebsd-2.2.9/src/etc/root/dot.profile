# $FreeBSD: src/etc/root/dot.profile,v 1.10.2.6 1999/09/05 11:02:13 peter Exp $
#
PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin
export PATH
HOME=/root
export HOME
TERM=${TERM:-cons25}
export TERM
PAGER=more
export PAGER
