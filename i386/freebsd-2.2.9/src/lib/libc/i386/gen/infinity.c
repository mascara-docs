/*
 * infinity.c
 * $FreeBSD: src/lib/libc/i386/gen/infinity.c,v 1.2.6.1 1999/09/05 11:16:30 peter Exp $
 */

#include <math.h>

/* bytes for +Infinity on a 387 */
char __infinity[] = { 0, 0, 0, 0, 0, 0, 0xf0, 0x7f };
