.\" Copyright 1995, 1996, 1997
.\"     Guy Helmer, Ames, Iowa 50014.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer as
.\"    the first lines of this file unmodified.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. The name of the author may not be used to endorse or promote products
.\"    derived from this software without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY GUY HELMER ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL GUY HELMER BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
.\" NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
.\" THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/usr.sbin/adduser/rmuser.8,v 1.1.2.5 1999/09/05 11:39:49 peter Exp $
.\"
.Dd February 23, 1997
.Dt RMUSER 8
.Os
.Sh NAME
.Nm rmuser
.Nd removes users from the system
.Sh SYNOPSIS
.Nm rmuser
.Op Fl y
.Op Ar username
.Sh DESCRIPTION
The utility
.Nm
.Pp
.Bl -enum
.It
Removes the user's 
.Xr crontab 1
entry (if any).
.It
Removes any 
.Xr at 1
jobs belonging to the user.
.It
Sends a SIGKILL signal to all processes owned by the user.
.It
Removes the user from the system's local password file.
.It
Removes the user's home directory (if it is owned by the user),
including handling of symbolic links in the path to the actual home
directory.
.It
Removes the incoming mail and pop daemon mail files belonging to the
user from 
.Pa /var/mail .
.It
Removes all files owned by the user from
.Pa /tmp ,
.Pa /var/tmp ,
and
.Pa /var/tmp/vi.recover .
.It
Removes the username from all groups to which it belongs in
.Pa /etc/group .
(If a group becomes empty and the group name is the same as the username,
the group is removed; this complements
.Xr adduser 8 's
per-user unique groups).
.El
.Pp
.Nm Rmuser
politely refuses to remove users whose uid is 0 (typically root), since
certain actions (namely, killing all the user's processes, and perhaps
removing the user's home directory) would cause damage to a running system.
If it is necessary to remove a user whose uid is 0, see
.Xr vipw 8
for information on directly editing the password file, by which the desired
user's
.Xr passwd 5
entry may be removed manually.
.Pp
If not running "affirmatively" (i.e., option
.Fl y
is not specified),
.Nm
shows the selected user's password file entry and asks for confirmation
that you wish to remove the user.  If the user's home directory is owned
by the user,
.Nm
asks whether you wish to remove the user's home directory and everything
below.
.Pp
As
.Nm
operates, it informs the user regarding the current activity.  If any
errors occur, they are posted to standard error and, if it is possible for
.Nm
to continue, it will.
.Pp
Available options:
.Pp
.Bl -tag -width username
.It Fl y
Affirm - any question that would be asked is answered implicitly in
the affirmative (i.e., yes).  A username must also be specified on the
command line if this option is used.
.It Ar \&username
Identifies the user to be removed; if not present,
.Nm
interactively asks for the user to be removed.
.Sh FILES
.Bl -tag -width /etc/master.passwd -compact
.It Pa /etc/master.passwd
.It Pa /etc/passwd
.It Pa /etc/group
.It Pa /etc/spwd.db
.It Pa /etc/pwd.db
.El
.Sh SEE ALSO
.Xr at 1 ,
.Xr chpass 1 ,
.Xr crontab 1 ,
.Xr finger 1 ,
.Xr passwd 1 ,
.Xr group 5 ,
.Xr passwd 5 ,
.Xr addgroup 8 ,
.Xr adduser 8 ,
.Xr pwd_mkdb 8 ,
.Xr rmgroup 8 ,
.Xr vipw 8
.Sh HISTORY
The
.Nm
command appeared in
.Fx 2.2 .
.\" .Sh AUTHOR
.\" Guy Helmer, Ames, Iowa
.Sh BUGS
.Nm Rmuser
does not comprehensively search the filesystem for all files
owned by the removed user and remove them; to do so on a system
of any size is prohibitively slow and I/O intensive.
.Nm Rmuser
also is unable to remove symbolic links that were created by the
user in
.Pa /tmp
or
.Pa /var/tmp
as symbolic links on 4.4BSD filesystems do not contain information
as to who created them.  Also, there may be other files created in
.Pa /var/mail
other than
.Pa /var/mail/username
and
.Pa /var/mail/.pop.username
that are not owned by the removed user but should be removed.
.Pp
.Nm Rmuser
has no knowledge of NIS (Yellow Pages), and it operates only on the
local password file.
