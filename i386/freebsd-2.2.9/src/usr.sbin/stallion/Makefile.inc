# $FreeBSD: src/usr.sbin/stallion/Makefile.inc,v 1.1.4.1 1999/09/05 11:41:22 peter Exp $

BOOTDIR=	/usr/libdata/stallion

.if exists(${.CURDIR}/../../Makefile.inc)
.include "${.CURDIR}/../../Makefile.inc"
.endif
