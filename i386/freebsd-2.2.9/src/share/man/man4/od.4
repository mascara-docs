.\" Copyright (c) 1996,1997 Shunsuke Akiyama <akiyama@jp.FreeBSD.org>.
.\"   All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by Shunsuke Akiyama.
.\" 4. Neither the name of the author nor the names of any co-contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY Shunsuke Akiyama AND CONTRIBUTORS ``AS IS''
.\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL Shunsuke Akiyama OR CONTRIBUTORS BE
.\" LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
.\" CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
.\" SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
.\" INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
.\" CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
.\" ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
.\" POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man4/od.4,v 1.1.2.4 1999/09/05 11:26:57 peter Exp $
.\"
.Dd March 16, 1997
.Dt OD 4
.Os FreeBSD
.Sh NAME
.Nm od
.Nd SCSI optical disk driver
.Sh SYNOPSIS
.Cd device od
.Cd device od1 at scbus0 target 3 lun 0
.Pp
To use a drive which returns bogus ASC/ASCQ values:
.Cd options OD_BOGUS_NOT_READY
.Pp
To enable an automatic spindown:
.Cd options OD_AUTO_TURNOFF
.Sh DESCRIPTION
The
.Nm
driver provides support for a
.Em SCSI
optical disk (AKA. Magneto-Optical or Phase change optical Disk) drive.
It enables the media change operation and the disk to be divided
up into a set of pseudo devices called
.Em partitions .
A partition has both a 
.Em raw
interface
and a
.Em block mode
interface.
In general the interfaces are similar to those described by 
.Xr wd 4
and
.Xr sd 4 .
.Pp
As the SCSI adapter is probed during boot, the 
.Em SCSI
bus is scanned for devices. Any devices found which answered as
.Sq Em Optical
and
.Sq Em removable
type device will be attached to the 
.Nm
driver.
In
.Fx 2.1.5
or later, the first device found will be attached as
.Em od0
and the next, 
.Em od1 ,
and so on.
It is possible to specify which od unit a device should
come on line as; refer to
.Xr scsi 4
for details on kernel configuration.
Beginning in
.Fx 2.2
the
.Nm
driver can handle any device which answers as being type
.Sq Em Direct
and
.Sq Em removable
as well as
.Sq Em Optical
and
.Sq Em removable
type devices.
.Sh PARTITIONING
The 
.Nm
driver allows the disk to have two levels of partitioning.
One layer, called the
.Dq slice layer ,
is used to separate the
.Tn FreeBSD
areas of the disk from areas used by other operating systems.
The second layer is the native
.Bx 4.4
partitioning scheme,
.Xr disklabel 5 ,
which is used to subdivide the
.Tn FreeBSD
slices into areas for individual filesystems.
For more information, see
.Xr fdisk 8
and
.Xr disklabel 8 ,
respectively.
.Pp
If an uninitialized disk is opened, the slice table will be
initialized with a fictitious
.Tn FreeBSD
slice spanning the entire disk.  Similarly, if an uninitialized
(or
.No non- Ns Tn FreeBSD )
slice is opened, its disklabel will be initialized with parameters
returned by the drive and a single
.Sq Li c
partition encompassing the entire slice.
.Sh KERNEL CONFIGURATION
Only one
.Nm
device is necessary in the config file; data structures are dynamically
allocated as disks are found on the
.Tn SCSI
bus.
.Sh IOCTLS
The following 
.Xr ioctl 2
calls apply to optical disks as well as to other disks.
They are defined in the header file
.Aq Pa sys/disklabel.h .
.Pp
.Bl -tag -width CDIOCPREVENT
.It Dv DIOCSBAD
Usually used to set up a bad-block mapping system on the disk. SCSI
drives incorporate their own bad-block mapping so this is not
implemented, however it MAY be implemented in the future as a 'kludged'
interface to the SCSI bad-block mapping.
.It Dv DIOCGDINFO
Read the in-core copy of the disklabel for the
drive from the kernel. This may be a fictitious disklabel if the drive 
has never been initialized, in which case it will contain information read
from the SCSI inquiry commands, and should be the same as
the information printed at boot.
.It Dv DIOCSDINFO
Give the driver a new disklabel to use. The driver will NOT try write
the new disklabel to the disk.
.It Dv DIOCWLABEL
Enable or Disable the driver's software write protect of the disklabel
on the disk.
.It Dv DIOCWDINFO
Give the driver a new disklabel to use. The driver WILL try write the
new disklabel to the disk.
.El
.Pp
And the following
.Xr ioctl 2
calls which apply to optical disks are defined in the header file
.Aq Pa sys/cdio.h .
.Bl -tag -width CDIOCPREVENT
.It Dv CDIOCEJECT
Eject the optical disk media.
.It Dv CDIOCALLOW
Tell the drive to allow manual ejection of the optical disk media.
.It Dv CDIOCPREVENT
Tell the drive to prevent manual ejection of the optical disk media.
.El
.Pp
In addition, the 
.Xr scsi 4
general ioctls may be used with the 
.Nm
driver, but only against the fourth (whole disk) partition.
.Sh NOTES
Currently the
.Nm
driver accepts 512, 1024 and 2048 byte/sector media.
Raw and block mode device access to non-512 byte/sector media
would be allowed only on each sector size boundary start position and 
I/O size.
.Sh FILES
.Bl -tag -width /dev/rodXXXXX -compact
.It Pa /dev/rod Ns Ar u
raw mode
.Tn SCSI
optical disk unit
.Ar u ,
accessed as an unpartitioned device
.Sm off
.It Pa /dev/od Ar u Pa s Ar n
.Sm on
block mode
.Tn SCSI
optical disk unit
.Ar u ,
slice
.Ar n ,
accessed as an unpartitioned device
.Sm off
.It Pa /dev/rod Ar u Pa s Ar n
.Sm on
raw mode
.Tn SCSI
optical disk unit
.Ar u ,
slice
.ar n ,
accessed as an unpartitioned device
.It Pa /dev/od Ns Ar u Ns Ar p
block mode
.Tn SCSI
optical disk unit
.Ar u ,
first
.Tn FreeBSD
slice, partition
.Ar p
.It Pa /dev/rod Ns Ar u Ns Ar p
raw mode
.Tn SCSI
optical disk unit
.Ar u ,
first
.Tn FreeBSD
slice, partition
.Ar p
.Sm off
.It Xo
.Pa /dev/od Ar u Pa s Ar n Ar p
.Xc
.Sm on
block mode
.Tn SCSI
optical disk unit
.Ar u ,
.No Ar n Ns th
slice, partition
.Ar p
.Sm off
.It Xo
.Pa /dev/rod Ar u Pa s Ar n Ar p
.Xc
raw mode
.Tn SCSI
optical disk unit
.Ar u ,
.No Ar n Ns th
slice, partition
.Ar p
.Sm off
.It Xo
.Pa /dev/rod Ar u Pa .ctl
.Xc
the control device unit
.Ar u ,
as being used by
.Xr scsi 8
.El
.Sh DIAGNOSTICS
None.
.Sh SEE ALSO
.Xr fdisk 1 ,
.Xr cd 4 ,
.Xr scsi 4 ,
.Xr sd 4 ,
.Xr disklabel 5 ,
.Xr disklabel 8
.Sh AUTHORS
.An Shunsuke Akiyama Aq akiyama@jp.FreeBSD.org
.Sh HISTORY
The
.Nm
driver first appeared in
.Fx 2.1.5 .
