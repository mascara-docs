#!/bin/bash

[ -z "$1" ] && {
        echo "# please specify a file to link to"
        exit 127
}

MAIN="$1"
CFLAGS="-c -Os -march=i686 -m32 -ffreestanding -Wall -I."
LDFLAGS="-melf_i386"

gcc $CFLAGS -o vbr.o $MAIN.c
ld $LDFLAGS -static -Tlinker.ld -nostdlib --nmagic -o vbr.elf vbr.o
objcopy -O binary vbr.elf vbr.bin
