/*********** MTX Multitasking System ************/

#include "type.h"   // type.h has PROC and all FS data structs

PROC proc[NPROC], *running, *freeList, *readyQueue, *sleepList;

int procsize = sizeof(PROC);
int nproc, color;
int inkmode = 1;

extern MINODE *root;

int body();
char *pname[]={"Sun", "Mercury", "Venus", "Earth",  "Mars", "Jupiter", 
               "Saturn", "Uranus", "Neptune" };

/****  queue.o wait.o fe.o are in mtxlib******
#include "queue.c"
#include "wait.c"
#include "forkexec.c"
**********************************************/
#include "vid.c"
#include "kbd.c"
#include "int.c"

int initialize()
{
  int i; PROC *p;

  printf("MTX initializing ....\n");
   
  for (i=0; i < NPROC; i++){
      proc[i].pid = i; 
      proc[i].status = FREE;
      proc[i].next = (PROC *)&proc[(i+1)];
      strcpy(proc[i].name, pname[i]);
  }
  proc[NPROC-1].next = NULL;
  freeList = &proc[0];         // all PROC are FREE initially

  readyQueue = 0;
  sleepList = 0;

  fs_init();
 
  // create P0
  p = getproc();              // get a FREE PROC

  p->status = READY;
  p->pri = 0;                /* lowest priority  0 */
  p->pid = 0;                /* process 0 or P0 */
  running = p;
  p->ppid = running->pid;    /* P0's parent is P0 */
  nproc = 1;

  p->cwd = root;
  for (i=0; i<NFD; i++)
    p->fd[i] = 0;
  printf("initialization complete\n"); 
}

int makeUimage(filename, p) char *filename; PROC *p;
{
  ushort i, segment;

  // make Umode image by loading /u1 into segment
  segment = (p->pid + 1)*0x1000;

  load(filename, segment);

  /***** Fill in U mode information in proc *****/

  /**************************************************
    We know segment=0x2000 + index*0x1000 ====>
    ustack is at the high end of this segment, say TOP.
    We must make ustak contain:
          1   2   3  4  5  6  7  8  9 10 11 12
       flag uCS uPC ax bx cx dx bp si di es ds
     0x0200 seg  0  0  0  0  0  0  0  0 seg seg
  
    So, first a loop to set all to 0, then
    put_word(seg, segment, -2*i); i=2,11,12;
   **************************************************/
 
   for (i=1; i<=12; i++){
       put_word(0, segment, -2*i);
   }

   put_word(0x0200,  segment, -2*1);   /* flag */  
   put_word(segment, segment, -2*2);   /* uCS */
   put_word(segment, segment, -2*11);  /* uES */
   put_word(segment, segment, -2*12);  /* uDS */

   /* initial USP relative to USS */
   p->usp = -2*12; 
   p->uss = segment;
   return 0;
}

int goUmode();
/***********************************************************
  kfork() creates a child proc and returns the child pid.
  When scheduled to run, the child process resumes to body();
************************************************************/
int kfork()
{
  PROC *p;
  int  i, child;
  ushort  segment;

  /*** get a PROC for child process: ***/
  if ( (p = getproc()) == NULL){
       printf("no more proc\n");
       return(-1);
  }

  /* initialize the new proc and its stack */
  p->status = READY;
  p->ppid = running->pid;
  p->parent = running;
  p->pri  = 1;                 // all of the same priority 1

  // clear all SAVed registers on stack
  for (i=1; i<10; i++)
      p->kstack[SSIZE-i] = 0;
 
  // fill in resume address
  p->kstack[SSIZE-1] = (int)goUmode;

  // save stack TOP address in PROC
  p->ksp = &(p->kstack[SSIZE - 9]);

  enqueue(&readyQueue, p);
  nproc++;

  segment = 0x1000*(p->pid+1);
  makeUimage("/u1", p);

  p->cwd = running->cwd;
  p->cwd->refCount++;

  for (i=0; i<NFD; i++){
       p->fd[i] = running->fd[i];
       if (p->fd[i] != 0)
 	 p->fd[i]->refCount++;
  }

  printf("Proc %d forked a child %d at segment=%x\n",
          running->pid, p->pid, segment);

  return(p->pid);
}

int do_switch()
{ 
   printf("P%d switch process\n", running->pid);
   tswitch();
}

int do_kfork()
{
   int new;
   new = kfork();
   if (new < 0)
       printf("kfork failed\n");
   else
       printf("P%d return from kfork() : child = %d\n",
               running->pid, new);
}

/*****************************************
   All proc share the same body function,
   as if called by the process itself.
******************************************/
int do_sleep()
{
  char c;
  printf("input an event # (0-9) to sleep on : ");
  c = getc() - '0';
  sleep(c);
}

int do_wk()
{
  char i;
  printf("input an event # (0-9) to wakeup : ");
  i = getc() - '0';
  wakeup(i);
}

 
int body()
{
  char c;
  while(1){
    printf("------------------------------------------\n"); 
    printf("I am process P%d    My parent=%d\n", running->pid, running->ppid);
    
    color = 0x000A + running->pid;
    printf("******************************************\n");
    printf("freeList   = "); printList(freeList);
    printf("readyQueue = "); printList(readyQueue);
    printf("sleepList  = "); printList(sleepList);
    printf("******************************************\n");

    printf("input a command : [s|q|f|w|u] : ");
    c = getc();   
    printf("%c\n", c);

    switch(c){
      case  's' : do_switch();  break;
      case  'q' : do_exit(100); break;   /* no return */
      case  'f' : do_kfork();   break;
      case  'w' : do_wait(0);   break;
      case  'u' : goUmode();    break;

      default   :              break;  
    }
  }
}

int int80h(), kbinth(), s0inth();

int set_vec(vector, addr) ushort vector, addr;
{
    ushort location,cs;
    location = vector << 2;
    put_word(addr, 0, location);
    put_word(0x1000,0,location+2);
}

//*************** main() ***************
main()
{
   vid_init();

   printf("\nWelcome to the MTX Operating System\n");
     initialize();
    
     set_vec(80, int80h);
     set_vec(9, kbinth);
     kbinit();

   printf("P0 forks P1\n");
     kfork();
   printf("P0 switches to P1\n");
     tswitch();
   printf("P0 resumes: all dead, happy ending!\n");
}

//******** scheduler *******************

int scheduler()
{ 
  if (running->status == READY)
    enqueue(&readyQueue, running);

  running = dequeue(&readyQueue);
}


