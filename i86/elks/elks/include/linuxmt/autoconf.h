/*
 * Automatically generated C config: don't edit
 */
#define AUTOCONF_INCLUDED

/*
 * Just accept the defaults unless you know what you are doing
 */

/*
 * Advanced - for Developers and Hackers only
 */
#undef  CONFIG_EXPERIMENTAL
#undef  CONFIG_OBSOLETE
#undef  CONFIG_MODULES

/*
 * Architecture
 */
#define CONFIG_ARCH_AUTO 1
#undef  CONFIG_ARCH_IBMPC
#undef  CONFIG_ARCH_SIBO
#define CONFIG_PC_AUTO 1
#undef  CONFIG_PC_XT
#undef  CONFIG_PC_AT
#undef  CONFIG_PC_MCA
#undef  CONFIG_ROMCODE

/*
 * Memory manager
 */
#define CONFIG_ADVANCED_MM 1
#undef  CONFIG_SWAP

/*
 * Hardware Facilities
 */

/*
 * Block Devices
 */
#define CONFIG_HW_FLOPPY_DRIVE 1
#define CONFIG_HW_HARD_DRIVE 1

/*
 * Driver Support
 */

/*
 * Block device drivers
 */
#define CONFIG_BLK_DEV_BIOS 1
#define CONFIG_BLK_DEV_BFD 1
#undef  CONFIG_BLK_DEV_BFD_HARD
#define CONFIG_BLK_DEV_BHD 1
#undef  CONFIG_DMA

/*
 * Additional block devices
 */
#undef  CONFIG_BLK_DEV_RAM

/*
 * Block device options
 */
#define CONFIG_BLK_DEV_CHAR 1

/*
 * Character device drivers
 */
#define CONFIG_CONSOLE_DIRECT 1
#undef  CONFIG_CONSOLE_BIOS
#undef  CONFIG_CONSOLE_SERIAL
#define CONFIG_DCON_VT52 1
#define CONFIG_DCON_ANSI 1
#undef  CONFIG_DCON_ANSI_PRINTK
#undef  CONFIG_KEYMAP_BE
#undef  CONFIG_KEYMAP_DE
#undef  CONFIG_KEYMAP_DV
#undef  CONFIG_KEYMAP_ES
#undef  CONFIG_KEYMAP_FR
#undef  CONFIG_KEYMAP_IT
#undef  CONFIG_KEYMAP_SE
#undef  CONFIG_KEYMAP_UK
#define CONFIG_KEYMAP_US 1

/*
 * Other character devices
 */
#define CONFIG_CHAR_DEV_RS 1
#define CONFIG_CHAR_DEV_LP 1
#define CONFIG_CHAR_DEV_MEM 1
#define CONFIG_PSEUDO_TTY 1

/*
 * File systems
 */
#undef  CONFIG_FS_RO
#define CONFIG_MINIX_FS 1
#undef  CONFIG_ROMFS_FS

/*
 * General filesystem settings
 */
#undef  CONFIG_FULL_VFS
#define CONFIG_FS_EXTERNAL_BUFFER 1
#define CONFIG_PIPE 1

/*
 * Executable file formats
 */
#define CONFIG_EXEC_ELKS 1

/*
 * Network support
 */
#define CONFIG_SOCKET 1
#undef  CONFIG_NANO
#define CONFIG_INET 1
#undef  CONFIG_CSLIP
#define CONFIG_INET_STATUS 1
#undef  CONFIG_UNIX

/*
 * Advanced socket options
 */
#undef  CONFIG_SOCK_CLIENTONLY

/*
 * Kernel hacking
 */
#undef  CONFIG_SYS_VERSION
#undef  CONFIG_OPT_SMALL
#undef  CONFIG_STRACE
