// Called from entry.S to get us going.
// entry.S already took care of defining envs, pages, vpd, and vpt.

#include <inc/lib.h>

asmlinkage void umain(int argc, char **argv);

const volatile struct Env *env;
const char *binaryname = "(PROGRAM NAME UNKNOWN)";

asmlinkage void
libmain(int argc, char **argv)
{
	extern uintptr_t sctors[], ectors[];
	uintptr_t *ctorva;

	// set env to point at our env structure in envs[].
	env = 0; /* Really? */

	// save the name of the program so that panic() can use it
	if (argc > 0)
		binaryname = argv[0];

	// Call any global constructors (e.g., defined by C++).
	// This relies on linker script magic to define the 'sctors' and
	// 'ectors' symbols; see user/user.ld.
	for (ctorva = ectors; ctorva > sctors; )
		((void(*)()) *--ctorva)();

	// call user main routine
	umain(argc, argv);

	// exit gracefully
	exit();
}

