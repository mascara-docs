# $FreeBSD: src/gnu/lib/Makefile.inc,v 1.4.8.1 1999/09/05 11:05:11 peter Exp $

SHLIB_MAJOR?= 2
SHLIB_MINOR?= 0


# NB: SHLIB major and minor nos. must also be specified in libg++/Makefile.inc
# (This is due to the deeper tree structure of libg++)
