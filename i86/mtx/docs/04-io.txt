                     460 Notes on I/O Functions

    When MTX starts, there is no OS support except BIOS. Therefore, all terminal
I/O must depend on getc()/putc() of BIOS. You have already implemented gets()
and prints(). Here, we outline how to implement a simple printf() function for
formatted printing of numbers in %c %s, %u, %d, %x, %l format. First, we
implement a printu() for printing unsigned (short) integers.

   typedef unsigned char    u8;
   typedef unsigned short  u16;
   typedef unsigned long   u32;

   char *ctable = "0123456789ABCDEF";
   u16  BASE = 10;

   int rpu(u16 x)
   {
       char c;
       if (x){
          c = ctable[x % BASE];
          rpu(x / BASE);
       }
       putc(c);
   }

   int printu(u16 x)
   {
      if (x==0)
         putc('0');
      else
         rpu(x);
      putc(' ');
   }

where rpu(x) recursively generates the digits of x % 10 and prints them on the
return path. For example, if x=123, the digits are generated in the order of
'3', 2', '1' but printed as '1', '2', '3' as they should. With printu(),
implementing a printd(), which prints signed integers, becomes trivial.

By setting BASE = 16, we can print in hex. By changing the parameter type to
u32, we can print long values, e.g. LBA sector and inode numbers. Assume that we
have prints(), printu(), printd(), printx(), printl() and printX(), where
printl() and printX() print 32-bit values in decimal and hex, respectively. It
is easy to implement a simple printf(char *fmt, ...) for formatted printing,
where fmt is a format string containing conversion symbols %c, %s, %u, %d, %x,
%l, %X.

Note that in 16-bit mode all parameters are passed as u16. For LONG values, you
must cast them by (long)item or (u32)item in function calls to force the BCC
compiler to pass them as 32-bit items.

int printf(char *fmt)
{
  char *cp = fmt;              // cp points at the fmt string
  u16  *ip = (int *)&fmt + 1;  // ip points at first item to be printed on stack
  u32  *up;
  while (*cp){
    if (*cp != '%'){
        putc(*cp);
        if (*cp=='\n')
            putc('\r');
        cp++;
        continue;
    }
    cp++;
    switch(*cp){
      case 'c' : putc  (*ip); break;
      case 's' : prints(*ip); break;
      case 'u' : printu(*ip); break;
      case 'd' : printi(*ip); break;
      case 'x' : printx(*ip); break;
      case 'l' : printl(*(u32 *)ip++); break;
      case 'X' : printX(*(u32 *)ip++); break;
    }
    cp++; ip++;
  }

This basic printf() function does not support field width or precision but it
is adequate for the simple printing task of MTX.
