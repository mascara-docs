.\" 
.\" Copyright (C) 1996
.\"   interface business GmbH
.\"   Tolkewitzer Strasse 49
.\"   D-01277 Dresden
.\"   F.R. Germany
.\"
.\" All rights reserved.
.\"
.\" Written by Joerg Wunsch <joerg_wunsch@interface-business.de>
.\"
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY
.\" EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
.\" PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
.\" CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
.\" OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
.\" BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
.\" LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
.\" USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
.\" DAMAGE.
.\"
.\" $FreeBSD: src/usr.sbin/wormcontrol/wormcontrol.8,v 1.3.2.5 1999/09/05 11:41:30 peter Exp $
.\"
.\" arrgh, hilit19 needs this" :-(
.Dd Jan 27, 1996
.Os
.Dt WORMCONTROL 8
.Sh NAME
.Nm wormcontrol
.Nd control the CD-R driver
.Sh SYNOPSIS
.Nm wormcontrol
.Op Ar options
.Ar file
.Nm wormcontrol
.Op Fl f Ar device
.Ar command
.Op Ar params...
.Sh DESCRIPTION
The
.Nm
utility is used to control the behaviour of the
.Xr worm 4
driver in order to adjust various parameters of a recordable CD
.Pq CD-R .
The command has two forms. The first one (compact form) is
used to quickly peform the common task of dumping data or audio
tracks to a disk, doing all the handling involved. The second form
is the traditional form and allows the user to control the various
phase of CD-R operation.
.Sh USAGE (compact form)
The following options are available:
.Bl -tag -width argument
.It Fl f Ar device
Use
.Ar device
instead of the one specified by the environment variable
.Pa WORMDEVICE ,
or the default device
.Pa /dev/rworm0 .
.It Fl d , Fl a
Write a data or audio track. One of
.Fl d
or
.Fl a
is compulsory.
.It Fl p Ar arg
Select preemphasis indication (only for audio tracks). Valid
arguments are
.Pa 1, on
or
.Pa 0, off
.It Fl s Ar speed
Select write speed (accepted values are
.Pa 1 ,
.Pa single ,
.Pa 2 ,
.Pa double ,
.Pa 4 ,
.Pa quad
).
.It Fl t
Perform a test (or dummy) write, not activating the laser. This is
useful for testing.
.It Fl e
Execute the fixate phase after writing this track, which is the
.Pa  ending
track. This is necessary for writing the TOC on the disk
and make it readable on a regular reader.
.It Fl n Ar 1|0
Select or not opening of next partition for writing (multisession
disks only).
.El
The typical command for writing a data track will then be
.Bd -literal
wormcontrol -f /dev/rwcd0a -d image.cd0
.Ed
which performs the quirk selection, prepdisk, preptrack, data write,
and fixate phases.
.Pp
The typical command for writing audio tracks will be
.Bd -literal
wormcontrol -f /dev/rwcd0a -a track01.cda
wormcontrol -f /dev/rwcd0a -a track01.cda
...
wormcontrol -f /dev/rwcd0a -a -e trackNN.cda
.Ed
where the
.Fl e
flags on the last command specifies that this is the ending track of
the disk thus the fixate operation must be performed.


.Sh USAGE (traditional form)
.Pp
Unlike many other devices, CD-R's require a very strict handling order.
Prior to writing data, the speed of the drive must be selected, and
the drive can also be turned into a
.Ql dummy
mode, where every action is only performed with the laser turned off.
This way, it's possible to test whether the environment provides a
sufficiently high data flow rate in order to actually burn the CD-R,
without destroying the medium in case of a catastrophic failure.
.Pp
In order to write a new track, the drive must be told whether the new
track will become an audio or a data track.  Audio tracks are written
with a block size of 2352 bytes, while data tracks have 2048 bytes per
block.  There are actually more data formats available, but the driver
currently does only support this
.Em Yellow Book
Mode 1 format.  For audio tracks, the driver does further need to know
whether the data are recorded with a preemphasis.
.Pp
Once all tracks of a session have been written, the disk must be
.Em fixated .
This writes the table of contents and leadout information to the disk.
The disk won't be usable without doing this.
.Pp
The following options are available:
.Bl -tag -width ident
.It Fl f Ar device
Use
.Ar device
instead of the default
.Pa /dev/rworm0 .
.It select Ar vendor-id model-id
Unfortunately, each CD-R drive vendor decided to implement a set of
SCSI commands of his own.  Thus, the
.Xr worm 4
driver needs to know which set of
.Ql quirk
functions to use for a particular device.  Currently, only devices
that have quirk information statically compiled into the driver will
work; it is however planned to make them available as loadable modules
in the future.  The
.Em select
command causes the driver to lookup the appropriate quirks.  The driver
matches the provided
.Ar vendor-id
and
.Ar model-id
against the list of known quirks.  An error will be returned if no
quirk record matches, and the device won't be usable for anything else
until a quirk record has been successfully selected.
.Pp
By now, the vendor/model names of
.Dq PLASMON
\&/
.Dq RF4100 ,
.Dq PHILIPS
\&/
.Dq CDD2000 ,
and
.Dq HP
\&/
.Dq 4020i
are known.
.It prepdisk Ar single \&| double Op Ar dummy
Prepare the disk for recording.  This must be done before any tracks
can be prepared, and remains in effect until the session has been
fixated.  Either single speed
.Pq for audio data
or double speed
.Pq for CD-ROM data
must be selected, and optionally, the argument
.Ar dummy
can be used to tell the drive to keep the laser turned off, for testing.
.It track Ar audio \&| data Op Ar preemp
Inform the driver about the format of the next track.  Either
.Ar audio
or
.Ar data
.Pq CD-ROM
must be selected, with an optional argument
.Ar preemp
that must be specified for an audio track where data records with
preemphasis are being used.  Once this command has been successfully
specified, the track is ready for being written.
.It fixate Ar toc-type Op Ar onp
Once all tracks have been written, this closes the current session.
The argument
.Ar toc-type
is a single digit between 0 and 4, with the following meaning:
.Bl -item
.It
0     CD audio
.It
1     CD-ROM
.It
2     CD-ROM with first track in mode 1
.It
3     CD-ROM with first track in mode 2
.It
4     CDI
.El
.Pp
The optional argument
.Ar onp
stands for
.Dq open next program area ,
which means that the next session on the CD-R will be opened and can
be recorded in the future.  Otherwise, the CD-R will be closed and
remains unchangeable.
.El
.Sh DIAGNOSTICS
Error codes for the underlying
.Xr ioctl 2
commands are printed by the
.Xr err 3
facility.
.Sh EXAMPLES
The typical sequence of burning a data CD-R looks like:
.Bd -literal
# wormcontrol select PLASMON RF4100
# wormcontrol prepdisk double
# wormcontrol track data
# rtprio 5 team -v 1m 5 < cdrom.image | rtprio 5 dd of=/dev/rworm0 obs=20k
# wormcontrol fixate 1
.Ed
.Pp
Note that the
.Xr dd 1
command above is mainly used in order to
.Dq slice
the data stream.  It's particularly useful when recording audio data
with their rather unusual blocksize.  Since the underlying device is a
.Em raw
device, the blocksize used in that command must be an integral multiple
of the CD-R blocksize.
.Pp
The mentioned command
.Xr team 1
is not part of the base system, but comes extremely handy in order to
pipe a constant data stream into the CD recorder.
.Sh SEE ALSO
.Xr dd 1 ,
.Xr team 1 ,
.Xr ioctl 2 ,
.Xr err 3 ,
.Xr worm 4
.Pp
.Pa /usr/share/examples/worm/*
.Sh HISTORY
.Nm Wormcontrol
is currently under development.
.Sh AUTHOR
The program has been contributed by
.ie t J\(:org Wunsch,
.el Joerg Wunsch,
Dresden.
