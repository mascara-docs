.\" Copyright (c) 1996
.\"	Julian Elischer <julian@freebsd.org>.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\"
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man4/sd.4,v 1.6.2.4 1999/09/05 11:26:57 peter Exp $
.\"
.Dd January 18, 1996
.Dt SD 4
.Os FreeBSD
.Sh NAME
.Nm sd
.Nd SCSI disk driver
.Sh SYNOPSIS
.Cd disk sd
.Cd disk sd1 at scbus0 target 4 lun 0
.Sh DESCRIPTION
The
.Nm sd
driver provides support for a 
.Tn SCSI
disk. It allows the disk
to be divided up into a set of pseudo devices called
.Em partitions .
In general the interfaces are similar to those described by 
.Xr wd 4 .
.Pp
Where the 
.Xr wd 4
device has a fairly low level interface to the system, 
.Tn SCSI
devices have a much higher level interface and talk to the system via
a 
.Tn SCSI
host adapter
(e.g.,
.Xr ahc 4 ) .
A
.Tn SCSI
adapter must also be separately configured into the system
before a
.Tn SCSI
disk can be configured.
.Pp
When the
.Tn SCSI
adapter is probed during boot, the 
.Tn SCSI
bus is scanned for devices. Any devices found which answer as 
.Sq Em Direct
type devices will be attached to the 
.Nm
driver.
In 
.Tn FreeBSD
releases prior to 2.1, the first found was attached as
.Li sd0 ,
the second
.Li sd1 ,
and so on.
Beginning in
.Fx 2.1
it became possible to lock down the assignment of
devices on the
.Tn SCSI
bus to particular units of the
.Nm
device; refer to
.Xr scsi 4
for details on kernel configuration.
.Sh PARTITIONING
The 
.Nm
driver allows the disk to have two levels of partitioning.
One layer, called the
.Dq slice layer ,
is used to separate the
.Tn FreeBSD
areas of the disk from areas used by other operating systems.
The second layer is the native
.Bx 4.4
partitioning scheme,
.Xr disklabel 5 ,
which is used to subdivide the
.Tn FreeBSD
slices into areas for individual filesystems and swap spaces.
For more information, see
.Xr fdisk 8
and
.Xr disklabel 8 ,
respectively.)
.Pp
If an uninitialized disk is opened, the slice table will be
initialized with a fictitious
.Tn FreeBSD
slice spanning the entire disk.  Similarly, if an uninitialized
(or
.No non- Ns Tn FreeBSD )
slice is opened, its disklabel will be initialized with parameters returned
by the drive and a single
.Sq Li c
partition encompassing the entire slice.
.Sh KERNEL CONFIGURATION
It is only necessary to explicitly configure one
.Nm
device; data structures are dynamically allocated as disks are found
on the
.Tn SCSI
bus.
.Sh IOCTLS
The following 
.Xr ioctl 2
calls apply to 
.Tn SCSI
disks as well as to other disks.  They are defined in the header file
.Aq Pa sys/disklabel.h .
.Pp
.Bl -tag -width DIOCSDINFO
.It Dv DIOCSBAD
Usually used to set up a bad-block mapping system on the disk. 
.Tn SCSI
drive incorporate their own bad-block mapping so this command is not
implemented.
.It Dv DIOCGDINFO
Read, from the kernel, the in-core copy of the disklabel for the
drive. This may be a fictitious disklabel if the drive has never
been initialized, in which case it will contain information read
from the
.Tn SCSI
inquiry commands.
.It Dv DIOCSDINFO
Give the driver a new disklabel to use. The driver
.Em will not
write the new
disklabel to the disk.
.It Dv DIOCWLABEL
Enable or disable the driver's software
write protect of the disklabel on the disk.
.It Dv DIOCWDINFO
Give the driver a new disklabel to use. The driver
.Em will
write the new disklabel to the disk.
.El
.Pp
In addition, the 
.Xr scsi 4
general
.Fn ioctl
commands may be used with the 
.Nm
driver, but only against the 
.Sq Li c
(whole disk) partition.
.Sh NOTES
If a removable device is attached to the 
.Nm
driver, then the act of changing the media will invalidate the
disklabel and information held within the kernel.  To avoid
corruption, all accesses to the device will be discarded until there
are no more open file descriptors referencing the device.  During this
period, all new open attempts will be rejected.  When no more open
file descriptors reference the device, the first next open will load a
new set of parameters (including disklabel) for the drive.
.Sh FILES
.Bl -tag -width /dev/rsdXXXXX -compact
.It Pa /dev/rsd Ns Ar u
raw mode
.Tn SCSI
disk unit
.Ar u ,
accessed as an unpartitioned device
.Sm off
.It Pa /dev/sd Ar u Pa s Ar n
.Sm on
block mode
.Tn SCSI
disk unit
.Ar u ,
slice
.Ar n ,
accessed as an unpartitioned device
.Sm off
.It Pa /dev/rsd Ar u Pa s Ar n
.Sm on
raw mode
.Tn SCSI
disk unit
.Ar u ,
slice
.ar n ,
accessed as an unpartitioned device
.It Pa /dev/sd Ns Ar u Ns Ar p
block mode
.Tn SCSI
disk unit
.Ar u ,
first
.Tn FreeBSD
slice, partition
.Ar p
.It Pa /dev/rsd Ns Ar u Ns Ar p
raw mode
.Tn SCSI
disk unit
.Ar u ,
first
.Tn FreeBSD
slice, partition
.Ar p
.Sm off
.It Xo
.Pa /dev/sd
.Ar u
.Pa s
.Ar n
.Ar p
.Xc
.Sm on
block mode
.Tn SCSI
disk unit
.Ar u ,
.No Ar n Ns th
slice, partition
.Ar p
.Sm off
.It Xo
.Pa /dev/rsd
.Ar u
.Pa s
.Ar n
.Ar p
.Xc
raw mode
.Tn SCSI
disk unit
.Ar u ,
.No Ar n Ns th
slice, partition
.Ar p
.El
.Sh DIAGNOSTICS
None.
.Sh SEE ALSO
.Xr wd 4 ,
.Xr disklabel 5 ,
.Xr disklabel 8 ,
.Xr fdisk 8 ,
.Xr sd 9
.Sh HISTORY
The
.Nm
driver was originally written for
.Tn Mach
2.5, and was ported to
.Tn FreeBSD
by Julian Elischer.  Support for slices was written by Bruce Evans.
