#!/bin/sh

verbose=false

if [ "x$1" = "x-x" ]
then
	verbose=true
	out=/dev/stdout
	err=/dev/stderr
else
	out=/dev/null
	err=/dev/null
fi

runbochs() {
	brkaddr=`grep monitor obj/kern/kernel.sym | sed -e's/ .*$//g'`
	echo "brkaddr $brkaddr"
	(echo vbreak 0x8:0x$brkaddr; echo c; echo die; echo quit) |
		bochs -q 'display_library: nogui' \
			'parport1: enabled=1, file="bochs.out"' \
			>$out 2>$err
}



gmake
runbochs

score=0

# echo -n "Printf: "
	awk 'BEGIN{printf("Printf: ");}' </dev/null
	if grep "480 decimal is 740 octal!" bochs.out >/dev/null
	then
		score=`echo 20+$score | bc`
		echo OK
	else
		echo WRONG
	fi

# echo -n "Printf: "
	awk 'BEGIN{printf("Backtrace: ");}' </dev/null
	cnt=`grep "ebp f0109...  eip f0100...  args" bochs.out|wc -w`
	if [ $cnt -eq 80 ]
	then
		score=`echo 30+$score | bc`
		echo OK
	else
		echo WRONG
	fi

echo "Score: $score/50"


