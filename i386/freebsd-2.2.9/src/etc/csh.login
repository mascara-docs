# $FreeBSD: src/etc/csh.login,v 1.16.2.3 1999/09/05 11:01:56 peter Exp $
#
# System-wide .login file for csh(1).
# Uncomment this to give you the default 4.2 behavior, where disk 
# information is shown in K-Blocks
# setenv BLOCKSIZE	K
# Uncomment this two lines to activate Russian locale
# setenv LANG ru_RU.KOI8-R
# setenv MM_CHARSET KOI8-R
# Uncomment this two lines to activate Italian locale
# setenv LANG it_IT.ISO_8859-1
# setenv MM_CHARSET ISO-8859-1
# For full locales list check /usr/share/locale/*
# Read system messages
# msgs -f
# Allow terminal messages
# mesg y
