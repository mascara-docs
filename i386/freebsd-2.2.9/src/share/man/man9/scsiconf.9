.\" Copyright (c) 1997
.\"	John-Mark Gurney.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. Neither the name of the author nor the names of any co-contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY John-Mark Gurney AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man9/scsiconf.9,v 1.1.2.4 1999/09/05 11:27:13 peter Exp $
.\"
.Dd January 29, 1997
.Dt SCSICONF 9
.Os FreeBSD
.Sh NAME
.Nm scsiconf
.Nd define how certain scsi devices behave
.Sh SYNOPSIS
.Fd #include <scsi/scsi_all.h>
.Fd #include <scsi/scsiconf.h>
.Dv static struct scsidevs knowndevs[] =
.Em ...
.Sh DESCRIPTION
The
.Dv struct scsidevs knowndevs
contains information about scsi devices that are known to have problems
or require special flags.
.Bd -literal -offset indent
struct scsidevs {
	u_int32_t	type;
	u_int32_t	driver;	/* normally the same as type */
	boolean		removable;
	char		*manufacturer;
	char		*model;
	char		*version;
	char		*devname;
	char		flags;	/* see below */
	u_int16_t	quirks;
	void		*devmodes;
};

#define	SC_SHOWME	...
#define	SC_ONE_LU	...
#define	SC_MORE_LUS	...

.Ed
.Pp
Matching is based first on device type, then on the manufacturer, model,
and revision strings returned by the device.  The returned strings are
fixed lengths of 8, 16 and 4 bytes respectively.  In the matching
pattern, a question mark
.Pq So ? Sc
matches any single character and a trailing
asterisk
.Pq So * Sc
matches remaining characters.  For patterns shorter
than their respective fields, trailing spaces are implied.
.Bl -tag -width manufacturer
.It Va type
This is the type of device that is returned by the device.  Look in
.Pa /sys/scsi/scsi_all.h
for the list of available types
.Pq look for the Dv T_xxx macros .
.It Va driver
This specifies the driver to assign to the devices that match this entry.
It usually has the same value as
.Va type ,
unless a particular device is lying about its identity.
.It Va removable
This specifies wether the device is removable
.Pq Dv T_REMOV
or fixed
.Pq Dv T_FIXED .
.It Va manufacturer
This is the string to match against the vendor identification returned by the
device.
.It Va model
This is the string to match against the product identification returned by the
device.
.It Va version
This is the string to match against the product revision level returned by the
device.
.It Va devname
The device name of the driver to attach to this scsi device.
.It Va flags
This is set to possible flags that are use when probing this device.
The values are:
.Bl -tag -width "SC_MORE_LUSXX" -compact
.It Dv SC_SHOWME
Be verbose about the decisions made when considering this entry at
device probe time.
.It Dv SC_ONE_LU
Probe only the first LUN if device matches this entry; used for broken
devices that erroneously respond on more than a single LUN.
.It Dv SC_MORE_LUS
The opposite, probe for all possible LUNs.  This is the default, but
can be used if a quirk entry is required for other purposes, like
overriding a type-specific default that would restrict probing to only
one LUN.
.El
.It Va quirks
This field specifies device specific flags.  See the device-specific man pages in
section 9 for more info on these flags.
.It Va devmodes
This is a pointer to a device specific struct.  Currently only used by
.Xr st 4
to set initial modes.
.El
.Sh FILES
.Bl -tag -width /sys/scsi/scsiconf.c -compact
.It Pa /sys/scsi/scsi_all.h
Lists the possible scsi devices
.It Pa /sys/scsi/scsiconf.h
Lists possible flags for both
.Va flags
and
.Va quirks
.It Pa /sys/scsi/scsiconf.c
Contains the
.Va knowndevs
data structure
.El
.Sh SEE ALSO
.Xr cd 4 ,
.Xr ch 4 ,
.Xr pt 4 ,
.Xr scsi 4 ,
.Xr sd 4 ,
.Xr st 4 ,
.Xr su 4 ,
.Xr uk 4 ,
.Xr cd 9 ,
.Xr sd 9 ,
.Xr st 9
.Sh HISTORY
The
.Nm
manual page first appeared in
.Fx 2.2 .
.Sh AUTHOR
This
manual page was written by John-Mark Gurney
.Aq gurney_j@efn.org .
.Sh BUGS
This isn't a complete description of what
.Pa /sys/scsi/scsiconf.c
contains, but the remaining functions there are internal to the SCSI
subsystem and thus of little interest for driver writers.
