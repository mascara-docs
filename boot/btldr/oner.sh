#!/bin/bash

[ -z "$1" ] && {
        echo "# please specify a file to link to"
        exit 127
}

MAIN="$1"
gcc -g -Os -march=i686 -m32 -ffreestanding -Wall -Werror -I. -static -nostdlib -Wl,-Tlinker.ld -Wl,--nmagic -Wl,--oformat=binary -o vbr.bin $MAIN.c
