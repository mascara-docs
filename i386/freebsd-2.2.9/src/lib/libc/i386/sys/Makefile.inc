#	from: Makefile.inc,v 1.1 1993/09/03 19:04:23 jtc Exp
# $FreeBSD: src/lib/libc/i386/sys/Makefile.inc,v 1.2.10.2 1999/09/05 11:16:33 peter Exp $

.PATH:	${.CURDIR}/../libc/${MACHINE}/sys

SRCS+=	i386_get_ldt.c i386_set_ldt.c

# Only build man pages with libc.
.if ${LIB} == "c"
MAN2+=	i386/sys/i386_get_ldt.2

MLINKS+=i386_get_ldt.2 i386_set_ldt.2
.endif
