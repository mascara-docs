.Dd October 4, 1996
.Dt FDISK 8
.\".Os BSD 4
.Sh NAME
.Nm fdisk
.Nd DOS partition maintenance program
.Sh SYNOPSIS
.Nm fdisk
.Op Fl i
.Op Fl u
.Op Fl a
.Op Fl 1234
.Op Ar disk
.Bl -tag -width time
.Nm fdisk
.Op Fl f Ar configfile
.Op Fl i
.Op Fl v
.Op Fl t
.Op Ar disk
.Sh PROLOGUE
In order for the BIOS to boot the kernel,
certain conventions must be adhered to.
Sector 0 of the disk must contain boot code,
a partition table,
and a magic number.
BIOS partitions can be used to break the disk up into several pieces.
The BIOS brings in sector 0
(does it really use the code?)
and verifies the magic number.
It then searches the 4 BIOS partitions described by sector 0
to determine which of them is
.Em active .
This boot then brings in the secondary boot block from the
.Em active
partition and runs it.
Under DOS,
you could have one or more partitions with one
.Em active .
The DOS
.Nm
program can be used to divide space on the disk into partitions and set one
.Em active .
.Sh DESCRIPTION
The
.Bx Free
program
.Nm
serves a similar purpose to the DOS program.  The first form is used to
display partition information or to interactively edit the partition
table.  The second is used to write a partition table using a
.Ar configfile
and is designed to be used by other scripts/programs.
.Pp
Options are:
.It Fl u
Is used for updating (editing) sector 0 of the disk.  Ignored if
.Fl f
is given.
.It Fl i
Initialize sector 0 of the disk.  This implies
.Fl u ,
unless
.Fl f
is given.
.It Fl a
Change the active partition only.  Ignored if
.Fl f
is given.
.It Fl 1234
Operate on a single fdisk entry only.  Ignored if
.Fl f
is given.
.It Fl f Ar configfile
Set partition values using the file
.Ar configfile .
The
.Ar configfile
always modifies existing partitions, unless
.Fl i
is also given, in which case all existing partitions are deleted (marked
as "unused") before the
.Ar configfile
is read.  The
.Ar configfile
can be "-", in which case
.Ar stdin
is read.  See
.Sx CONFIGURATION FILE ,
below, for file syntax.
.Pp
.Em WARNING Ns :
when
.Fl f
is used, you are not asked if you really want to write the partition
table (as you are in the interactive mode).  Use with caution!
.It Fl t
Test mode; do not write partition values.  Generally used with the
.Fl f
option to see what would be written to the partition table.  Implies
.Fl v .
.It Fl v
Be verbose.  When
.Fl f
is used,
.Nm
prints out the partition table that is written to the disk.
.El
.Pp
The final disk name can be provided as a
.Sq bare
disk name only, e.g.
.Ql sd0 ,
or as a fully qualified device node under
.Pa /dev .
If omitted, the disks
.Ql wd0 ,
.Ql sd0 ,
and
.Ql od0
are being searched in that order, until one is
being found responding.
.Pp
When called with no arguments, it prints the sector 0 partition table.
An example follows:

.Bd -literal
	******* Working on device /dev/rwd0 *******
	parameters extracted from in-core disklabel are:
	cylinders=769 heads=15 sectors/track=33 (495 blks/cyl)

	parameters to be used for BIOS calculations are:
	cylinders=769 heads=15 sectors/track=33 (495 blks/cyl)
		
	Warning: BIOS sector numbering starts with sector 1
	Information from DOS bootblock is:
	The data for partition 1 is:
	sysid 165,(FreeBSD/NetBSD/386BSD)
    	    start 495, size 380160 (185 Meg), flag 0
		beg: cyl 1/ sector 1/ head 0;
		end: cyl 768/ sector 33/ head 14
	The data for partition 2 is:
	sysid 164,(unknown)
    	    start 378180, size 2475 (1 Meg), flag 0
		beg: cyl 764/ sector 1/ head 0;
		end: cyl 768/ sector 33/ head 14
	The data for partition 3 is:
	<UNUSED>
	The data for partition 4 is:
	sysid 99,(ISC UNIX, other System V/386, GNU HURD or Mach)
    	    start 380656, size 224234 (109 Meg), flag 80
		beg: cyl 769/ sector 2/ head 0;
		end: cyl 197/ sector 33/ head 14
.Ed
.Pp
The disk is divided into three partitions that happen to fill the disk.
The second partition overlaps the end of the first.
(Used for debugging purposes)
.Bl -tag -width "cyl, sector and head"
.It Em "sysid"
is used to label the partition.
.Bx Free
reserves the
magic number 165 decimal (A5 in hex).
.It Em "start and size"
fields provide the start address
and size of a partition in sectors.
.It Em "flag 80"
specifies that this is the active partition.
.It Em "cyl, sector and head"
fields are used to specify the beginning address
and end address for the partition.
.It Em "Note:"
these numbers are calculated using BIOS's understanding of the disk geometry
and saved in the bootblock.
.El
.Pp
The flags
.Fl i
or
.Fl u
are used to indicate that the partition data is to be updated, unless the
.Fl f
option is used.  If the
.Fl f
option is not used, the
.Nm
program will enter a conversational mode.
This mode is designed not to change any data unless you explicitly tell it to.
.Nm Fdisk
selects defaults for its questions to guarantee the above behavior.
.Pp
It displays each partition
and ask if you want to edit it.
If you say yes,
it will step through each field showing the old value
and asking for a new one.
When you are done with a partition,
.Nm
will display it and ask if it is correct.
.Nm Fdisk
will then proceed to the next entry.
.Pp
Getting the
.Em cyl, sector,
and
.Em head
fields correct is tricky.
So by default,
they will be calculated for you;
you can specify them if you choose.
.Pp
After all the partitions are processed,
you are given the option to change the
.Em active
partition.
Finally,
when the all the data for the first sector has been accumulated,
you are asked if you really want to rewrite sector 0.
Only if you answer yes,
will the data be written to disk.
.Pp
The difference between the
.Fl u
flag and
.Fl i
flag is that
the
.Fl u
flag just edits the fields as they appear on the disk.
While the
.Fl i
flag is used to "initialize" sector 0;
it will setup the last BIOS partition to use the whole disk for
.Bx Free ;
and make it active.
.Sh NOTES
The automatic calculation of starting cylinder etc. uses
a set of figures that represent what the BIOS thinks is the
geometry of the drive.
These figures are by default taken from the incore disklabel,
but the program initially gives you an opportunity to change them.
This allows the user to create a bootblock that can work with drives
that use geometry translation under the BIOS.
.Pp
If you hand craft your disk layout,
please make sure that the
.Bx Free
partition starts on a cylinder boundary.
A number of decisions made later may assume this.
(This might not be necessary later.)
.Pp
Editing an existing partition will most likely cause you to
lose all the data in that partition.
.Pp
You should run this program interactively once or twice to see how it
works.  This is completely safe as long as you answer the last question
in the negative.  There are subtleties that the program detects that are
not fully explained in this manual page.
.Sh CONFIGURATION FILE
When the
.Fl f
option is given, a disk's partition table can be written using values
from a
.Ar configfile .
The syntax of this file is very simple.  Each line is either a comment or
a specification, and whitespace (except for newlines) are ignored:
.Bl -tag -width Ds
.It Xo
.Ic #
.No Ar comment ...
.Xc
Lines beginning with a "#" are comments and are ignored.
.It Xo
.Ic g
.No Ar spec1
.No Ar spec2
.No Ar spec3
.Xc
Set the BIOS geometry used in partition calculations.  There must be
three values specified, with a letter preceding each number:
.Bl -tag -width Ds
.Sm off
.It Cm c No Ar num
.Sm on
Set the number of cylinders to
.Ar num .
.Sm off
.It Cm h No Ar num
.Sm on
Set the number of heads to
.Ar num .
.Sm off
.It Cm s No Ar num
.Sm on
Set the number of sectors/track to
.Ar num .
.El
.Pp
These specs can occur in any order, as the leading letter determines
which value is which; however, all three must be specified.
.Pp
This line must occur before any lines that specify partition
information.
.Pp
It is an error if the following is not true:
.Pp
.nf
        1 <= number of cylinders
        1 <= number of heads <= 256
        1 <= number of sectors/track < 64
.fi
.Pp
The number of cylinders should be less than or equal to 1024, but this
is not enforced, although a warning will be output.  Note that bootable
.Bx Free
partitions (the "/" filesystem) must lie completely within the
first 1024 cylinders; if this is not true, booting may fail.
Non-bootable partitions do not have this restriction.
.Pp
Example (all of these are equivalent), for a disk with 1019 cylinders,
39 heads, and 63 sectors:
.Pp
.nf
        g       c1019   h39     s63
        g       h39     c1019   s63
        g       s63     h39     c1019
.fi
.It Xo
.Ic p
.No Ar partition
.No Ar type
.No Ar start
.No Ar length
.Xc
Set the partition given by
.Ar partition
(1-4) to type
.Ar type ,
starting at sector
.Ar start
for
.Ar length
sectors.
.Pp
Only those partitions explicitly mentioned by these lines are modified;
any partition not referenced by a "p" line will not be modified.
However, if an invalid partition table is present, or the
.Fl i
option is specified, all existing partition entries will be cleared
(marked as unused), and these "p" lines will have to be used to
explicitly set partition information.  If multiple partitions need to be
set, multiple "p" lines must be specified; one for each partition.
.Pp
These partition lines must occur after any geometry specification lines,
if one is present.
.Pp
The
.Ar type
is 165 for
.Bx Free
partitions.  Specifying a partition type of zero is
the same as clearing the partition and marking it as unused; however,
dummy values (such as "0") must still be specified for
.Ar start
and
.Ar length .
.Pp
Note: the start offset will be rounded upwards to a head boundary if
necessary, and the end offset will be rounded downwards to a cylinder
boundary if necessary.
.Pp
Example: to clear partition 4 and mark it as unused:
.Pp
.nf
        p       4       0       0       0
.fi
.Pp
Example: to set partition 1 to a
.Bx Free
partition, starting at sector 1
for 2503871 sectors (note: these numbers will be rounded upwards and
downwards to correspond to head and cylinder boundaries):
.Pp
.nf
        p       1       165     1       2503871
.fi
.It Xo
.Ic a
.No Ar partition
.Xc
Make
.Ar partition
the active partition.  Can occur anywhere in the config file, but only
one must be present.
.Pp
Example: to make partition 1 the active partition:
.Pp
.nf
        a       1
.fi

.El
.Pp
.Sh SEE ALSO
.Xr disklabel 8
.Sh BUGS
The entire program should be made more user-friendly.
.Pp
Throughout this man page, the term
.Sq partition
is used where it should actually be
.Sq slice ,
in order to conform with the terms used elsewhere.
.Pp
You cannot use this command to completely dedicate a disk to
.Bx Free .
The
.Xr disklabel 8
command must be used for this.
