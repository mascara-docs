.\" $FreeBSD: src/share/man/man4/scsi.4,v 1.6.2.4 1999/09/05 11:26:57 peter Exp $
.\" Copyright (c) 1996
.\"	Julian Elischer <julian@freebsd.org>.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\"
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.Dd August 27, 1993
.Dt SD 4
.Os FreeBSD
.Sh NAME
.Nm scsi
.Nd scsi system
.Sh SYNOPSIS
.Cd "controller scbus0"
.Cd "controller scbus1 at ahc0"
.Cd "controller scbus3 at ahc1 bus 0"
.Cd "controller scbus2 at ahc1 bus 1"
.Cd "device cd0"
.Cd "device ch0"
.Cd "disk sd0"
.Cd "tape st0"
.Cd "device ch1 at scbus0 target 4 unit 0"
.Sh DESCRIPTION
The
.Em scsi
system provides a uniform and modular system for the implementation
of drivers to control various scsi devices, and to utilize different
scsi host adapters through host adapter drivers. When the system probes the 
.Em SCSI
busses, it attaches any devices it finds to the appropriate
drivers. If no driver seems appropriate, then it attaches the device to the
uk (unknown) driver so that user level scsi ioctls may
still be performed against the device.
.Sh KERNEL CONFIGURATION
The option SCSIDEBUG enables the debug ioctl.
.Pp
All devices and the SCSI busses support boot time allocation so that
an upper number of devices and controllers does not need to be configured;
.Em "device sd0"
will suffice for any number of disk drivers.
.Pp
The devices are either
.Em wired
so they appear as a particular device unit or
.Em counted
so that they appear as the next available unused unit.
.Pp
To configure a driver in the kernel without wiring down the device use a
config line similar to
.Em "device ch0"
to include the changer driver.
.Pp
To wire down a unit use a config line similar to
.Em "device ch1 at scbus0 target 4 unit 0"
to assign changer 1 as the changer with SCSI ID 4,
SCSI logical unit 0 on SCSI bus 0.
Individual scbuses can be wired down to specific controllers with
a config line similar to
.Em "controller scbus0 at ahc0"
which assigns scsi bus 0 to the first unit using the ahc driver.
For controllers supporting more than one bus,
the particular bus can be specified as in
.Em "controller scbus3 at ahc1 bus 1"
which assigns scbus 1 to the second bus probed on the ahc1 device.
.Pp
When you have a mixture of wired down and counted devices then the
counting begins with the first non-wired down unit for a particular
type.  That is, if you have a disk wired down as
.Em "disk sd1" ,
then the first non-wired disk shall come on line as
.Em sd2 .
.Sh IOCTLS
There are a number of ioctls that work on any 
.Em SCSI
device. They are defined in
.Em sys/scsiio.h
and can be applied against any scsi device that permits them.
For the tape, it must be applied against the control
device. See the manual page for each device type for more information about
how generic scsi ioctls may be applied to a specific device.
.Bl -tag -width DIOCSDINFO____
.It Dv SCIOCRESET*
reset a device.
.It Dv SCIOCDEBUG
Turn on debugging.. All scsi operations originating from this device's driver
will be traced to the console, along with other information. Debugging is
controlled by four bits, described in the header file. If no debugging is
configured into the kernel, debugging will have no effect. 
.Em SCSI
debugging is controlled by the configuration option
.Em SCSIDEBUG.
.It Dv SCIOCCOMMAND
Take a scsi command and data from a user process and apply them to the scsi
device. Return all status information and return data to the process. The 
ioctl will return a successful status even if the device rejected the
command. As all status is returned to the user, it is up to the user
process to examine this information to decide the success of the command.
.It Dv SCIOCREPROBE
Ask the system to probe the scsi busses for any new devices. If it finds
any, they will be attached to the appropriate drivers. The search can be
narrowed to a specific bus, target or lun. The new device may or may not
be related to the device on which the ioctl was performed.
.It Dv SCIOCIDENTIFY
Ask the driver what it's bus, target and lun are.
.It Dv SCIOCDECONFIG
Ask the device to disappear. This may not happen if the device is in use.
.El
.Sh NOTES
the generic scsi part of the system is still being mapped out.
Watch this space for changes.
.Pp
 A device by the name of su (scsi_user)
(e.g  su0-0-0) will map bus, target and lun to  minor numbers. I have not
yet decided yet whether this device will be able to open a device that is
already controlled by an explicit driver.
.Sh ADAPTERS
The system allows common device drivers to work through many different
types of adapters. The adapters take requests from the upper layers and do
all IO between the 
.Em SCSI
bus and the system. The maximum size of a transfer is governed by the
adapter. Most adapters can transfer 64KB in a single operation, however
many can transfer larger amounts.
.Sh TARGET MODE
Some adapters support 
.Em Target mode
in which the system is capable of operating as a device, responding to
operations initiated by another system. Target mode will be supported for
some adapters, but is not yet complete for this version of the scsi system.
.Sh FILES
see other scsi device entries.
.Sh DIAGNOSTICS
When the kernel is compiled with option SCSIDEBUG, the SCIOCDEBUG ioctl 
can be used to enable various amounts of tracing information on any 
specific device. Devices not being traced will not produce trace information.
The four bits that make up the debug level, each control certain types
of debugging information. 
.Bl -tag -width THIS_WIDE_PLEASE
.It Dv Bit 0
Bit 0  shows all scsi bus operations including scsi commands,
error information and the first 48 bytes of any data transferred.
.It Dv Bit 1
Bit 1 shows routines called.
.It Dv Bit 2
Bit 2 shows information about what branches are taken and often some
of the return values of functions.
.It Dv Bit 3
Bit 3 shows more detailed information including DMA scatter-gather logs.
.El
.Sh SEE ALSO
.Xr aha 4 ,
.Xr ahb 4 ,
.Xr bt 4 ,
.Xr cd 4 ,
.Xr ch 4 ,
.Xr sd 4 ,
.Xr st 4 ,
.Xr su 4 ,
.Xr uha 4 ,
.Xr uk 4
.Sh HISTORY
This
.Nm
system appeared in MACH 2.5 at TRW.
