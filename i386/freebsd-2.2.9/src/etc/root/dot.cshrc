# $FreeBSD: src/etc/root/dot.cshrc,v 1.18.2.4 1999/09/05 11:02:13 peter Exp $
#
#csh .cshrc file

alias h		history 25
alias j		jobs -l
alias la	ls -a
alias lf	ls -FA
alias ll	ls -lA

setenv	EDITOR	/usr/bin/ee
setenv	PAGER	more
setenv	BLOCKSIZE	K

if ($?prompt) then
	# An interactive shell -- set some stuff up
	set prompt = "`hostname -s`# "
	set filec
	set history = 100
	set savehist = 100
	set mail = (/var/mail/$USER)

	# make mail(1) happy:
	setenv	crt	24
endif
