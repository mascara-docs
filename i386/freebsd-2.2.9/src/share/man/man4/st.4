.\" Copyright (c) 1996
.\"	Julian Elischer <julian@freebsd.org>.  All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\"
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/share/man/man4/st.4,v 1.9.2.3 1999/09/05 11:26:57 peter Exp $
.\"
.Dd January 17, 1996
.Dt ST 4
.Os FreeBSD
.Sh NAME
.Nm st
.Nd SCSI tape driver
.Sh SYNOPSIS
.Cd tape st
.Cd device st1 at scbus0 target 4 lun 0
.Sh DESCRIPTION
The
.Nm
driver provides support for a 
.Tn SCSI
tape. It allows the tape
to be run in up to four different modes depending on minor numbers
and supports several different `sub-modes'.
The device can have both a
.Em raw
interface
and a
.Em block
interface; however, only the raw interface is usually used (or recommended).
In general the interfaces are similar to those described by 
.Xr wt 4 
or
.Xr mt 4 .
.Pp
Where the 
.Xr wt 4
device has a fairly low level interface to the system, 
.Tn SCSI
devices have a much higher level interface and talk to the system via
a 
.Tn SCSI
adapter and a
.Tn SCSI
adapter driver
(e.g.,
.Xr ahc 4 ) .
A
.Tn SCSI
adapter must also be separately configured into the system
before a
.Tn SCSI
tape can be configured.
.Pp
As the
.Tn SCSI
adapter is probed during boot, the 
.Tn SCSI
bus is scanned for devices. Any devices found which answer as
.Sq Em Sequential
type devices will be attached to the 
.Nm
driver.
In
.Tn FreeBSD
releases prior to 2.1, the first found is attached as
.Em st0
and the next, 
.Em st1 ,
etc.
Beginning in 
.Fx 2.1
it is possible to specify what
.Nm
unit a device should
come on line as; refer to
.Xr scsi 4
for details on kernel configuration.
.Sh MOUNT SESSIONS
The 
.Nm
driver is based around the concept of a 
.Dq Em mount session ,
which is defined as the period between the time that a tape is
mounted, and the time when it is unmounted.  Any parameters set during
a mount session remain in effect for the remainder of the session or
until replaced. The tape can be unmounted, bringing the session to a
close in several ways.  These include:
.Bl -enum
.It
Closing an `unmount device',
referred to as sub-mode 00 below. An example is 
.Pa /dev/rst0 .
.It
Using the MTOFFL
.Xr ioctl 2
command, reachable through the
.Sq Cm offline
command of
.Xr st 1 .
.It
Opening a different mode will implicitly unmount the tape, thereby closing
off the mode that was previously mounted.  All parameters will be loaded
freshly from the new mode.  (See below for more on modes.)
.El
.Pp
Parameters that are required to last across the unmounting of a tape
should be set on the control device.  This is sub-mode 3 (see below) and is
reached through a file with a name of the form
.Sm off
.No Xo
.Pa /dev/st
.Ar Y
.Pa ctl.
.Ar X
.Xc ,
.Sm on
where
.Ar Y
is the drive number and
.Ar X
is the mode number.
.Sh MODES AND SUB-MODES
There are four 
.Sq operation
modes. These are controlled by bits 2 and 3 of the minor number and
are designed to allow users to easily read and write different formats
of tape on devices that allow multiple formats.  The parameters for
each mode can be set individually by hand with the
.Xr mt 1
command.  When a device corresponding to a particular mode is first
mounted, The operating parameters for that
mount session
are copied from that mode.  Further changes to the parameters during the
session will change those in effect for the session but not those set
in the operation mode.  To change the parameters for an operation mode, 
one must either assign the parameters to the control device, or compile
them into the
.Dq Em Rogues Gallery
table in the driver's source code.
.Pp
In addition to the four operating modes mentioned above, 
bits 0 and 1 of the minor number are interpreted as
.Sq sub-modes .
The sub-modes differ in the action taken when the device is closed:
.Bl -tag -width XXXX
.It 00
A close will rewind the device; if the tape has been 
written, then a file mark will be written before the rewind is requested.
The device is unmounted.
.It 01
A close will leave the tape mounted.
If the tape was written to, a file mark will be written.
No other head positioning takes place.
Any further reads or writes will occur directly after the
last read, or the written file mark.
.It 10
A close will rewind the device. If the tape has been 
written, then a file mark will be written before the rewind is requested.
On completion of the rewind an unload command will be issued.
The device is unmounted.
.It 11
This is a special mode, known as the 
.Dq control device
for the mode.  Parameters set for the mode while in this sub-mode will
be remembered from one mount to the next.  This allows the system
administrator to set different characteristics (e.g., density,
blocksize)
.\" (and eventually compression)
on each mode, and have the different modes keep those parameters
independent of any parameter changes a user may invoke during a single
mount session.  At the completion of the user's mount session, drive
parameters will revert to those set by the administrator.  I/O
operations cannot be performed on this device/sub-mode.  General
.Xr scsi 4
ioctls can
.Em only
be performed against the control device.
.El
.Sh BLOCKING MODES
.Tn SCSI
tapes may run in either 
.Sq Em variable
or
.Sq Em fixed
block-size modes.  Most 
.Tn QIC Ns -type
devices run in fixed block-size mode, where most nine-track tapes and
many new cartridge formats allow variable block-size.  The difference
between the two is as follows:
.Bl -inset
.It Variable block-size:
Each write made to the device results in a single logical record
written to the tape.  One can never read or write 
.Em part
of a record from tape (though you may request a larger block and read
a smaller record); nor can one read multiple blocks.  Data from a
single write is therefore read by a single read. The block size used
may be any value supported by the device, the
.Tn SCSI
adapter and the system (usually between 1 byte and 64 Kbytes,
sometimes more).
.Pp
When reading a variable record/block from the tape, the head is
logically considered to be immediately after the last item read,
and before the next item after that. If the next item is a file mark,
but it was never read, then the next
process to read will immediately hit the file mark and receive an end-of-file notification.
.It Fixed block-size
Data written by the user is passed to the tape as a succession of
fixed size blocks.  It may be contiguous in memory, but it is
considered to be a series of independent blocks. One may never write
an amount of data that is not an exact multiple of the blocksize.  One
may read and write the same data as a different set of records, In
other words, blocks that were written together may be read separately,
and vice-versa.
.Pp
If one requests more blocks than remain in the file, the drive will
encounter the file mark.  Because there is some data to return (unless
there were no records before the file mark), the read will succeed,
returning that data, The next read will return immediately with an
EOF.  (As above, if the file mark is never read, it remains for the next process to read if in no-rewind mode.)
.El
.Sh FILE MARK HANDLING
The handling of file marks on write is automatic. If the user has
written to the tape, and has not done a read since the last write,
then a file mark will be written to the tape when the device is
closed.  If a rewind is requested after a write, then the driver
assumes that the last file on the tape has been written, and ensures
that there are two file marks written to the tape.  The exception to
this is that there seems to be a standard (which we follow, but don't
understand why) that certain types of tape do not actually write two
file marks to tape, but when read, report a `phantom' file mark when the
last file is read.  These devices include the QIC family of devices.
(It might be that this set of devices is the same set as that of fixed
block devices.  This has not been determined yet, and they are treated
as separate behaviors by the driver at this time.)
.Sh KERNEL CONFIGURATION
Because different tape drives behave differently, there is a mechanism 
within the source to
.Nm
to quickly and conveniently recognize and deal
with brands and models of drive that have special requirements.
.Pp
There is a table (called the
.Dq Em Rogues Gallery )
in which the identification
strings of known errant drives can be stored.  Alongside each is
a set of flags that allows the setting of densities and blocksizes for each 
of the four modes, along with a set of `QUIRK' flags that can be
used to enable or disable sections of code within the driver if a particular
drive is recognized.
.Sh IOCTLS
The following 
.Xr ioctl 2
calls apply to
.Tn SCSI
tapes.  Some also apply to other tapes.  They are defined
in the header file
.Aq Pa /sys/mtio.h .
.\"
.\" Almost all of this discussion belongs in a separate mt(4)
.\" manual page, since it is common to all magnetic tapes.
.\"
.Pp
.Bl -tag -width MTIOCEEOT
.It Dv MTIOCGET
.Pq Li "struct mtget"
Retrieve the status and parameters of the tape.
.It Dv MTIOCTOP
.Pq Li "struct mtop"
Perform a multiplexed operation.  The argument structure is as follows:
.Bd -literal -offset indent
struct mtop {
	short	mt_op;
	daddr_t	mt_count;
};
.Ed
.Pp
The following operation values are defined for
.Va mt_op :
.Bl -tag -width MTSELDNSTY
.It Dv MTWEOF
Write
.Va mt_count
end of file marks at the present head position.
.It Dv MTFSF
Skip over
.Va mt_count
file marks. Leave the head on the EOM side of the last skipped
file mark.
.It Dv MTBSF
Skip
.Em backwards
over
.Va mt_count
file marks. Leave the head on the BOM (beginning of media)
side of the last skipped file mark.
.It Dv MTFSR
Skip forwards over 
.Va mt_count
records.
.It Dv MTBSR
Skip backwards over
.Va mt_count
records.
.It Dv MTREW
Rewind the device to the beginning of the media.
.It Dv MTOFFL
Rewind the media (and, if possible, eject). Even if the device cannot
eject the media it will often no longer respond to normal requests.
.It Dv MTNOP
No-op; set status only.
.It Dv MTCACHE
Enable controller buffering.
.It Dv MTNOCACHE
Disable controller buffering.
.It Dv MTSETBSIZ
Set the blocksize to use for the device/mode. If the device is capable of
variable blocksize operation, and the blocksize is set to 0, then the drive
will be driven in variable mode. This parameter is in effect for the present
mount session only, unless set on the control device.
.It Dv MTSETDNSTY
Set the density value (see 
.Xr mt 1 )
to use when running in the mode opened (minor bits 2 and 3).
This parameter is in effect for the present
mount session only, unless set on the control device.
.El
.It Dv MTIOCIEOT
Set end-of-tape processing (not presently supported for
.Nm
devices).
.It Dv MTIOCEEOT
Set end-of-tape processing (not presently supported for
.Nm
devices).
.El
.Pp
In addition, the 
.Nm
driver will allow the use of any of the general 
.Xr scsi 4
.Fn ioctl
commands, on the control device only.
.Sh FILES
.Bl -tag -width /dev/[n][e]rst[0-9].[0-3] -compact
.It Pa /dev/[n][e]rst[0-9].[0-3]
general form:
.It Pa /dev/rst0.0	
Mode 0, rewind on close
.It Pa /dev/nrst0.2	
Mode 2, No rewind on close
.It Pa /dev/erst0.3
Mode 3, Eject on close (if capable)
.It Pa /dev/rst0	
Another name for rst0.0
.It Pa /dev/nrst0	
Another name for nrst0.0
.It Pa /dev/st0ctl.0	
Parameters set to this device become the default parameters for [en]rst0.0
.It Pa /dev/st0ctl.1	
Parameters set to this device become the default parameters for [en]rst0.1
.It Pa /dev/st0ctl.2	
Parameters set to this device become the default parameters for [en]rst0.2
.It Pa /dev/st0ctl.3	
Parameters set to this device become the default parameters for [en]rst0.3
.El
.Sh DIAGNOSTICS
None.
.Sh SEE ALSO
.Xr mt 1 ,
.Xr scsi 4 ,
.Xr st 9
.Sh HISTORY
This
.Nm
driver was originally written for
.Tn Mach
2.5, and was ported to 
.Tn FreeBSD
by Julian Elischer.
