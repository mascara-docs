.\"	$NetBSD: $
.\"
.\" Copyright (c) 1996 Jason R. Thorpe <thorpej@and.com>
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgements:
.\"	This product includes software developed by Jason R. Thorpe
.\"	for And Communications, http://www.and.com/
.\" 4. The name of the author may not be used to endorse or promote products
.\"    derived from this software without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
.\" OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
.\" INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
.\" BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
.\" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
.\" AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\" OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD: src/bin/chio/chio.1,v 1.1.1.1.2.4 1999/09/05 10:59:31 peter Exp $
.\"
.Dd April 2, 1996
.Dt CHIO 1
.Os
.Sh NAME
.Nm chio
.Nd medium changer control utility
.Sh SYNOPSIS
.Nm chio
.Op Fl f Ar changer
.Ar command
.Ar arg1
.Ar arg2
.Oo
.Ar arg3 Oo ...
.Oc
.Oc
.Sh DESCRIPTION
.Nm Chio
is used to control the operation of medium changers, such as those found
in tape and optical disk jukeboxes.
.Pp
The options are as follows:
.Bl -tag -width indent
.It Fl f Ar changer
Use the device
.Pa changer
rather than the default device
.Pa /dev/ch0 .
.El
.Pp
The default changer may be overridden by setting the environment variable
.Ev CHANGER
to the desired changer device.
.Pp
A medium changer apparatus is made up of
.Em elements .
There are four element types:
.Em picker
(medium transport),
.Em slot
(storage),
.Em portal
(import/export), and
.Em drive
(data transfer).  In this command description, the shorthand
.Em ET
will be used to represent an element type, and
.Em EU
will be used to represent an element unit.  For example, to represent
the first robotic arm in the changer, the
.Em ET
would be
.Dq picker
and the
.Em EU
would be
.Dq 0 .
.Pp
.Sh SUPPORTED COMMANDS
.Bl -tag -width indent
.It Xo Nm chio move
.Ar <from ET> <from EU> <to ET> <to EU>
.Op Ar inv
.Xc
Move the media unit from
.Pa <from ET/EU>
to
.Pa <to ET/EU> .
If the optional modifier
.Pa inv
is specified, the media unit will be inverted before insertion.
.It Xo Nm chio exchange
.Ar <src ET> <src EU> <dst1 ET> <dst1 EU>
.Op Ar <dst2 ET> <dst2 ET>
.Op Ar inv1
.Op Ar inv2
.Xc
Perform a media unit exchange operation.  The media unit in
.Pa <src ET/EU>
is moved to
.Pa <dst1 ET/EU>
and the media unit previously in
.Pa <dst1 ET/EU>
is moved to
.Pa <dst2 ET/EU> .
In the case of a simple exchange,
.Pa <dst2 ET/EU>
is omitted and the values
.Pa <src ET/EU>
are used in their place.
The optional modifiers
.Pa inv1
and
.Pa inv2
specify whether the media units are to be inverted before insertion into
.Pa <dst1 ET/EU>
and
.Pa <dst2 ET/EU>
respectively.
.Pp
Note that not all medium changers support the
.Nm exchange
operation; The changer must have multiple free pickers or emulate
multiple free pickers with transient storage.
.It Xo Nm chio position
.Ar <to ET> <to EU>
.Op Ar inv
.Xc
Position the picker in front of the element described by
.Pa <to ET/EU> .
If the optional modifier
.Pa inv
is specified, the media unit will be inverted before insertion.
.Pp
Note that not all changers behave as expected when issued this command.
.It Nm chio params
Report the number of slots, drives, pickers, and portals in the changer,
and which picker unit the changer is currently configured to use.
.It Nm chio getpicker
Report which picker unit the changer is currently configured to use.
.It Xo Nm chio setpicker
.Ar <unit>
.Xc
Configure the changer to use picker
.Pa <unit> .
.It Xo Nm chio status
.Op Ar <type>
.Xc
Report the status of all elements in the changer.  If
.Pa <type>
is specified, report the status of all elements of type
.Pa <type> .
.El
.Pp
The status bits are defined as follows:
.Bl -tag -width indent
.It FULL
Element contains a media unit.
.It IMPEXP
Media was deposited into element by an outside human operator.
.It EXCEPT
Element is in an abnormal state.
.It ACCESS
Media in this element is accessible by a picker.
.It EXENAB
Element supports passing media (exporting) to an outside human operator.
.It INENAB
Element supports receiving media (importing) from an outside human operator.
.El
.Sh EXAMPLES
.Bl -tag -width indent
.It Nm chio move slot 3 drive 0
Move the media in slot 3 (fourth slot) to drive 0 (first drive).
.It Nm chio setpicker 2
Configure the changer to use picker 2 (third picker) for operations.
.El
.Sh FILES
.Bl -tag -width /dev/ch0 -compact
.It Pa /dev/ch0
default changer device
.El
.Sh SEE ALSO
.Xr mt 1 ,
.Xr ch 4 ,
.Xr mount 8
.Sh AUTHORS
The
.Nm
program and SCSI changer driver were written by
.An Jason R. Thorpe Aq thorpej@and.com
for And Communications, http://www.and.com/
