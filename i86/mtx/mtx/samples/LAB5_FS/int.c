int color = 0x000A;

int kmode()
{
  body();
}


int do_ps()
{
  // YOUR do_ps code
}

int chname(y) char * y;
{
   // YOUR chname code
}

int kcinth()
{
   u16 seg, off;
   u32 position;
   u16 x, y, z, w, ww;
   int r;

   seg = running->uss; off = running->usp;
   x = get_word(seg, off + 2*13);
   y = get_word(seg, off + 2*14);
   z = get_word(seg, off + 2*15);
   w = get_word(seg, off + 2*16);
   ww= get_word(seg, off + 2*17);

   switch(x){
       case 0 : r = running->pid;     break;
       case 1 : r = do_ps();          break;
       case 2 : r = chname(y);        break;
       case 3 : r = kmode();          break;
       case 4 : r = tswitch();        break;
       case 5 : r = do_wait(y);       break;
       case 6 : do_exit(y);           break;
       
       case 7 : r = ufork();          break;       
       case 8 : r = exec(y);          break;       

 
       case 10: r = kputc(y);         break;
       case 11: r = kgetc();          break;

       case 21: r =  kmkdir(y,z);     break;
       case 22: r =  krmdir(y,z);     break;
       case 23: r =  kcreat(y,z);     break;
       case 24: r =  krm(y,z);        break;

       case 25: r =  kchdir(y,z);     break;
       case 26: r =  kgetcwd(y,z);    break; 
       case 27: r =  kstat(y,z);      break;

       case 28: r =  kopen(y,z);      break;
       case 29: r =  kclose(y,z);     break;

       case 30: r =  kread(y,z,w);    break;
       case 31: r =  kwrite(y,z,w);   break;

       case 32: position = w;
                position = position << 16;
                position += z;
                r =  mylseek(y,position,ww);  break; 

       case 33: r =  kchmod(y,z);     break;
       case 34: r =  kchown(y,z);     break;

       case 35: r =  kdup(y,z);       break;
       case 36: r =  kdup2(y,z);      break;

       case 99: do_exit(y);           break;
 
       default: printf("invalid syscall # : %d\n", x);
                break;
   }
   put_word(r, seg, off + 2*8);
}
