.\" Man page for man
.\"
.\" Copyright (c) 1990, 1991, John W. Eaton.
.\"
.\" You may distribute under the terms of the GNU General Public
.\" License as specified in the README file that comes with the man 1.0
.\" distribution.  
.\"
.\" John W. Eaton
.\" jwe@che.utexas.edu
.\" Department of Chemical Engineering
.\" The University of Texas at Austin
.\" Austin, Texas  78712
.\"
.Dd Jan 5, 1991
.Dt MAN 1
.Sh NAME
.Nm man
.Nd format and display the on-line manual pages
.Sh SYNOPSIS
.Nm man
.Op Fl adfhktw
.Op Fl m Ar system
.Op Fl p Ar string
.Op Fl M Ar path
.Op Fl P Ar pager
.Op Fl S Ar list
.Op Ar section
.Ar name ...
.Sh DESCRIPTION
.Nm Man
formats and displays the on-line manual pages.  This version knows
about the
.Ev MANPATH
and
.Ev PAGER
environment variables, so you can have
your own set(s) of personal man pages and choose whatever program you
like to display the formatted pages.  If section is specified, man
only looks in that section of the manual.  You may also specify the
order to search the sections for entries and which preprocessors to
run on the source files via command line options or environment
variables.  If enabled by the system administrator, formatted man
pages will also be compressed with the `%compress%' command to save
space.
.Pp
The options are as follows:
.Bl -tag -width Fl
.It Fl M Ar path
Specify an alternate manpath.  By default, man uses
.Nm manpath
to determine the path to search.  This option overrides the
.Ev MANPATH
environment variable.
.It Fl P Ar pager
Specify which pager to use.  By default, man uses
.Nm %pager% ,
This option overrides the
.Ev PAGER
environment variable.
.It Fl S Ar list
List is a colon separated list of manual sections to search.
This option overrides the
.Ev MANSECT
environment variable.
.It Fl a
By default, man will exit after displaying the first manual page it
finds.  Using this option forces man to display all the manual pages
that match
.Ar name ,
not just the first.
.It Fl d
Don't actually display the man pages, but do print gobs of debugging
information.
.It Fl f
Equivalent to
.Nm whatis .
.It Fl h
Print a one line help message and exit.
.It Fl k
Equivalent to
.Nm apropos .
.It Fl m Ar system
Specify an alternate set of man pages to search based on the system
name given.
.It Fl p Ar string
Specify the sequence of preprocessors to run before nroff or troff.
Not all installations will have a full set of preprocessors.
Some of the preprocessors and the letters used to designate them are: 
eqn (e), grap (g), pic (p), tbl (t), vgrind (v), refer (r).
This option overrides the
.Ev MANROFFSEQ
environment variable.
.It Fl t
Use
.Nm %troff%
to format the manual page, passing the output to stdout.
The output from
.Nm %troff%
may need to be passed through some filter or another before being
printed.
.It Fl w
Don't actually display the man pages, but do print the location(s) of
the files that would be formatted or displayed.
.El
.Sh ENVIRONMENT
.Bl -tag -width MANROFFSEQ -compact
.It Ev MANPATH
If
.Ev MANPATH
is set, its value is used as the path to search for manual pages.
.It Ev MANROFFSEQ
If
.Ev MANROFFSEQ
is set, its value is used to determine the set of preprocessors run
before running nroff or troff.  By default, pages are passed through
the table preprocessor before nroff.
.It Ev MANSEC
If
.Ev MANSEC
is set, its value is used to determine which manual sections to search.
.It Ev PAGER
If
.Ev PAGER
is set, its value is used as the name of the program to use to display
the man page.  By default,
.Nm %pager%
is used.
.El
.Sh SEE ALSO
.Xr apropos 1 ,
.Xr whatis 1 ,
.Xr manpath 1 ,
.Xr more 1 ,
.Xr groff 1 ,
.Xr man 7 ,
.Xr mdoc 7 ,
.Xr mdoc.samples 7
.Sh BUGS
The
.Fl t
option only works if a troff-like program is installed.
