/* $FreeBSD: src/sbin/modload/pathnames.h,v 1.2.6.1 1999/09/05 11:24:02 peter Exp $ */
#include <paths.h>

#define	_PATH_LKM	"/dev/lkm"
#define _PATH_LD	"/usr/bin/ld"
